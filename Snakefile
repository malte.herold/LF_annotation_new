
INDEX=[0,1,2,3,4,5]
HMM_DIR="/work/projects/lcsbdata/HMM/"
HMM_DBS=["KEGG","Pfam-A","SwissProt","TIGRPFAM","metacyc"]
HMM_files=["KO.hmm","Pfam-A.hmm","swissprot.hmm","tigrpfam.hmm","metacyc.hmm"]
INFASTA="lf_merged.faa"

THREADS=12


rule ALL:
    input:
        "besthitsAllDB.tsv"
    output:
        "hmm_anno.done"
    shell:
        """
        touch hmm_anno.done
        """



rule KO_hmmsearch_per_tp:
    params:
        srcdir="src",
        HMM_DB="%s/%s/%s"%(HMM_DIR,HMM_DBS[0],HMM_files[0])
    input:
        INFASTA
    output:
        "hmm_anno/%s.%s.res"%(INFASTA,HMM_DBS[0])
    shell:
        """
        hmmsearch --cpu {THREADS} --noali --notextw --tblout {output[0]} {params.HMM_DB} {input[0]} > /dev/null
        """
      
rule Pfam_hmmsearch_per_tp:
    params:
        srcdir="src",
        HMM_DB="%s/%s/%s"%(HMM_DIR,HMM_DBS[1],HMM_files[1])
    input:
        INFASTA
    output:
        "hmm_anno/%s.%s.res"%(INFASTA,HMM_DBS[1])
    shell:
        """
        hmmsearch --cpu {THREADS} --noali --notextw --tblout {output[0]} {params.HMM_DB} {input[0]} > /dev/null
        """

rule swissprot_hmmsearch_per_tp:
    params:
        srcdir="src",
        HMM_DB="%s/%s/%s"%(HMM_DIR,HMM_DBS[2],HMM_files[2])
    input:
        INFASTA
    output:
        "hmm_anno/%s.%s.res"%(INFASTA,HMM_DBS[2])
    shell:
        """
        hmmsearch --cpu {THREADS} --noali --notextw --tblout {output[0]} {params.HMM_DB} {input[0]} > /dev/null
        """

rule tigr_hmmsearch_per_tp:
    params:
        srcdir="src",
        HMM_DB="%s/%s/%s"%(HMM_DIR,HMM_DBS[3],HMM_files[3])

    input:
        INFASTA
    output:
        "hmm_anno/%s.%s.res"%(INFASTA,HMM_DBS[3])
    shell:
        """
        hmmsearch --cpu {THREADS} --noali --notextw --tblout {output[0]} {params.HMM_DB} {input[0]} > /dev/null
        """

rule metacyc_hmmsearch_per_tp:
    params:
        srcdir="src",
        HMM_DB="%s/%s/%s"%(HMM_DIR,HMM_DBS[4],HMM_files[4])

    input:
        INFASTA
    output:
        "hmm_anno/%s.%s.res"%(INFASTA,HMM_DBS[4])
    shell:
        """
        hmmsearch --cpu {THREADS} --noali --notextw --tblout {output[0]} {params.HMM_DB} {input[0]} > /dev/null
        """


rule consolidate_KO:
    params:
        srcdir="src",
        resfile="hmm_anno/lf_table."
    input:
        "hmm_anno/%s.%s.res"%(INFASTA,HMM_DBS[0]),
        "hmm_anno/%s.%s.res"%(INFASTA,HMM_DBS[1]),
        "hmm_anno/%s.%s.res"%(INFASTA,HMM_DBS[2]),
        "hmm_anno/%s.%s.res"%(INFASTA,HMM_DBS[3]),
        "hmm_anno/%s.%s.res"%(INFASTA,HMM_DBS[4]),
        #"hmm_anno/%s.%s.res"%(INFASTA,HMM_DBS[5]),

    output:
        "hmm_anno/lf_table.kegg.tsv"
    shell:
        """
        perl {params.srcdir}/consolidate_hmmscan_results.pl  {params.resfile} {input[0]} {input[4]} {input[1]} {input[2]} {input[3]}
        """
        
rule get_best_KO_per_gene:
    params:
        srcdir="src/"
    input:
        "hmm_anno/lf_table.kegg.tsv",
        "hmm_anno/lf_table.pfam.tsv",
        "hmm_anno/lf_table.swissprot.tsv",
        "hmm_anno/lf_table.tigrpfam.tsv",
        "hmm_anno/lf_table.metacyc.tsv"
    output:
        "besthitsAllDB.tsv"
    shell:
        """
        python {params.srcdir}/150310_MUST_hmmBestAll.py {input[0]} {input[4]} {input[1]} {input[2]} {input[3]} -g $(grep -c "^>" {INFASTA})
        """

