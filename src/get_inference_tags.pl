#!/usr/bin/env perl

use strict;
use warnings;
use Bio::SeqIO;


my $ingbk=shift;


my $seqio_object = Bio::SeqIO->new(-file => $ingbk, -format => "genbank" );

while (my $seq_object = $seqio_object->next_seq) {
	for my $feat_object ($seq_object -> get_SeqFeatures) {
		if ($feat_object->primary_tag eq "CDS") {
			my ($lt) = $feat_object->get_tag_values('locus_tag');
			my ($prod,$inf) = $feat_object->get_tag_values('inference');
			if ($inf) {
				print "$lt\t$inf\t$prod\n";
			}else{
				print "$lt\tNA\t$prod\n";
			}
		}
		#print "primary tag:", $feat_object->primary_tag,"\n";
		
	}	
		

}





