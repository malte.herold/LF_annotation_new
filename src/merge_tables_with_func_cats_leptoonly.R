
library(plyr)
##indata
setwd("/home/mh/Uni_Lux/REPOS/LF_annotation_new")

#functional categories assignment
manual_cats="manually_assigned_categories.csv"
kegg_cats="functional_categories_lf_merged.tsv"

## transcriptome data
#tpm="/media/mh/Elements/Uni_Lux/sysmetex_rna/Analysis/tables/ALL_tpm_normalizedbyspeciessum.tsv"
#raw="/media/mh/Elements/Uni_Lux/sysmetex_rna/Analysis/tables/ALL_rawcounts.tsv"
raw="/media/mh/Elements/Uni_Lux/sysmetex_rna_lepto/Analysis/tables/LXX.Cn_rawcounts.tsv"
tpm="/media/mh/Elements/Uni_Lux/sysmetex_rna_lepto/Analysis/tables/LXX.Cn_tpm.tsv"


##read in tabread.csv()les
manu=read.csv(manual_cats,sep="\t",colClasses=c("character","character"))
colnames(manu)=c("Geneid","Manually_assigned_category")

Kegg_cog=read.csv(kegg_cats,sep="\t",fill=T,colClasses=rep("character",7))
#Kegg_cog[191,]=c("PROKKA_02297","K00353",NA,NA,NA,NA,NA)
colnames(Kegg_cog)=c("Geneid","KO","Class_from_KO_Brite","Subclass_from_KO_Brite","COG","COG_category","Class_from_Pwy_association")
Kegg_cog=Kegg_cog[c(1,2,7,3,4,5,6)]

#tpm_tab=read.csv(tpm,sep="\t",header=T,colClasses=c("character","character",rep("numeric",30)))
#raw_tab=read.csv(raw,sep="\t",header=T,colClasses=c("character","character",rep("numeric",30)))
tpm_tab=read.csv(tpm,sep="\t",header=T,colClasses=c("character",rep("numeric",3),"character"))
raw_tab=read.csv(raw,sep="\t",header=T,colClasses=c("character",rep("numeric",3),"character"))
## merge tables
mm=join(tpm_tab,manu)
mmm=join(mm,Kegg_cog)

rr=join(raw_tab,manu)
rrr=join(rr,Kegg_cog)
###reorder and write output
#for lepto only skip reorder
#out_tpm=mmm[c(1,2,33,34,35,36,37,38,39,seq(3,32))]
#out_raw=rrr[c(1,2,33,34,35,36,37,38,39,seq(3,32))]
out_tpm=mmm
out_raw=rrr

#write.table(out_raw,"/media/mh/Elements/Uni_Lux/sysmetex_rna/Analysis/tables/ALL_rawcounts_with_funcats.tsv",sep="\t",quote = F,col.names=T,row.names=F)
#write.table(out_tpm,"/media/mh/Elements/Uni_Lux/sysmetex_rna/Analysis/tables/ALL_tpm_with_funcats.tsv",sep="\t",quote = F,col.names=T,row.names=F)
write.table(out_raw,"/media/mh/Elements/Uni_Lux/sysmetex_rna_lepto/Analysis/tables/LXX.Cn_rawcounts_with_funcats.tsv",sep="\t",quote = F,col.names=T,row.names=F)
write.table(out_tpm,"/media/mh/Elements/Uni_Lux/sysmetex_rna_lepto/Analysis/tables/LXX.Cn_tpm_with_funcats.tsv",sep="\t",quote = F,col.names=T,row.names=F)


