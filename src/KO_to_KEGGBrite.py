#!/usr/bin/env python

###
#use bioservices.kegg to assign KO-brite (functional categories) to list of genes annotated with Kegg-orthologue association
###

##usage: python3 src/KO_to_KEGGBrite.py > functional_categories_lf_merged.tsv

#from __future__ import print_function
from bioservices.kegg import KEGG, KEGGParser
import sys
import re

k=KEGG()
p=KEGGParser()

#infile=sys.argv[1]
infile="hmm_anno/lf_table.kegg_new.besthits.tsv"

#lookup files for cog categories
cogfile="cog/cognames2003-2014.tab"
funlookup=dict()
with open(cogfile,"r",encoding="ISO-8859-1") as f:
    next(f)
    for line in f:
        line=line.rstrip()
        parts=line.split("\t")
        funlookup[parts[0]]=parts[1]

#get brite
def get_parse_ko(ko,k,p):
    a=k.get(ko)
    try:
        b=p.parse(a)
        return(b)
    except AttributeError:
        print("entry for %s not parsable, maybe entry does not exist"%ko,file=sys.stderr)
        return(False)

def get_ortho_brite(b):
    try:
       britestr=b["BRITE"]
       #parse information for KEGG Orthology BR:ko00001
       parts=britestr.split("\n")
       outlist=list()
       for ele in parts:
            if ("BR:ko00001" in ele):
                appendmode=1
            elif ("BR:" in ele):
                appendmode=0
            if(appendmode==1):
                outlist.append(ele)
       return(outlist)
    except KeyError:
         return(False)

def get_cog_links(b):
    try:
       cogstr=b["DBLINKS"]["COG"]
       parts=cogstr.split(" ")
       outstr=";".join(parts)
       return(outstr)
    except KeyError:
       return("NA")

def get_pwy_class(b,k,p):
    try:
        pwy=b["PATHWAY"]
        clist=list()
        for key in pwy.keys():
            ff=get_parse_ko(key,k,p)
            if(ff):
                c=ff["CLASS"]
                parts=c.split(";")
                #avoid multiples:
                if not parts[1] in clist:
                    clist.append(parts[1])
            else:
                clist.append("NA")
        cstr=";".join(clist)
        return(cstr)
    except KeyError:
        return("NA")

with open(infile,"r") as f:
    next(f)
    for line in f:
        line=line.rstrip()
        parts=line.split("\t")
        #remove KEGG:
        ko=parts[1].replace("KEGG:","")
        #if more than 1 "best hit" use only first one
        pp=ko.split(";")
        ko=pp[0]
        b=get_parse_ko(ko,k,p)
        if (b):
            britelist=get_ortho_brite(b)
            if(britelist):
                c=britelist[2]
                d=britelist[3]
            cog=get_cog_links(b)
            #try:
            cogparts=cog.split(";")
            funstr=""
            for ele in cogparts:
                if (ele=="NA"):
                    continue
                try:
                    if not funlookup[ele] in funstr:
                        funstr=funstr+funlookup[ele]
                except KeyError:
                    print("did not find %s in lookup table"%ele,file=sys.stderr)
            pwyclass=get_pwy_class(b,k,p)
            out="\t".join([parts[0],ko,c,d,cog,funstr,pwyclass])
            print(out)
        else:
            print("\t".join([parts[0],ko,"NA","NA","NA","","NA"]))
