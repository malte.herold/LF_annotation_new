#                                                                              --- full sequence ---- --- best 1 domain ---- --- domain number estimation ----
# target name        accession  query name                          accession    E-value  score  bias   E-value  score  bias   exp reg clu  ov env dom rep inc description of target
#------------------- ----------                -------------------- ---------- --------- ------ ----- --------- ------ -----   --- --- --- --- --- --- --- --- ---------------------
PROKKA_01538         -          1-ACYLGLYCEROL-3-P-ACYLTRANSFER-RXN -            1.1e-37  126.5   0.0   1.2e-37  126.4   0.0   1.0   1   0   0   1   1   1   1 1-acyl-sn-glycerol-3-phosphate acyltransferase
PROKKA_00761         -          1-ACYLGLYCEROL-3-P-ACYLTRANSFER-RXN -            1.5e-25   86.9   0.0   2.3e-25   86.3   0.0   1.2   1   0   0   1   1   1   1 long-chain acyl-CoA synthetase
PROKKA_02160         -          1.1.1.141-RXN        -            4.6e-23   78.8   0.8   6.7e-23   78.3   0.8   1.1   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier-protein] reductase
PROKKA_00235         -          1.1.1.141-RXN        -            6.3e-16   55.4   0.0   1.2e-15   54.5   0.0   1.4   1   1   0   1   1   1   1 3-oxoacyl-[acyl-carrier protein] reductase
PROKKA_00773         -          1.1.1.141-RXN        -            8.3e-06   22.2   0.0   1.8e-05   21.1   0.0   1.5   1   1   0   1   1   1   1 short chain dehydrogenase
PROKKA_02149         -          1.1.1.141-RXN        -             0.0022   14.3   0.0    0.0072   12.6   0.0   1.7   1   1   0   1   1   1   1 NADP-dependent 3-hydroxy acid dehydrogenase YdfG
PROKKA_00572         -          1.1.1.145-RXN        -            2.1e-23   80.0   0.0   2.3e-20   69.9   0.0   2.3   2   1   0   2   2   2   2 UDP-glucose 4-epimerase
PROKKA_00620         -          1.1.1.145-RXN        -            2.6e-18   63.2   0.0   1.3e-16   57.6   0.0   2.5   2   1   0   2   2   2   1 dihydroflavonol-4-reductase
PROKKA_00656         -          1.1.1.145-RXN        -            2.3e-16   56.8   0.0     4e-16   56.0   0.0   1.3   1   0   0   1   1   1   1 UDP-galactose 4-epimerase
PROKKA_00488         -          1.1.1.145-RXN        -            8.2e-15   51.7   0.0   1.1e-12   44.7   0.0   2.1   2   0   0   2   2   2   2 dTDP-glucose 4,6-dehydratase
PROKKA_01629         -          1.1.1.145-RXN        -            1.3e-12   44.4   0.0   3.3e-12   43.1   0.0   1.7   2   1   0   2   2   2   1 ADP-L-glycero-D-manno-heptose 6-epimerase
PROKKA_00566         -          1.1.1.145-RXN        -            2.2e-12   43.7   0.0   2.8e-12   43.4   0.0   1.1   1   0   0   1   1   1   1 UDP-glucuronate 4-epimerase
PROKKA_00469         -          1.1.1.145-RXN        -            5.3e-10   35.9   0.0     2e-07   27.4   0.0   2.2   1   1   1   2   2   2   2 NADH dehydrogenase
PROKKA_01783         -          1.1.1.145-RXN        -              3e-07   26.8   0.0   7.6e-07   25.5   0.0   1.6   2   0   0   2   2   2   1 Nucleoside-diphosphate-sugar epimerase
PROKKA_01526         -          1.1.1.145-RXN        -            4.4e-05   19.7   0.0   0.00015   17.9   0.0   1.8   2   1   0   2   2   2   1 UDP-glucuronate decarboxylase
PROKKA_00683         -          1.1.1.145-RXN        -             0.0019   14.3   0.0      0.07    9.1   0.0   2.3   2   0   0   2   2   2   1 UDP-glucose 4-epimerase
PROKKA_00961         -          1.1.1.149-RXN        -            0.00013   18.5   0.0   0.00022   17.7   0.0   1.3   1   0   0   1   1   1   1 putative oxidoreductase
PROKKA_01886         -          1.1.1.149-RXN        -              0.011   12.2   0.0      0.67    6.3   0.0   2.3   2   0   0   2   2   2   0 putative oxidoreductase
PROKKA_00961         -          1.1.1.188-RXN        -            3.1e-05   20.2   0.0     5e-05   19.5   0.0   1.2   1   0   0   1   1   1   1 putative oxidoreductase
PROKKA_01886         -          1.1.1.188-RXN        -             0.0019   14.3   0.0      0.36    6.8   0.0   2.4   2   1   0   2   2   2   2 putative oxidoreductase
PROKKA_01736         -          1.1.1.188-RXN        -             0.0036   13.4   0.0      0.35    6.9   0.0   2.6   2   1   0   2   2   2   1 putative oxidoreductase
PROKKA_02160         -          1.1.1.209-RXN        -              6e-11   39.5   0.4     1e-09   35.4   0.4   2.2   1   1   0   1   1   1   1 3-oxoacyl-[acyl-carrier-protein] reductase
PROKKA_00514         -          1.1.1.211-RXN        -            1.6e-11   40.2   0.0   2.1e-11   39.7   0.0   1.1   1   0   0   1   1   1   1 DSF synthase
PROKKA_01610         -          1.1.1.211-RXN        -            2.5e-08   29.6   0.0   3.2e-08   29.2   0.0   1.1   1   0   0   1   1   1   1 3-hydroxybutyryl-CoA dehydrogenase
PROKKA_00235         -          1.1.1.51-RXN         -            3.5e-29   99.2   0.0   4.7e-29   98.7   0.0   1.2   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier protein] reductase
PROKKA_02160         -          1.1.1.51-RXN         -            2.4e-28   96.4   1.1   3.1e-28   96.0   1.1   1.1   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier-protein] reductase
PROKKA_00773         -          1.1.1.51-RXN         -            3.4e-20   69.6   0.0   4.7e-20   69.2   0.0   1.1   1   0   0   1   1   1   1 short chain dehydrogenase
PROKKA_02149         -          1.1.1.51-RXN         -            7.4e-19   65.3   0.0   9.2e-19   64.9   0.0   1.1   1   0   0   1   1   1   1 NADP-dependent 3-hydroxy acid dehydrogenase YdfG
PROKKA_00820         -          1.1.1.51-RXN         -            1.5e-11   41.3   0.0     2e-11   40.8   0.0   1.1   1   0   0   1   1   1   1 Enoyl-[acyl-carrier-protein] reductase [NADH]
PROKKA_00170         -          1.1.1.51-RXN         -            1.5e-05   21.6   0.0   0.00076   16.0   0.0   2.0   2   0   0   2   2   2   1 NAD(P)-dependent dehydrogenase, short-chain alcohol dehydrogenase family
PROKKA_00572         -          1.1.1.51-RXN         -             0.0049   13.3   0.0    0.0093   12.4   0.0   1.4   1   0   0   1   1   1   1 UDP-glucose 4-epimerase
PROKKA_00961         -          1.1.1.52-RXN         -            8.2e-06   22.3   0.0   1.2e-05   21.7   0.0   1.2   1   0   0   1   1   1   1 putative oxidoreductase
PROKKA_01886         -          1.1.1.52-RXN         -             0.0013   15.1   0.0     0.071    9.3   0.0   2.5   2   1   0   2   2   2   2 putative oxidoreductase
PROKKA_01736         -          1.1.1.52-RXN         -             0.0021   14.3   0.0    0.0048   13.2   0.0   1.5   2   0   0   2   2   2   1 putative oxidoreductase
PROKKA_02149         -          1.1.1.53-RXN         -            1.4e-11   41.4   0.0   3.8e-11   39.9   0.0   1.7   1   1   1   2   2   2   1 NADP-dependent 3-hydroxy acid dehydrogenase YdfG
PROKKA_02160         -          1.1.1.53-RXN         -            1.3e-09   34.9   0.3     2e-07   27.7   0.2   2.1   1   1   1   2   2   2   2 3-oxoacyl-[acyl-carrier-protein] reductase
PROKKA_00235         -          1.1.1.53-RXN         -            7.4e-08   29.2   0.0   6.8e-07   26.0   0.0   2.0   1   1   0   2   2   2   1 3-oxoacyl-[acyl-carrier protein] reductase
PROKKA_00409         -          1.1.1.8-RXN          -           2.3e-100  333.1   0.0  3.1e-100  332.7   0.0   1.0   1   0   0   1   1   1   1 glycerol-3-phosphate dehydrogenase (NAD(P)+)
PROKKA_00876         -          1.1.1.8-RXN          -             0.0081   12.5   0.0     0.017   11.4   0.0   1.6   1   0   0   1   1   1   1 malate dehydrogenase (NAD)
PROKKA_02536         -          1.1.1.83-RXN         -            1.3e-57  192.5   0.0   1.6e-57  192.1   0.0   1.0   1   0   0   1   1   1   1 3-isopropylmalate dehydrogenase
PROKKA_00965         -          1.1.1.83-RXN         -            1.3e-48  162.9   0.0   1.4e-48  162.8   0.0   1.0   1   0   0   1   1   1   1 isocitrate dehydrogenase (NAD+)
PROKKA_02380         -          1.11.1.12-RXN        -              0.014   11.8   0.0      0.02   11.3   0.0   1.2   1   0   0   1   1   1   0 Peroxiredoxin
PROKKA_00084         -          1.13.12.7-RXN        -            3.8e-23   79.0   0.0   1.1e-17   60.9   0.0   2.3   2   1   0   2   2   2   2 acetyl-CoA synthetase
PROKKA_00761         -          1.13.12.7-RXN        -            4.1e-18   62.3   0.0   4.3e-11   39.2   0.0   2.1   2   0   0   2   2   2   2 long-chain acyl-CoA synthetase
PROKKA_00852         -          1.14.11.2-RXN        -            0.00037   16.5   0.1   0.00077   15.4   0.0   1.5   1   1   1   2   2   2   1 thioredoxin
PROKKA_00318         -          1.14.11.2-RXN        -             0.0075   12.2   0.0    0.0087   11.9   0.0   1.1   1   0   0   1   1   1   1 thioredoxin
PROKKA_02431         -          1.14.13.45-RXN       -            0.00017   17.2   0.0   0.00018   17.1   0.0   1.0   1   0   0   1   1   1   1 nitrite reductase (NADH) small subunit
PROKKA_01451         -          1.14.13.8-RXN        -             0.0081   11.8   0.1      0.24    7.0   0.0   2.2   2   0   0   2   2   2   2 NADH-quinone oxidoreductase subunit F
PROKKA_01944         -          1.14.13.8-RXN        -              0.015   10.9   0.0      0.22    7.1   0.0   2.3   3   0   0   3   3   3   0 NADPH-dependent glutamate synthase beta chain
PROKKA_01479         -          1.14.19.2-RXN        -            0.00021   17.7   0.0   0.00034   17.0   0.0   1.3   1   1   0   1   1   1   1 acyl-[acyl-carrier-protein] desaturase
PROKKA_01547         -          1.14.19.2-RXN        -            0.00039   16.8   0.1   0.00095   15.5   0.0   1.6   1   1   1   2   2   2   1 acyl-[acyl-carrier-protein] desaturase
PROKKA_00500         -          1.14.19.3-RXN        -            7.7e-09   32.0   8.2   3.2e-05   20.1   3.0   2.1   2   0   0   2   2   2   2 Fatty acid desaturase
PROKKA_01035         -          1.17.4.2-RXN         -              0.049    7.8   0.0     0.071    7.3   0.0   1.1   1   0   0   1   1   1   0 hypothetical protein
PROKKA_01351         -          1.17.99.1-RXN        -             0.0011   16.2   0.1     0.045   11.1   0.1   2.1   1   1   0   1   1   1   1 Cytochrome C oxidase, cbb3-type, subunit III
PROKKA_00052         -          1.2.1.13-RXN         -           2.6e-137  455.1   0.0  2.9e-137  454.9   0.0   1.0   1   0   0   1   1   1   1 glyceraldehyde 3-phosphate dehydrogenase
PROKKA_02282         -          1.2.1.27-RXN         -            2.8e-42  142.1   0.0   3.7e-42  141.7   0.0   1.0   1   0   0   1   1   1   1 Acyl-CoA reductase
PROKKA_00106         -          1.2.1.27-RXN         -            5.4e-06   22.4   0.0    0.0049   12.6   0.0   2.3   1   1   1   2   2   2   2 glutamate-5-semialdehyde dehydrogenase
PROKKA_00084         -          1.2.1.31-RXN         -            4.3e-08   28.0   0.0   1.3e-05   19.8   0.0   2.3   2   1   0   2   2   2   2 acetyl-CoA synthetase
PROKKA_00137         -          1.2.1.40-RXN         -            5.7e-88  292.8   1.8   6.4e-88  292.6   1.8   1.0   1   0   0   1   1   1   1 dihydrolipoamide dehydrogenase
PROKKA_01391         -          1.2.1.40-RXN         -            1.7e-50  169.2   1.2     2e-50  169.0   1.2   1.0   1   0   0   1   1   1   1 dihydrolipoamide dehydrogenase
PROKKA_00559         -          1.2.1.40-RXN         -            9.6e-49  163.5   3.0   1.7e-48  162.6   3.0   1.3   1   1   0   1   1   1   1 mercuric reductase
PROKKA_01909         -          1.2.1.40-RXN         -            1.3e-40  136.7   0.0   1.6e-40  136.4   0.0   1.0   1   0   0   1   1   1   1 dihydrolipoamide dehydrogenase
PROKKA_01157         -          1.2.1.40-RXN         -            2.5e-35  119.2   3.3   3.7e-35  118.7   3.3   1.1   1   0   0   1   1   1   1 mercuric reductase
PROKKA_01201         -          1.2.1.40-RXN         -            5.3e-13   45.7   0.2   8.3e-13   45.0   0.2   1.2   1   0   0   1   1   1   1 nitrite reductase (NADH) large subunit
PROKKA_02417         -          1.2.1.40-RXN         -              7e-07   25.5   1.4   2.3e-05   20.5   0.6   2.0   2   0   0   2   2   2   2 thioredoxin reductase (NADPH)
PROKKA_02282         -          1.2.1.45-RXN         -            7.3e-51  170.5   0.0     1e-50  170.0   0.0   1.1   1   0   0   1   1   1   1 Acyl-CoA reductase
PROKKA_02282         -          1.2.1.9-RXN          -            5.5e-94  312.8   0.0   6.6e-94  312.5   0.0   1.0   1   0   0   1   1   1   1 Acyl-CoA reductase
PROKKA_00106         -          1.2.1.9-RXN          -            0.00013   17.9   0.1     0.014   11.3   0.0   2.2   1   1   1   2   2   2   2 glutamate-5-semialdehyde dehydrogenase
PROKKA_02541         -          1.2.3.3-RXN          -            3.1e-57  191.5   0.0   3.9e-57  191.1   0.0   1.1   1   0   0   1   1   1   1 acetolactate synthase, large subunit
PROKKA_01780         -          1.2.4.4-RXN          -              9e-92  305.6   0.0     1e-91  305.4   0.0   1.0   1   0   0   1   1   1   1 pyruvate dehydrogenase E1 component beta subunit
PROKKA_01779         -          1.2.4.4-RXN          -              1e-52  176.7   0.0   1.2e-52  176.5   0.0   1.0   1   0   0   1   1   1   1 pyruvate dehydrogenase E1 component alpha subunit
PROKKA_00681         -          1.2.4.4-RXN          -            1.3e-30  103.6   0.3   1.3e-24   83.8   0.1   2.1   2   0   0   2   2   2   2 1-deoxy-D-xylulose-5-phosphate synthase
PROKKA_02458         -          1.2.4.4-RXN          -             0.0021   13.7   0.0    0.0032   13.2   0.0   1.2   1   0   0   1   1   1   1 pyruvate ferredoxin oxidoreductase alpha subunit
PROKKA_01389         -          1.2.4.4-RXN          -             0.0032   13.1   0.0     0.005   12.5   0.0   1.2   1   0   0   1   1   1   1 xylulose-5-phosphate/fructose-6-phosphate phosphoketolase
PROKKA_02160         -          1.3.1.19-RXN         -            4.6e-19   65.8   6.7   1.5e-18   64.1   6.7   1.7   1   1   0   1   1   1   1 3-oxoacyl-[acyl-carrier-protein] reductase
PROKKA_02149         -          1.3.1.19-RXN         -            1.5e-08   31.4   0.0     2e-08   30.9   0.0   1.2   1   0   0   1   1   1   1 NADP-dependent 3-hydroxy acid dehydrogenase YdfG
PROKKA_00235         -          1.3.1.19-RXN         -            5.1e-08   29.6   0.0   6.4e-08   29.3   0.0   1.1   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier protein] reductase
PROKKA_00773         -          1.3.1.19-RXN         -            2.1e-05   21.0   0.0   2.8e-05   20.6   0.0   1.1   1   0   0   1   1   1   1 short chain dehydrogenase
PROKKA_00572         -          1.3.1.19-RXN         -             0.0029   14.0   0.2    0.0046   13.3   0.2   1.2   1   0   0   1   1   1   1 UDP-glucose 4-epimerase
PROKKA_00170         -          1.3.1.19-RXN         -               0.02   11.2   0.1      0.17    8.2   0.1   2.0   1   1   0   1   1   1   0 NAD(P)-dependent dehydrogenase, short-chain alcohol dehydrogenase family
PROKKA_01451         -          1.3.1.2-RXN          -            7.7e-42  140.1   0.0   2.4e-41  138.4   0.0   1.6   1   1   0   1   1   1   1 NADH-quinone oxidoreductase subunit F
PROKKA_01944         -          1.3.1.2-RXN          -            9.1e-31  103.4   2.9   1.3e-30  102.9   0.0   2.3   2   1   0   2   2   2   1 NADPH-dependent glutamate synthase beta chain
PROKKA_01979         -          1.3.1.2-RXN          -            1.3e-05   20.1   0.0   1.8e-05   19.7   0.0   1.1   1   0   0   1   1   1   1 Dihydroorotate dehydrogenase
PROKKA_02450         -          1.3.1.2-RXN          -             0.0031   12.3   7.1    0.0038   12.0   7.1   1.0   1   0   0   1   1   1   1 pyruvate ferredoxin oxidoreductase
PROKKA_01182         -          1.3.1.2-RXN          -              0.019    9.7   4.6     0.042    8.5   4.6   1.6   1   1   0   1   1   1   0 ferredoxin III, nif-specific
PROKKA_02455         -          1.3.1.2-RXN          -              0.089    7.5   6.1     0.094    7.4   6.1   1.0   1   0   0   1   1   1   0 Pyruvate:ferredoxin oxidoreductase, delta subunit
PROKKA_02160         -          1.3.1.25-RXN         -            6.5e-35  118.2   2.6   6.8e-34  114.8   2.6   1.9   1   1   0   1   1   1   1 3-oxoacyl-[acyl-carrier-protein] reductase
PROKKA_00235         -          1.3.1.25-RXN         -            4.1e-23   79.5   0.1   4.7e-23   79.3   0.1   1.0   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier protein] reductase
PROKKA_02149         -          1.3.1.25-RXN         -            1.2e-15   55.0   0.2   1.5e-15   54.7   0.2   1.1   1   0   0   1   1   1   1 NADP-dependent 3-hydroxy acid dehydrogenase YdfG
PROKKA_00773         -          1.3.1.25-RXN         -            2.6e-08   30.9   0.0   3.8e-08   30.4   0.0   1.1   1   0   0   1   1   1   1 short chain dehydrogenase
PROKKA_00170         -          1.3.1.25-RXN         -            7.1e-08   29.5   0.0   8.7e-08   29.2   0.0   1.1   1   0   0   1   1   1   1 NAD(P)-dependent dehydrogenase, short-chain alcohol dehydrogenase family
PROKKA_00820         -          1.3.1.25-RXN         -            0.00014   18.7   0.2   0.00088   16.1   0.2   1.9   1   1   0   1   1   1   1 Enoyl-[acyl-carrier-protein] reductase [NADH]
PROKKA_02160         -          1.3.1.29-RXN         -            8.7e-14   49.0   2.4   2.3e-13   47.6   2.4   1.6   1   1   0   1   1   1   1 3-oxoacyl-[acyl-carrier-protein] reductase
PROKKA_00235         -          1.3.1.29-RXN         -            2.6e-09   34.3   0.0   3.4e-09   33.9   0.0   1.1   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier protein] reductase
PROKKA_00773         -          1.3.1.29-RXN         -            1.4e-07   28.7   0.0   2.5e-07   27.8   0.0   1.3   1   1   0   1   1   1   1 short chain dehydrogenase
PROKKA_02149         -          1.3.1.29-RXN         -            1.6e-05   21.9   0.0   0.00011   19.1   0.0   2.0   1   1   0   1   1   1   1 NADP-dependent 3-hydroxy acid dehydrogenase YdfG
PROKKA_00820         -          1.3.1.29-RXN         -             0.0015   15.4   0.2     0.002   15.0   0.2   1.2   1   0   0   1   1   1   1 Enoyl-[acyl-carrier-protein] reductase [NADH]
PROKKA_00170         -          1.3.1.29-RXN         -             0.0062   13.4   0.0      0.18    8.6   0.0   2.0   2   0   0   2   2   2   1 NAD(P)-dependent dehydrogenase, short-chain alcohol dehydrogenase family
PROKKA_01157         -          1.3.99.5-RXN         -              6e-05   19.0   1.1   0.00011   18.1   1.1   1.4   1   0   0   1   1   1   1 mercuric reductase
PROKKA_00585         -          1.3.99.5-RXN         -             0.0051   12.6   0.2    0.0078   12.0   0.2   1.2   1   0   0   1   1   1   1 succinate dehydrogenase / fumarate reductase flavoprotein subunit
PROKKA_01542         -          1.5.1.15-RXN         -            4.2e-85  282.6   0.0   5.2e-85  282.3   0.0   1.0   1   0   0   1   1   1   1 methylenetetrahydrofolate dehydrogenase (NADP+) / methenyltetrahydrofolate cyclohydrolase
PROKKA_01543         -          1.5.1.20-RXN         -            9.6e-09   31.5   0.0   1.3e-08   31.0   0.0   1.2   1   0   0   1   1   1   1 5,10-methylenetetrahydrofolate reductase
PROKKA_01852         -          1.5.8.2-RXN          -            4.1e-29   98.5   0.0   5.1e-29   98.2   0.0   1.0   1   0   0   1   1   1   1 2,4-dienoyl-CoA reductase
PROKKA_02084         -          1.5.8.2-RXN          -            7.2e-16   54.7   0.0     5e-15   52.0   0.0   1.8   2   0   0   2   2   2   1 N-ethylmaleimide reductase
PROKKA_01201         -          1.6.5.4-RXN          -            3.1e-17   59.4   0.0   5.3e-17   58.7   0.0   1.2   1   0   0   1   1   1   1 nitrite reductase (NADH) large subunit
PROKKA_01391         -          1.6.5.4-RXN          -            1.3e-08   31.0   0.0   5.1e-08   29.1   0.0   1.9   1   1   0   1   1   1   1 dihydrolipoamide dehydrogenase
PROKKA_01909         -          1.6.5.4-RXN          -            1.4e-08   30.9   0.0   3.4e-08   29.7   0.0   1.5   2   0   0   2   2   2   1 dihydrolipoamide dehydrogenase
PROKKA_00559         -          1.6.5.4-RXN          -            1.6e-06   24.1   0.0   9.2e-06   21.7   0.0   2.0   1   1   1   2   2   2   1 mercuric reductase
PROKKA_01157         -          1.6.5.4-RXN          -            6.1e-06   22.2   0.0   1.3e-05   21.2   0.0   1.4   1   0   0   1   1   1   1 mercuric reductase
PROKKA_02417         -          1.6.5.4-RXN          -            0.00014   17.8   0.6   0.00025   16.9   0.2   1.6   2   0   0   2   2   2   1 thioredoxin reductase (NADPH)
PROKKA_00137         -          1.6.5.4-RXN          -            0.00039   16.3   0.4    0.0021   13.9   0.1   1.9   2   0   0   2   2   2   1 dihydrolipoamide dehydrogenase
PROKKA_01451         -          1.6.5.4-RXN          -             0.0046   12.8   0.3     0.011   11.5   0.1   1.6   2   0   0   2   2   2   1 NADH-quinone oxidoreductase subunit F
PROKKA_01678         -          1.6.5.4-RXN          -               0.02   10.6   0.0      0.04    9.7   0.0   1.4   1   0   0   1   1   1   0 sulfide:quinone oxidoreductase
PROKKA_01477         -          1.7.7.2-RXN          -            1.5e-79  264.9   0.0   1.9e-79  264.6   0.0   1.0   1   0   0   1   1   1   1 sulfite reductase (ferredoxin)
PROKKA_01201         -          1.7.7.2-RXN          -            2.1e-54  182.0   0.0   3.2e-54  181.3   0.0   1.2   1   0   0   1   1   1   1 nitrite reductase (NADH) large subunit
PROKKA_02160         -          11-BETA-HYDROXYSTEROID-DEHYDROGENASE-RXN -            3.6e-25   86.1   0.4   4.6e-25   85.8   0.4   1.0   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier-protein] reductase
PROKKA_00235         -          11-BETA-HYDROXYSTEROID-DEHYDROGENASE-RXN -            1.2e-17   61.3   0.0   1.6e-17   61.0   0.0   1.1   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier protein] reductase
PROKKA_00773         -          11-BETA-HYDROXYSTEROID-DEHYDROGENASE-RXN -            9.9e-17   58.4   0.0   4.6e-16   56.2   0.0   1.8   1   1   0   1   1   1   1 short chain dehydrogenase
PROKKA_02149         -          11-BETA-HYDROXYSTEROID-DEHYDROGENASE-RXN -            6.1e-14   49.2   0.0   1.5e-13   47.9   0.0   1.5   1   1   0   1   1   1   1 NADP-dependent 3-hydroxy acid dehydrogenase YdfG
PROKKA_00170         -          11-BETA-HYDROXYSTEROID-DEHYDROGENASE-RXN -            5.2e-06   23.0   0.0    0.0016   14.8   0.0   2.0   2   0   0   2   2   2   2 NAD(P)-dependent dehydrogenase, short-chain alcohol dehydrogenase family
PROKKA_01392         -          11-BETA-HYDROXYSTEROID-DEHYDROGENASE-RXN -             0.0083   12.5   0.0     0.011   12.1   0.0   1.2   1   0   0   1   1   1   1 Nucleoside-diphosphate-sugar epimerase
PROKKA_02084         -          12-OXOPHYTODIENOATE-REDUCTASE-RXN -           6.6e-102  338.5   0.0  7.5e-102  338.3   0.0   1.0   1   0   0   1   1   1   1 N-ethylmaleimide reductase
PROKKA_01852         -          12-OXOPHYTODIENOATE-REDUCTASE-RXN -            9.2e-32  107.6   0.0   1.4e-31  107.0   0.0   1.3   1   0   0   1   1   1   1 2,4-dienoyl-CoA reductase
PROKKA_01879         -          1PFRUCTPHOSN-RXN     -            2.1e-07   27.6   2.1   2.6e-07   27.3   0.9   1.6   1   1   0   1   1   1   1 D-beta-D-heptose 7-phosphate kinase / D-beta-D-heptose 1-phosphate adenosyltransferase
PROKKA_00904         -          1PFRUCTPHOSN-RXN     -            3.1e-06   23.8   0.0   4.4e-06   23.3   0.0   1.3   1   0   0   1   1   1   1 Sugar or nucleoside kinase, ribokinase family
PROKKA_01706         -          1TRANSKETO-RXN       -           1.1e-248  824.0   0.0  1.4e-248  823.7   0.0   1.0   1   0   0   1   1   1   1 transketolase
PROKKA_00681         -          1TRANSKETO-RXN       -            6.8e-49  163.8   0.0   2.8e-27   92.4   0.0   3.0   2   1   1   3   3   3   3 1-deoxy-D-xylulose-5-phosphate synthase
PROKKA_01780         -          1TRANSKETO-RXN       -            7.2e-07   25.0   0.0   9.7e-07   24.5   0.0   1.1   1   0   0   1   1   1   1 pyruvate dehydrogenase E1 component beta subunit
PROKKA_01779         -          1TRANSKETO-RXN       -            7.1e-05   18.4   0.0   8.6e-05   18.1   0.0   1.1   1   0   0   1   1   1   1 pyruvate dehydrogenase E1 component alpha subunit
PROKKA_02453         -          1TRANSKETO-RXN       -              0.011   11.1   0.0     0.016   10.5   0.0   1.2   1   0   0   1   1   1   0 pyruvate ferredoxin oxidoreductase alpha subunit
PROKKA_01702         -          2-DEHYDROPANTOATE-REDUCT-RXN -               0.04   10.0   0.2     0.065    9.3   0.2   1.2   1   0   0   1   1   1   0 2-polyprenyl-6-methoxyphenol hydroxylase
PROKKA_02197         -          2-HALOACID-DEHALOGENASE-RXN -             0.0092   12.8   0.0     0.064   10.0   0.0   2.0   2   0   0   2   2   2   1 phosphoglycolate phosphatase
PROKKA_02537         -          2-ISOPROPYLMALATESYN-RXN -           1.1e-196  651.5   0.1  1.3e-196  651.3   0.1   1.0   1   0   0   1   1   1   1 2-isopropylmalate synthase
PROKKA_01358         -          2-ISOPROPYLMALATESYN-RXN -           6.2e-124  411.5   0.0  8.1e-124  411.1   0.0   1.0   1   0   0   1   1   1   1 (R)-citramalate synthase
PROKKA_00019         -          2-ISOPROPYLMALATESYN-RXN -           2.7e-109  363.2   0.0  3.5e-109  362.8   0.0   1.0   1   0   0   1   1   1   1 2-isopropylmalate synthase
PROKKA_01188         -          2-ISOPROPYLMALATESYN-RXN -            1.8e-78  261.5   0.0   2.1e-78  261.2   0.0   1.0   1   0   0   1   1   1   1 homocitrate synthase NifV
PROKKA_00782         -          2-METHYLENEGLUTARATE-MUTASE-RXN -             0.0034   12.8   0.0    0.0052   12.2   0.0   1.2   1   0   0   1   1   1   1 5-methyltetrahydrofolate--homocysteine methyltransferase
PROKKA_01901         -          2-NITROPROPANE-DIOXYGENASE-RXN -              1e-07   28.6   2.5    0.0014   15.0   0.0   2.5   2   1   0   2   2   2   2 IMP dehydrogenase
PROKKA_02265         -          2-NITROPROPANE-DIOXYGENASE-RXN -              0.026   10.8   0.1     0.044   10.1   0.1   1.3   1   0   0   1   1   1   0 1-(5-phosphoribosyl)-5-[(5-phosphoribosylamino)methylideneamino] imidazole-4-carboxamide isomerase
PROKKA_02456         -          2-OXOGLUTARATE-SYNTHASE-RXN -            7.4e-16   55.9   0.1     1e-15   55.4   0.1   1.2   1   0   0   1   1   1   1 pyruvate ferredoxin oxidoreductase gamma subunit
PROKKA_02451         -          2-OXOGLUTARATE-SYNTHASE-RXN -              6e-15   53.0   0.5   7.6e-15   52.6   0.5   1.2   1   0   0   1   1   1   1 pyruvate ferredoxin oxidoreductase gamma subunit
PROKKA_01034         -          2.1.1.113-RXN        -            4.3e-08   29.7   0.0     0.003   13.8   0.0   2.3   2   0   0   2   2   2   2 DNA methylase
PROKKA_02513         -          2.1.1.131-RXN        -            6.8e-44  148.0   0.1   7.7e-44  147.8   0.1   1.0   1   0   0   1   1   1   1 precorrin-3B C17-methyltransferase
PROKKA_01569         -          2.1.1.131-RXN        -            6.1e-06   23.7   1.4   9.7e-06   23.0   1.4   1.3   1   0   0   1   1   1   1 cobalt-precorrin 4 C11-methyltransferase 
PROKKA_01993         -          2.1.1.131-RXN        -            6.3e-05   20.4   0.3   0.00011   19.6   0.3   1.4   1   0   0   1   1   1   1 uroporphyrinogen III methyltransferase / synthase
PROKKA_01571         -          2.1.1.131-RXN        -               0.01   13.2   0.3     0.035   11.4   0.0   1.9   2   0   0   2   2   2   0 precorrin-2/cobalt-factor-2 C20-methyltransferase
PROKKA_02510         -          2.1.1.132-RXN        -            1.5e-05   22.5   0.0   5.4e-05   20.7   0.0   1.8   1   1   0   1   1   1   1 precorrin-6Y C5,15-methyltransferase (decarboxylating)
PROKKA_01569         -          2.1.1.132-RXN        -             0.0051   14.3   0.1     0.011   13.2   0.1   1.5   1   0   0   1   1   1   1 cobalt-precorrin 4 C11-methyltransferase 
PROKKA_01970         -          2.1.1.17-RXN         -              0.011   11.9   2.6     0.012   11.8   2.6   1.0   1   0   0   1   1   1   0 hypothetical protein
PROKKA_01365         -          2.1.1.34-RXN         -            5.6e-28   95.2   0.0   6.8e-28   95.0   0.0   1.0   1   0   0   1   1   1   1 23S rRNA (guanosine2251-2'-O)-methyltransferase
PROKKA_01738         -          2.1.1.63-RXN         -            6.1e-44  147.2   0.0   7.2e-44  147.0   0.0   1.0   1   0   0   1   1   1   1 methylated-DNA-[protein]-cysteine S-methyltransferase
PROKKA_00986         -          2.1.1.72-RXN         -              6e-13   46.0   0.0   3.5e-10   37.0   0.0   2.2   1   1   0   1   1   1   1 DNA adenine methylase
PROKKA_00564         -          2.1.1.72-RXN         -            2.3e-12   44.1   0.0   1.7e-09   34.7   0.0   2.4   1   1   0   1   1   1   1 DNA adenine methylase
PROKKA_02080         -          2.1.1.72-RXN         -            3.9e-08   30.3   0.0   4.3e-08   30.1   0.0   1.2   1   0   0   1   1   1   1 adenine-specific DNA-methyltransferase
PROKKA_02207         -          2.1.1.72-RXN         -            3.9e-08   30.3   0.1   8.3e-08   29.2   0.1   1.7   1   0   0   1   1   1   1 putative helicase
PROKKA_02272         -          2.1.1.72-RXN         -            9.7e-07   25.7   0.0   1.1e-06   25.5   0.0   1.1   1   0   0   1   1   1   1 release factor glutamine methyltransferase
PROKKA_02595         -          2.1.1.72-RXN         -             0.0011   15.8   0.0    0.0011   15.8   0.0   1.1   1   0   0   1   1   1   1 hypothetical protein
PROKKA_01336         -          2.1.1.72-RXN         -             0.0011   15.7   0.0    0.0012   15.6   0.0   1.1   1   0   0   1   1   1   1 16S rRNA (guanine966-N2)-methyltransferase
PROKKA_00778         -          2.1.1.72-RXN         -             0.0012   15.6   0.0    0.0013   15.5   0.0   1.2   1   0   0   1   1   1   1 SAM-dependent methyltransferase
PROKKA_01058         -          2.1.1.72-RXN         -             0.0014   15.5   0.0    0.0014   15.5   0.0   1.1   1   0   0   1   1   1   1 hypothetical protein
PROKKA_00177         -          2.1.1.77-RXN         -            7.7e-09   32.7   0.0   1.1e-08   32.2   0.0   1.2   1   0   0   1   1   1   1 tRNA (adenine57-N1/adenine58-N1)-methyltransferase
PROKKA_01286         -          2.1.1.77-RXN         -            2.9e-08   30.8   0.0   5.1e-08   30.0   0.0   1.4   1   0   0   1   1   1   1 Methyltransferase domain-containing protein
PROKKA_02511         -          2.1.1.77-RXN         -            9.1e-05   19.4   0.0   0.00014   18.8   0.0   1.3   1   1   0   1   1   1   1 precorrin-6Y C5,15-methyltransferase (decarboxylating)
PROKKA_00176         -          2.1.1.79-RXN         -            1.6e-06   24.5   0.0   2.2e-06   24.1   0.0   1.1   1   0   0   1   1   1   1 Methyltransferase domain-containing protein
PROKKA_02511         -          2.1.1.79-RXN         -            1.1e-05   21.8   0.0   1.5e-05   21.3   0.0   1.1   1   0   0   1   1   1   1 precorrin-6Y C5,15-methyltransferase (decarboxylating)
PROKKA_01286         -          2.1.1.79-RXN         -            6.5e-05   19.2   0.0   8.8e-05   18.8   0.0   1.1   1   0   0   1   1   1   1 Methyltransferase domain-containing protein
PROKKA_02148         -          2.1.1.79-RXN         -             0.0056   12.8   0.0    0.0082   12.3   0.0   1.2   1   0   0   1   1   1   1 Methyltransferase domain-containing protein
PROKKA_02272         -          2.1.1.79-RXN         -              0.015   11.4   0.0     0.021   11.0   0.0   1.1   1   0   0   1   1   1   0 release factor glutamine methyltransferase
PROKKA_01949         -          2.1.3.1-RXN          -            2.4e-14   49.8   0.0   3.1e-14   49.5   0.0   1.1   1   0   0   1   1   1   1 acetyl-CoA carboxylase carboxyltransferase subunit alpha
PROKKA_01441         -          2.1.3.1-RXN          -             0.0048   12.5   0.0    0.0069   12.0   0.0   1.2   1   0   0   1   1   1   1 acetyl-CoA carboxylase carboxyl transferase subunit alpha
PROKKA_02178         -          2.3.1.128-RXN        -            1.7e-23   80.5   0.0   2.3e-23   80.0   0.0   1.2   1   0   0   1   1   1   1 ribosomal-protein-alanine N-acetyltransferase
PROKKA_01270         -          2.3.1.128-RXN        -            1.8e-07   28.2   0.0   2.6e-07   27.7   0.0   1.1   1   0   0   1   1   1   1 Acetyltransferase (GNAT) domain-containing protein
PROKKA_02130         -          2.3.1.128-RXN        -              2e-07   28.0   0.0   2.3e-07   27.8   0.0   1.1   1   0   0   1   1   1   1 Ribosomal protein S18 acetylase RimI
PROKKA_00350         -          2.3.1.128-RXN        -            0.00016   18.6   0.0   0.00021   18.2   0.0   1.1   1   0   0   1   1   1   1 L-2,4-diaminobutyric acid acetyltransferase
PROKKA_00891         -          2.3.1.128-RXN        -            0.00039   17.3   0.0    0.0005   17.0   0.0   1.2   1   0   0   1   1   1   1 putative N-acetyltransferase YhbS
PROKKA_01330         -          2.4.1.117-RXN        -            1.6e-08   30.8   0.0   3.2e-08   29.8   0.0   1.4   1   1   0   1   1   1   1 dolichol-phosphate mannosyltransferase
PROKKA_00547         -          2.4.1.117-RXN        -            2.6e-08   30.0   0.0     4e-08   29.4   0.0   1.2   1   0   0   1   1   1   1 Glycosyltransferase, catalytic subunit of cellulose synthase and poly-beta-1,6-N-acetylglucosamine synthase
PROKKA_00541         -          2.4.1.117-RXN        -            3.4e-07   26.4   0.0   5.3e-07   25.7   0.0   1.2   1   0   0   1   1   1   1 Glycosyltransferase involved in cell wall bisynthesis
PROKKA_01310         -          2.4.1.117-RXN        -            3.9e-06   22.9   0.0   5.8e-06   22.3   0.0   1.1   1   0   0   1   1   1   1 Glycosyltransferase involved in cell wall bisynthesis
PROKKA_01298         -          2.4.1.117-RXN        -            6.1e-06   22.3   0.0   9.3e-06   21.7   0.0   1.2   1   0   0   1   1   1   1 Glycosyl transferase family 2
PROKKA_00531         -          2.4.1.117-RXN        -            9.6e-06   21.6   0.0   1.4e-05   21.1   0.0   1.1   1   0   0   1   1   1   1 Glycosyltransferase involved in cell wall bisynthesis
PROKKA_00529         -          2.4.1.117-RXN        -            2.7e-05   20.1   0.0   3.9e-05   19.6   0.0   1.2   1   0   0   1   1   1   1 Glycosyl transferase family 2
PROKKA_00530         -          2.4.1.117-RXN        -            7.4e-05   18.7   0.0    0.0001   18.3   0.0   1.3   1   0   0   1   1   1   1 Glycosyltransferase involved in cell wall bisynthesis
PROKKA_01306         -          2.4.1.117-RXN        -              0.012   11.4   0.0     0.027   10.3   0.0   1.5   1   1   0   1   1   1   0 (heptosyl)LPS beta-1,4-glucosyltransferase
PROKKA_00499         -          2.4.1.19-RXN         -            7.5e-25   84.3   0.1   4.9e-19   65.1   0.3   3.4   2   2   0   2   2   2   2 maltose alpha-D-glucosyltransferase/ alpha-amylase
PROKKA_02234         -          2.4.1.19-RXN         -            4.6e-11   38.8   0.0   1.5e-08   30.4   0.0   2.5   2   1   0   2   2   2   2 glycogen operon protein
PROKKA_00498         -          2.4.1.19-RXN         -            1.6e-09   33.6   0.0   5.4e-05   18.7   0.0   2.7   2   2   0   2   2   2   2 maltose alpha-D-glucosyltransferase/ alpha-amylase
PROKKA_00497         -          2.4.1.19-RXN         -            4.4e-06   22.3   0.0    0.0016   13.8   0.0   2.6   3   0   0   3   3   3   2 maltooligosyl trehalose synthase 
PROKKA_00505         -          2.4.1.19-RXN         -            6.4e-05   18.4   0.0     0.012   10.9   0.1   2.3   1   1   1   2   2   2   2 alpha-1,4-glucan:maltose-1-phosphate maltosyltransferase
PROKKA_00496         -          2.4.1.19-RXN         -              0.011   11.0   0.0     0.042    9.1   0.0   1.8   2   0   0   2   2   2   0 maltooligosyl trehalose hydrolase
PROKKA_00499         -          2.4.1.24-RXN         -            1.2e-69  232.5   0.0   2.5e-55  185.3   0.3   2.0   2   0   0   2   2   2   2 maltose alpha-D-glucosyltransferase/ alpha-amylase
PROKKA_00498         -          2.4.1.24-RXN         -            3.2e-37  125.6   0.0   5.9e-28   95.0   0.0   2.1   2   0   0   2   2   2   2 maltose alpha-D-glucosyltransferase/ alpha-amylase
PROKKA_00497         -          2.4.1.24-RXN         -            5.6e-05   19.2   0.0   8.6e-05   18.6   0.0   1.1   1   0   0   1   1   1   1 maltooligosyl trehalose synthase 
PROKKA_00496         -          2.4.1.24-RXN         -            0.00027   16.9   0.0    0.0011   15.0   0.0   1.9   2   1   1   3   3   3   1 maltooligosyl trehalose hydrolase
PROKKA_02234         -          2.4.1.24-RXN         -            0.00089   15.2   0.3     0.041    9.7   0.1   2.4   2   1   0   3   3   3   1 glycogen operon protein
PROKKA_00505         -          2.4.1.24-RXN         -               0.01   11.7   0.0      0.19    7.5   0.0   2.1   2   0   0   2   2   2   0 alpha-1,4-glucan:maltose-1-phosphate maltosyltransferase
PROKKA_01310         -          2.4.1.41-RXN         -            2.1e-09   33.9   0.0   5.6e-06   22.6   0.0   2.0   2   0   0   2   2   2   2 Glycosyltransferase involved in cell wall bisynthesis
PROKKA_00530         -          2.4.1.41-RXN         -            3.9e-07   26.4   0.0   5.2e-07   26.0   0.0   1.1   1   0   0   1   1   1   1 Glycosyltransferase involved in cell wall bisynthesis
PROKKA_01298         -          2.4.1.41-RXN         -            9.4e-06   21.9   0.0   1.2e-05   21.5   0.0   1.2   1   0   0   1   1   1   1 Glycosyl transferase family 2
PROKKA_00529         -          2.4.1.41-RXN         -            7.8e-05   18.8   0.0   0.00011   18.4   0.0   1.1   1   0   0   1   1   1   1 Glycosyl transferase family 2
PROKKA_00081         -          2.4.1.46-RXN         -            1.8e-27   93.2   0.0   2.7e-27   92.6   0.0   1.3   1   1   0   1   1   1   1 processive 1,2-diacylglycerol beta-glucosyltransferase
PROKKA_00544         -          2.4.1.52-RXN         -            3.3e-06   22.8   0.0   4.2e-06   22.5   0.0   1.0   1   0   0   1   1   1   1 Glycosyltransferase involved in cell wall bisynthesis
PROKKA_01809         -          2.4.1.52-RXN         -            0.00026   16.5   0.0   0.00036   16.1   0.0   1.1   1   0   0   1   1   1   1 Glycosyltransferase involved in cell wall bisynthesis
PROKKA_01811         -          2.4.1.52-RXN         -            0.00034   16.2   0.0    0.0005   15.6   0.0   1.1   1   0   0   1   1   1   1 alpha-1,2-rhamnosyltransferase
PROKKA_00546         -          2.4.1.52-RXN         -             0.0018   13.8   0.0    0.0023   13.4   0.0   1.1   1   0   0   1   1   1   1 Glycosyltransferase involved in cell wall bisynthesis
PROKKA_01317         -          2.4.1.52-RXN         -             0.0034   12.9   0.0    0.0093   11.4   0.0   1.5   1   1   1   2   2   2   1 Glycosyltransferase involved in cell wall bisynthesis
PROKKA_01324         -          2.4.1.52-RXN         -              0.023   10.1   0.2      0.03    9.8   0.2   1.1   1   0   0   1   1   1   0 hypothetical protein
PROKKA_00544         -          2.4.1.56-RXN         -            4.7e-11   39.6   0.0   7.9e-11   38.9   0.0   1.3   1   1   0   1   1   1   1 Glycosyltransferase involved in cell wall bisynthesis
PROKKA_00545         -          2.4.1.56-RXN         -            1.8e-10   37.7   0.0   4.7e-10   36.3   0.0   1.6   1   1   0   1   1   1   1 Glycosyltransferase involved in cell wall bisynthesis
PROKKA_01317         -          2.4.1.56-RXN         -            1.9e-05   21.2   0.0   2.7e-05   20.7   0.0   1.2   1   0   0   1   1   1   1 Glycosyltransferase involved in cell wall bisynthesis
PROKKA_00546         -          2.4.1.56-RXN         -            3.1e-05   20.4   0.0     5e-05   19.8   0.0   1.3   1   0   0   1   1   1   1 Glycosyltransferase involved in cell wall bisynthesis
PROKKA_01632         -          2.4.1.56-RXN         -            0.00013   18.4   0.0    0.0025   14.2   0.0   2.2   2   0   0   2   2   2   1 Glycosyltransferase involved in cell wall bisynthesis
PROKKA_01816         -          2.4.1.56-RXN         -            0.00053   16.4   0.0    0.0015   14.9   0.0   1.6   1   1   0   1   1   1   1 Glycosyltransferase involved in cell wall bisynthesis
PROKKA_01811         -          2.4.1.56-RXN         -             0.0013   15.1   0.0     0.002   14.5   0.0   1.2   1   0   0   1   1   1   1 alpha-1,2-rhamnosyltransferase
PROKKA_01330         -          2.4.1.83-RXN         -            1.6e-29  100.4   0.0     2e-29  100.1   0.0   1.0   1   0   0   1   1   1   1 dolichol-phosphate mannosyltransferase
PROKKA_00530         -          2.4.1.83-RXN         -            1.2e-10   38.5   0.0   1.8e-10   38.0   0.0   1.2   1   0   0   1   1   1   1 Glycosyltransferase involved in cell wall bisynthesis
PROKKA_00529         -          2.4.1.83-RXN         -            6.4e-10   36.2   0.0     1e-09   35.5   0.0   1.3   1   0   0   1   1   1   1 Glycosyl transferase family 2
PROKKA_01310         -          2.4.1.83-RXN         -            7.7e-09   32.6   0.0   1.1e-08   32.1   0.0   1.2   1   0   0   1   1   1   1 Glycosyltransferase involved in cell wall bisynthesis
PROKKA_00531         -          2.4.1.83-RXN         -            9.9e-09   32.3   0.0   1.3e-08   31.8   0.0   1.1   1   0   0   1   1   1   1 Glycosyltransferase involved in cell wall bisynthesis
PROKKA_01298         -          2.4.1.83-RXN         -            2.5e-08   31.0   0.0   3.7e-08   30.4   0.0   1.3   1   0   0   1   1   1   1 Glycosyl transferase family 2
PROKKA_00541         -          2.4.1.83-RXN         -            2.4e-07   27.7   0.0   3.6e-07   27.1   0.0   1.2   1   0   0   1   1   1   1 Glycosyltransferase involved in cell wall bisynthesis
PROKKA_00547         -          2.4.1.83-RXN         -            8.9e-06   22.6   0.0   1.6e-05   21.8   0.0   1.3   1   0   0   1   1   1   1 Glycosyltransferase, catalytic subunit of cellulose synthase and poly-beta-1,6-N-acetylglucosamine synthase
PROKKA_01306         -          2.4.1.83-RXN         -            7.1e-05   19.6   0.0   0.00012   18.9   0.0   1.3   1   0   0   1   1   1   1 (heptosyl)LPS beta-1,4-glucosyltransferase
PROKKA_01308         -          2.4.1.83-RXN         -             0.0005   16.9   0.0   0.00079   16.2   0.0   1.2   1   0   0   1   1   1   1 Glycosyltransferase, GT2 family
PROKKA_00532         -          2.4.1.83-RXN         -             0.0021   14.8   0.0    0.0033   14.2   0.0   1.2   1   0   0   1   1   1   1 galactofuranosylgalactofuranosylrhamnosyl-N-acetylglucosaminyl-diphospho-decaprenol beta-1,5/1,6-galactofuranosyltransferase
PROKKA_00623         -          2.4.1.83-RXN         -             0.0053   13.5   0.0      0.01   12.6   0.0   1.4   1   0   0   1   1   1   1 ceramide glucosyltransferase
PROKKA_02285         -          2.4.99.1-RXN         -            2.9e-29   99.0   2.6   3.3e-29   98.9   2.6   1.0   1   0   0   1   1   1   1 phosphate transport system protein
PROKKA_01536         -          2.5.1.19-RXN         -            1.5e-96  321.1   0.0   1.8e-96  320.8   0.0   1.0   1   0   0   1   1   1   1 3-phosphoshikimate 1-carboxyvinyltransferase
PROKKA_02271         -          2.5.1.19-RXN         -            1.3e-09   34.3   0.1   1.2e-07   27.8   0.0   2.9   1   1   1   2   2   2   2 UDP-N-acetylglucosamine 1-carboxyvinyltransferase
PROKKA_02188         -          2.5.1.32-RXN         -            3.1e-33  112.4   0.0   3.9e-33  112.0   0.0   1.0   1   0   0   1   1   1   1 farnesyl-diphosphate farnesyltransferase
PROKKA_02186         -          2.5.1.32-RXN         -            5.9e-25   85.2   0.0     7e-25   85.0   0.0   1.1   1   0   0   1   1   1   1 squalene synthase HpnC
PROKKA_01338         -          2.6.1.64-RXN         -            4.8e-60  201.0   0.0   5.4e-60  200.8   0.0   1.1   1   0   0   1   1   1   1 aspartate aminotransferase
PROKKA_00241         -          2.6.1.64-RXN         -            3.2e-38  129.1   0.0   1.3e-37  127.0   0.0   1.8   1   1   0   1   1   1   1 LL-diaminopimelate aminotransferase apoenzyme
PROKKA_01352         -          2.6.1.64-RXN         -            3.6e-35  119.0   0.0   6.5e-35  118.2   0.0   1.4   1   1   0   1   1   1   1 alanine-synthesizing transaminase
PROKKA_01224         -          2.6.1.64-RXN         -            3.1e-05   20.4   0.0   4.8e-05   19.8   0.0   1.2   1   0   0   1   1   1   1 histidinol-phosphate aminotransferase
PROKKA_02268         -          2.6.1.64-RXN         -             0.0013   15.0   0.0     0.002   14.4   0.0   1.3   1   0   0   1   1   1   1 histidinol-phosphate aminotransferase
PROKKA_01781         -          2.6.1.64-RXN         -              0.006   12.9   0.0     0.021   11.1   0.0   1.9   2   1   0   2   2   2   1 CDP-6-deoxy-D-xylo-4-hexulose-3-dehydrase
PROKKA_01558         -          2.6.1.64-RXN         -             0.0088   12.3   0.0     0.013   11.8   0.0   1.2   1   0   0   1   1   1   1 L-threonine O-3-phosphate decarboxylase 
PROKKA_01338         -          2.6.1.7-RXN          -            4.8e-60  201.0   0.0   5.4e-60  200.8   0.0   1.1   1   0   0   1   1   1   1 aspartate aminotransferase
PROKKA_00241         -          2.6.1.7-RXN          -            3.2e-38  129.1   0.0   1.3e-37  127.0   0.0   1.8   1   1   0   1   1   1   1 LL-diaminopimelate aminotransferase apoenzyme
PROKKA_01352         -          2.6.1.7-RXN          -            3.6e-35  119.0   0.0   6.5e-35  118.2   0.0   1.4   1   1   0   1   1   1   1 alanine-synthesizing transaminase
PROKKA_01224         -          2.6.1.7-RXN          -            3.1e-05   20.4   0.0   4.8e-05   19.8   0.0   1.2   1   0   0   1   1   1   1 histidinol-phosphate aminotransferase
PROKKA_02268         -          2.6.1.7-RXN          -             0.0013   15.0   0.0     0.002   14.4   0.0   1.3   1   0   0   1   1   1   1 histidinol-phosphate aminotransferase
PROKKA_01781         -          2.6.1.7-RXN          -              0.006   12.9   0.0     0.021   11.1   0.0   1.9   2   1   0   2   2   2   1 CDP-6-deoxy-D-xylo-4-hexulose-3-dehydrase
PROKKA_01558         -          2.6.1.7-RXN          -             0.0088   12.3   0.0     0.013   11.8   0.0   1.2   1   0   0   1   1   1   1 L-threonine O-3-phosphate decarboxylase 
PROKKA_02143         -          2.7.3.9-RXN          -              2e-45  152.6   0.1   8.7e-31  104.3   0.0   2.8   2   1   1   3   3   3   2 phosphoenolpyruvate synthase
PROKKA_01643         -          2.7.4.10-RXN         -            9.1e-42  140.1   0.0   1.1e-41  139.8   0.0   1.0   1   0   0   1   1   1   1 Adenylate kinase
PROKKA_00770         -          2.7.7.13-RXN         -            3.9e-50  167.9   0.0     5e-50  167.6   0.0   1.1   1   0   0   1   1   1   1 mannose-1-phosphate guanylyltransferase / mannose-6-phosphate isomerase
PROKKA_01782         -          2.7.7.13-RXN         -            1.1e-23   81.0   0.0   1.3e-23   80.7   0.0   1.0   1   0   0   1   1   1   1 D-glycero-alpha-D-manno-heptose 1-phosphate guanylyltransferase
PROKKA_00489         -          2.7.7.13-RXN         -            7.6e-17   58.5   0.0   9.1e-17   58.3   0.0   1.1   1   0   0   1   1   1   1 Glucose-1-phosphate thymidylyltransferase
PROKKA_00537         -          2.7.7.13-RXN         -            2.9e-12   43.5   0.0   5.7e-12   42.5   0.0   1.4   1   1   0   1   1   1   1 UTP--glucose-1-phosphate uridylyltransferase
PROKKA_00121         -          2.7.7.13-RXN         -            9.1e-12   41.8   0.0   5.4e-11   39.3   0.0   1.8   1   1   0   1   1   1   1 UDP-glucose pyrophosphorylase 
PROKKA_02107         -          2.7.7.13-RXN         -            1.4e-09   34.7   0.0   2.6e-06   23.9   0.0   3.0   2   1   1   3   3   3   2 bifunctional UDP-N-acetylglucosamine pyrophosphorylase / Glucosamine-1-phosphate N-acetyltransferase
PROKKA_02429         -          2.7.7.13-RXN         -            1.6e-08   31.1   0.0   6.3e-08   29.2   0.0   2.0   1   1   0   1   1   1   1 glucose-1-phosphate adenylyltransferase
PROKKA_01292         -          2.7.7.13-RXN         -            1.9e-06   24.4   6.4    0.0074   12.5   0.1   3.4   2   2   1   3   3   3   3 UDP-3-O-[3-hydroxymyristoyl] glucosamine N-acyltransferase
PROKKA_02470         -          2.7.7.13-RXN         -            0.00017   17.9   0.0   0.00018   17.8   0.0   1.1   1   0   0   1   1   1   1 mannose-1-phosphate guanylyltransferase / phosphomannomutase
PROKKA_02463         -          2.7.7.14-RXN         -            1.3e-06   25.0   0.0   1.6e-06   24.7   0.0   1.2   1   0   0   1   1   1   1 rfaE bifunctional protein, domain II
PROKKA_02463         -          2.7.7.15-RXN         -              5e-05   20.3   0.0   6.3e-05   19.9   0.0   1.1   1   0   0   1   1   1   1 rfaE bifunctional protein, domain II
PROKKA_01782         -          2.7.7.33-RXN         -            1.2e-20   71.4   0.0   7.4e-14   49.2   0.0   2.1   1   1   1   2   2   2   2 D-glycero-alpha-D-manno-heptose 1-phosphate guanylyltransferase
PROKKA_02429         -          2.7.7.33-RXN         -            7.7e-08   29.5   0.0   1.1e-06   25.7   0.0   2.1   1   1   0   1   1   1   1 glucose-1-phosphate adenylyltransferase
PROKKA_00537         -          2.7.7.33-RXN         -            2.3e-05   21.4   0.0     3e-05   20.9   0.0   1.2   1   0   0   1   1   1   1 UTP--glucose-1-phosphate uridylyltransferase
PROKKA_00489         -          2.7.7.33-RXN         -            2.4e-05   21.3   0.1   0.00081   16.3   0.1   2.1   2   0   0   2   2   2   1 Glucose-1-phosphate thymidylyltransferase
PROKKA_00121         -          2.7.7.33-RXN         -              6e-05   20.0   0.0   6.7e-05   19.8   0.0   1.2   1   0   0   1   1   1   1 UDP-glucose pyrophosphorylase 
PROKKA_02470         -          2.7.7.33-RXN         -            0.00015   18.7   0.0    0.0013   15.6   0.0   1.9   1   1   1   2   2   2   1 mannose-1-phosphate guanylyltransferase / phosphomannomutase
PROKKA_02463         -          2.7.7.39-RXN         -            3.2e-10   37.6   0.0   6.6e-10   36.6   0.0   1.7   1   1   0   1   1   1   1 rfaE bifunctional protein, domain II
PROKKA_01337         -          2.7.7.39-RXN         -             0.0029   15.1   0.0    0.0063   14.0   0.0   1.5   1   0   0   1   1   1   1 Phosphopantetheine adenylyltransferase
PROKKA_00809         -          2.7.7.8-RXN          -           3.5e-297  984.6   9.9  4.6e-297  984.2   9.9   1.0   1   0   0   1   1   1   1 polyribonucleotide nucleotidyltransferase
PROKKA_00838         -          2.7.7.8-RXN          -            9.3e-50  166.7  16.5   3.2e-13   45.9   0.1   5.6   1   1   5   6   6   6   5 SSU ribosomal protein S1P
PROKKA_02246         -          2.7.7.8-RXN          -            1.4e-09   33.9   0.2   3.6e-08   29.2   0.1   2.0   1   1   0   2   2   2   2 ribonuclease PH
PROKKA_00128         -          2.7.7.8-RXN          -             0.0074   11.6   0.2     0.011   11.1   0.2   1.2   1   0   0   1   1   1   1 RNAse R
PROKKA_01696         -          2.7.8.11-RXN         -            1.5e-09   35.2   4.4   4.4e-09   33.7   1.7   2.0   2   0   0   2   2   2   1 CDP-diacylglycerol--glycerol-3-phosphate 3-phosphatidyltransferase
PROKKA_01873         -          2.7.8.11-RXN         -            2.2e-08   31.4   3.1   5.2e-08   30.2   3.1   1.7   1   1   0   1   1   1   1 CDP-diacylglycerol--glycerol-3-phosphate 3-phosphatidyltransferase
PROKKA_02538         -          2.7.8.11-RXN         -             0.0055   13.8   0.5    0.0055   13.8   0.5   2.0   2   0   0   2   2   2   1 CDP-diacylglycerol---serine O-phosphatidyltransferase
PROKKA_00597         -          2.7.8.15-RXN         -            8.5e-19   64.9  10.8   1.4e-18   64.2  10.8   1.4   1   1   0   1   1   1   1 Phospho-N-acetylmuramoyl-pentapeptide-transferase
PROKKA_01814         -          2.7.8.6-RXN          -            2.4e-88  294.1   3.1   1.6e-73  245.2   0.2   2.0   2   0   0   2   2   2   2 undecaprenyl-phosphate galactose phosphotransferase
PROKKA_00551         -          2.7.8.6-RXN          -            4.4e-34  115.2   0.0   4.4e-34  115.2   0.0   2.0   2   1   0   2   2   2   1 sugar transferase, PEP-CTERM system associated/exopolysaccharide biosynthesis polyprenyl glycosylphosphotransferase
PROKKA_01578         -          2.8.1.6-RXN          -           2.3e-106  353.1   0.0  2.9e-106  352.7   0.0   1.0   1   0   0   1   1   1   1 biotin synthase
PROKKA_02236         -          2.8.1.6-RXN          -            1.9e-05   21.3   0.0   2.5e-05   20.9   0.0   1.2   1   0   0   1   1   1   1 lipoic acid synthetase
PROKKA_01199         -          2.8.1.6-RXN          -              0.003   14.1   0.0    0.0057   13.1   0.0   1.4   1   0   0   1   1   1   1 cyclic pyranopterin phosphate synthase
PROKKA_01702         -          24-DICHLOROPHENOL-6-MONOOXYGENASE-RXN -            2.4e-07   26.8   0.0   2.7e-05   20.0   0.0   2.7   2   1   0   2   2   2   2 2-polyprenyl-6-methoxyphenol hydroxylase
PROKKA_02377         -          24-DICHLOROPHENOL-6-MONOOXYGENASE-RXN -             0.0041   12.8   0.1     0.027   10.1   0.1   1.9   2   0   0   2   2   2   1 geranylgeranyl reductase family protein
PROKKA_01779         -          2OXOGLUTDECARB-RXN   -            1.1e-05   20.8   0.0   1.3e-05   20.5   0.0   1.1   1   0   0   1   1   1   1 pyruvate dehydrogenase E1 component alpha subunit
PROKKA_02097         -          2PGADEHYDRAT-RXN     -           1.3e-178  591.3   0.0  1.6e-178  591.0   0.0   1.0   1   0   0   1   1   1   1 enolase
PROKKA_01706         -          2TRANSKETO-RXN       -           1.1e-248  824.0   0.0  1.4e-248  823.7   0.0   1.0   1   0   0   1   1   1   1 transketolase
PROKKA_00681         -          2TRANSKETO-RXN       -            6.8e-49  163.8   0.0   2.8e-27   92.4   0.0   3.0   2   1   1   3   3   3   3 1-deoxy-D-xylulose-5-phosphate synthase
PROKKA_01780         -          2TRANSKETO-RXN       -            7.2e-07   25.0   0.0   9.7e-07   24.5   0.0   1.1   1   0   0   1   1   1   1 pyruvate dehydrogenase E1 component beta subunit
PROKKA_01779         -          2TRANSKETO-RXN       -            7.1e-05   18.4   0.0   8.6e-05   18.1   0.0   1.1   1   0   0   1   1   1   1 pyruvate dehydrogenase E1 component alpha subunit
PROKKA_02453         -          2TRANSKETO-RXN       -              0.011   11.1   0.0     0.016   10.5   0.0   1.2   1   0   0   1   1   1   0 pyruvate ferredoxin oxidoreductase alpha subunit
PROKKA_01544         -          3-CH3-2-OXOBUTANOATE-OH-CH3-XFER-RXN -            1.4e-94  313.7   0.0   1.7e-94  313.4   0.0   1.0   1   0   0   1   1   1   1 ketopantoate hydroxymethyltransferase 
PROKKA_01590         -          3-DEHYDROQUINATE-DEHYDRATASE-RXN -            6.3e-25   85.4   0.0   5.6e-14   49.4   0.0   2.0   1   1   1   2   2   2   2 3-dehydroquinate dehydratase
PROKKA_00788         -          3-DEHYDROQUINATE-DEHYDRATASE-RXN -              0.011   12.3   0.0     0.011   12.2   0.0   1.1   1   0   0   1   1   1   0 shikimate dehydrogenase 
PROKKA_00887         -          3-DEHYDROQUINATE-SYNTHASE-RXN -              2e-68  228.0   0.0   2.4e-68  227.8   0.0   1.0   1   0   0   1   1   1   1 3-dehydroquinate synthase
PROKKA_01536         -          3-DEHYDROQUINATE-SYNTHASE-RXN -            3.2e-35  118.0   0.0   4.3e-35  117.6   0.0   1.1   1   0   0   1   1   1   1 3-phosphoshikimate 1-carboxyvinyltransferase
PROKKA_00788         -          3-DEHYDROQUINATE-SYNTHASE-RXN -            1.4e-25   86.0   0.0   1.6e-25   85.9   0.0   1.0   1   0   0   1   1   1   1 shikimate dehydrogenase 
PROKKA_00885         -          3-DEHYDROQUINATE-SYNTHASE-RXN -            5.1e-18   61.0   0.0     6e-18   60.7   0.0   1.0   1   0   0   1   1   1   1 shikimate kinase
PROKKA_00886         -          3-DEHYDROQUINATE-SYNTHASE-RXN -            6.1e-12   40.8   0.0   7.4e-12   40.5   0.0   1.0   1   0   0   1   1   1   1 3-dehydroquinate synthase
PROKKA_00514         -          3-HYDROXBUTYRYL-COA-DEHYDRATASE-RXN -            1.8e-13   47.6   0.0   2.4e-13   47.2   0.0   1.1   1   0   0   1   1   1   1 DSF synthase
PROKKA_02160         -          3-HYDROXYBUTYRATE-DEHYDROGENASE-RXN -            1.5e-18   64.3   0.3     2e-18   63.9   0.3   1.1   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier-protein] reductase
PROKKA_00235         -          3-HYDROXYBUTYRATE-DEHYDROGENASE-RXN -            2.3e-12   44.0   0.0   5.6e-12   42.7   0.0   1.5   1   1   0   1   1   1   1 3-oxoacyl-[acyl-carrier protein] reductase
PROKKA_02149         -          3-HYDROXYBUTYRATE-DEHYDROGENASE-RXN -            4.9e-10   36.3   0.0   3.8e-09   33.4   0.0   1.9   1   1   0   1   1   1   1 NADP-dependent 3-hydroxy acid dehydrogenase YdfG
PROKKA_00773         -          3-HYDROXYBUTYRATE-DEHYDROGENASE-RXN -            1.9e-07   27.8   0.0   3.9e-07   26.8   0.0   1.4   2   0   0   2   2   2   1 short chain dehydrogenase
PROKKA_00170         -          3-HYDROXYBUTYRATE-DEHYDROGENASE-RXN -              9e-05   19.0   0.0    0.0013   15.2   0.0   2.0   2   0   0   2   2   2   1 NAD(P)-dependent dehydrogenase, short-chain alcohol dehydrogenase family
PROKKA_01610         -          3-HYDROXYBUTYRYL-COA-DEHYDROGENASE-RXN -            3.6e-21   72.9   0.0   3.8e-20   69.6   0.0   1.9   2   0   0   2   2   2   1 3-hydroxybutyryl-CoA dehydrogenase
PROKKA_00570         -          3-HYDROXYBUTYRYL-COA-DEHYDROGENASE-RXN -            0.00018   18.1   0.0   0.00035   17.2   0.0   1.4   1   0   0   1   1   1   1 UDP-N-acetyl-D-glucosamine dehydrogenase
PROKKA_00255         -          3-HYDROXYBUTYRYL-COA-DEHYDROGENASE-RXN -              0.035   10.6   0.0     0.067    9.6   0.0   1.3   1   0   0   1   1   1   0 methylenetetrahydrofolate--tRNA-(uracil-5-)-methyltransferase
PROKKA_01293         -          3-HYDROXYDECANOYL-ACP-DEHYDR-RXN -            5.6e-05   20.3   0.1   7.7e-05   19.8   0.1   1.3   1   0   0   1   1   1   1 3-hydroxyacyl-[acyl-carrier-protein] dehydratase
PROKKA_01462         -          3-HYDROXYISOBUTYRATE-DEHYDROGENASE-RXN -            2.8e-45  151.9   0.0   3.8e-45  151.4   0.0   1.2   1   0   0   1   1   1   1 3-hydroxyisobutyrate dehydrogenase
PROKKA_00462         -          3-HYDROXYISOBUTYRATE-DEHYDROGENASE-RXN -            3.1e-41  138.6   0.0   3.9e-41  138.2   0.0   1.0   1   0   0   1   1   1   1 3-hydroxyisobutyrate dehydrogenase
PROKKA_01836         -          3-HYDROXYISOBUTYRATE-DEHYDROGENASE-RXN -            1.8e-40  136.1   1.1   2.3e-40  135.7   1.1   1.0   1   0   0   1   1   1   1 3-hydroxyisobutyrate dehydrogenase
PROKKA_02151         -          3-HYDROXYISOBUTYRATE-DEHYDROGENASE-RXN -            5.5e-20   68.8   0.1   4.1e-17   59.4   0.0   2.2   2   0   0   2   2   2   2 6-phosphogluconate dehydrogenase (decarboxylating) 
PROKKA_00570         -          3-HYDROXYISOBUTYRATE-DEHYDROGENASE-RXN -             0.0062   12.8   0.0      0.22    7.7   0.0   2.4   1   1   0   1   1   1   1 UDP-N-acetyl-D-glucosamine dehydrogenase
PROKKA_02536         -          3-ISOPROPYLMALDEHYDROG-RXN -           2.1e-135  448.5   0.0  2.3e-135  448.4   0.0   1.0   1   0   0   1   1   1   1 3-isopropylmalate dehydrogenase
PROKKA_00965         -          3-ISOPROPYLMALDEHYDROG-RXN -            1.7e-77  258.0   0.0   1.9e-77  257.9   0.0   1.0   1   0   0   1   1   1   1 isocitrate dehydrogenase (NAD+)
PROKKA_00487         -          3-ISOPROPYLMALISOM-RXN -           1.1e-227  753.6   0.0  1.2e-227  753.4   0.0   1.0   1   0   0   1   1   1   1 3-isopropylmalate dehydratase, large subunit 
PROKKA_00581         -          3-ISOPROPYLMALISOM-RXN -            1.2e-64  215.9   0.1   3.9e-62  207.7   0.0   2.4   2   1   0   2   2   2   1 aconitase
PROKKA_00486         -          3-ISOPROPYLMALISOM-RXN -            9.5e-08   28.4   0.0   0.00073   15.5   0.0   3.0   2   1   1   3   3   3   3 3-isopropylmalate/(R)-2-methylmalate dehydratase small subunit
PROKKA_00961         -          3-OXO-5-BETA-STEROID-4-DEHYDROGENASE-RXN -            0.00067   15.9   0.0    0.0011   15.1   0.0   1.3   1   0   0   1   1   1   1 putative oxidoreductase
PROKKA_01886         -          3-OXO-5-BETA-STEROID-4-DEHYDROGENASE-RXN -             0.0079   12.3   0.0       1.9    4.6   0.0   2.7   2   1   0   2   2   2   2 putative oxidoreductase
PROKKA_01736         -          3-OXO-5-BETA-STEROID-4-DEHYDROGENASE-RXN -              0.022   10.9   0.0       0.1    8.7   0.0   1.8   1   1   0   1   1   1   0 putative oxidoreductase
PROKKA_02160         -          3-OXOACYL-ACP-REDUCT-RXN -            1.1e-51  173.1   1.7   3.9e-44  148.3   0.3   2.1   1   1   1   2   2   2   2 3-oxoacyl-[acyl-carrier-protein] reductase
PROKKA_00235         -          3-OXOACYL-ACP-REDUCT-RXN -            3.2e-26   89.4   0.1   8.4e-24   81.4   0.1   2.1   1   1   0   1   1   1   1 3-oxoacyl-[acyl-carrier protein] reductase
PROKKA_02149         -          3-OXOACYL-ACP-REDUCT-RXN -            4.4e-10   36.2   0.0   5.5e-05   19.5   0.0   2.7   1   1   1   2   2   2   2 NADP-dependent 3-hydroxy acid dehydrogenase YdfG
PROKKA_00773         -          3-OXOACYL-ACP-REDUCT-RXN -              3e-05   20.3   0.0    0.0016   14.7   0.0   2.1   1   1   1   2   2   2   2 short chain dehydrogenase
PROKKA_00170         -          3-OXOACYL-ACP-REDUCT-RXN -             0.0022   14.2   0.0    0.0079   12.4   0.0   1.9   1   1   1   2   2   2   1 NAD(P)-dependent dehydrogenase, short-chain alcohol dehydrogenase family
PROKKA_02158         -          3-OXOACYL-ACP-SYNTH-BASE-RXN -             1e-116  387.3   0.0  1.2e-116  387.0   0.0   1.0   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier-protein] synthase II
PROKKA_02162         -          3-OXOACYL-ACP-SYNTH-BASE-RXN -            1.2e-41  140.0   0.0   1.1e-31  107.2   0.0   2.2   1   1   1   2   2   2   2 3-oxoacyl-[acyl-carrier-protein] synthase-3
PROKKA_00043         -          3-OXOACYL-ACP-SYNTH-BASE-RXN -            1.6e-08   31.0   0.6   0.00087   15.4   0.2   2.2   2   0   0   2   2   2   2 3-oxoacyl-[acyl-carrier-protein] synthase-3
PROKKA_02158         -          3-OXOACYL-ACP-SYNTH-RXN -             1e-116  387.3   0.0  1.2e-116  387.0   0.0   1.0   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier-protein] synthase II
PROKKA_02162         -          3-OXOACYL-ACP-SYNTH-RXN -            1.2e-41  140.0   0.0   1.1e-31  107.2   0.0   2.2   1   1   1   2   2   2   2 3-oxoacyl-[acyl-carrier-protein] synthase-3
PROKKA_00043         -          3-OXOACYL-ACP-SYNTH-RXN -            1.6e-08   31.0   0.6   0.00087   15.4   0.2   2.2   2   0   0   2   2   2   2 3-oxoacyl-[acyl-carrier-protein] synthase-3
PROKKA_01342         -          3-OXOADIPATE-COA-TRANSFERASE-RXN -              0.013   12.0   2.3      0.02   11.4   2.3   1.5   1   1   0   1   1   1   0 methylthioribose-1-phosphate isomerase 
PROKKA_00137         -          3-OXOSTEROID-1-DEHYDROGENASE-RXN -            8.1e-05   18.9   0.9   0.00014   18.2   0.9   1.3   1   0   0   1   1   1   1 dihydrolipoamide dehydrogenase
PROKKA_01909         -          3-OXOSTEROID-1-DEHYDROGENASE-RXN -            0.00069   15.8   0.0     0.001   15.2   0.0   1.2   1   0   0   1   1   1   1 dihydrolipoamide dehydrogenase
PROKKA_00427         -          3-OXOSTEROID-1-DEHYDROGENASE-RXN -             0.0014   14.8   2.0     0.067    9.3   0.1   2.1   2   0   0   2   2   2   2 dissimilatory adenylylsulfate reductase alpha subunit precursor
PROKKA_01157         -          3-OXOSTEROID-1-DEHYDROGENASE-RXN -             0.0015   14.7   1.5    0.0028   13.8   1.5   1.3   1   0   0   1   1   1   1 mercuric reductase
PROKKA_01529         -          3-OXOSTEROID-1-DEHYDROGENASE-RXN -              0.013   11.7   0.0     0.017   11.2   0.0   1.2   1   0   0   1   1   1   0 glycine oxidase
PROKKA_01719         -          3-OXOSTEROID-1-DEHYDROGENASE-RXN -              0.018   11.2   0.4     0.027   10.6   0.4   1.2   1   0   0   1   1   1   0 oxygen-dependent protoporphyrinogen oxidase
PROKKA_00735         -          3-OXOSTEROID-1-DEHYDROGENASE-RXN -              0.031   10.4   1.5     0.048    9.8   1.5   1.3   1   0   0   1   1   1   0 C-3',4' desaturase CrtD
PROKKA_00585         -          3-OXOSTEROID-1-DEHYDROGENASE-RXN -              0.057    9.5   8.4       0.1    8.7   0.4   3.1   3   1   0   3   3   3   0 succinate dehydrogenase / fumarate reductase flavoprotein subunit
PROKKA_01546         -          3.1.11.2-RXN         -            1.1e-46  156.5   0.0   1.5e-46  156.1   0.0   1.1   1   0   0   1   1   1   1 Exodeoxyribonuclease III 
PROKKA_00678         -          3.1.11.6-RXN         -            8.4e-88  292.3   0.1   1.2e-84  281.9   0.0   2.2   1   1   1   2   2   2   2 Exodeoxyribonuclease VII large subunit
PROKKA_00128         -          3.1.13.1-RXN         -            7.6e-52  173.4   0.0     1e-51  173.0   0.0   1.1   1   0   0   1   1   1   1 RNAse R
PROKKA_00657         -          3.1.21.2-RXN         -            4.1e-35  118.4   0.0   5.1e-35  118.1   0.0   1.0   1   0   0   1   1   1   1 Endonuclease IV
PROKKA_00026         -          3.1.21.3-RXN         -            6.8e-09   32.5   0.0   9.7e-09   32.0   0.0   1.1   1   0   0   1   1   1   1 ATP-dependent helicase IRC3
PROKKA_02080         -          3.1.21.5-RXN         -            1.5e-09   33.6   0.0   6.1e-07   25.0   0.0   2.9   3   0   0   3   3   3   3 adenine-specific DNA-methyltransferase
PROKKA_00292         -          3.1.22.4-RXN         -            6.8e-20   68.7   0.0   8.7e-20   68.3   0.0   1.0   1   0   0   1   1   1   1 Holliday junction endonuclease RuvC
PROKKA_02157         -          3.1.26.3-RXN         -            4.8e-65  216.2   0.0   5.7e-65  216.0   0.0   1.0   1   0   0   1   1   1   1 ribonuclease-3
PROKKA_02494         -          3.1.26.3-RXN         -            8.8e-47  156.5   0.0     2e-46  155.3   0.0   1.5   1   1   0   1   1   1   1 ribonuclease-3
PROKKA_02254         -          3.1.26.4-RXN         -            3.4e-08   31.4   0.0   4.2e-08   31.0   0.0   1.3   1   1   0   1   1   1   1 ribonuclease HI
PROKKA_00846         -          3.1.26.4-RXN         -            1.6e-07   29.2   0.0   2.2e-07   28.7   0.0   1.4   1   1   0   1   1   1   1 RNase HII
PROKKA_01035         -          3.1.27.10-RXN        -            2.5e-12   43.6   0.0   2.9e-05   20.3   0.0   3.2   3   0   0   3   3   3   3 hypothetical protein
PROKKA_01718         -          3.1.3.16-RXN         -            3.7e-05   20.5   0.0   3.7e-05   20.5   0.0   1.1   1   0   0   1   1   1   1 serine/threonine protein phosphatase 1
PROKKA_02237         -          3.1.3.46-RXN         -            8.2e-10   34.7   0.0     1e-09   34.4   0.0   1.0   1   0   0   1   1   1   1 phosphoglycerate mutase 
PROKKA_00962         -          3.1.3.46-RXN         -            3.2e-07   26.2   0.0   4.3e-07   25.8   0.0   1.0   1   0   0   1   1   1   1 alpha-ribazole phosphatase
PROKKA_00258         -          3.1.3.46-RXN         -             0.0036   12.9   0.0     0.005   12.4   0.0   1.1   1   0   0   1   1   1   1 ATP-dependent HslUV protease ATP-binding subunit HslU
PROKKA_00841         -          3.1.3.46-RXN         -              0.017   10.6   0.0     0.026   10.0   0.0   1.2   1   0   0   1   1   1   0 signal recognition particle subunit FFH/SRP54 (srp54)
PROKKA_00139         -          3.1.6.12-RXN         -             0.0016   13.9   0.2    0.0023   13.4   0.2   1.1   1   0   0   1   1   1   1 Phosphoesterase family protein
PROKKA_00499         -          3.2.1.10-RXN         -            6.1e-50  167.6   0.4   2.5e-43  145.8   1.7   2.2   1   1   1   2   2   2   2 maltose alpha-D-glucosyltransferase/ alpha-amylase
PROKKA_00498         -          3.2.1.10-RXN         -            1.1e-29  100.7   0.1   4.4e-24   82.3   0.0   2.5   2   1   1   3   3   3   2 maltose alpha-D-glucosyltransferase/ alpha-amylase
PROKKA_02234         -          3.2.1.10-RXN         -            2.8e-09   33.4   0.1   3.5e-09   33.1   0.1   1.2   1   0   0   1   1   1   1 glycogen operon protein
PROKKA_00505         -          3.2.1.10-RXN         -            3.3e-05   20.0   0.1   0.00032   16.8   0.0   2.0   2   0   0   2   2   2   1 alpha-1,4-glucan:maltose-1-phosphate maltosyltransferase
PROKKA_00496         -          3.2.1.10-RXN         -            4.5e-05   19.6   0.0   6.5e-05   19.0   0.0   1.2   1   0   0   1   1   1   1 maltooligosyl trehalose hydrolase
PROKKA_00497         -          3.2.1.10-RXN         -            0.00016   17.8   0.0   0.00029   16.9   0.0   1.3   1   0   0   1   1   1   1 maltooligosyl trehalose synthase 
PROKKA_00003         -          3.2.1.10-RXN         -              0.021   10.7   0.0      0.04    9.8   0.0   1.5   1   1   0   1   1   1   0 DNA gyrase subunit B 
PROKKA_00499         -          3.2.1.135-RXN        -            2.1e-36  122.8   1.0   2.3e-26   89.7   3.3   3.3   3   0   0   3   3   3   3 maltose alpha-D-glucosyltransferase/ alpha-amylase
PROKKA_00498         -          3.2.1.135-RXN        -            7.9e-25   84.6   0.0   4.3e-16   55.8   0.0   3.0   3   0   0   3   3   3   2 maltose alpha-D-glucosyltransferase/ alpha-amylase
PROKKA_02234         -          3.2.1.135-RXN        -            7.9e-18   61.5   0.2   2.1e-07   27.1   0.0   3.1   3   0   0   3   3   3   3 glycogen operon protein
PROKKA_00496         -          3.2.1.135-RXN        -            2.9e-16   56.3   0.0   4.4e-12   42.5   0.0   2.9   3   0   0   3   3   3   2 maltooligosyl trehalose hydrolase
PROKKA_00497         -          3.2.1.135-RXN        -            5.6e-14   48.8   0.0   1.9e-13   47.1   0.0   1.8   2   0   0   2   2   2   1 maltooligosyl trehalose synthase 
PROKKA_02244         -          3.2.1.21-RXN         -            4.7e-07   25.9   0.0     6e-07   25.6   0.0   1.1   1   0   0   1   1   1   1 beta-N-acetylhexosaminidase
PROKKA_01343         -          3.2.1.33-RXN         -            4.3e-24   80.7   0.0   2.7e-15   51.5   0.1   2.1   2   0   0   2   2   2   2 4-alpha-glucanotransferase
PROKKA_00505         -          3.2.1.33-RXN         -              0.012    9.7   0.0     0.016    9.2   0.0   1.1   1   0   0   1   1   1   0 alpha-1,4-glucan:maltose-1-phosphate maltosyltransferase
PROKKA_00578         -          3.2.1.50-RXN         -              0.024    9.7   0.6     0.035    9.2   0.6   1.1   1   0   0   1   1   1   0 citryl-CoA synthetase large subunit
PROKKA_02234         -          3.2.1.60-RXN         -            0.00059   16.0   0.0   0.00099   15.2   0.0   1.3   1   0   0   1   1   1   1 glycogen operon protein
PROKKA_02234         -          3.2.1.68-RXN         -           1.6e-148  493.4   0.4  5.8e-146  484.9   0.4   2.1   1   1   0   1   1   1   1 glycogen operon protein
PROKKA_00496         -          3.2.1.68-RXN         -            7.6e-12   41.3   0.0   1.8e-09   33.4   0.0   3.3   3   1   0   3   3   3   1 maltooligosyl trehalose hydrolase
PROKKA_00499         -          3.2.1.68-RXN         -            1.3e-10   37.3   0.0   0.00013   17.4   0.0   3.1   3   1   1   4   4   4   2 maltose alpha-D-glucosyltransferase/ alpha-amylase
PROKKA_00499         -          3.2.1.70-RXN         -            6.3e-80  266.4   1.4   4.1e-67  224.2   1.1   3.0   2   1   1   3   3   3   2 maltose alpha-D-glucosyltransferase/ alpha-amylase
PROKKA_00498         -          3.2.1.70-RXN         -            8.7e-53  176.9   0.3   1.1e-40  136.9   0.1   3.0   2   1   1   3   3   3   2 maltose alpha-D-glucosyltransferase/ alpha-amylase
PROKKA_00496         -          3.2.1.70-RXN         -              6e-09   32.2   0.0     1e-08   31.4   0.0   1.3   1   0   0   1   1   1   1 maltooligosyl trehalose hydrolase
PROKKA_02234         -          3.2.1.70-RXN         -            7.1e-07   25.3   0.1   1.4e-05   21.1   0.0   2.3   2   1   1   3   3   3   1 glycogen operon protein
PROKKA_00497         -          3.2.1.70-RXN         -            1.8e-06   24.0   0.0   6.9e-06   22.1   0.0   1.9   2   0   0   2   2   2   1 maltooligosyl trehalose synthase 
PROKKA_00505         -          3.2.1.70-RXN         -             0.0097   11.7   0.0     0.079    8.7   0.0   2.0   1   1   0   1   1   1   1 alpha-1,4-glucan:maltose-1-phosphate maltosyltransferase
PROKKA_00499         -          3.2.1.98-RXN         -            1.1e-11   40.7   0.7   2.2e-10   36.5   0.2   2.7   3   1   0   3   3   3   1 maltose alpha-D-glucosyltransferase/ alpha-amylase
PROKKA_00498         -          3.2.1.98-RXN         -            3.8e-05   19.2   0.0   0.00067   15.0   0.0   2.7   3   1   0   3   3   3   1 maltose alpha-D-glucosyltransferase/ alpha-amylase
PROKKA_02234         -          3.2.1.98-RXN         -             0.0029   12.9   0.1    0.0093   11.3   0.0   1.7   2   0   0   2   2   2   1 glycogen operon protein
PROKKA_00496         -          3.2.1.98-RXN         -             0.0041   12.4   0.4     0.059    8.6   0.0   2.6   3   1   0   3   3   3   1 maltooligosyl trehalose hydrolase
PROKKA_01888         -          3.2.2.17-RXN         -            1.3e-32  110.2   0.0   1.8e-32  109.8   0.0   1.0   1   0   0   1   1   1   1 DNA-(apurinic or apyrimidinic site) lyase /endonuclease III
PROKKA_01452         -          3.2.2.17-RXN         -            4.5e-14   49.6   0.0   8.1e-14   48.8   0.0   1.4   1   1   0   1   1   1   1 endonuclease-3
PROKKA_00244         -          3.2.2.17-RXN         -            0.00015   18.5   0.0   0.00026   17.7   0.0   1.4   1   0   0   1   1   1   1 A/G-specific DNA-adenine glycosylase
PROKKA_01737         -          3.2.2.21-RXN         -            1.1e-52  176.7   0.1   1.5e-52  176.2   0.1   1.1   1   0   0   1   1   1   1 DNA-3-methyladenine glycosylase II
PROKKA_02473         -          3.2.2.23-RXN         -              7e-60  199.7   0.0   8.7e-60  199.4   0.0   1.0   1   0   0   1   1   1   1 DNA-(apurinic or apyrimidinic site) lyase
PROKKA_00008         -          3.4.11.1-RXN         -           1.5e-124  413.9   0.0  2.1e-124  413.5   0.0   1.0   1   0   0   1   1   1   1 leucyl aminopeptidase
PROKKA_00319         -          3.4.11.15-RXN        -           3.6e-100  333.6   0.0   1.7e-99  331.3   0.0   1.8   2   0   0   2   2   2   1 puromycin-sensitive aminopeptidase
PROKKA_01642         -          3.4.11.18-RXN        -            1.6e-88  293.5   0.0     2e-88  293.2   0.0   1.0   1   0   0   1   1   1   1 methionine aminopeptidase, type I 
PROKKA_02433         -          3.4.11.18-RXN        -            2.4e-08   30.7   0.0   5.2e-08   29.6   0.0   1.5   1   0   0   1   1   1   1 Xaa-Pro aminopeptidase
PROKKA_00319         -          3.4.11.2-RXN         -           5.9e-175  581.2   0.0  7.6e-175  580.8   0.0   1.0   1   0   0   1   1   1   1 puromycin-sensitive aminopeptidase
PROKKA_01835         -          3.4.11.5-RXN         -            5.2e-09   33.1   0.0   3.3e-08   30.4   0.1   1.9   2   0   0   2   2   2   1 Pimeloyl-ACP methyl ester carboxylesterase
PROKKA_00319         -          3.4.11.7-RXN         -           9.2e-137  454.4   0.0  1.1e-136  454.1   0.0   1.0   1   0   0   1   1   1   1 puromycin-sensitive aminopeptidase
PROKKA_02433         -          3.4.11.9-RXN         -            4.5e-38  128.2   0.0   5.5e-38  127.9   0.0   1.1   1   0   0   1   1   1   1 Xaa-Pro aminopeptidase
PROKKA_01642         -          3.4.11.9-RXN         -            2.8e-17   59.6   0.0     3e-17   59.5   0.0   1.0   1   0   0   1   1   1   1 methionine aminopeptidase, type I 
PROKKA_02433         -          3.4.13.9-RXN         -              2e-46  155.9   0.0   2.8e-46  155.5   0.0   1.1   1   0   0   1   1   1   1 Xaa-Pro aminopeptidase
PROKKA_01642         -          3.4.13.9-RXN         -            6.5e-23   78.7   0.0   8.1e-23   78.3   0.0   1.0   1   0   0   1   1   1   1 methionine aminopeptidase, type I 
PROKKA_01335         -          3.4.17.8-RXN         -             0.0012   15.7   1.6    0.0018   15.1   1.5   1.3   1   1   0   1   1   1   1 Putative peptidoglycan binding domain-containing protein
PROKKA_01412         -          3.4.21.19-RXN        -             0.0025   14.2   0.5     0.019   11.3   0.0   2.0   2   0   0   2   2   2   1 hypothetical protein
PROKKA_00122         -          3.4.21.53-RXN        -           1.5e-295  979.7   0.0  1.9e-295  979.3   0.0   1.0   1   0   0   1   1   1   1 ATP-dependent proteinase. Serine peptidase. MEROPS family S16
PROKKA_01889         -          3.4.21.53-RXN        -           1.7e-290  963.0   0.0    2e-290  962.7   0.0   1.0   1   0   0   1   1   1   1 ATP-dependent Lon protease
PROKKA_01898         -          3.4.21.53-RXN        -            4.7e-12   42.2   0.0   5.3e-12   42.0   0.0   1.0   1   0   0   1   1   1   1 Lon protease
PROKKA_01699         -          3.4.21.53-RXN        -            1.5e-06   24.0   0.6    0.0037   12.7   0.2   2.5   2   1   0   2   2   2   2 ATP-dependent Clp protease ATP-binding subunit ClpX
PROKKA_00927         -          3.4.21.53-RXN        -            5.9e-06   22.0   1.3   4.3e-05   19.2   0.8   2.2   2   0   0   2   2   2   1 ATP-dependent Clp protease ATP-binding subunit ClpC
PROKKA_01998         -          3.4.21.53-RXN        -            7.4e-06   21.7   0.0   1.3e-05   20.9   0.0   1.3   1   0   0   1   1   1   1 cell division protease FtsH
PROKKA_02180         -          3.4.21.53-RXN        -            1.8e-05   20.4   0.1    0.0093   11.4   0.0   2.2   2   0   0   2   2   2   2 DNA repair protein RadA/Sms
PROKKA_02440         -          3.4.21.53-RXN        -            0.00017   17.2  10.8    0.0049   12.4   0.0   3.5   4   0   0   4   4   4   2 ATP-dependent Clp protease ATP-binding subunit ClpB
PROKKA_00290         -          3.4.21.53-RXN        -            0.00021   16.9   0.0   0.00029   16.4   0.0   1.1   1   0   0   1   1   1   1 Holliday junction DNA helicase subunit RuvB
PROKKA_00335         -          3.4.21.53-RXN        -            0.00091   14.8   0.1   0.00091   14.8   0.1   1.7   2   0   0   2   2   2   1 proteasome-associated ATPase
PROKKA_01985         -          3.4.21.53-RXN        -             0.0078   11.7   0.0     0.014   10.8   0.0   1.3   1   0   0   1   1   1   1 proteasome-associated ATPase
PROKKA_01802         -          3.4.21.53-RXN        -              0.017   10.5   0.0     0.023   10.2   0.0   1.1   1   0   0   1   1   1   0 DNA replication protein DnaC
PROKKA_00220         -          3.4.21.53-RXN        -              0.029    9.8   0.1     0.045    9.2   0.1   1.2   1   0   0   1   1   1   0 flagellar biosynthesis protein FlhF
PROKKA_02248         -          3.4.21.53-RXN        -                4.2    2.7  11.0      0.73    5.2   0.1   3.1   3   0   0   3   3   3   0 ATP-binding cassette, subfamily F, member 3
PROKKA_01880         -          3.4.21.89-RXN        -            7.1e-64  212.1   0.0   9.2e-64  211.7   0.0   1.1   1   0   0   1   1   1   1 signal peptidase I
PROKKA_00983         -          3.4.21.89-RXN        -            5.7e-07   26.3   0.0   2.9e-06   23.9   0.0   2.0   1   1   0   1   1   1   1 conjugative transfer signal peptidase TraF
PROKKA_01700         -          3.4.21.92-RXN        -            1.3e-86  286.7   0.1   1.6e-86  286.4   0.1   1.0   1   0   0   1   1   1   1 ATP-dependent Clp protease, protease subunit
PROKKA_00927         -          3.4.21.92-RXN        -            8.9e-24   81.3   0.2   1.1e-23   81.0   0.2   1.2   1   0   0   1   1   1   1 ATP-dependent Clp protease ATP-binding subunit ClpC
PROKKA_02440         -          3.4.21.92-RXN        -            2.3e-11   40.8   3.2   3.6e-11   40.1   3.2   1.4   1   1   0   1   1   1   1 ATP-dependent Clp protease ATP-binding subunit ClpB
PROKKA_00839         -          3.4.21.92-RXN        -             0.0019   14.9   0.0    0.0026   14.4   0.0   1.2   1   0   0   1   1   1   1 protease-4
PROKKA_02477         -          3.4.23.36-RXN        -            1.5e-41  139.0   1.6   1.8e-41  138.7   1.6   1.0   1   0   0   1   1   1   1 signal peptidase II
PROKKA_01756         -          3.4.24.35-RXN        -                4.9    2.4  14.0       7.5    1.9  14.0   1.2   1   0   0   1   1   1   0 hypothetical protein
PROKKA_00395         -          3.4.24.55-RXN        -              5e-21   71.2   0.0     7e-21   70.7   0.0   1.1   1   0   0   1   1   1   1 zinc protease
PROKKA_00811         -          3.4.24.55-RXN        -              1e-17   60.3   0.0   7.2e-14   47.5   0.0   2.2   2   0   0   2   2   2   2 putative Zn-dependent peptidase
PROKKA_00395         -          3.4.24.56-RXN        -              2e-11   39.5   0.0   3.4e-11   38.7   0.0   1.3   1   1   0   1   1   1   1 zinc protease
PROKKA_00811         -          3.4.24.56-RXN        -            6.9e-10   34.4   0.0     1e-09   33.8   0.0   1.3   2   0   0   2   2   2   1 putative Zn-dependent peptidase
PROKKA_00928         -          3.4.24.57-RXN        -            3.7e-80  266.5   0.0   4.5e-80  266.3   0.0   1.0   1   0   0   1   1   1   1 N6-L-threonylcarbamoyladenine synthase
PROKKA_00395         -          3.4.24.61-RXN        -            1.6e-05   20.0   0.0   2.1e-05   19.6   0.0   1.1   1   0   0   1   1   1   1 zinc protease
PROKKA_00811         -          3.4.24.61-RXN        -            0.00014   16.9   0.0   0.00019   16.4   0.0   1.1   1   0   0   1   1   1   1 putative Zn-dependent peptidase
PROKKA_00811         -          3.4.24.64-RXN        -            5.6e-85  283.1   0.0   7.5e-85  282.7   0.0   1.1   1   0   0   1   1   1   1 putative Zn-dependent peptidase
PROKKA_00395         -          3.4.24.64-RXN        -            3.6e-41  138.6   0.0   7.2e-41  137.6   0.0   1.4   1   1   0   1   1   1   1 zinc protease
PROKKA_00394         -          3.4.24.64-RXN        -            4.9e-32  108.5   0.0   6.9e-32  108.0   0.0   1.2   1   0   0   1   1   1   1 zinc protease
PROKKA_00338         -          3.4.25.1-RXN         -            1.8e-26   90.1   0.0   2.3e-26   89.8   0.0   1.1   1   0   0   1   1   1   1 proteasome beta subunit
PROKKA_01982         -          3.4.25.1-RXN         -            2.9e-16   56.7   0.0   3.5e-16   56.4   0.0   1.1   1   0   0   1   1   1   1 proteasome beta subunit
PROKKA_01744         -          3.4.25.1-RXN         -            1.2e-09   35.0   0.0   1.6e-09   34.6   0.0   1.1   1   0   0   1   1   1   1 putative proteasome-type protease
PROKKA_00257         -          3.4.25.1-RXN         -            2.5e-06   24.2   0.0   7.3e-06   22.6   0.0   1.7   2   0   0   2   2   2   1 HslV component of HslUV peptidase. Threonine peptidase. MEROPS family T01B
PROKKA_00339         -          3.4.25.1-RXN         -            0.00017   18.2   0.0    0.0003   17.4   0.0   1.5   1   1   0   1   1   1   1 proteasome alpha subunit
PROKKA_01376         -          3.5.1.26-RXN         -            1.1e-24   84.0   0.2   8.6e-19   64.6   0.3   2.1   1   1   1   2   2   2   2 beta-aspartyl-peptidase (threonine type)
PROKKA_02411         -          3.5.1.27-RXN         -            1.5e-33  113.0   0.0   1.7e-33  112.9   0.0   1.0   1   0   0   1   1   1   1 peptide deformylase
PROKKA_02515         -          3.5.1.27-RXN         -            1.9e-32  109.4   0.0   2.3e-32  109.2   0.0   1.1   1   0   0   1   1   1   1 peptide deformylase
PROKKA_01435         -          3.5.1.28-RXN         -            2.1e-32  110.1   0.0   2.8e-32  109.7   0.0   1.2   1   0   0   1   1   1   1 N-acetylmuramoyl-L-alanine amidase
PROKKA_02101         -          3.5.2.12-RXN         -            8.4e-40  133.9   0.0     2e-36  122.7   0.0   2.1   2   0   0   2   2   2   2 aspartyl/glutamyl-tRNA(Asn/Gln) amidotransferase subunit A
PROKKA_01367         -          3.5.5.1-RXN          -            1.4e-12   44.4   0.0   8.2e-07   25.5   0.0   2.8   2   1   0   2   2   2   2 NAD+ synthase (glutamine-hydrolysing)
PROKKA_00264         -          3.5.5.1-RXN          -            2.5e-10   37.0   0.0     1e-09   35.0   0.0   2.0   1   1   0   1   1   1   1 putative amidohydrolase
PROKKA_01958         -          3.6.1.17-RXN         -            9.4e-06   22.7   0.0     1e-05   22.5   0.0   1.3   1   0   0   1   1   1   1 ADP-ribose pyrophosphatase
PROKKA_00025         -          3.6.1.17-RXN         -            7.4e-05   19.8   0.0   9.4e-05   19.4   0.0   1.2   1   0   0   1   1   1   1 8-oxo-dGTP diphosphatase
PROKKA_01718         -          3.6.1.41-RXN         -            1.4e-18   64.5   0.0   1.3e-13   48.3   0.0   2.2   2   0   0   2   2   2   2 serine/threonine protein phosphatase 1
PROKKA_02050         -          3.6.3.6-RXN          -            4.1e-07   27.2   3.7     9e-07   26.1   3.7   1.4   1   1   0   1   1   1   1 Cu+-exporting ATPase
PROKKA_00454         -          3.6.3.6-RXN          -            1.2e-06   25.7   0.5   6.8e-06   23.2   0.2   1.5   1   1   0   1   1   1   1 K+-transporting ATPase ATPase B chain
PROKKA_00313         -          3.6.3.6-RXN          -            0.00033   17.8   0.0   0.00044   17.4   0.0   1.3   1   0   0   1   1   1   1 Cu+-exporting ATPase
PROKKA_02050         -          3.6.3.8-RXN          -            3.7e-91  303.6  12.8   6.3e-45  150.6   0.5   3.0   2   1   1   3   3   3   3 Cu+-exporting ATPase
PROKKA_00454         -          3.6.3.8-RXN          -            4.1e-64  214.1   8.2   1.8e-27   92.8   0.4   3.0   2   1   1   3   3   3   3 K+-transporting ATPase ATPase B chain
PROKKA_00313         -          3.6.3.8-RXN          -              7e-64  213.3   0.0   2.7e-37  125.3   0.0   3.0   2   1   0   2   2   2   2 Cu+-exporting ATPase
PROKKA_02050         -          3.6.3.9-RXN          -            4.1e-08   29.8   0.0   1.3e-07   28.2   0.0   1.7   1   1   0   1   1   1   1 Cu+-exporting ATPase
PROKKA_01842         -          3.6.4.1-RXN          -             0.0014   13.6  13.1    0.0017   13.3  13.1   1.0   1   0   0   1   1   1   1 hypothetical protein
PROKKA_01563         -          3.6.4.1-RXN          -             0.0052   11.7   4.3    0.0065   11.4   4.3   1.2   1   0   0   1   1   1   1 DNA recombination protein RmuC
PROKKA_00676         -          3.6.4.1-RXN          -              0.024    9.6  10.1     0.024    9.6  10.1   1.1   1   0   0   1   1   1   0 ribonucrease Y
PROKKA_02200         -          3.6.4.1-RXN          -               0.17    6.8  19.6      0.24    6.2  19.6   1.2   1   0   0   1   1   1   0 phage shock protein A (PspA) family protein
PROKKA_00980         -          3.6.4.2-RXN          -              0.021    8.0   0.0     0.026    7.7   0.0   1.0   1   0   0   1   1   1   0 type IV secretion system protein VirB11
PROKKA_01916         -          3.7.1.8-RXN          -             0.0011   15.1   0.0     0.002   14.4   0.0   1.3   1   0   0   1   1   1   1 carbamoyl-phosphate synthase large subunit
PROKKA_02484         -          325-BISPHOSPHATE-NUCLEOTIDASE-RXN -            6.4e-06   22.5   0.1     0.014   11.5   0.0   2.1   2   0   0   2   2   2   2 myo-inositol-1(or 4)-monophosphatase
PROKKA_02237         -          3PGAREARR-RXN        -            1.1e-19   68.3   0.0   3.5e-07   27.1   0.0   3.1   2   1   1   3   3   3   3 phosphoglycerate mutase 
PROKKA_01838         -          4-CARBOXYMUCONOLACTONE-DECARBOXYLASE-RXN -            9.8e-10   35.9   0.0   1.2e-09   35.6   0.0   1.2   1   0   0   1   1   1   1 4-carboxymuconolactone decarboxylase
PROKKA_00084         -          4-COUMARATE--COA-LIGASE-RXN -            2.8e-30  102.7   0.0   7.6e-30  101.3   0.0   1.6   1   1   0   1   1   1   1 acetyl-CoA synthetase
PROKKA_00761         -          4-COUMARATE--COA-LIGASE-RXN -            4.4e-19   65.8   0.0   2.4e-09   33.6   0.0   2.1   2   0   0   2   2   2   2 long-chain acyl-CoA synthetase
PROKKA_00084         -          4-HYDROXYBENZOATE--COA-LIGASE-RXN -            4.5e-41  138.1   0.0   7.3e-41  137.4   0.0   1.2   1   0   0   1   1   1   1 acetyl-CoA synthetase
PROKKA_00761         -          4-HYDROXYBENZOATE--COA-LIGASE-RXN -            6.7e-11   38.5   0.0   5.8e-05   18.9   0.0   2.3   2   0   0   2   2   2   2 long-chain acyl-CoA synthetase
PROKKA_01702         -          4-HYDROXYBENZOATE-3-MONOOXYGENASE-RXN -            7.5e-10   35.3   0.6   0.00015   17.8   0.0   2.1   2   0   0   2   2   2   2 2-polyprenyl-6-methoxyphenol hydroxylase
PROKKA_00735         -          4-HYDROXYBENZOATE-3-MONOOXYGENASE-RXN -              0.023   10.6   0.4     0.033   10.1   0.4   1.2   1   0   0   1   1   1   0 C-3',4' desaturase CrtD
PROKKA_02358         -          4.1.1.64-RXN         -            1.1e-47  160.0   0.3     2e-44  149.2   0.3   2.8   1   1   0   1   1   1   1 acetylornithine/N-succinyldiaminopimelate aminotransferase
PROKKA_01321         -          4.1.1.64-RXN         -            2.5e-41  139.0   0.0   3.3e-41  138.6   0.0   1.0   1   0   0   1   1   1   1 adenosylmethionine-8-amino-7-oxononanoate aminotransferase
PROKKA_00349         -          4.1.1.64-RXN         -            1.1e-28   97.4   0.1   2.1e-28   96.5   0.1   1.5   1   1   0   1   1   1   1 diaminobutyrate-2-oxoglutarate transaminase
PROKKA_02461         -          4.1.1.64-RXN         -            8.5e-20   68.1   0.0   2.3e-19   66.7   0.0   1.6   1   1   0   1   1   1   1 glutamate-1-semialdehyde 2,1-aminomutase
PROKKA_02541         -          4.1.1.74-RXN         -            1.4e-26   90.5   0.0   9.3e-26   87.8   0.0   2.1   1   1   0   1   1   1   1 acetolactate synthase, large subunit
PROKKA_01355         -          4.1.99.4-RXN         -            4.7e-07   26.3   0.0    0.0013   14.9   0.0   2.1   2   0   0   2   2   2   2 threonine synthase
PROKKA_01951         -          4.1.99.4-RXN         -            6.8e-05   19.2   0.0   0.00028   17.1   0.0   1.9   2   0   0   2   2   2   1 tryptophan synthase beta chain
PROKKA_02352         -          4.2.1.41-RXN         -            3.8e-20   69.3   0.1   5.1e-20   68.9   0.1   1.1   1   0   0   1   1   1   1 4-hydroxy-tetrahydrodipicolinate synthase
PROKKA_02158         -          4.2.1.61-RXN         -            5.9e-09   30.3   0.0   6.2e-09   30.3   0.0   1.0   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier-protein] synthase II
PROKKA_01717         -          4.2.1.61-RXN         -            9.1e-08   26.4   0.0   1.1e-07   26.2   0.0   1.0   1   0   0   1   1   1   1 NADPH2:quinone reductase
PROKKA_00022         -          4.2.1.61-RXN         -            5.9e-07   23.7   0.0     7e-07   23.4   0.0   1.1   1   0   0   1   1   1   1 NADPH:quinone reductase
PROKKA_01239         -          4.2.1.61-RXN         -            4.1e-05   17.6   0.0   5.3e-05   17.2   0.0   1.0   1   0   0   1   1   1   1 NADPH2:quinone reductase
PROKKA_02161         -          4.2.1.61-RXN         -            0.00037   14.4   0.0   0.00037   14.4   0.0   1.0   1   0   0   1   1   1   1 [acyl-carrier-protein] S-malonyltransferase
PROKKA_01888         -          4.2.99.18-RXN        -            9.1e-19   65.1   0.0   3.7e-15   53.3   0.0   2.0   1   1   1   2   2   2   2 DNA-(apurinic or apyrimidinic site) lyase /endonuclease III
PROKKA_01452         -          4.2.99.18-RXN        -            2.9e-16   56.9   0.0   3.6e-16   56.6   0.0   1.1   1   0   0   1   1   1   1 endonuclease-3
PROKKA_01338         -          4.4.1.14-RXN         -              2e-30  103.0   0.0   2.4e-30  102.7   0.0   1.0   1   0   0   1   1   1   1 aspartate aminotransferase
PROKKA_01352         -          4.4.1.14-RXN         -            1.8e-18   63.6   0.0   8.9e-18   61.3   0.0   1.8   1   1   0   1   1   1   1 alanine-synthesizing transaminase
PROKKA_00241         -          4.4.1.14-RXN         -            2.2e-11   40.2   0.0   5.3e-11   39.0   0.0   1.5   1   1   0   1   1   1   1 LL-diaminopimelate aminotransferase apoenzyme
PROKKA_01224         -          4.4.1.14-RXN         -            2.9e-06   23.3   0.0   5.2e-06   22.5   0.0   1.3   1   1   0   1   1   1   1 histidinol-phosphate aminotransferase
PROKKA_01558         -          4.4.1.14-RXN         -              0.004   13.0   0.0    0.0059   12.4   0.0   1.1   1   0   0   1   1   1   1 L-threonine O-3-phosphate decarboxylase 
PROKKA_01653         -          4OH2OXOGLUTARALDOL-RXN -             0.0041   13.6   0.0    0.0046   13.5   0.0   1.1   1   0   0   1   1   1   1 large subunit ribosomal protein L22
PROKKA_01580         -          5-AMINOLEVULINIC-ACID-SYNTHASE-RXN -            7.2e-66  220.1   0.0   9.6e-66  219.7   0.0   1.0   1   0   0   1   1   1   1 8-amino-7-oxononanoate synthase
PROKKA_01174         -          5-AMINOLEVULINIC-ACID-SYNTHASE-RXN -            0.00051   16.1   0.0   0.00074   15.5   0.0   1.2   1   0   0   1   1   1   1 cysteine desulfurase
PROKKA_01540         -          5-FORMYL-THF-CYCLO-LIGASE-RXN -            2.1e-22   76.4   0.0   2.7e-21   72.8   0.0   2.0   1   1   0   1   1   1   1 5-formyltetrahydrofolate cyclo-ligase
PROKKA_00903         -          5-METHYLTHIOADENOSINE-PHOSPHORYLASE-RXN -            2.5e-66  220.8   0.0   3.2e-66  220.5   0.0   1.0   1   0   0   1   1   1   1 methylthioadenosine phosphorylase
PROKKA_00084         -          5.1.1.11-RXN         -            7.6e-18   60.6   0.0   2.2e-13   45.8   0.0   3.0   3   0   0   3   3   3   3 acetyl-CoA synthetase
PROKKA_00761         -          5.1.1.11-RXN         -            0.00015   16.6   0.0    0.0026   12.5   0.0   2.0   2   0   0   2   2   2   2 long-chain acyl-CoA synthetase
PROKKA_01629         -          5.1.3.20-RXN         -            1.6e-94  313.9   0.1   1.8e-94  313.7   0.1   1.0   1   0   0   1   1   1   1 ADP-L-glycero-D-manno-heptose 6-epimerase
PROKKA_00572         -          5.1.3.20-RXN         -            3.7e-40  135.3   0.0   4.5e-40  135.1   0.0   1.0   1   0   0   1   1   1   1 UDP-glucose 4-epimerase
PROKKA_00566         -          5.1.3.20-RXN         -              2e-26   90.2   0.0   3.3e-25   86.2   0.0   2.0   1   1   0   1   1   1   1 UDP-glucuronate 4-epimerase
PROKKA_00656         -          5.1.3.20-RXN         -            3.9e-24   82.7   0.0   8.5e-24   81.6   0.0   1.5   1   1   0   1   1   1   1 UDP-galactose 4-epimerase
PROKKA_00683         -          5.1.3.20-RXN         -            2.1e-20   70.5   0.0   1.1e-19   68.1   0.0   1.8   1   1   0   1   1   1   1 UDP-glucose 4-epimerase
PROKKA_00488         -          5.1.3.20-RXN         -            3.2e-17   60.0   0.0   4.8e-17   59.4   0.0   1.2   1   0   0   1   1   1   1 dTDP-glucose 4,6-dehydratase
PROKKA_01783         -          5.1.3.20-RXN         -            5.9e-15   52.5   0.0   9.3e-15   51.9   0.0   1.3   1   0   0   1   1   1   1 Nucleoside-diphosphate-sugar epimerase
PROKKA_01526         -          5.1.3.20-RXN         -            9.7e-15   51.8   0.0   1.4e-14   51.3   0.0   1.2   1   0   0   1   1   1   1 UDP-glucuronate decarboxylase
PROKKA_01778         -          5.1.3.20-RXN         -            2.9e-11   40.4   0.0   5.1e-09   33.1   0.0   2.2   2   0   0   2   2   2   2 GDP-L-fucose synthase
PROKKA_01635         -          5.1.3.20-RXN         -            1.2e-05   22.0   0.0   1.5e-05   21.7   0.0   1.1   1   0   0   1   1   1   1 dTDP-4-dehydrorhamnose reductase
PROKKA_00620         -          5.1.3.20-RXN         -            0.00011   18.8   0.0   0.00053   16.6   0.0   2.0   2   1   0   2   2   2   1 dihydroflavonol-4-reductase
PROKKA_00469         -          5.1.3.20-RXN         -            0.00077   16.0   0.0      0.21    8.0   0.0   2.3   2   1   0   2   2   2   2 NADH dehydrogenase
PROKKA_01392         -          5.1.3.20-RXN         -              0.026   11.0   0.0     0.035   10.6   0.0   1.2   1   0   0   1   1   1   0 Nucleoside-diphosphate-sugar epimerase
PROKKA_02350         -          5.3.3.10-RXN         -            5.6e-34  114.6   0.0   6.9e-34  114.3   0.0   1.0   1   0   0   1   1   1   1 2-keto-4-pentenoate hydratase/2-oxohepta-3-ene-1,7-dioic acid hydratase (catechol pathway)
PROKKA_00852         -          5.3.4.1-RXN          -            1.6e-18   64.2   0.0   1.8e-18   64.1   0.0   1.0   1   0   0   1   1   1   1 thioredoxin
PROKKA_00318         -          5.3.4.1-RXN          -            5.1e-13   46.2   0.0     6e-13   45.9   0.0   1.1   1   0   0   1   1   1   1 thioredoxin
PROKKA_02379         -          5.3.4.1-RXN          -            0.00013   18.5   0.0   0.00017   18.2   0.0   1.3   1   0   0   1   1   1   1 Peroxiredoxin
PROKKA_00823         -          5.3.4.1-RXN          -            0.00019   18.0   0.0   0.00021   17.9   0.0   1.0   1   0   0   1   1   1   1 Glutaredoxin
PROKKA_02380         -          5.3.4.1-RXN          -             0.0013   15.3   0.1    0.0028   14.2   0.1   1.4   1   1   0   1   1   1   1 Peroxiredoxin
PROKKA_00831         -          5.3.4.1-RXN          -              0.024   11.1   0.0     0.024   11.1   0.0   1.1   1   0   0   1   1   1   0 Thiol-disulfide isomerase or thioredoxin
PROKKA_01366         -          5.3.4.1-RXN          -              0.024   11.1   0.0     0.032   10.7   0.0   1.2   1   0   0   1   1   1   0 Thiol-disulfide isomerase or thioredoxin
PROKKA_01372         -          5.5.1.2-RXN          -            3.6e-55  184.5   1.3   4.5e-55  184.1   1.3   1.1   1   0   0   1   1   1   1 Adenylosuccinate lyase 
PROKKA_01556         -          5.5.1.2-RXN          -            4.9e-16   55.5   0.7   7.9e-16   54.8   0.3   1.4   2   0   0   2   2   2   1 fumarase, class II
PROKKA_02355         -          5.5.1.2-RXN          -            1.5e-07   27.5   0.0   1.9e-07   27.2   0.0   1.2   1   0   0   1   1   1   1 argininosuccinate lyase 
PROKKA_00254         -          5.99.1.2-RXN         -            2.7e-15   53.2   0.0     4e-15   52.7   0.0   1.2   1   0   0   1   1   1   1 DNA topoisomerase-1
PROKKA_00004         -          5.99.1.3-RXN         -           1.7e-171  569.5   0.0  2.1e-171  569.2   0.0   1.0   1   0   0   1   1   1   1 DNA gyrase subunit A 
PROKKA_00003         -          5.99.1.3-RXN         -           2.1e-157  522.9   0.0  1.6e-141  470.3   0.0   2.0   1   1   1   2   2   2   2 DNA gyrase subunit B 
PROKKA_00848         -          5.99.1.3-RXN         -             0.0087   11.2   6.8    0.0099   11.0   6.8   1.0   1   0   0   1   1   1   1 Septal ring factor EnvC, activator of murein hydrolases AmiA and AmiB
PROKKA_02440         -          5.99.1.3-RXN         -              0.012   10.7   3.7    0.0097   11.0   1.7   1.7   2   0   0   2   2   2   0 ATP-dependent Clp protease ATP-binding subunit ClpB
PROKKA_02097         -          6-BETA-HYDROXYHYOSCYAMINE-EPOXIDASE-RXN -           1.4e-120  400.6   0.0  1.7e-120  400.3   0.0   1.0   1   0   0   1   1   1   1 enolase
PROKKA_01579         -          6-CARBOXYHEXANOATE--COA-LIGASE-RXN -            3.4e-26   89.3   0.0   4.6e-26   88.8   0.0   1.1   1   0   0   1   1   1   1 6-carboxyhexanoate--CoA ligase
PROKKA_02237         -          6-PHOSPHOFRUCTO-2-KINASE-RXN -            1.2e-07   27.5   0.0   1.5e-07   27.2   0.0   1.0   1   0   0   1   1   1   1 phosphoglycerate mutase 
PROKKA_00962         -          6-PHOSPHOFRUCTO-2-KINASE-RXN -            1.1e-06   24.3   0.0   1.4e-06   23.9   0.0   1.1   1   0   0   1   1   1   1 alpha-ribazole phosphatase
PROKKA_00258         -          6-PHOSPHOFRUCTO-2-KINASE-RXN -            0.00042   15.8   0.0   0.00058   15.4   0.0   1.1   1   0   0   1   1   1   1 ATP-dependent HslUV protease ATP-binding subunit HslU
PROKKA_00841         -          6-PHOSPHOFRUCTO-2-KINASE-RXN -             0.0054   12.2   0.0    0.0083   11.5   0.0   1.2   1   0   0   1   1   1   1 signal recognition particle subunit FFH/SRP54 (srp54)
PROKKA_01916         -          6.3.4.16-RXN         -           3.7e-301  999.3   0.0  4.6e-301  999.0   0.0   1.0   1   0   0   1   1   1   1 carbamoyl-phosphate synthase large subunit
PROKKA_01918         -          6.3.4.16-RXN         -            5.9e-73  243.0   0.0   7.3e-73  242.7   0.0   1.0   1   0   0   1   1   1   1 carbamoyl-phosphate synthase small subunit
PROKKA_01593         -          6.3.4.16-RXN         -            1.5e-09   32.8   0.0   2.1e-09   32.4   0.0   1.1   1   0   0   1   1   1   1 acetyl-CoA carboxylase, biotin carboxylase subunit
PROKKA_01955         -          6.3.4.16-RXN         -            3.4e-09   31.6   0.0   3.8e-09   31.5   0.0   1.0   1   0   0   1   1   1   1 anthranilate synthase, component II 
PROKKA_02242         -          6.3.4.16-RXN         -               0.04    8.2   0.0     0.055    7.8   0.0   1.3   1   1   0   1   1   1   0 putative glutamine amidotransferase
PROKKA_02151         -          6PGLUCONDEHYDROG-RXN -            2.4e-67  224.9   0.2     9e-55  183.4   0.2   2.8   1   1   0   2   2   2   2 6-phosphogluconate dehydrogenase (decarboxylating) 
PROKKA_01836         -          6PGLUCONDEHYDROG-RXN -              1e-08   31.5   0.1   8.2e-05   18.7   0.0   2.0   2   0   0   2   2   2   2 3-hydroxyisobutyrate dehydrogenase
PROKKA_01462         -          6PGLUCONDEHYDROG-RXN -            1.4e-07   27.7   0.0   2.4e-07   27.0   0.0   1.3   1   1   0   1   1   1   1 3-hydroxyisobutyrate dehydrogenase
PROKKA_00462         -          6PGLUCONDEHYDROG-RXN -              0.022   10.7   0.0     0.026   10.4   0.0   1.4   1   1   0   1   1   1   0 3-hydroxyisobutyrate dehydrogenase
PROKKA_00570         -          6PGLUCONDEHYDROG-RXN -              0.034   10.0   0.0     0.053    9.4   0.0   1.2   1   0   0   1   1   1   0 UDP-N-acetyl-D-glucosamine dehydrogenase
PROKKA_01707         -          6PGLUCONOLACT-RXN    -            8.3e-05   19.3   0.0   0.00029   17.5   0.0   1.9   1   1   0   1   1   1   1 6-phosphogluconolactonase
PROKKA_02160         -          7-ALPHA-HYDROXYSTEROID-DEH-RXN -            1.8e-46  156.0   1.0   2.4e-46  155.6   1.0   1.0   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier-protein] reductase
PROKKA_00235         -          7-ALPHA-HYDROXYSTEROID-DEH-RXN -            1.9e-29  100.3   0.0   2.2e-29  100.0   0.0   1.1   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier protein] reductase
PROKKA_02149         -          7-ALPHA-HYDROXYSTEROID-DEH-RXN -            4.9e-15   53.0   0.0   6.5e-15   52.6   0.0   1.1   1   0   0   1   1   1   1 NADP-dependent 3-hydroxy acid dehydrogenase YdfG
PROKKA_00773         -          7-ALPHA-HYDROXYSTEROID-DEH-RXN -            7.4e-14   49.2   0.0     1e-13   48.7   0.0   1.0   1   0   0   1   1   1   1 short chain dehydrogenase
PROKKA_00820         -          7-ALPHA-HYDROXYSTEROID-DEH-RXN -            3.6e-09   33.8   0.0   5.9e-09   33.1   0.0   1.4   1   1   0   1   1   1   1 Enoyl-[acyl-carrier-protein] reductase [NADH]
PROKKA_00170         -          7-ALPHA-HYDROXYSTEROID-DEH-RXN -            0.00017   18.5   0.0    0.0015   15.5   0.0   1.9   2   0   0   2   2   2   1 NAD(P)-dependent dehydrogenase, short-chain alcohol dehydrogenase family
PROKKA_01580         -          7KAPSYN-RXN          -            8.4e-94  311.5   0.0   1.1e-93  311.1   0.0   1.0   1   0   0   1   1   1   1 8-amino-7-oxononanoate synthase
PROKKA_01224         -          7KAPSYN-RXN          -             0.0006   15.8   0.0    0.0008   15.3   0.0   1.1   1   0   0   1   1   1   1 histidinol-phosphate aminotransferase
PROKKA_02373         -          7KAPSYN-RXN          -             0.0016   14.4   0.0    0.0037   13.1   0.0   1.7   1   1   0   1   1   1   1 glycine hydroxymethyltransferase
PROKKA_00495         -          7KAPSYN-RXN          -             0.0037   13.2   0.0    0.0055   12.6   0.0   1.2   1   0   0   1   1   1   1 glutamate decarboxylase
PROKKA_00084         -          ACETATE--COA-LIGASE-RXN -           1.6e-244  810.3   0.0  1.8e-244  810.1   0.0   1.0   1   0   0   1   1   1   1 acetyl-CoA synthetase
PROKKA_00761         -          ACETATE--COA-LIGASE-RXN -            7.4e-09   31.6   0.0   0.00014   17.5   0.0   2.8   2   1   0   2   2   2   2 long-chain acyl-CoA synthetase
PROKKA_01377         -          ACETATEKIN-RXN       -           7.8e-100  331.4   0.0   2.5e-99  329.7   0.0   1.6   1   1   1   2   2   2   1 acetate kinase
PROKKA_02160         -          ACETOINDEHYDROG-A-RXN -            2.2e-23   80.0   1.9   1.3e-22   77.5   1.9   1.8   1   1   0   1   1   1   1 3-oxoacyl-[acyl-carrier-protein] reductase
PROKKA_00235         -          ACETOINDEHYDROG-A-RXN -            6.1e-19   65.4   0.0   7.7e-19   65.1   0.0   1.1   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier protein] reductase
PROKKA_02149         -          ACETOINDEHYDROG-A-RXN -            5.4e-08   29.6   0.0   6.3e-08   29.4   0.0   1.1   1   0   0   1   1   1   1 NADP-dependent 3-hydroxy acid dehydrogenase YdfG
PROKKA_00773         -          ACETOINDEHYDROG-A-RXN -             0.0031   14.0   0.0      0.01   12.3   0.0   1.7   1   1   0   1   1   1   1 short chain dehydrogenase
PROKKA_02540         -          ACETOLACTREDUCTOISOM-RXN -           1.1e-125  416.5   0.0  1.3e-125  416.2   0.0   1.0   1   0   0   1   1   1   1 ketol-acid reductoisomerase
PROKKA_01535         -          ACETOLACTREDUCTOISOM-RXN -            0.00084   15.6   0.0    0.0012   15.0   0.0   1.1   1   0   0   1   1   1   1 prephenate dehydrogenase
PROKKA_02541         -          ACETOLACTSYN-RXN     -           7.7e-244  807.5   0.0  1.4e-240  796.8   0.0   2.1   2   0   0   2   2   2   2 acetolactate synthase, large subunit
PROKKA_02540         -          ACETOOHBUTREDUCTOISOM-RXN -           1.1e-125  416.5   0.0  1.3e-125  416.2   0.0   1.0   1   0   0   1   1   1   1 ketol-acid reductoisomerase
PROKKA_01535         -          ACETOOHBUTREDUCTOISOM-RXN -            0.00084   15.6   0.0    0.0012   15.0   0.0   1.1   1   0   0   1   1   1   1 prephenate dehydrogenase
PROKKA_02541         -          ACETOOHBUTSYN-RXN    -           7.7e-244  807.5   0.0  1.4e-240  796.8   0.0   2.1   2   0   0   2   2   2   2 acetolactate synthase, large subunit
PROKKA_02162         -          ACETYL-COA-ACETYLTRANSFER-RXN -              2e-06   24.3   0.1   3.3e-06   23.6   0.1   1.4   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier-protein] synthase-3
PROKKA_02158         -          ACETYL-COA-ACETYLTRANSFER-RXN -            9.1e-05   18.8   3.0   0.00048   16.4   0.7   2.3   3   0   0   3   3   3   2 3-oxoacyl-[acyl-carrier-protein] synthase II
PROKKA_02413         -          ACETYLESTERASE-RXN   -             0.0041   13.5   0.1     0.011   12.1   0.0   1.7   2   0   0   2   2   2   1 phospholipase/carboxylesterase
PROKKA_00259         -          ACETYLGLUTKIN-RXN    -            2.5e-89  296.7   0.0     3e-89  296.4   0.0   1.0   1   0   0   1   1   1   1 N-acetylglutamate kinase
PROKKA_02146         -          ACETYLGLUTKIN-RXN    -            6.6e-21   71.8   0.0   8.9e-21   71.4   0.0   1.2   1   0   0   1   1   1   1 N-acetylglutamate synthase
PROKKA_01357         -          ACETYLGLUTKIN-RXN    -            6.1e-06   22.6   0.1   1.2e-05   21.6   0.1   1.4   1   0   0   1   1   1   1 aspartate kinase
PROKKA_00105         -          ACETYLGLUTKIN-RXN    -            1.8e-05   21.1   0.0     0.014   11.6   0.0   2.6   2   1   1   3   3   3   2 glutamate 5-kinase
PROKKA_02358         -          ACETYLORNTRANSAM-RXN -           1.7e-135  448.9   0.0  2.2e-135  448.6   0.0   1.0   1   0   0   1   1   1   1 acetylornithine/N-succinyldiaminopimelate aminotransferase
PROKKA_00349         -          ACETYLORNTRANSAM-RXN -            2.5e-72  240.9   0.0   3.2e-72  240.5   0.0   1.0   1   0   0   1   1   1   1 diaminobutyrate-2-oxoglutarate transaminase
PROKKA_01321         -          ACETYLORNTRANSAM-RXN -              2e-54  182.0   0.0   1.7e-53  178.9   0.0   1.9   1   1   0   1   1   1   1 adenosylmethionine-8-amino-7-oxononanoate aminotransferase
PROKKA_02461         -          ACETYLORNTRANSAM-RXN -            3.8e-40  134.9   0.0   5.7e-40  134.4   0.0   1.1   1   0   0   1   1   1   1 glutamate-1-semialdehyde 2,1-aminomutase
PROKKA_02352         -          ACNEULY-RXN          -            6.2e-34  114.9   0.0     7e-34  114.7   0.0   1.0   1   0   0   1   1   1   1 4-hydroxy-tetrahydrodipicolinate synthase
PROKKA_01877         -          ACNEULY-RXN          -              0.022   11.3   0.0     0.036   10.6   0.0   1.2   1   0   0   1   1   1   0 CTP synthase
PROKKA_00581         -          ACONITATEDEHYDR-RXN  -           1.3e-137  457.0   0.0  3.2e-137  455.7   0.0   1.6   1   1   0   1   1   1   1 aconitase
PROKKA_00487         -          ACONITATEDEHYDR-RXN  -              2e-77  257.9   0.0   2.3e-77  257.7   0.0   1.0   1   0   0   1   1   1   1 3-isopropylmalate dehydratase, large subunit 
PROKKA_00486         -          ACONITATEDEHYDR-RXN  -            6.8e-11   37.9   0.0   1.6e-10   36.7   0.0   1.5   2   0   0   2   2   2   1 3-isopropylmalate/(R)-2-methylmalate dehydratase small subunit
PROKKA_02158         -          ACP-S-ACETYLTRANSFER-RXN -            5.9e-09   30.3   0.0   6.3e-09   30.2   0.0   1.0   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier-protein] synthase II
PROKKA_01717         -          ACP-S-ACETYLTRANSFER-RXN -            7.7e-08   26.6   0.0     9e-08   26.4   0.0   1.0   1   0   0   1   1   1   1 NADPH2:quinone reductase
PROKKA_00022         -          ACP-S-ACETYLTRANSFER-RXN -            3.5e-07   24.5   0.0   4.1e-07   24.2   0.0   1.1   1   0   0   1   1   1   1 NADPH:quinone reductase
PROKKA_01239         -          ACP-S-ACETYLTRANSFER-RXN -              4e-05   17.6   0.0   5.1e-05   17.3   0.0   1.0   1   0   0   1   1   1   1 NADPH2:quinone reductase
PROKKA_02161         -          ACP-S-ACETYLTRANSFER-RXN -              9e-05   16.4   0.0   9.1e-05   16.4   0.0   1.0   1   0   0   1   1   1   1 [acyl-carrier-protein] S-malonyltransferase
PROKKA_00647         -          ACSERLY-RXN          -           1.9e-115  382.6   0.0  2.3e-115  382.3   0.0   1.0   1   0   0   1   1   1   1 cysteine synthase 
PROKKA_01355         -          ACSERLY-RXN          -            2.9e-15   53.4   0.1   6.6e-15   52.3   0.1   1.4   1   1   0   1   1   1   1 threonine synthase
PROKKA_01951         -          ACSERLY-RXN          -            7.1e-10   35.7   0.2    0.0001   18.7   0.0   2.4   2   1   0   2   2   2   2 tryptophan synthase beta chain
PROKKA_00649         -          ACSERLY-RXN          -            1.7e-07   27.9   0.4   5.3e-06   23.0   0.0   2.1   2   0   0   2   2   2   1 threonine synthase
PROKKA_02105         -          ACYLAMINOACYL-PEPTIDASE-RXN -            1.7e-09   34.0   0.0   0.00018   17.4   0.0   3.1   1   1   2   3   3   3   3 TolB protein
PROKKA_00899         -          ACYLAMINOACYL-PEPTIDASE-RXN -            1.4e-08   30.9   0.0     5e-06   22.5   0.0   2.1   1   1   1   2   2   2   2 TolB protein
PROKKA_00078         -          ACYLAMINOACYL-PEPTIDASE-RXN -            4.2e-06   22.8   0.0   0.00019   17.3   0.0   2.0   2   0   0   2   2   2   2 carboxymethylenebutenolidase
PROKKA_00761         -          ACYLCOASYN-RXN       -            1.4e-73  245.4   0.0   2.1e-73  244.8   0.0   1.2   1   0   0   1   1   1   1 long-chain acyl-CoA synthetase
PROKKA_00084         -          ACYLCOASYN-RXN       -            1.1e-46  156.5   0.0   3.3e-24   82.3   0.0   2.1   2   0   0   2   2   2   2 acetyl-CoA synthetase
PROKKA_01554         -          ACYLPHOSPHATASE-RXN  -              1e-16   58.2   0.1   1.1e-16   58.1   0.1   1.1   1   0   0   1   1   1   1 acylphosphatase
PROKKA_01919         -          ADENINE-DEAMINASE-RXN -             0.0022   13.5   0.0     0.034    9.6   0.0   2.0   2   0   0   2   2   2   2 dihydroorotase
PROKKA_00904         -          ADENOSINE-KINASE-RXN -             0.0031   13.8   0.0    0.0069   12.6   0.0   1.5   1   1   0   1   1   1   1 Sugar or nucleoside kinase, ribokinase family
PROKKA_02440         -          ADENOSINETRIPHOSPHATASE-RXN -           5.1e-105  349.1  13.7   1.5e-98  327.8  12.1   2.9   1   1   1   2   2   2   2 ATP-dependent Clp protease ATP-binding subunit ClpB
PROKKA_00927         -          ADENOSINETRIPHOSPHATASE-RXN -            9.2e-91  302.2  12.9   3.5e-89  297.0  12.9   2.3   1   1   0   1   1   1   1 ATP-dependent Clp protease ATP-binding subunit ClpC
PROKKA_01889         -          ADENOSINETRIPHOSPHATASE-RXN -            2.2e-69  231.9   0.1   2.5e-69  231.7   0.1   1.2   1   0   0   1   1   1   1 ATP-dependent Lon protease
PROKKA_00122         -          ADENOSINETRIPHOSPHATASE-RXN -            4.7e-67  224.2   0.0   5.2e-67  224.1   0.0   1.2   1   0   0   1   1   1   1 ATP-dependent proteinase. Serine peptidase. MEROPS family S16
PROKKA_01699         -          ADENOSINETRIPHOSPHATASE-RXN -            1.8e-15   54.5   0.1   2.1e-15   54.2   0.1   1.1   1   0   0   1   1   1   1 ATP-dependent Clp protease ATP-binding subunit ClpX
PROKKA_01998         -          ADENOSINETRIPHOSPHATASE-RXN -            2.6e-14   50.6   0.0   4.6e-14   49.8   0.0   1.5   1   1   0   1   1   1   1 cell division protease FtsH
PROKKA_00335         -          ADENOSINETRIPHOSPHATASE-RXN -            1.2e-13   48.5   0.1   1.8e-13   47.8   0.1   1.3   1   1   0   1   1   1   1 proteasome-associated ATPase
PROKKA_01985         -          ADENOSINETRIPHOSPHATASE-RXN -            8.2e-12   42.4   0.0     9e-12   42.3   0.0   1.2   1   0   0   1   1   1   1 proteasome-associated ATPase
PROKKA_00290         -          ADENOSINETRIPHOSPHATASE-RXN -            1.7e-09   34.7   0.0   2.4e-09   34.3   0.0   1.1   1   0   0   1   1   1   1 Holliday junction DNA helicase subunit RuvB
PROKKA_00258         -          ADENOSINETRIPHOSPHATASE-RXN -            3.3e-09   33.8   0.0   8.2e-09   32.5   0.0   1.5   1   1   0   1   1   1   1 ATP-dependent HslUV protease ATP-binding subunit HslU
PROKKA_02317         -          ADENOSINETRIPHOSPHATASE-RXN -            2.4e-07   27.7   0.0   2.9e-07   27.4   0.0   1.2   1   0   0   1   1   1   1 Nif-specific regulatory protein
PROKKA_00474         -          ADENOSINETRIPHOSPHATASE-RXN -            5.5e-07   26.5   0.0   1.4e-06   25.2   0.0   1.5   1   1   0   1   1   1   1 MoxR-like ATPase
PROKKA_02248         -          ADENOSINETRIPHOSPHATASE-RXN -            1.6e-06   25.0   1.1    0.0031   14.2   0.2   2.7   1   1   0   2   2   2   1 ATP-binding cassette, subfamily F, member 3
PROKKA_00156         -          ADENOSINETRIPHOSPHATASE-RXN -            2.2e-06   24.6   0.0   3.6e-06   23.8   0.0   1.4   1   0   0   1   1   1   1 DNA polymerase-3 subunit gamma/tau
PROKKA_01385         -          ADENOSINETRIPHOSPHATASE-RXN -            2.8e-06   24.2   0.0    0.0052   13.4   0.0   2.3   2   0   0   2   2   2   2 ABC-2 type transport system ATP-binding protein
PROKKA_01400         -          ADENOSINETRIPHOSPHATASE-RXN -            6.7e-06   22.9   0.2   1.3e-05   22.0   0.0   1.6   2   1   0   2   2   1   1 sulfate-transporting ATPase
PROKKA_00659         -          ADENOSINETRIPHOSPHATASE-RXN -            1.2e-05   22.1   0.0   1.2e-05   22.1   0.0   1.1   1   0   0   1   1   1   1 phospholipid/cholesterol/gamma-HCH transport system ATP-binding protein
PROKKA_00785         -          ADENOSINETRIPHOSPHATASE-RXN -            1.5e-05   21.8   0.0   2.9e-05   20.9   0.0   1.5   1   1   0   1   1   1   1 ATP-dependent Clp protease ATP-binding subunit ClpC
PROKKA_01867         -          ADENOSINETRIPHOSPHATASE-RXN -            2.2e-05   21.2   0.0   3.2e-05   20.7   0.0   1.2   1   0   0   1   1   1   1 two-component system, NtrC family, nitrogen regulation response regulator GlnG
PROKKA_00097         -          ADENOSINETRIPHOSPHATASE-RXN -            3.4e-05   20.6   0.0   3.5e-05   20.6   0.0   1.1   1   0   0   1   1   1   1 peptide/nickel transport system ATP-binding protein
PROKKA_00524         -          ADENOSINETRIPHOSPHATASE-RXN -            5.8e-05   19.9   0.0   8.1e-05   19.4   0.0   1.2   1   0   0   1   1   1   1 hypothetical protein
PROKKA_00980         -          ADENOSINETRIPHOSPHATASE-RXN -              7e-05   19.6   0.0   8.6e-05   19.3   0.0   1.1   1   0   0   1   1   1   1 type IV secretion system protein VirB11
PROKKA_00220         -          ADENOSINETRIPHOSPHATASE-RXN -            8.4e-05   19.3   0.0   0.00011   18.9   0.0   1.4   1   1   0   1   1   1   1 flagellar biosynthesis protein FlhF
PROKKA_01614         -          ADENOSINETRIPHOSPHATASE-RXN -            0.00011   18.9   0.0   0.00022   17.9   0.0   1.3   1   1   0   1   1   1   1 ABC-2 type transport system ATP-binding protein
PROKKA_01471         -          ADENOSINETRIPHOSPHATASE-RXN -            0.00013   18.7   0.0   0.00013   18.7   0.0   1.2   1   0   0   1   1   1   1 MoxR-like ATPase
PROKKA_00096         -          ADENOSINETRIPHOSPHATASE-RXN -            0.00016   18.4   0.0   0.00018   18.2   0.0   1.2   1   0   0   1   1   1   1 peptide/nickel transport system ATP-binding protein
PROKKA_02286         -          ADENOSINETRIPHOSPHATASE-RXN -            0.00018   18.3   0.0   0.00022   17.9   0.0   1.4   1   1   0   1   1   1   1 phosphate ABC transporter ATP-binding protein, PhoT family
PROKKA_00841         -          ADENOSINETRIPHOSPHATASE-RXN -            0.00019   18.2   1.2   0.00031   17.5   1.2   1.6   1   1   0   1   1   1   1 signal recognition particle subunit FFH/SRP54 (srp54)
PROKKA_00438         -          ADENOSINETRIPHOSPHATASE-RXN -            0.00066   16.4   0.0   0.00087   16.0   0.0   1.2   1   0   0   1   1   1   1 Transcriptional regulator containing PAS, AAA-type ATPase, and DNA-binding Fis domains
PROKKA_00534         -          ADENOSINETRIPHOSPHATASE-RXN -            0.00081   16.1   0.0   0.00098   15.8   0.0   1.2   1   0   0   1   1   1   1 two-component system, NtrC family, response regulator
PROKKA_00675         -          ADENOSINETRIPHOSPHATASE-RXN -              0.001   15.8   0.0    0.0035   14.0   0.0   1.5   1   1   0   1   1   1   1 Recombination protein MgsA
PROKKA_01586         -          ADENOSINETRIPHOSPHATASE-RXN -             0.0014   15.3   0.0    0.0028   14.3   0.0   1.3   1   1   0   1   1   1   1 guanylate kinase
PROKKA_01501         -          ADENOSINETRIPHOSPHATASE-RXN -             0.0015   15.2   0.0    0.0015   15.2   0.0   1.2   1   0   0   1   1   1   1 phospholipid/cholesterol/gamma-HCH transport system ATP-binding protein
PROKKA_01299         -          ADENOSINETRIPHOSPHATASE-RXN -             0.0017   15.0   0.0     0.002   14.8   0.0   1.4   1   0   0   1   1   1   1 ATP-binding cassette, subfamily B, MsbA
PROKKA_01792         -          ADENOSINETRIPHOSPHATASE-RXN -             0.0018   15.0   0.0    0.0018   14.9   0.0   1.1   1   0   0   1   1   1   1 hypothetical protein
PROKKA_00723         -          ADENOSINETRIPHOSPHATASE-RXN -             0.0018   14.9   0.0    0.0029   14.2   0.0   1.5   2   0   0   2   2   1   1 type II secretion system protein E (GspE)
PROKKA_00250         -          ADENOSINETRIPHOSPHATASE-RXN -             0.0019   14.9   0.0     0.002   14.8   0.0   1.2   1   1   0   1   1   1   1 NitT/TauT family transport system ATP-binding protein
PROKKA_00399         -          ADENOSINETRIPHOSPHATASE-RXN -             0.0025   14.5   0.0    0.0031   14.2   0.0   1.3   1   0   0   1   1   1   1 GTP-binding protein HflX
PROKKA_00393         -          ADENOSINETRIPHOSPHATASE-RXN -             0.0029   14.3   0.0    0.0048   13.5   0.0   1.3   1   1   0   1   1   1   1 MoxR-like ATPase
PROKKA_01537         -          ADENOSINETRIPHOSPHATASE-RXN -             0.0043   13.7   0.0    0.0043   13.7   0.0   1.2   1   0   0   1   1   1   1 cytidylate kinase
PROKKA_01192         -          ADENOSINETRIPHOSPHATASE-RXN -             0.0051   13.5   0.0    0.0051   13.5   0.0   1.1   1   0   0   1   1   1   1 thiamine transport system ATP-binding protein
PROKKA_01789         -          ADENOSINETRIPHOSPHATASE-RXN -             0.0053   13.4   0.0     0.012   12.3   0.0   1.4   1   1   0   1   1   1   1 DNA replication protein DnaC
PROKKA_00885         -          ADENOSINETRIPHOSPHATASE-RXN -             0.0053   13.4   0.0    0.0053   13.4   0.0   1.2   1   0   0   1   1   1   1 shikimate kinase
PROKKA_01021         -          ADENOSINETRIPHOSPHATASE-RXN -              0.007   13.0   0.0    0.0087   12.7   0.0   1.3   1   1   0   1   1   1   1 exodeoxyribonuclease-5
PROKKA_01802         -          ADENOSINETRIPHOSPHATASE-RXN -             0.0086   12.7   0.0    0.0086   12.7   0.0   1.2   1   0   0   1   1   1   1 DNA replication protein DnaC
PROKKA_02468         -          ADENOSINETRIPHOSPHATASE-RXN -              0.011   12.4   0.0     0.011   12.4   0.0   1.2   1   0   0   1   1   1   0 GTP-binding protein
PROKKA_00789         -          ADENOSINETRIPHOSPHATASE-RXN -              0.011   12.3   0.0     0.013   12.2   0.0   1.2   1   0   0   1   1   1   0 fused signal recognition particle receptor
PROKKA_02275         -          ADENOSINETRIPHOSPHATASE-RXN -              0.014   12.0   0.1     0.021   11.4   0.1   1.3   1   0   0   1   1   1   0 hypothetical protein
PROKKA_01938         -          ADENOSINETRIPHOSPHATASE-RXN -              0.022   11.3   0.0     0.042   10.4   0.0   1.3   1   1   0   1   1   1   0 ABC-2 type transport system ATP-binding protein
PROKKA_00911         -          ADENOSINETRIPHOSPHATASE-RXN -              0.023   11.3   0.0     0.025   11.2   0.0   1.2   1   0   0   1   1   1   0 hypothetical protein
PROKKA_01643         -          ADENOSINETRIPHOSPHATASE-RXN -              0.024   11.2   0.1     0.035   10.7   0.1   1.4   1   1   0   1   1   1   0 Adenylate kinase
PROKKA_01812         -          ADENOSINETRIPHOSPHATASE-RXN -              0.026   11.2   0.0     0.027   11.1   0.0   1.3   1   1   0   1   1   1   0 lipopolysaccharide transport system ATP-binding protein
PROKKA_00196         -          ADENOSINETRIPHOSPHATASE-RXN -              0.029   11.0   0.0      0.03   10.9   0.0   1.2   1   0   0   1   1   1   0 sigma-54 specific transcriptional regulator, flagellar regulatory protein A
PROKKA_02180         -          ADENOSINETRIPHOSPHATASE-RXN -               0.03   10.9   0.0     0.057   10.0   0.0   1.4   1   1   0   1   1   1   0 DNA repair protein RadA/Sms
PROKKA_00879         -          ADENOSINETRIPHOSPHATASE-RXN -              0.035   10.7   0.3     0.053   10.1   0.2   1.5   1   1   0   1   1   1   0 lipopolysaccharide export system ATP-binding protein
PROKKA_00645         -          ADENOSYLHOMOCYSTEINASE-RXN -             4e-163  540.1   0.1  4.5e-163  540.0   0.1   1.0   1   0   0   1   1   1   1 adenosylhomocysteinase
PROKKA_02540         -          ADENOSYLHOMOCYSTEINASE-RXN -             0.0056   12.4   0.0     0.013   11.2   0.0   1.5   2   0   0   2   2   2   1 ketol-acid reductoisomerase
PROKKA_01511         -          ADENOSYLHOMOCYSTEINASE-RXN -             0.0074   12.0   1.0     0.023   10.4   0.2   2.0   2   0   0   2   2   2   1 D-3-phosphoglycerate dehydrogenase
PROKKA_01553         -          ADENPRIBOSYLTRAN-RXN -            3.7e-63  209.5   0.0   6.1e-63  208.8   0.0   1.3   1   0   0   1   1   1   1 adenine phosphoribosyltransferase
PROKKA_00110         -          ADENPRIBOSYLTRAN-RXN -            2.4e-05   21.2   0.0   3.7e-05   20.6   0.0   1.2   1   0   0   1   1   1   1 putative amidophosphoribosyltransferases
PROKKA_00049         -          ADENPRIBOSYLTRAN-RXN -             0.0013   15.6   0.0    0.0018   15.1   0.0   1.2   1   0   0   1   1   1   1 orotate phosphoribosyltransferase
PROKKA_01921         -          ADENPRIBOSYLTRAN-RXN -             0.0015   15.4   0.0    0.0027   14.5   0.0   1.4   1   0   0   1   1   1   1 pyrimidine operon attenuation protein / uracil phosphoribosyltransferase
PROKKA_00407         -          ADENPRIBOSYLTRAN-RXN -             0.0039   14.0   0.1     0.065   10.0   0.1   2.1   2   0   0   2   2   2   1 putative phosphoribosyltransferase
PROKKA_01643         -          ADENYL-KIN-RXN       -            8.3e-75  248.3   0.0   9.1e-75  248.1   0.0   1.0   1   0   0   1   1   1   1 Adenylate kinase
PROKKA_01586         -          ADENYL-KIN-RXN       -              0.001   15.9   0.1      0.46    7.2   0.1   2.2   2   0   0   2   2   2   2 guanylate kinase
PROKKA_01998         -          ADENYL-KIN-RXN       -              0.001   15.9   0.1    0.0025   14.6   0.1   1.6   1   0   0   1   1   1   1 cell division protease FtsH
PROKKA_00290         -          ADENYL-KIN-RXN       -              0.001   15.9   0.0    0.0018   15.1   0.0   1.3   1   0   0   1   1   1   1 Holliday junction DNA helicase subunit RuvB
PROKKA_02142         -          ADENYL-KIN-RXN       -             0.0011   15.8   0.0      0.27    8.0   0.0   2.2   2   0   0   2   2   2   2 GTP-binding protein
PROKKA_02125         -          ADENYL-KIN-RXN       -             0.0018   15.1   0.0     0.013   12.3   0.0   2.0   1   1   0   1   1   1   1 thymidylate kinase
PROKKA_01889         -          ADENYL-KIN-RXN       -             0.0019   15.0   0.1    0.0049   13.6   0.1   1.6   1   0   0   1   1   1   1 ATP-dependent Lon protease
PROKKA_00841         -          ADENYL-KIN-RXN       -             0.0032   14.2   0.2     0.013   12.2   0.0   2.0   2   0   0   2   2   2   1 signal recognition particle subunit FFH/SRP54 (srp54)
PROKKA_00789         -          ADENYL-KIN-RXN       -             0.0041   13.9   0.0    0.0062   13.3   0.0   1.3   1   0   0   1   1   1   1 fused signal recognition particle receptor
PROKKA_01537         -          ADENYL-KIN-RXN       -             0.0057   13.4   0.0    0.0076   13.0   0.0   1.3   1   0   0   1   1   1   1 cytidylate kinase
PROKKA_00335         -          ADENYL-KIN-RXN       -             0.0079   13.0   0.4     0.079    9.7   0.1   2.3   2   0   0   2   2   2   1 proteasome-associated ATPase
PROKKA_00980         -          ADENYL-KIN-RXN       -              0.012   12.4   0.1     0.021   11.5   0.1   1.3   1   0   0   1   1   1   0 type IV secretion system protein VirB11
PROKKA_00096         -          ADENYL-KIN-RXN       -              0.014   12.1   0.1     0.022   11.5   0.1   1.2   1   0   0   1   1   1   0 peptide/nickel transport system ATP-binding protein
PROKKA_02369         -          ADENYL-KIN-RXN       -              0.016   12.0   0.0     0.042   10.6   0.0   1.7   2   0   0   2   2   2   0 dTMP kinase
PROKKA_02248         -          ADENYL-KIN-RXN       -               0.92    6.2   6.2       1.9    5.1   0.1   3.1   4   0   0   4   4   4   0 ATP-binding cassette, subfamily F, member 3
PROKKA_01492         -          ADENYLATECYC-RXN     -            6.9e-24   81.2   0.0   7.2e-24   81.1   0.0   1.1   1   0   0   1   1   1   1 adenylate cyclase
PROKKA_02295         -          ADENYLOSUCCINATE-SYNTHASE-RXN -           1.2e-172  571.4   0.0  1.5e-172  571.2   0.0   1.0   1   0   0   1   1   1   1 Adenylosuccinate synthetase
PROKKA_01842         -          ADENYLOSUCCINATE-SYNTHASE-RXN -               0.71    5.6   4.6      0.42    6.3   2.8   1.5   1   1   0   1   1   1   0 hypothetical protein
PROKKA_00427         -          ADENYLYLSULFATE-REDUCTASE-RXN -           1.2e-113  377.9   0.1  6.8e-113  375.4   0.1   2.1   1   1   0   1   1   1   1 dissimilatory adenylylsulfate reductase alpha subunit precursor
PROKKA_02443         -          ADENYLYLSULFATE-REDUCTASE-RXN -             0.0015   14.1   0.1      0.23    6.9   0.0   2.4   3   0   0   3   3   3   2 L-aspartate oxidase
PROKKA_00585         -          ADENYLYLSULFATE-REDUCTASE-RXN -             0.0082   11.7   0.9      0.43    6.0   0.0   2.5   3   0   0   3   3   3   2 succinate dehydrogenase / fumarate reductase flavoprotein subunit
PROKKA_00428         -          ADENYLYLSULFATE-REDUCTASE-RXN -              0.012   11.2   0.1     0.015   10.8   0.1   1.1   1   0   0   1   1   1   0 adenylylsulfate reductase subunit B
PROKKA_00841         -          ADENYLYLSULFKIN-RXN  -            1.8e-05   21.3   0.1   8.2e-05   19.1   0.0   2.0   2   0   0   2   2   2   1 signal recognition particle subunit FFH/SRP54 (srp54)
PROKKA_00666         -          ADENYLYLSULFKIN-RXN  -            2.1e-05   21.1   0.0     0.011   12.1   0.0   2.4   2   0   0   2   2   2   2 Excinuclease ABC subunit A
PROKKA_00552         -          ADENYLYLSULFKIN-RXN  -            0.00032   17.1   0.0   0.00041   16.8   0.0   1.3   1   0   0   1   1   1   1 tyrosine-protein kinase Etk/Wzc
PROKKA_01062         -          ADENYLYLSULFKIN-RXN  -            0.00049   16.5   0.1    0.0038   13.6   0.1   2.0   1   1   0   1   1   1   1 dTMP kinase
PROKKA_00789         -          ADENYLYLSULFKIN-RXN  -             0.0066   12.8   0.1     0.012   12.0   0.1   1.3   1   0   0   1   1   1   1 fused signal recognition particle receptor
PROKKA_00413         -          ADENYLYLSULFKIN-RXN  -             0.0086   12.5   0.0     0.011   12.1   0.0   1.1   1   0   0   1   1   1   1 ATP-binding protein involved in chromosome partitioning
PROKKA_01339         -          ADENYLYLSULFKIN-RXN  -              0.013   11.8   0.0      0.02   11.2   0.0   1.2   1   0   0   1   1   1   0 lipoprotein-releasing system ATP-binding protein
PROKKA_02190         -          ADENYLYLSULFKIN-RXN  -              0.029   10.7   0.1     0.045   10.1   0.1   1.2   1   0   0   1   1   1   0 chromosome partitioning protein
PROKKA_01372         -          AICARSYN-RXN         -           1.5e-124  413.1   0.0  1.9e-124  412.8   0.0   1.0   1   0   0   1   1   1   1 Adenylosuccinate lyase 
PROKKA_01556         -          AICARSYN-RXN         -            3.9e-16   55.8   0.0   5.5e-16   55.3   0.0   1.2   1   0   0   1   1   1   1 fumarase, class II
PROKKA_02355         -          AICARSYN-RXN         -            3.9e-15   52.4   0.0   5.4e-15   52.0   0.0   1.2   1   0   0   1   1   1   1 argininosuccinate lyase 
PROKKA_01332         -          AICARTRANSFORM-RXN   -           2.6e-206  683.5   0.0  2.9e-206  683.4   0.0   1.0   1   0   0   1   1   1   1 phosphoribosylaminoimidazolecarboxamide formyltransferase / IMP cyclohydrolase
PROKKA_01916         -          AICARTRANSFORM-RXN   -            3.9e-05   19.8   0.0   6.3e-05   19.1   0.0   1.2   1   0   0   1   1   1   1 carbamoyl-phosphate synthase large subunit
PROKKA_01488         -          AIRCARBOXY-RXN       -            4.9e-66  220.4   0.1   6.1e-66  220.1   0.1   1.0   1   0   0   1   1   1   1 5-(carboxyamino)imidazole ribonucleotide mutase
PROKKA_01489         -          AIRCARBOXY-RXN       -            5.9e-38  128.0   0.0   7.9e-38  127.6   0.0   1.0   1   0   0   1   1   1   1 5-(carboxyamino)imidazole ribonucleotide synthase
PROKKA_02154         -          AIRS-RXN             -           9.5e-127  421.4   0.0  1.2e-126  421.1   0.0   1.0   1   0   0   1   1   1   1 phosphoribosylformylglycinamidine cyclo-ligase
PROKKA_01333         -          AIRS-RXN             -            7.8e-95  315.8   0.0   9.6e-95  315.5   0.0   1.0   1   0   0   1   1   1   1 phosphoribosylamine--glycine ligase
PROKKA_01626         -          AIRS-RXN             -            3.9e-06   22.5   0.6   0.00011   17.7   0.1   2.1   2   0   0   2   2   2   2 phosphoribosylformylglycinamidine synthase subunit II 
PROKKA_01593         -          AIRS-RXN             -            0.00011   17.7   0.0   0.00015   17.3   0.0   1.1   1   0   0   1   1   1   1 acetyl-CoA carboxylase, biotin carboxylase subunit
PROKKA_00123         -          AIRS-RXN             -            0.00022   16.7   0.0    0.0005   15.5   0.0   1.5   2   0   0   2   2   2   1 thiamine-monophosphate kinase
PROKKA_01580         -          AKBLIG-RXN           -            2.3e-80  267.4   0.0   2.9e-80  267.1   0.0   1.0   1   0   0   1   1   1   1 8-amino-7-oxononanoate synthase
PROKKA_02373         -          AKBLIG-RXN           -            2.1e-07   27.2   0.0   3.2e-07   26.6   0.0   1.2   1   0   0   1   1   1   1 glycine hydroxymethyltransferase
PROKKA_01174         -          AKBLIG-RXN           -             0.0078   12.2   0.0     0.013   11.4   0.0   1.3   1   0   0   1   1   1   1 cysteine desulfurase
PROKKA_01512         -          ALANINE--GLYOXYLATE-AMINOTRANSFERASE-RXN -            9.3e-78  259.0   0.0   1.1e-77  258.8   0.0   1.0   1   0   0   1   1   1   1 aspartate aminotransferase
PROKKA_01174         -          ALANINE--GLYOXYLATE-AMINOTRANSFERASE-RXN -            1.3e-10   37.9   0.0   1.9e-10   37.4   0.0   1.1   1   0   0   1   1   1   1 cysteine desulfurase
PROKKA_00421         -          ALANINE--GLYOXYLATE-AMINOTRANSFERASE-RXN -            0.00041   16.6   0.0    0.0006   16.0   0.0   1.1   1   0   0   1   1   1   1 cysteine desulfurase
PROKKA_00190         -          ALANINE--GLYOXYLATE-AMINOTRANSFERASE-RXN -             0.0044   13.2   0.0    0.0066   12.6   0.0   1.1   1   0   0   1   1   1   1 glycine dehydrogenase (decarboxylating) beta subunit 
PROKKA_00468         -          ALANINE--GLYOXYLATE-AMINOTRANSFERASE-RXN -              0.014   11.5   0.0     0.019   11.1   0.0   1.1   1   0   0   1   1   1   0 cysteine desulfurase
PROKKA_00863         -          ALANINE--TRNA-LIGASE-RXN -           2.5e-288  956.1   0.0    3e-288  955.9   0.0   1.0   1   0   0   1   1   1   1 alanyl-tRNA synthetase
PROKKA_00090         -          ALANINE--TRNA-LIGASE-RXN -            3.1e-11   39.4   0.0   4.3e-11   38.9   0.0   1.1   1   0   0   1   1   1   1 threonyl-tRNA synthetase
PROKKA_01338         -          ALANINE-AMINOTRANSFERASE-RXN -            8.8e-23   77.8   0.0   2.1e-15   53.5   0.0   2.1   2   0   0   2   2   2   2 aspartate aminotransferase
PROKKA_01352         -          ALANINE-AMINOTRANSFERASE-RXN -            1.3e-14   50.9   0.0   1.6e-11   40.7   0.0   2.1   2   0   0   2   2   2   2 alanine-synthesizing transaminase
PROKKA_00241         -          ALANINE-AMINOTRANSFERASE-RXN -            2.7e-11   40.0   0.0   2.3e-07   27.0   0.0   2.1   2   0   0   2   2   2   2 LL-diaminopimelate aminotransferase apoenzyme
PROKKA_01558         -          ALANINE-AMINOTRANSFERASE-RXN -             0.0034   13.2   0.0    0.0052   12.6   0.0   1.2   1   0   0   1   1   1   1 L-threonine O-3-phosphate decarboxylase 
PROKKA_01909         -          ALANINE-DEHYDROGENASE-RXN -            0.00062   15.9   0.0      0.27    7.2   0.0   2.2   2   0   0   2   2   2   2 dihydrolipoamide dehydrogenase
PROKKA_00598         -          ALANINE-DEHYDROGENASE-RXN -             0.0052   12.8   0.0    0.0099   11.9   0.0   1.4   2   0   0   2   2   2   1 UDP-N-acetylmuramoylalanine--D-glutamate ligase
PROKKA_00507         -          ALANINE-DEHYDROGENASE-RXN -             0.0054   12.8   0.1    0.0095   12.0   0.1   1.3   1   0   0   1   1   1   1 putative zinc-type alcohol dehydrogenase-like protein
PROKKA_00876         -          ALANINE-DEHYDROGENASE-RXN -               0.01   11.8   0.2     0.016   11.2   0.2   1.3   1   0   0   1   1   1   0 malate dehydrogenase (NAD)
PROKKA_02501         -          ALANINE-DEHYDROGENASE-RXN -              0.016   11.2   0.1     0.025   10.6   0.1   1.2   1   0   0   1   1   1   0 tRNA uridine 5-carboxymethylaminomethyl modification enzyme
PROKKA_01451         -          ALANINE-DEHYDROGENASE-RXN -              0.019   11.0   0.1      0.59    6.1   0.1   2.2   2   0   0   2   2   2   0 NADH-quinone oxidoreductase subunit F
PROKKA_01201         -          ALANINE-DEHYDROGENASE-RXN -              0.021   10.8   0.5     0.035   10.1   0.5   1.3   1   0   0   1   1   1   0 nitrite reductase (NADH) large subunit
PROKKA_02386         -          ALARACECAT-RXN       -            8.7e-77  255.6   0.0   1.4e-76  254.9   0.0   1.2   1   1   0   1   1   1   1 alanine racemase
PROKKA_00507         -          ALCOHOL-DEHYDROG-GENERIC-RXN -            1.4e-47  160.0   0.0   1.7e-47  159.7   0.0   1.0   1   0   0   1   1   1   1 putative zinc-type alcohol dehydrogenase-like protein
PROKKA_02069         -          ALCOHOL-DEHYDROG-GENERIC-RXN -              4e-45  151.9   0.0   4.5e-45  151.8   0.0   1.0   1   0   0   1   1   1   1 alcohol dehydrogenase, propanol-preferring
PROKKA_00329         -          ALCOHOL-DEHYDROG-GENERIC-RXN -            1.1e-28   97.9   0.0   1.4e-28   97.5   0.0   1.2   1   0   0   1   1   1   1 alcohol dehydrogenase, propanol-preferring
PROKKA_00022         -          ALCOHOL-DEHYDROG-GENERIC-RXN -            7.1e-28   95.2   0.0   6.8e-26   88.7   0.0   2.0   1   1   0   1   1   1   1 NADPH:quinone reductase
PROKKA_01717         -          ALCOHOL-DEHYDROG-GENERIC-RXN -            2.5e-18   63.8   0.0     3e-10   37.2   0.0   2.1   1   1   1   2   2   2   2 NADPH2:quinone reductase
PROKKA_01239         -          ALCOHOL-DEHYDROG-GENERIC-RXN -            3.7e-18   63.2   0.0   7.4e-15   52.4   0.0   2.0   1   1   0   2   2   2   2 NADPH2:quinone reductase
PROKKA_00507         -          ALCOHOL-DEHYDROGENASE-NADPORNOP+-RXN -           9.5e-115  380.7   0.1    1e-114  380.5   0.1   1.0   1   0   0   1   1   1   1 putative zinc-type alcohol dehydrogenase-like protein
PROKKA_02069         -          ALCOHOL-DEHYDROGENASE-NADPORNOP+-RXN -            4.1e-55  184.6   0.0   4.5e-55  184.4   0.0   1.0   1   0   0   1   1   1   1 alcohol dehydrogenase, propanol-preferring
PROKKA_00329         -          ALCOHOL-DEHYDROGENASE-NADPORNOP+-RXN -            1.9e-36  123.2   0.0     6e-36  121.5   0.0   1.6   2   0   0   2   2   2   1 alcohol dehydrogenase, propanol-preferring
PROKKA_00022         -          ALCOHOL-DEHYDROGENASE-NADPORNOP+-RXN -            2.3e-26   90.0   0.0   3.6e-26   89.4   0.0   1.2   1   0   0   1   1   1   1 NADPH:quinone reductase
PROKKA_01717         -          ALCOHOL-DEHYDROGENASE-NADPORNOP+-RXN -            1.6e-17   60.9   0.0   5.7e-11   39.4   0.0   2.1   1   1   1   2   2   2   2 NADPH2:quinone reductase
PROKKA_01239         -          ALCOHOL-DEHYDROGENASE-NADPORNOP+-RXN -            1.5e-11   41.3   0.2   2.2e-09   34.2   0.0   2.2   1   1   1   2   2   2   2 NADPH2:quinone reductase
PROKKA_00565         -          ALCOHOL-OXIDASE-RXN  -              0.023   10.4   0.1     0.031    9.9   0.1   1.1   1   0   0   1   1   1   0 UDP-galactopyranose mutase
PROKKA_02282         -          ALDEHYDE-DEHYDROGENASE-NADORNOP+-RXN -            3.8e-70  234.1   0.0   4.5e-70  233.9   0.0   1.0   1   0   0   1   1   1   1 Acyl-CoA reductase
PROKKA_00106         -          ALDEHYDE-DEHYDROGENASE-NADORNOP+-RXN -            5.2e-10   35.7   0.0   4.9e-05   19.3   0.0   2.1   1   1   1   2   2   2   2 glutamate-5-semialdehyde dehydrogenase
PROKKA_02410         -          ALDEHYDE-DEHYDROGENASE-NADP+-RXN -            9.9e-15   50.8   0.0   1.3e-14   50.4   0.0   1.2   1   0   0   1   1   1   1 methionyl-tRNA formyltransferase
PROKKA_02153         -          ALDEHYDE-DEHYDROGENASE-NADP+-RXN -            5.2e-09   31.9   0.0   5.6e-09   31.8   0.0   1.1   1   0   0   1   1   1   1 phosphoribosylglycinamide formyltransferase-1
PROKKA_00961         -          ALDEHYDE-REDUCTASE-RXN -            9.6e-16   54.9   0.0   8.7e-14   48.5   0.0   2.7   1   1   0   1   1   1   1 putative oxidoreductase
PROKKA_01736         -          ALDEHYDE-REDUCTASE-RXN -            1.3e-14   51.2   0.0   7.6e-10   35.5   0.0   3.0   2   1   0   2   2   2   2 putative oxidoreductase
PROKKA_01886         -          ALDEHYDE-REDUCTASE-RXN -            8.2e-13   45.3   0.0   1.9e-07   27.6   0.0   3.5   2   1   0   2   2   2   2 putative oxidoreductase
PROKKA_02282         -          ALDHDEHYDROG-RXN     -            2.3e-71  238.0   0.0   2.9e-71  237.7   0.0   1.0   1   0   0   1   1   1   1 Acyl-CoA reductase
PROKKA_00106         -          ALDHDEHYDROG-RXN     -            6.7e-10   35.3   0.0   5.5e-05   19.1   0.0   2.6   2   1   0   2   2   2   2 glutamate-5-semialdehyde dehydrogenase
PROKKA_01549         -          ALKYLGLYCERONE-PHOSPHATE-SYNTHASE-RXN -              1e-11   41.0   0.0   6.5e-07   25.1   0.0   3.0   3   0   0   3   3   3   3 FAD/FMN-containing dehydrogenase
PROKKA_02377         -          ALKYLHALIDASE-RXN    -            6.5e-12   41.6   0.0   0.00019   17.0   0.1   3.0   3   0   0   3   3   3   3 geranylgeranyl reductase family protein
PROKKA_01702         -          ALKYLHALIDASE-RXN    -            6.3e-05   18.6   0.0   0.00024   16.7   0.0   1.8   2   0   0   2   2   2   1 2-polyprenyl-6-methoxyphenol hydroxylase
PROKKA_00565         -          ALKYLHALIDASE-RXN    -            0.00042   15.9   0.1   0.00062   15.3   0.1   1.2   1   0   0   1   1   1   1 UDP-galactopyranose mutase
PROKKA_00735         -          ALKYLHALIDASE-RXN    -              0.002   13.6   1.1    0.0029   13.1   1.1   1.1   1   0   0   1   1   1   1 C-3',4' desaturase CrtD
PROKKA_00137         -          ALKYLHALIDASE-RXN    -              0.016   10.7   2.7     0.049    9.1   0.2   2.3   2   1   1   3   3   3   0 dihydrolipoamide dehydrogenase
PROKKA_01451         -          ALKYLHALIDASE-RXN    -               0.03    9.8   0.0     0.044    9.2   0.0   1.1   1   0   0   1   1   1   0 NADH-quinone oxidoreductase subunit F
PROKKA_01529         -          ALKYLHALIDASE-RXN    -               0.03    9.7   0.0     0.044    9.2   0.0   1.1   1   0   0   1   1   1   0 glycine oxidase
PROKKA_01919         -          ALLANTOINASE-RXN     -              7e-45  150.8   0.0   1.1e-44  150.1   0.0   1.1   1   0   0   1   1   1   1 dihydroorotase
PROKKA_02101         -          AMIDASE-RXN          -            8.1e-68  226.4   0.0     1e-67  226.1   0.0   1.0   1   0   0   1   1   1   1 aspartyl/glutamyl-tRNA(Asn/Gln) amidotransferase subunit A
PROKKA_02386         -          AMINO-ACID-RACEMASE-RXN -              0.013   13.3   0.0     0.037   11.9   0.0   1.8   1   0   0   1   1   1   0 alanine racemase
PROKKA_02282         -          AMINOBUTDEHYDROG-RXN -            3.6e-53  178.1   0.0   5.1e-53  177.6   0.0   1.1   1   0   0   1   1   1   1 Acyl-CoA reductase
PROKKA_01690         -          AMINOCYL-TRNA-HYDROLASE-RXN -            1.4e-45  152.6   0.0   1.7e-45  152.4   0.0   1.0   1   0   0   1   1   1   1 peptidyl-tRNA hydrolase, PTH1 family
PROKKA_01896         -          AMINOGLYCOSIDE-N6-ACETYLTRANSFERASE-RXN -             0.0033   14.4   0.0    0.0039   14.2   0.0   1.1   1   0   0   1   1   1   1 Acetyltransferase (GNAT) family protein
PROKKA_02178         -          AMINOGLYCOSIDE-N6-ACETYLTRANSFERASE-RXN -             0.0036   14.3   0.0    0.0053   13.7   0.0   1.1   1   0   0   1   1   1   1 ribosomal-protein-alanine N-acetyltransferase
PROKKA_02130         -          AMINOGLYCOSIDE-N6-ACETYLTRANSFERASE-RXN -             0.0054   13.7   0.0    0.0073   13.3   0.0   1.4   1   1   0   1   1   1   1 Ribosomal protein S18 acetylase RimI
PROKKA_00350         -          AMINOGLYCOSIDE-N6-ACETYLTRANSFERASE-RXN -             0.0071   13.3   0.0    0.0088   13.0   0.0   1.1   1   0   0   1   1   1   1 L-2,4-diaminobutyric acid acetyltransferase
PROKKA_01270         -          AMINOGLYCOSIDE-N6-ACETYLTRANSFERASE-RXN -               0.02   11.9   0.0     0.021   11.8   0.0   1.1   1   0   0   1   1   1   0 Acetyltransferase (GNAT) domain-containing protein
PROKKA_01372         -          AMPSYN-RXN           -           6.3e-125  414.4   0.0  7.5e-125  414.1   0.0   1.0   1   0   0   1   1   1   1 Adenylosuccinate lyase 
PROKKA_01556         -          AMPSYN-RXN           -            1.3e-16   57.4   0.0   1.8e-16   56.9   0.0   1.2   1   0   0   1   1   1   1 fumarase, class II
PROKKA_02355         -          AMPSYN-RXN           -            1.4e-15   54.0   0.0     2e-15   53.5   0.0   1.2   1   0   0   1   1   1   1 argininosuccinate lyase 
PROKKA_01955         -          ANTHRANSYN-RXN       -            1.1e-77  259.2   0.0   1.3e-77  258.9   0.0   1.0   1   0   0   1   1   1   1 anthranilate synthase, component II 
PROKKA_01956         -          ANTHRANSYN-RXN       -            8.6e-60  200.3   0.0     1e-59  200.0   0.0   1.1   1   0   0   1   1   1   1 anthranilate synthase component 1
PROKKA_00363         -          ANTHRANSYN-RXN       -            2.1e-33  113.4   0.0   2.2e-33  113.3   0.0   1.1   1   0   0   1   1   1   1 anthranilate synthase component 1
PROKKA_01918         -          ANTHRANSYN-RXN       -            1.5e-14   51.3   0.0   2.1e-14   50.8   0.0   1.1   1   0   0   1   1   1   1 carbamoyl-phosphate synthase small subunit
PROKKA_01902         -          ANTHRANSYN-RXN       -            1.1e-09   35.3   0.0   1.7e-09   34.6   0.0   1.4   1   0   0   1   1   1   1 GMP synthase (glutamine-hydrolysing)
PROKKA_01953         -          ANTHRANSYN-RXN       -              9e-05   19.1   0.0   0.00019   18.0   0.0   1.3   1   1   0   1   1   1   1 indole-3-glycerol phosphate synthase
PROKKA_01877         -          ANTHRANSYN-RXN       -               0.03   10.8   0.0     0.052   10.0   0.0   1.3   1   0   0   1   1   1   0 CTP synthase
PROKKA_02457         -          AQUACOBALAMIN-REDUCTASE-NADPH-RXN -            1.1e-12   43.0   0.3   2.5e-12   41.8   0.1   1.5   1   1   1   2   2   2   1 pyruvate ferredoxin oxidoreductase beta subunit
PROKKA_02452         -          AQUACOBALAMIN-REDUCTASE-NADPH-RXN -            3.1e-12   41.5   0.0   5.4e-12   40.7   0.0   1.3   2   0   0   2   2   2   1 pyruvate ferredoxin oxidoreductase beta subunit
PROKKA_02458         -          AQUACOBALAMIN-REDUCTASE-NADPH-RXN -            9.6e-12   39.9   0.0   1.2e-11   39.5   0.0   1.0   1   0   0   1   1   1   1 pyruvate ferredoxin oxidoreductase alpha subunit
PROKKA_02453         -          AQUACOBALAMIN-REDUCTASE-NADPH-RXN -              9e-08   26.7   0.0   1.7e-07   25.8   0.0   1.3   1   1   0   1   1   1   1 pyruvate ferredoxin oxidoreductase alpha subunit
PROKKA_00695         -          AQUACOBALAMIN-REDUCTASE-NADPH-RXN -             0.0075   10.4   1.6      0.15    6.1   2.2   1.9   1   1   1   2   2   2   2 NADH-quinone oxidoreductase subunit I
PROKKA_01182         -          AQUACOBALAMIN-REDUCTASE-NADPH-RXN -              0.044    7.8   2.3     0.062    7.3   2.3   1.2   1   1   0   1   1   1   0 ferredoxin III, nif-specific
PROKKA_02354         -          ARGDECARBOX-RXN      -            2.6e-10   36.6   0.0   4.5e-10   35.8   0.0   1.3   1   0   0   1   1   1   1 diaminopimelate decarboxylase
PROKKA_00929         -          ARGININE--TRNA-LIGASE-RXN -           2.4e-134  446.1   0.0  6.5e-134  444.7   0.0   1.7   1   1   0   1   1   1   1 arginyl-tRNA synthetase
PROKKA_00378         -          ARGININE--TRNA-LIGASE-RXN -              1e-08   31.3   0.0   7.1e-05   18.6   0.0   2.2   2   0   0   2   2   2   2 leucyl-tRNA synthetase
PROKKA_01364         -          ARGININE--TRNA-LIGASE-RXN -            0.00035   16.3   0.0     0.039    9.6   0.4   2.3   1   1   1   2   2   2   2 cysteinyl-tRNA synthetase
PROKKA_00867         -          ARGININE--TRNA-LIGASE-RXN -             0.0027   13.4   0.1    0.0094   11.6   0.0   1.7   2   0   0   2   2   2   1 valyl-tRNA synthetase
PROKKA_02355         -          ARGSUCCINLYA-RXN     -             3e-176  584.0   0.0  4.1e-176  583.6   0.0   1.0   1   0   0   1   1   1   1 argininosuccinate lyase 
PROKKA_01372         -          ARGSUCCINLYA-RXN     -            5.6e-20   68.8   0.0   9.4e-20   68.1   0.0   1.2   1   1   0   1   1   1   1 Adenylosuccinate lyase 
PROKKA_01556         -          ARGSUCCINLYA-RXN     -            4.6e-09   32.8   0.0   9.1e-09   31.8   0.0   1.3   1   1   0   1   1   1   1 fumarase, class II
PROKKA_02356         -          ARGSUCCINSYN-RXN     -           4.9e-158  523.9   0.0  6.3e-158  523.5   0.0   1.0   1   0   0   1   1   1   1 argininosuccinate synthase
PROKKA_02496         -          ARGSUCCINSYN-RXN     -             0.0098   12.4   0.2     0.086    9.3   0.0   2.1   2   0   0   2   2   2   1 tRNA-specific 2-thiouridylase
PROKKA_00495         -          AROMATIC-L-AMINO-ACID-DECARBOXYLASE-RXN -              0.001   14.9   0.0    0.0014   14.5   0.0   1.3   1   0   0   1   1   1   1 glutamate decarboxylase
PROKKA_00421         -          AROMATIC-L-AMINO-ACID-DECARBOXYLASE-RXN -              0.011   11.6   0.0     0.015   11.1   0.0   1.2   1   0   0   1   1   1   0 cysteine desulfurase
PROKKA_01736         -          ARYL-ALCOHOL-DEHYDROGENASE-NADP+-RXN -            2.8e-46  155.2   0.0   3.5e-46  154.8   0.0   1.1   1   0   0   1   1   1   1 putative oxidoreductase
PROKKA_01886         -          ARYL-ALCOHOL-DEHYDROGENASE-NADP+-RXN -              3e-31  105.7   0.0   3.6e-31  105.4   0.0   1.0   1   0   0   1   1   1   1 putative oxidoreductase
PROKKA_00961         -          ARYL-ALCOHOL-DEHYDROGENASE-NADP+-RXN -            4.9e-20   68.8   0.0   7.5e-20   68.2   0.0   1.2   1   0   0   1   1   1   1 putative oxidoreductase
PROKKA_01498         -          ARYL-ALCOHOL-DEHYDROGENASE-NADP+-RXN -             0.0069   12.4   0.0    0.0069   12.4   0.0   1.0   1   0   0   1   1   1   1 Aldo/keto reductase family protein
PROKKA_02069         -          ARYL-ALCOHOL-DEHYDROGENASE-RXN -            1.4e-22   77.6   6.7   1.3e-14   51.3   1.3   2.0   1   1   1   2   2   2   2 alcohol dehydrogenase, propanol-preferring
PROKKA_00022         -          ARYL-ALCOHOL-DEHYDROGENASE-RXN -            4.4e-22   75.9   0.0   1.5e-12   44.6   0.0   2.0   2   0   0   2   2   2   2 NADPH:quinone reductase
PROKKA_01717         -          ARYL-ALCOHOL-DEHYDROGENASE-RXN -            1.7e-19   67.4   0.7   8.5e-13   45.4   0.1   2.1   1   1   1   2   2   2   2 NADPH2:quinone reductase
PROKKA_00507         -          ARYL-ALCOHOL-DEHYDROGENASE-RXN -            3.6e-16   56.5   2.5   3.8e-15   53.1   2.5   1.9   1   1   0   1   1   1   1 putative zinc-type alcohol dehydrogenase-like protein
PROKKA_00329         -          ARYL-ALCOHOL-DEHYDROGENASE-RXN -            3.1e-14   50.1   0.7   1.1e-13   48.4   0.7   1.7   2   0   0   2   2   2   1 alcohol dehydrogenase, propanol-preferring
PROKKA_01239         -          ARYL-ALCOHOL-DEHYDROGENASE-RXN -            4.2e-07   26.6   6.2   6.4e-06   22.8   2.4   2.0   1   1   1   2   2   2   2 NADPH2:quinone reductase
PROKKA_01494         -          ARYLDIALKYL-PHOSPHATASE-RXN -            0.00019   17.5   0.0      0.26    7.1   0.0   3.1   2   1   2   4   4   4   2 40-residue YVTN family beta-propeller repeat-containing protein
PROKKA_01494         -          ARYLDIALKYLPHOSPHATASE-RXN -            0.00019   17.5   0.0      0.26    7.1   0.0   3.1   2   1   2   4   4   4   2 40-residue YVTN family beta-propeller repeat-containing protein
PROKKA_01627         -          ASNSYNB-RXN          -            1.2e-19   67.5   0.0   1.7e-19   67.0   0.0   1.1   1   0   0   1   1   1   1 amidophosphoribosyltransferase
PROKKA_00569         -          ASNSYNB-RXN          -            4.1e-09   32.8   0.0   5.3e-09   32.4   0.0   1.1   1   0   0   1   1   1   1 glucosamine--fructose-6-phosphate aminotransferase (isomerizing)
PROKKA_01551         -          ASNSYNB-RXN          -             0.0055   12.5   0.0     0.012   11.4   0.0   1.4   2   0   0   2   2   2   1 glutamate synthase (NADPH/NADH) large chain
PROKKA_01367         -          ASNSYNB-RXN          -             0.0083   11.9   0.0     0.012   11.4   0.0   1.2   1   0   0   1   1   1   1 NAD+ synthase (glutamine-hydrolysing)
PROKKA_01338         -          ASPAMINOTRANS-RXN    -            8.8e-92  305.1   0.0   1.1e-91  304.8   0.0   1.0   1   0   0   1   1   1   1 aspartate aminotransferase
PROKKA_00241         -          ASPAMINOTRANS-RXN    -            2.1e-62  208.4   0.0   2.5e-62  208.2   0.0   1.0   1   0   0   1   1   1   1 LL-diaminopimelate aminotransferase apoenzyme
PROKKA_01352         -          ASPAMINOTRANS-RXN    -            1.3e-60  202.5   0.0   1.6e-60  202.2   0.0   1.0   1   0   0   1   1   1   1 alanine-synthesizing transaminase
PROKKA_02268         -          ASPAMINOTRANS-RXN    -            5.5e-23   78.7   0.0   6.4e-23   78.4   0.0   1.0   1   0   0   1   1   1   1 histidinol-phosphate aminotransferase
PROKKA_01224         -          ASPAMINOTRANS-RXN    -            5.1e-21   72.2   0.0   5.9e-21   72.0   0.0   1.0   1   0   0   1   1   1   1 histidinol-phosphate aminotransferase
PROKKA_01512         -          ASPAMINOTRANS-RXN    -            7.7e-14   48.6   0.0   2.3e-11   40.4   0.0   2.0   2   0   0   2   2   2   2 aspartate aminotransferase
PROKKA_01558         -          ASPAMINOTRANS-RXN    -            8.3e-08   28.7   0.0   9.2e-08   28.5   0.0   1.2   1   0   0   1   1   1   1 L-threonine O-3-phosphate decarboxylase 
PROKKA_01781         -          ASPAMINOTRANS-RXN    -            2.5e-07   27.1   0.0   3.7e-07   26.6   0.0   1.2   1   0   0   1   1   1   1 CDP-6-deoxy-D-xylo-4-hexulose-3-dehydrase
PROKKA_01174         -          ASPAMINOTRANS-RXN    -            1.6e-05   21.2   0.0   2.4e-05   20.6   0.0   1.1   1   0   0   1   1   1   1 cysteine desulfurase
PROKKA_01606         -          ASPAMINOTRANS-RXN    -            6.1e-05   19.3   0.0   0.00017   17.8   0.0   1.7   1   1   0   1   1   1   1 dTDP-4-amino-4,6-dideoxygalactose transaminase
PROKKA_02358         -          ASPAMINOTRANS-RXN    -            9.3e-05   18.7   0.0     0.015   11.4   0.0   2.2   2   0   0   2   2   2   2 acetylornithine/N-succinyldiaminopimelate aminotransferase
PROKKA_00307         -          ASPAMINOTRANS-RXN    -            0.00019   17.6   0.0   0.00026   17.2   0.0   1.1   1   0   0   1   1   1   1 methionine-gamma-lyase
PROKKA_01906         -          ASPARAGINE--TRNA-LIGASE-RXN -            3.3e-39  132.1   0.0   4.1e-29   98.8   0.0   2.1   2   0   0   2   2   2   2 aspartyl-tRNA synthetase 
PROKKA_01341         -          ASPARAGINE--TRNA-LIGASE-RXN -            1.7e-25   86.9   0.0   2.1e-15   53.6   0.0   2.1   2   0   0   2   2   2   2 lysyl-tRNA synthetase, class II
PROKKA_01556         -          ASPARTASE-RXN        -           1.3e-131  436.9   0.0  1.6e-131  436.6   0.0   1.0   1   0   0   1   1   1   1 fumarase, class II
PROKKA_02355         -          ASPARTASE-RXN        -            1.5e-10   37.7   0.0   2.2e-10   37.1   0.0   1.2   1   0   0   1   1   1   1 argininosuccinate lyase 
PROKKA_01372         -          ASPARTASE-RXN        -              6e-06   22.5   0.0     1e-05   21.7   0.0   1.3   1   0   0   1   1   1   1 Adenylosuccinate lyase 
PROKKA_01906         -          ASPARTATE--TRNA-LIGASE-RXN -           3.4e-218  723.0   0.0  4.1e-218  722.7   0.0   1.0   1   0   0   1   1   1   1 aspartyl-tRNA synthetase 
PROKKA_01341         -          ASPARTATE--TRNA-LIGASE-RXN -            2.1e-50  168.9   0.0   2.6e-32  109.2   0.0   2.0   2   0   0   2   2   2   2 lysyl-tRNA synthetase, class II
PROKKA_00095         -          ASPARTATE--TRNA-LIGASE-RXN -             0.0057   12.3   0.5      0.33    6.5   0.1   2.2   2   0   0   2   2   2   2 phenylalanyl-tRNA synthetase, alpha subunit
PROKKA_02243         -          ASPARTATE-RACEMASE-RXN -            8.4e-08   29.5   0.0   1.1e-07   29.1   0.0   1.1   1   0   0   1   1   1   1 glutamate racemase
PROKKA_02535         -          ASPARTATE-SEMIALDEHYDE-DEHYDROGENASE-RXN -             3e-125  415.1   0.0  3.8e-125  414.7   0.0   1.0   1   0   0   1   1   1   1 aspartate-semialdehyde dehydrogenase
PROKKA_02392         -          ASPARTATE-SEMIALDEHYDE-DEHYDROGENASE-RXN -            1.2e-12   44.8   0.0   2.4e-08   30.6   0.0   2.8   1   1   1   2   2   2   2 N-acetyl-gamma-glutamyl-phosphate reductase
PROKKA_00052         -          ASPARTATE-SEMIALDEHYDE-DEHYDROGENASE-RXN -            0.00016   18.1   0.0   0.00032   17.1   0.0   1.4   2   0   0   2   2   2   1 glyceraldehyde 3-phosphate dehydrogenase
PROKKA_01357         -          ASPARTATEKIN-RXN     -           4.6e-118  392.6  11.8  1.5e-103  344.6   6.1   2.0   1   1   1   2   2   2   2 aspartate kinase
PROKKA_01354         -          ASPARTATEKIN-RXN     -            7.1e-22   74.4   0.3   1.3e-20   70.2   0.3   2.5   1   1   0   1   1   1   1 homoserine dehydrogenase
PROKKA_02388         -          ASPARTATEKIN-RXN     -            3.3e-08   29.2   0.0   3.7e-08   29.1   0.0   1.1   1   0   0   1   1   1   1 uridylate kinase
PROKKA_00105         -          ASPARTATEKIN-RXN     -            0.00013   17.3   0.0   0.00024   16.4   0.0   1.3   1   0   0   1   1   1   1 glutamate 5-kinase
PROKKA_01920         -          ASPCARBTRANS-RXN     -            8.4e-86  284.9   0.0   9.3e-86  284.8   0.0   1.0   1   0   0   1   1   1   1 aspartate carbamoyltransferase
PROKKA_02357         -          ASPCARBTRANS-RXN     -            6.7e-44  147.3   0.0   7.9e-44  147.0   0.0   1.0   1   0   0   1   1   1   1 ornithine carbamoyltransferase
PROKKA_02102         -          ASPDECARBOX-RXN      -            1.4e-48  161.3   0.0   1.6e-48  161.2   0.0   1.0   1   0   0   1   1   1   1 L-aspartate 1-decarboxylase
PROKKA_02426         -          ATP-ADENYLYLTRANSFERASE-RXN -             0.0014   14.6   0.0     0.002   14.0   0.0   1.2   1   0   0   1   1   1   1 UDPglucose--hexose-1-phosphate uridylyltransferase
PROKKA_00579         -          ATP-CITRATE-PRO-S--LYASE-RXN -            4.2e-26   87.9   0.5   5.4e-26   87.5   0.5   1.0   1   0   0   1   1   1   1 succinyl-CoA synthetase alpha subunit
PROKKA_00587         -          ATP-CITRATE-PRO-S--LYASE-RXN -            1.6e-17   59.5   0.1   2.1e-17   59.1   0.1   1.0   1   0   0   1   1   1   1 succinyl-CoA synthetase alpha subunit
PROKKA_00578         -          ATP-CITRATE-PRO-S--LYASE-RXN -              2e-14   49.2   0.3   2.6e-14   48.9   0.3   1.0   1   0   0   1   1   1   1 citryl-CoA synthetase large subunit
PROKKA_00582         -          ATP-CITRATE-PRO-S--LYASE-RXN -            1.2e-08   30.1   0.0   1.5e-08   29.8   0.0   1.2   1   0   0   1   1   1   1 citryl-CoA lyase
PROKKA_00862         -          ATP-PYROPHOSPHATASE-RXN -            5.9e-08   29.5   0.2   1.5e-07   28.2   0.2   1.6   1   1   0   1   1   1   1 recombination protein RecA
PROKKA_02180         -          ATP-PYROPHOSPHATASE-RXN -            4.1e-07   26.8   0.2   1.8e-06   24.7   0.2   2.0   1   1   0   1   1   1   1 DNA repair protein RadA/Sms
PROKKA_00744         -          ATP-PYROPHOSPHATASE-RXN -            5.5e-05   19.8   0.0   6.6e-05   19.6   0.0   1.1   1   0   0   1   1   1   1 Rad51 protein
PROKKA_02270         -          ATPPHOSPHORIBOSYLTRANS-RXN -            1.8e-56  188.6   0.0   2.1e-56  188.4   0.0   1.0   1   0   0   1   1   1   1 ATP phosphoribosyltransferase
PROKKA_02282         -          BADH-RXN             -            2.3e-66  221.6   0.0   2.9e-66  221.3   0.0   1.0   1   0   0   1   1   1   1 Acyl-CoA reductase
PROKKA_00106         -          BADH-RXN             -            5.5e-05   19.2   0.1    0.0014   14.6   0.0   2.1   2   0   0   2   2   2   2 glutamate-5-semialdehyde dehydrogenase
PROKKA_02282         -          BENZALDEHYDE-DEHYDROGENASE-NAD+-RXN -              6e-58  193.9   0.0   7.7e-58  193.5   0.0   1.0   1   0   0   1   1   1   1 Acyl-CoA reductase
PROKKA_01612         -          BENZOATE-12-DIOXYGENASE-RXN -            0.00053   16.8   0.0    0.0006   16.6   0.0   1.1   1   0   0   1   1   1   1 Ferredoxin-NADP reductase
PROKKA_02541         -          BENZOYLFORMATE-DECARBOXYLASE-RXN -              2e-33  113.0   0.0   6.5e-33  111.2   0.0   1.7   1   1   0   1   1   1   1 acetolactate synthase, large subunit
PROKKA_00594         -          BETA-LACTAMASE-RXN   -            0.00061   16.8   0.1    0.0014   15.6   0.1   1.5   1   1   0   1   1   1   1 peptidoglycan synthetase FtsI 
PROKKA_00297         -          BETA-PHOSPHOGLUCOMUTASE-RXN -            3.7e-10   36.9   0.0   5.4e-10   36.4   0.0   1.2   1   0   0   1   1   1   1 haloacid dehalogenase superfamily, subfamily IA, variant 3 with third motif having DD or ED
PROKKA_02197         -          BETA-PHOSPHOGLUCOMUTASE-RXN -            5.8e-07   26.5   0.0   2.5e-06   24.4   0.0   1.7   1   1   0   1   1   1   1 phosphoglycolate phosphatase
PROKKA_00059         -          BETA-PHOSPHOGLUCOMUTASE-RXN -            7.4e-06   22.9   0.0   1.1e-05   22.3   0.0   1.2   1   0   0   1   1   1   1 phosphoglycolate phosphatase
PROKKA_01296         -          BILIVERDIN-REDUCTASE-RXN -            4.7e-10   36.4   0.1   1.7e-08   31.2   0.0   2.1   2   0   0   2   2   2   2 putative dehydrogenase
PROKKA_01593         -          BIOTIN-CARBOXYL-RXN  -           3.8e-200  662.6   0.0    5e-200  662.2   0.0   1.0   1   0   0   1   1   1   1 acetyl-CoA carboxylase, biotin carboxylase subunit
PROKKA_01916         -          BIOTIN-CARBOXYL-RXN  -              1e-23   80.9   0.0   3.6e-13   46.1   0.0   2.3   2   0   0   2   2   2   2 carbamoyl-phosphate synthase large subunit
PROKKA_01333         -          BIOTIN-CARBOXYL-RXN  -            1.7e-07   27.4   0.0   2.5e-07   26.8   0.0   1.2   1   0   0   1   1   1   1 phosphoribosylamine--glycine ligase
PROKKA_01489         -          BIOTIN-CARBOXYL-RXN  -            0.00097   15.0   0.0    0.0014   14.5   0.0   1.2   1   0   0   1   1   1   1 5-(carboxyamino)imidazole ribonucleotide synthase
PROKKA_00869         -          BIOTINLIG-RXN        -              5e-20   69.0   0.0   6.7e-20   68.6   0.0   1.2   1   0   0   1   1   1   1 BirA family transcriptional regulator, biotin operon repressor / biotin-[acetyl-CoA-carboxylase] ligase
PROKKA_02110         -          BIPHENYL-23-DIOL-12-DIOXYGENASE-RXN -              4e-05   20.1   0.0   5.1e-05   19.7   0.0   1.1   1   0   0   1   1   1   1 Glyoxalase/Bleomycin resistance protein/Dioxygenase superfamily protein
PROKKA_00083         -          BIPHENYL-23-DIOL-12-DIOXYGENASE-RXN -              0.016   11.5   0.0     0.025   10.9   0.0   1.3   1   0   0   1   1   1   0 lactoylglutathione lyase
PROKKA_02237         -          BISPHOSPHOGLYCERATE-MUTASE-RXN -            5.5e-50  167.3   0.1   2.4e-33  112.7   0.0   2.0   1   1   1   2   2   2   2 phosphoglycerate mutase 
PROKKA_00116         -          BRANCHED-CHAINAMINOTRANSFERILEU-RXN -            3.6e-62  207.6   0.0   4.6e-62  207.3   0.0   1.0   1   0   0   1   1   1   1 branched-chain amino acid aminotransferase
PROKKA_00362         -          BRANCHED-CHAINAMINOTRANSFERILEU-RXN -            1.3e-07   28.1   0.0   3.6e-06   23.4   0.0   2.0   2   0   0   2   2   2   2 branched-chain amino acid aminotransferase
PROKKA_00116         -          BRANCHED-CHAINAMINOTRANSFERLEU-RXN -            3.6e-62  207.6   0.0   4.6e-62  207.3   0.0   1.0   1   0   0   1   1   1   1 branched-chain amino acid aminotransferase
PROKKA_00362         -          BRANCHED-CHAINAMINOTRANSFERLEU-RXN -            1.3e-07   28.1   0.0   3.6e-06   23.4   0.0   2.0   2   0   0   2   2   2   2 branched-chain amino acid aminotransferase
PROKKA_00116         -          BRANCHED-CHAINAMINOTRANSFERVAL-RXN -            3.6e-62  207.6   0.0   4.6e-62  207.3   0.0   1.0   1   0   0   1   1   1   1 branched-chain amino acid aminotransferase
PROKKA_00362         -          BRANCHED-CHAINAMINOTRANSFERVAL-RXN -            1.3e-07   28.1   0.0   3.6e-06   23.4   0.0   2.0   2   0   0   2   2   2   2 branched-chain amino acid aminotransferase
PROKKA_00259         -          CARBAMATE-KINASE-RXN -            4.5e-06   23.0   0.0   0.00016   17.9   0.0   2.1   2   0   0   2   2   2   2 N-acetylglutamate kinase
PROKKA_00105         -          CARBAMATE-KINASE-RXN -             0.0005   16.3   0.0   0.00071   15.8   0.0   1.2   1   0   0   1   1   1   1 glutamate 5-kinase
PROKKA_00030         -          CARBODEHYDRAT-RXN    -             0.0023   14.8   0.0     0.023   11.6   0.0   2.2   2   1   0   2   2   2   1 carbonic anhydrase
PROKKA_02160         -          CARBONYL-REDUCTASE-NADPH-RXN -            1.5e-30  103.9   0.6     5e-30  102.1   0.6   1.7   1   1   0   1   1   1   1 3-oxoacyl-[acyl-carrier-protein] reductase
PROKKA_00235         -          CARBONYL-REDUCTASE-NADPH-RXN -            7.9e-17   58.9   0.0     2e-16   57.6   0.0   1.5   1   1   0   1   1   1   1 3-oxoacyl-[acyl-carrier protein] reductase
PROKKA_02149         -          CARBONYL-REDUCTASE-NADPH-RXN -            4.9e-12   43.2   0.0   3.2e-11   40.5   0.0   1.9   1   1   0   1   1   1   1 NADP-dependent 3-hydroxy acid dehydrogenase YdfG
PROKKA_00170         -          CARBONYL-REDUCTASE-NADPH-RXN -            6.8e-10   36.1   0.0   8.1e-09   32.6   0.0   2.0   1   1   0   1   1   1   1 NAD(P)-dependent dehydrogenase, short-chain alcohol dehydrogenase family
PROKKA_00773         -          CARBONYL-REDUCTASE-NADPH-RXN -            1.8e-08   31.5   0.0     1e-06   25.7   0.0   2.0   1   1   0   1   1   1   1 short chain dehydrogenase
PROKKA_01392         -          CARBONYL-REDUCTASE-NADPH-RXN -            4.8e-06   23.5   0.0   7.2e-06   22.9   0.0   1.3   1   0   0   1   1   1   1 Nucleoside-diphosphate-sugar epimerase
PROKKA_01783         -          CARBONYL-REDUCTASE-NADPH-RXN -             0.0009   16.1   0.0    0.0065   13.3   0.0   1.9   2   0   0   2   2   2   1 Nucleoside-diphosphate-sugar epimerase
PROKKA_00572         -          CARBONYL-REDUCTASE-NADPH-RXN -             0.0077   13.0   0.0     0.012   12.3   0.0   1.4   1   0   0   1   1   1   1 UDP-glucose 4-epimerase
PROKKA_00078         -          CARBOXYMETHYLENEBUTENOLIDASE-RXN -            1.4e-30  103.6   0.0   2.1e-30  103.0   0.0   1.2   1   0   0   1   1   1   1 carboxymethylenebutenolidase
PROKKA_01946         -          CARBOXYPEPTIDASE-B-RXN -             0.0019   14.5   0.0    0.0032   13.7   0.0   1.3   1   1   0   1   1   1   1 Zinc carboxypeptidase
PROKKA_01916         -          CARBPSYN-RXN         -                  0 1407.1   0.0         0 1406.8   0.0   1.0   1   0   0   1   1   1   1 carbamoyl-phosphate synthase large subunit
PROKKA_01918         -          CARBPSYN-RXN         -           3.7e-106  352.9   0.0  4.3e-106  352.7   0.0   1.0   1   0   0   1   1   1   1 carbamoyl-phosphate synthase small subunit
PROKKA_01955         -          CARBPSYN-RXN         -            2.4e-17   58.6   0.0   2.9e-17   58.3   0.0   1.0   1   0   0   1   1   1   1 anthranilate synthase, component II 
PROKKA_01593         -          CARBPSYN-RXN         -            3.6e-14   48.1   0.0   5.4e-14   47.5   0.0   1.2   1   0   0   1   1   1   1 acetyl-CoA carboxylase, biotin carboxylase subunit
PROKKA_01902         -          CARBPSYN-RXN         -            2.1e-05   19.0   0.0     3e-05   18.5   0.0   1.1   1   0   0   1   1   1   1 GMP synthase (glutamine-hydrolysing)
PROKKA_01333         -          CARBPSYN-RXN         -            0.00053   14.4   0.0   0.00072   13.9   0.0   1.1   1   0   0   1   1   1   1 phosphoribosylamine--glycine ligase
PROKKA_02242         -          CARBPSYN-RXN         -             0.0022   12.3   0.0    0.0093   10.2   0.0   1.7   1   1   0   1   1   1   1 putative glutamine amidotransferase
PROKKA_01519         -          CARBPSYN-RXN         -             0.0036   11.6   0.0    0.0042   11.4   0.0   1.0   1   0   0   1   1   1   1 GMP synthase (glutamine-hydrolysing)
PROKKA_01332         -          CARBPSYN-RXN         -              0.033    8.4   0.0     0.043    8.0   0.0   1.1   1   0   0   1   1   1   0 phosphoribosylaminoimidazolecarboxamide formyltransferase / IMP cyclohydrolase
PROKKA_02110         -          CATECHOL-23-DIOXYGENASE-RXN -            1.3e-06   25.2   0.0   1.4e-06   25.1   0.0   1.0   1   0   0   1   1   1   1 Glyoxalase/Bleomycin resistance protein/Dioxygenase superfamily protein
PROKKA_02530         -          CATECHOL-23-DIOXYGENASE-RXN -            0.00011   18.9   0.0    0.0082   12.8   0.0   2.1   2   0   0   2   2   2   2 glyoxylase I family protein
PROKKA_00083         -          CATECHOL-23-DIOXYGENASE-RXN -             0.0013   15.4   0.0    0.0026   14.4   0.0   1.4   1   1   0   1   1   1   1 lactoylglutathione lyase
PROKKA_00656         -          CDP-GLUCOSE-46-DEHYDRATASE-RXN -            0.00018   18.9   0.0   0.00045   17.6   0.0   1.7   1   0   0   1   1   1   1 UDP-galactose 4-epimerase
PROKKA_00566         -          CDP-GLUCOSE-46-DEHYDRATASE-RXN -             0.0016   15.9   0.0    0.0031   14.9   0.0   1.5   1   0   0   1   1   1   1 UDP-glucuronate 4-epimerase
PROKKA_00488         -          CDP-GLUCOSE-46-DEHYDRATASE-RXN -             0.0063   13.9   0.0     0.013   12.9   0.0   1.5   1   0   0   1   1   1   1 dTDP-glucose 4,6-dehydratase
PROKKA_02170         -          CDPDIGLYSYN-RXN      -            3.6e-34  115.7  10.7   1.3e-19   67.9   0.5   2.1   1   1   1   2   2   2   2 phosphatidate cytidylyltransferase
PROKKA_00739         -          CELLULOSE-SYNTHASE-UDP-FORMING-RXN -             1e-169  563.5   1.0  1.3e-169  563.1   1.0   1.0   1   0   0   1   1   1   1 cellulose synthase (UDP-forming)
PROKKA_02310         -          CELLULOSE-SYNTHASE-UDP-FORMING-RXN -           1.3e-115  384.1   0.0  1.6e-115  383.9   0.0   1.0   1   0   0   1   1   1   1 Glycosyltransferase, catalytic subunit of cellulose synthase and poly-beta-1,6-N-acetylglucosamine synthase
PROKKA_00738         -          CELLULOSE-SYNTHASE-UDP-FORMING-RXN -            1.5e-35  118.8   0.4     2e-35  118.4   0.4   1.0   1   0   0   1   1   1   1 cellulose synthase subunit
PROKKA_02307         -          CELLULOSE-SYNTHASE-UDP-FORMING-RXN -            1.5e-21   72.4   0.0     3e-21   71.4   0.0   1.3   1   1   0   1   1   1   1 cellulose synthase subunit
PROKKA_02308         -          CELLULOSE-SYNTHASE-UDP-FORMING-RXN -            2.8e-15   51.6   0.0   3.2e-15   51.4   0.0   1.0   1   0   0   1   1   1   1 PilZ domain-containing protein
PROKKA_00547         -          CELLULOSE-SYNTHASE-UDP-FORMING-RXN -            2.9e-05   18.4   0.1   4.4e-05   17.8   0.1   1.5   1   1   0   1   1   1   1 Glycosyltransferase, catalytic subunit of cellulose synthase and poly-beta-1,6-N-acetylglucosamine synthase
PROKKA_00223         -          CHER-RXN             -            7.4e-33  111.1   0.0   8.9e-33  110.9   0.0   1.0   1   0   0   1   1   1   1 chemotaxis protein methyltransferase CheR
PROKKA_02310         -          CHITIN-SYNTHASE-RXN  -             0.0069   11.7   0.3      0.04    9.2   0.0   2.1   2   1   0   2   2   2   1 Glycosyltransferase, catalytic subunit of cellulose synthase and poly-beta-1,6-N-acetylglucosamine synthase
PROKKA_02446         -          CHITIN-SYNTHASE-RXN  -                5.9    2.0   9.2         7    1.8   9.2   1.0   1   0   0   1   1   1   0 Phosphate-starvation-inducible E
PROKKA_00961         -          CHLORDECONE-REDUCTASE-RXN -            7.2e-05   19.2   0.0   0.00012   18.5   0.0   1.2   1   0   0   1   1   1   1 putative oxidoreductase
PROKKA_01886         -          CHLORDECONE-REDUCTASE-RXN -              0.018   11.3   0.0         2    4.6   0.0   2.5   2   1   0   2   2   2   0 putative oxidoreductase
PROKKA_01736         -          CHLORDECONE-REDUCTASE-RXN -              0.032   10.5   0.0     0.082    9.1   0.0   1.7   2   0   0   2   2   2   0 putative oxidoreductase
PROKKA_02282         -          CHMS-DEHYDROGENASE-RXN -            7.3e-51  170.5   0.0     1e-50  170.0   0.0   1.1   1   0   0   1   1   1   1 Acyl-CoA reductase
PROKKA_00961         -          CHOLESTENONE-5-BETA-REDUCTASE-RXN -            4.5e-05   19.6   0.0   7.2e-05   19.0   0.0   1.3   1   0   0   1   1   1   1 putative oxidoreductase
PROKKA_01886         -          CHOLESTENONE-5-BETA-REDUCTASE-RXN -              0.004   13.2   0.0      0.48    6.4   0.0   2.7   2   1   0   2   2   2   2 putative oxidoreductase
PROKKA_00565         -          CHOLESTEROL-OXIDASE-RXN -            1.7e-05   20.9   0.0   2.1e-05   20.5   0.0   1.1   1   0   0   1   1   1   1 UDP-galactopyranose mutase
PROKKA_00559         -          CHOLESTEROL-OXIDASE-RXN -             0.0051   12.7   0.3    0.0051   12.7   0.3   1.5   2   0   0   2   2   2   1 mercuric reductase
PROKKA_00255         -          CHOLESTEROL-OXIDASE-RXN -             0.0085   11.9   0.0     0.013   11.3   0.0   1.2   1   0   0   1   1   1   1 methylenetetrahydrofolate--tRNA-(uracil-5-)-methyltransferase
PROKKA_00137         -          CHOLESTEROL-OXIDASE-RXN -              0.019   10.8   1.3     0.018   10.8   0.3   1.4   2   0   0   2   2   2   0 dihydrolipoamide dehydrogenase
PROKKA_00884         -          CHORISMATE-SYNTHASE-RXN -           3.7e-101  335.8   0.0  4.5e-101  335.5   0.0   1.0   1   0   0   1   1   1   1 chorismate synthase
PROKKA_01533         -          CHORISMATEMUT-RXN    -            1.7e-58  195.7   0.0     2e-58  195.5   0.0   1.0   1   0   0   1   1   1   1 chorismate mutase / prephenate dehydratase
PROKKA_00620         -          CINNAMOYL-COA-REDUCTASE-RXN -            4.2e-23   78.9   0.0     8e-21   71.4   0.0   2.0   2   0   0   2   2   2   2 dihydroflavonol-4-reductase
PROKKA_00572         -          CINNAMOYL-COA-REDUCTASE-RXN -            1.9e-11   40.6   0.0     3e-11   39.9   0.0   1.2   1   0   0   1   1   1   1 UDP-glucose 4-epimerase
PROKKA_00469         -          CINNAMOYL-COA-REDUCTASE-RXN -            9.8e-08   28.4   0.0   1.6e-07   27.7   0.0   1.3   1   0   0   1   1   1   1 NADH dehydrogenase
PROKKA_01629         -          CINNAMOYL-COA-REDUCTASE-RXN -            2.4e-06   23.8   0.0   1.4e-05   21.3   0.0   1.9   1   1   0   1   1   1   1 ADP-L-glycero-D-manno-heptose 6-epimerase
PROKKA_00656         -          CINNAMOYL-COA-REDUCTASE-RXN -              0.011   11.7   0.1      0.45    6.5   0.0   2.4   3   0   0   3   3   3   0 UDP-galactose 4-epimerase
PROKKA_00507         -          CINNAMYL-ALCOHOL-DEHYDROGENASE-RXN -             6e-128  423.9   0.0  7.4e-128  423.6   0.0   1.0   1   0   0   1   1   1   1 putative zinc-type alcohol dehydrogenase-like protein
PROKKA_02069         -          CINNAMYL-ALCOHOL-DEHYDROGENASE-RXN -            9.3e-57  189.7   0.0   1.2e-56  189.3   0.0   1.1   1   0   0   1   1   1   1 alcohol dehydrogenase, propanol-preferring
PROKKA_00329         -          CINNAMYL-ALCOHOL-DEHYDROGENASE-RXN -            1.4e-35  120.0   0.0   1.8e-35  119.7   0.0   1.1   1   0   0   1   1   1   1 alcohol dehydrogenase, propanol-preferring
PROKKA_00022         -          CINNAMYL-ALCOHOL-DEHYDROGENASE-RXN -            4.5e-07   26.3   0.0   1.8e-05   21.1   0.0   2.1   1   1   0   1   1   1   1 NADPH:quinone reductase
PROKKA_01239         -          CINNAMYL-ALCOHOL-DEHYDROGENASE-RXN -              2e-06   24.2   3.3   2.2e-05   20.7   0.1   2.8   1   1   1   3   3   3   1 NADPH2:quinone reductase
PROKKA_01717         -          CINNAMYL-ALCOHOL-DEHYDROGENASE-RXN -              0.014   11.5   0.0      0.92    5.5   0.0   2.1   2   0   0   2   2   2   0 NADPH2:quinone reductase
PROKKA_01337         -          CITC-RXN             -             0.0002   17.8   0.0     0.066    9.5   0.0   2.1   1   1   1   2   2   2   2 Phosphopantetheine adenylyltransferase
PROKKA_00582         -          CITSYN-RXN           -            1.4e-28   97.0   0.0   1.7e-21   73.7   0.0   2.0   1   1   1   2   2   2   2 citryl-CoA lyase
PROKKA_02514         -          COBALADENOSYLTRANS-RXN -            2.3e-56  187.8   0.2   2.6e-56  187.6   0.2   1.0   1   0   0   1   1   1   1 cob(I)alamin adenosyltransferase
PROKKA_00695         -          COENZYME-F420-HYDROGENASE-RXN -            9.6e-05   19.3   0.1   0.00011   19.1   0.1   1.2   1   0   0   1   1   1   1 NADH-quinone oxidoreductase subunit I
PROKKA_01962         -          COENZYME-F420-HYDROGENASE-RXN -            0.00066   16.6   0.1   0.00093   16.1   0.1   1.2   1   0   0   1   1   1   1 Polyferredoxin
PROKKA_00692         -          COENZYME-F420-HYDROGENASE-RXN -               0.01   12.7   0.7     0.015   12.1   0.7   1.4   1   0   0   1   1   1   0 NAD(P)-dependent iron-only hydrogenase diaphorase component flavoprotein
PROKKA_01300         -          CPM-KDOSYNTH-RXN     -            2.8e-53  178.3   0.0   3.9e-53  177.9   0.0   1.1   1   0   0   1   1   1   1 3-deoxy-D-manno-octulosonic-acid transferase
PROKKA_01878         -          CPM-KDOSYNTH-RXN     -            7.2e-15   51.8   0.0   6.6e-11   38.7   0.0   3.0   3   0   0   3   3   3   3 3-deoxy-manno-octulosonate cytidylyltransferase (CMP-KDO synthetase)
PROKKA_02332         -          CPM-KDOSYNTH-RXN     -              0.019   10.8   0.0      0.03   10.2   0.0   1.2   1   0   0   1   1   1   0 Glycosyl transferases group 1
PROKKA_02433         -          CREATINASE-RXN       -            1.9e-10   37.1   0.2   3.4e-10   36.3   0.2   1.5   1   1   0   1   1   1   1 Xaa-Pro aminopeptidase
PROKKA_01642         -          CREATINASE-RXN       -            5.8e-07   25.6   0.0   9.1e-07   25.0   0.0   1.4   1   1   0   1   1   1   1 methionine aminopeptidase, type I 
PROKKA_02189         -          CREATININASE-RXN     -            1.5e-31  106.7   0.0     5e-31  105.0   0.0   1.7   1   1   0   1   1   1   1 creatinine amidohydrolase
PROKKA_01877         -          CTPSYN-RXN           -           3.8e-241  798.5   0.0  4.9e-241  798.1   0.0   1.0   1   0   0   1   1   1   1 CTP synthase
PROKKA_02242         -          CTPSYN-RXN           -            0.00026   16.8   0.0     0.057    9.1   0.0   2.0   2   0   0   2   2   2   2 putative glutamine amidotransferase
PROKKA_01918         -          CTPSYN-RXN           -             0.0039   12.9   0.5      0.32    6.6   0.0   2.1   1   1   0   2   2   2   2 carbamoyl-phosphate synthase small subunit
PROKKA_01625         -          CTPSYN-RXN           -             0.0053   12.5   0.0    0.0073   12.0   0.0   1.1   1   0   0   1   1   1   1 phosphoribosylformylglycinamidine synthase
PROKKA_00143         -          CYANAMIDE-HYDRATASE-RXN -             0.0047   13.9   0.0    0.0078   13.1   0.0   1.2   1   0   0   1   1   1   1 HD domain-containing protein
PROKKA_00616         -          CYCLOARTENOL-SYNTHASE-RXN -            1.6e-47  158.9   0.0   5.9e-47  157.1   0.0   1.7   1   1   0   1   1   1   1 squalene-hopene/tetraprenyl-beta-curcumene cyclase
PROKKA_01535         -          CYCLOHEXADIENYL-DEHYDROGENASE-RXN -            7.6e-28   94.8   0.0   9.4e-28   94.5   0.0   1.0   1   0   0   1   1   1   1 prephenate dehydrogenase
PROKKA_00427         -          CYCLOHEXADIENYL-DEHYDROGENASE-RXN -              0.028   10.9   0.2     0.042   10.3   0.2   1.2   1   0   0   1   1   1   0 dissimilatory adenylylsulfate reductase alpha subunit precursor
PROKKA_00499         -          CYCLOMALTODEXTRINASE-RXN -            6.6e-24   81.2   0.8   1.3e-18   63.7   0.2   3.1   3   1   0   3   3   3   2 maltose alpha-D-glucosyltransferase/ alpha-amylase
PROKKA_00498         -          CYCLOMALTODEXTRINASE-RXN -            2.7e-15   52.7   0.0   2.3e-09   33.2   0.0   3.0   3   0   0   3   3   3   2 maltose alpha-D-glucosyltransferase/ alpha-amylase
PROKKA_00496         -          CYCLOMALTODEXTRINASE-RXN -            4.7e-14   48.6   0.0     5e-12   41.9   0.0   2.7   3   0   0   3   3   3   2 maltooligosyl trehalose hydrolase
PROKKA_00497         -          CYCLOMALTODEXTRINASE-RXN -            1.9e-10   36.7   0.0   3.1e-10   36.0   0.0   1.2   1   0   0   1   1   1   1 maltooligosyl trehalose synthase 
PROKKA_02234         -          CYCLOMALTODEXTRINASE-RXN -            3.2e-10   36.0   0.3   2.1e-05   20.1   0.2   3.6   3   1   0   3   3   3   2 glycogen operon protein
PROKKA_00505         -          CYCLOMALTODEXTRINASE-RXN -              0.012   11.0   0.1     0.021   10.2   0.1   1.3   1   0   0   1   1   1   0 alpha-1,4-glucan:maltose-1-phosphate maltosyltransferase
PROKKA_00307         -          CYSTAGLY-RXN         -            1.4e-87  291.4   0.0   1.7e-87  291.2   0.0   1.0   1   0   0   1   1   1   1 methionine-gamma-lyase
PROKKA_01512         -          CYSTAGLY-RXN         -            9.6e-05   18.6   0.0   0.00015   18.0   0.0   1.2   1   0   0   1   1   1   1 aspartate aminotransferase
PROKKA_00190         -          CYSTAGLY-RXN         -             0.0047   13.1   0.0    0.0069   12.5   0.0   1.1   1   0   0   1   1   1   1 glycine dehydrogenase (decarboxylating) beta subunit 
PROKKA_00307         -          CYSTATHIONINE-BETA-LYASE-RXN -           5.4e-108  358.5   0.0  6.5e-108  358.2   0.0   1.0   1   0   0   1   1   1   1 methionine-gamma-lyase
PROKKA_01512         -          CYSTATHIONINE-BETA-LYASE-RXN -            1.1e-07   28.3   0.2   1.7e-07   27.7   0.2   1.2   1   0   0   1   1   1   1 aspartate aminotransferase
PROKKA_00190         -          CYSTATHIONINE-BETA-LYASE-RXN -            1.8e-05   20.9   0.0   2.6e-05   20.5   0.0   1.1   1   0   0   1   1   1   1 glycine dehydrogenase (decarboxylating) beta subunit 
PROKKA_00647         -          CYSTATHIONINE-BETA-SYNTHASE-RXN -            1.3e-86  287.9   0.0   1.6e-86  287.6   0.0   1.0   1   0   0   1   1   1   1 cysteine synthase 
PROKKA_01355         -          CYSTATHIONINE-BETA-SYNTHASE-RXN -            6.2e-16   55.3   0.7   4.1e-12   42.8   0.2   2.1   2   0   0   2   2   2   2 threonine synthase
PROKKA_00649         -          CYSTATHIONINE-BETA-SYNTHASE-RXN -            1.1e-06   24.9   0.1   5.6e-06   22.6   0.0   1.8   2   0   0   2   2   2   1 threonine synthase
PROKKA_01951         -          CYSTATHIONINE-BETA-SYNTHASE-RXN -            9.1e-06   21.9   0.1     0.019   11.0   0.0   2.1   2   0   0   2   2   2   2 tryptophan synthase beta chain
PROKKA_01364         -          CYSTEINE--TRNA-LIGASE-RXN -           4.7e-156  517.5   0.0    6e-156  517.1   0.0   1.0   1   0   0   1   1   1   1 cysteinyl-tRNA synthetase
PROKKA_02128         -          CYSTEINE--TRNA-LIGASE-RXN -              7e-18   61.9   0.0   7.4e-09   32.2   0.0   2.1   2   0   0   2   2   2   2 methionyl-tRNA synthetase
PROKKA_00378         -          CYSTEINE--TRNA-LIGASE-RXN -            3.6e-12   43.1   0.0   4.8e-08   29.5   0.0   2.1   2   0   0   2   2   2   2 leucyl-tRNA synthetase
PROKKA_02476         -          CYSTEINE--TRNA-LIGASE-RXN -            7.5e-10   35.5   0.0     5e-09   32.7   0.0   1.9   2   0   0   2   2   2   1 Isoleucyl-tRNA synthetase
PROKKA_00929         -          CYSTEINE--TRNA-LIGASE-RXN -            1.5e-05   21.3   0.0     0.011   11.9   0.0   2.1   2   0   0   2   2   2   2 arginyl-tRNA synthetase
PROKKA_00867         -          CYSTEINE--TRNA-LIGASE-RXN -            0.00048   16.3   0.3     0.042    9.9   0.0   2.7   3   0   0   3   3   3   1 valyl-tRNA synthetase
PROKKA_01338         -          CYSTEINE-S-CONJUGATE-BETA-LYASE-RXN -            8.9e-43  144.1   0.0   1.8e-42  143.1   0.0   1.4   1   0   0   1   1   1   1 aspartate aminotransferase
PROKKA_00241         -          CYSTEINE-S-CONJUGATE-BETA-LYASE-RXN -            2.6e-20   70.0   0.0   5.6e-20   68.9   0.0   1.5   1   1   0   1   1   1   1 LL-diaminopimelate aminotransferase apoenzyme
PROKKA_01352         -          CYSTEINE-S-CONJUGATE-BETA-LYASE-RXN -            9.4e-17   58.3   0.0   2.8e-16   56.7   0.0   1.8   1   1   0   1   1   1   1 alanine-synthesizing transaminase
PROKKA_01612         -          CYTOCHROME-B5-REDUCTASE-RXN -            1.1e-21   74.2   0.0   2.6e-21   73.0   0.0   1.5   1   1   0   1   1   1   1 Ferredoxin-NADP reductase
PROKKA_00278         -          CYTOCHROME-B5-REDUCTASE-RXN -            7.6e-17   58.3   0.0   8.7e-17   58.1   0.0   1.1   1   0   0   1   1   1   1 NAD(P)H-flavin reductase
PROKKA_00765         -          CYTOCHROME-B5-REDUCTASE-RXN -             0.0024   14.1   0.0    0.0031   13.7   0.0   1.1   1   0   0   1   1   1   1 Ferredoxin-NADP reductase
PROKKA_02122         -          CYTOCHROME-C-PEROXIDASE-RXN -            4.5e-94  312.7   0.0   5.6e-94  312.4   0.0   1.0   1   0   0   1   1   1   1 cytochrome c peroxidase
PROKKA_01969         -          CYTOCHROME-C-PEROXIDASE-RXN -              0.019   11.5   0.0     0.021   11.4   0.0   1.1   1   0   0   1   1   1   0 Cytochrome c
PROKKA_01511         -          D-2-HYDROXY-ACID-DEHYDROGENASE-RXN -              9e-70  232.5   0.1   1.7e-69  231.6   0.1   1.4   1   0   0   1   1   1   1 D-3-phosphoglycerate dehydrogenase
PROKKA_01991         -          D-2-HYDROXY-ACID-DEHYDROGENASE-RXN -            4.2e-05   20.0   0.0   6.8e-05   19.3   0.0   1.2   1   0   0   1   1   1   1 glutamyl-tRNA reductase
PROKKA_01836         -          D-2-HYDROXY-ACID-DEHYDROGENASE-RXN -             0.0013   15.1   0.0    0.0023   14.2   0.0   1.3   1   0   0   1   1   1   1 3-hydroxyisobutyrate dehydrogenase
PROKKA_02540         -          D-2-HYDROXY-ACID-DEHYDROGENASE-RXN -             0.0029   13.9   0.0    0.0045   13.3   0.0   1.2   1   0   0   1   1   1   1 ketol-acid reductoisomerase
PROKKA_02151         -          D-2-HYDROXY-ACID-DEHYDROGENASE-RXN -             0.0064   12.8   0.1    0.0085   12.4   0.1   1.1   1   0   0   1   1   1   1 6-phosphogluconate dehydrogenase (decarboxylating) 
PROKKA_00645         -          D-2-HYDROXY-ACID-DEHYDROGENASE-RXN -              0.012   11.9   0.1     0.024   10.9   0.1   1.4   1   0   0   1   1   1   0 adenosylhomocysteinase
PROKKA_00241         -          D-ALANINE-AMINOTRANSFERASE-RXN -            5.4e-58  193.9   0.0   7.3e-58  193.5   0.0   1.0   1   0   0   1   1   1   1 LL-diaminopimelate aminotransferase apoenzyme
PROKKA_01352         -          D-ALANINE-AMINOTRANSFERASE-RXN -            5.9e-41  137.8   0.0   7.7e-41  137.4   0.0   1.0   1   0   0   1   1   1   1 alanine-synthesizing transaminase
PROKKA_01338         -          D-ALANINE-AMINOTRANSFERASE-RXN -            6.9e-26   88.2   0.0   8.4e-26   87.9   0.0   1.0   1   0   0   1   1   1   1 aspartate aminotransferase
PROKKA_01528         -          D-AMINO-ACID-OXIDASE-RXN -            0.00066   15.7   0.2     0.001   15.1   0.2   1.2   1   0   0   1   1   1   1 UDPglucose 6-dehydrogenase
PROKKA_01529         -          D-AMINO-ACID-OXIDASE-RXN -               0.02   10.8   0.0      0.96    5.3   0.1   2.1   2   0   0   2   2   2   0 glycine oxidase
PROKKA_01549         -          D-LACTATE-DEHYDROGENASE-CYTOCHROME-RXN -            5.5e-27   91.4   0.0   2.6e-13   46.2   0.0   2.4   2   0   0   2   2   2   2 FAD/FMN-containing dehydrogenase
PROKKA_01909         -          D-NOPALINE-DEHYDROGENASE-RXN -            2.8e-07   27.3   0.7   0.00053   16.5   0.2   2.3   2   1   0   2   2   2   2 dihydrolipoamide dehydrogenase
PROKKA_01529         -          D-NOPALINE-DEHYDROGENASE-RXN -            3.6e-06   23.6   0.0   6.6e-06   22.8   0.0   1.3   1   1   0   1   1   1   1 glycine oxidase
PROKKA_00137         -          D-NOPALINE-DEHYDROGENASE-RXN -            3.7e-06   23.6   0.6     0.013   11.9   0.4   2.7   3   0   0   3   3   3   2 dihydrolipoamide dehydrogenase
PROKKA_00565         -          D-NOPALINE-DEHYDROGENASE-RXN -            6.1e-06   22.9   0.2   8.4e-06   22.4   0.2   1.1   1   0   0   1   1   1   1 UDP-galactopyranose mutase
PROKKA_01719         -          D-NOPALINE-DEHYDROGENASE-RXN -            9.2e-05   19.0   0.2   0.00016   18.2   0.2   1.3   1   0   0   1   1   1   1 oxygen-dependent protoporphyrinogen oxidase
PROKKA_01944         -          D-NOPALINE-DEHYDROGENASE-RXN -            0.00024   17.7   0.2     0.002   14.6   0.0   2.1   2   0   0   2   2   2   1 NADPH-dependent glutamate synthase beta chain
PROKKA_00255         -          D-NOPALINE-DEHYDROGENASE-RXN -            0.00026   17.5   0.0    0.0004   16.9   0.0   1.1   1   0   0   1   1   1   1 methylenetetrahydrofolate--tRNA-(uracil-5-)-methyltransferase
PROKKA_01157         -          D-NOPALINE-DEHYDROGENASE-RXN -            0.00037   17.0   6.5     0.022   11.2   1.4   2.5   2   1   0   2   2   2   2 mercuric reductase
PROKKA_01451         -          D-NOPALINE-DEHYDROGENASE-RXN -             0.0005   16.6   0.8    0.0021   14.5   0.0   2.1   2   0   0   2   2   2   1 NADH-quinone oxidoreductase subunit F
PROKKA_01854         -          D-NOPALINE-DEHYDROGENASE-RXN -            0.00059   16.4   0.0   0.00099   15.6   0.0   1.3   1   0   0   1   1   1   1 L-2-hydroxyglutarate oxidase LhgO
PROKKA_00598         -          D-NOPALINE-DEHYDROGENASE-RXN -            0.00084   15.9   0.0    0.0013   15.2   0.0   1.2   1   0   0   1   1   1   1 UDP-N-acetylmuramoylalanine--D-glutamate ligase
PROKKA_01391         -          D-NOPALINE-DEHYDROGENASE-RXN -             0.0011   15.5   0.2      0.41    7.0   0.0   2.4   2   0   0   2   2   2   2 dihydrolipoamide dehydrogenase
PROKKA_01702         -          D-NOPALINE-DEHYDROGENASE-RXN -             0.0018   14.7   0.4    0.0048   13.3   0.4   1.6   2   0   0   2   2   2   1 2-polyprenyl-6-methoxyphenol hydroxylase
PROKKA_02417         -          D-NOPALINE-DEHYDROGENASE-RXN -             0.0031   14.0   0.0     0.023   11.1   0.0   2.0   2   0   0   2   2   2   1 thioredoxin reductase (NADPH)
PROKKA_01201         -          D-NOPALINE-DEHYDROGENASE-RXN -              0.007   12.8   0.3     0.013   11.9   0.3   1.4   1   0   0   1   1   1   1 nitrite reductase (NADH) large subunit
PROKKA_02501         -          D-NOPALINE-DEHYDROGENASE-RXN -             0.0074   12.7   0.3     0.011   12.2   0.3   1.3   1   0   0   1   1   1   1 tRNA uridine 5-carboxymethylaminomethyl modification enzyme
PROKKA_00570         -          D-NOPALINE-DEHYDROGENASE-RXN -             0.0085   12.5   0.0     0.014   11.9   0.0   1.3   1   0   0   1   1   1   1 UDP-N-acetyl-D-glucosamine dehydrogenase
PROKKA_02377         -          D-NOPALINE-DEHYDROGENASE-RXN -             0.0096   12.4   0.0      0.11    8.9   0.1   2.2   3   0   0   3   3   3   1 geranylgeranyl reductase family protein
PROKKA_01489         -          D-NOPALINE-DEHYDROGENASE-RXN -              0.013   12.0   0.0      0.02   11.3   0.0   1.2   1   0   0   1   1   1   0 5-(carboxyamino)imidazole ribonucleotide synthase
PROKKA_00652         -          D-NOPALINE-DEHYDROGENASE-RXN -              0.021   11.3   0.1     0.026   10.9   0.1   1.2   1   0   0   1   1   1   0 adenylyltransferase and sulfurtransferase
PROKKA_00559         -          D-NOPALINE-DEHYDROGENASE-RXN -               0.29    7.5   8.8      0.66    6.3   0.1   3.2   2   1   1   3   3   3   0 mercuric reductase
PROKKA_01356         -          D-PPENTOMUT-RXN      -              0.032   10.1   0.0     0.044    9.7   0.0   1.1   1   0   0   1   1   1   0 2,3-bisphosphoglycerate-independent phosphoglycerate mutase
PROKKA_00961         -          D-THREO-ALDOSE-1-DEHYDROGENASE-RXN -            1.1e-15   55.0   0.0     2e-13   47.5   0.0   2.1   1   1   0   1   1   1   1 putative oxidoreductase
PROKKA_01736         -          D-THREO-ALDOSE-1-DEHYDROGENASE-RXN -              2e-13   47.5   0.1   1.2e-06   25.3   0.0   2.2   1   1   1   2   2   2   2 putative oxidoreductase
PROKKA_01886         -          D-THREO-ALDOSE-1-DEHYDROGENASE-RXN -            9.8e-09   32.1   0.0   9.2e-08   28.9   0.0   1.9   1   1   0   1   1   1   1 putative oxidoreductase
PROKKA_00961         -          D-XYLOSE-1-DEHYDROGENASE-NADP+-RXN -              5e-09   33.0   0.0   4.8e-05   19.9   0.0   2.8   1   1   1   2   2   2   2 putative oxidoreductase
PROKKA_01736         -          D-XYLOSE-1-DEHYDROGENASE-NADP+-RXN -            8.4e-08   29.0   0.1     0.011   12.1   0.0   3.1   2   1   1   3   3   3   3 putative oxidoreductase
PROKKA_01886         -          D-XYLOSE-1-DEHYDROGENASE-NADP+-RXN -              1e-06   25.4   0.0     0.035   10.5   0.0   3.7   2   1   1   3   3   3   3 putative oxidoreductase
PROKKA_00507         -          D-XYLULOSE-REDUCTASE-RXN -            5.6e-13   46.0   0.7   6.7e-13   45.7   0.7   1.1   1   0   0   1   1   1   1 putative zinc-type alcohol dehydrogenase-like protein
PROKKA_02069         -          D-XYLULOSE-REDUCTASE-RXN -            8.6e-13   45.4   1.5   1.9e-12   44.3   1.5   1.4   1   1   0   1   1   1   1 alcohol dehydrogenase, propanol-preferring
PROKKA_00022         -          D-XYLULOSE-REDUCTASE-RXN -            1.3e-08   31.7   0.0   2.9e-08   30.5   0.0   1.6   1   1   0   1   1   1   1 NADPH:quinone reductase
PROKKA_01239         -          D-XYLULOSE-REDUCTASE-RXN -            1.2e-05   21.9   0.5   1.5e-05   21.5   0.5   1.2   1   0   0   1   1   1   1 NADPH2:quinone reductase
PROKKA_00329         -          D-XYLULOSE-REDUCTASE-RXN -             0.0015   15.0   0.0    0.0022   14.4   0.0   1.1   1   0   0   1   1   1   1 alcohol dehydrogenase, propanol-preferring
PROKKA_01717         -          D-XYLULOSE-REDUCTASE-RXN -             0.0052   13.2   0.0       0.2    7.9   0.0   2.1   1   1   1   2   2   2   1 NADPH2:quinone reductase
PROKKA_01529         -          DAADEHYDROG-RXN      -            1.2e-15   54.3   0.0     1e-10   38.2   0.0   2.1   2   0   0   2   2   2   2 glycine oxidase
PROKKA_01854         -          DAADEHYDROG-RXN      -            3.2e-05   20.0   0.0     0.042    9.8   0.0   2.1   2   0   0   2   2   2   2 L-2-hydroxyglutarate oxidase LhgO
PROKKA_00565         -          DAADEHYDROG-RXN      -             0.0012   14.9   0.1    0.0017   14.3   0.1   1.1   1   0   0   1   1   1   1 UDP-galactopyranose mutase
PROKKA_01451         -          DAADEHYDROG-RXN      -             0.0045   12.9   0.2    0.0092   11.9   0.0   1.5   2   0   0   2   2   2   1 NADH-quinone oxidoreductase subunit F
PROKKA_02417         -          DAADEHYDROG-RXN      -             0.0062   12.5   0.0      0.87    5.4   0.0   2.3   2   0   0   2   2   2   2 thioredoxin reductase (NADPH)
PROKKA_01534         -          DAHPSYN-RXN          -            2.4e-34  116.1   0.0   6.3e-34  114.8   0.0   1.4   1   1   0   1   1   1   1 3-deoxy-D-arabinoheptulosonate-7-phosphate synthase
PROKKA_01876         -          DAHPSYN-RXN          -            5.8e-06   22.7   0.0   1.3e-05   21.5   0.0   1.6   1   1   0   1   1   1   1 2-dehydro-3-deoxyphosphooctonate aldolase (KDO 8-P synthase)
PROKKA_01533         -          DAHPSYN-RXN          -              0.014   11.6   0.0     0.023   10.8   0.0   1.3   1   0   0   1   1   1   0 chorismate mutase / prephenate dehydratase
PROKKA_00604         -          DALADALALIG-RXN      -            5.1e-80  265.9   0.0   6.8e-80  265.5   0.0   1.0   1   0   0   1   1   1   1 D-alanine--D-alanine ligase
PROKKA_01916         -          DALADALALIG-RXN      -            6.7e-19   65.1   0.0   9.4e-12   41.6   0.0   2.2   2   0   0   2   2   2   2 carbamoyl-phosphate synthase large subunit
PROKKA_01593         -          DALADALALIG-RXN      -            1.6e-13   47.5   0.0   2.1e-13   47.1   0.0   1.2   1   0   0   1   1   1   1 acetyl-CoA carboxylase, biotin carboxylase subunit
PROKKA_01333         -          DALADALALIG-RXN      -            8.2e-06   22.1   0.0   1.3e-05   21.5   0.0   1.2   1   0   0   1   1   1   1 phosphoribosylamine--glycine ligase
PROKKA_01489         -          DALADALALIG-RXN      -             0.0054   12.9   0.0     0.025   10.7   0.0   1.8   1   1   0   1   1   1   1 5-(carboxyamino)imidazole ribonucleotide synthase
PROKKA_01321         -          DAPASYN-RXN          -           4.5e-123  408.3   0.0  5.8e-123  407.9   0.0   1.0   1   0   0   1   1   1   1 adenosylmethionine-8-amino-7-oxononanoate aminotransferase
PROKKA_02358         -          DAPASYN-RXN          -            2.7e-63  211.3   0.0   2.9e-62  207.9   0.0   1.9   1   1   0   1   1   1   1 acetylornithine/N-succinyldiaminopimelate aminotransferase
PROKKA_00349         -          DAPASYN-RXN          -            2.9e-45  151.9   0.0   3.7e-45  151.5   0.0   1.0   1   0   0   1   1   1   1 diaminobutyrate-2-oxoglutarate transaminase
PROKKA_02461         -          DAPASYN-RXN          -            1.1e-25   87.3   0.0   4.9e-25   85.2   0.0   1.9   1   1   1   2   2   2   1 glutamate-1-semialdehyde 2,1-aminomutase
PROKKA_01574         -          DCMP-DEAMINASE-RXN   -            5.3e-10   36.5   0.0   7.2e-05   19.8   0.0   2.2   1   1   1   2   2   2   2 diaminohydroxyphosphoribosylaminopyrimidine deaminase 
PROKKA_01986         -          DCTP-DEAM-RXN        -              2e-23   80.3   0.0     2e-13   47.7   0.0   2.3   2   0   0   2   2   2   2 dCTP deaminase
PROKKA_00261         -          DCTP-DEAM-RXN        -            6.3e-12   42.8   0.0   7.9e-12   42.5   0.0   1.2   1   0   0   1   1   1   1 dUTP pyrophosphatase
PROKKA_02248         -          DEOXYADENOSINE-KINASE-RXN -             0.0037   14.3   0.0         1    6.3   0.0   2.8   3   0   0   3   3   3   2 ATP-binding cassette, subfamily F, member 3
PROKKA_02248         -          DEOXYCYTIDINE-KINASE-RXN -              0.023   11.1   0.0       2.6    4.3   0.0   2.3   2   0   0   2   2   2   0 ATP-binding cassette, subfamily F, member 3
PROKKA_00904         -          DEOXYGLUCONOKIN-RXN  -            9.4e-08   28.7   0.0   1.3e-07   28.2   0.0   1.1   1   0   0   1   1   1   1 Sugar or nucleoside kinase, ribokinase family
PROKKA_01879         -          DEOXYGLUCONOKIN-RXN  -            0.00033   17.0   0.2     0.061    9.6   0.2   2.1   2   0   0   2   2   2   2 D-beta-D-heptose 7-phosphate kinase / D-beta-D-heptose 1-phosphate adenosyltransferase
PROKKA_02014         -          DEOXYRIBODIPYRIMIDINE-PHOTOLYASE-RXN -           1.9e-114  380.5   0.0  2.5e-114  380.1   0.0   1.0   1   0   0   1   1   1   1 deoxyribodipyrimidine photo-lyase
PROKKA_02264         -          DEOXYRIBOSE-P-ALD-RXN -            0.00089   16.0   0.1      0.14    8.8   0.0   2.1   2   0   0   2   2   2   2 cyclase
PROKKA_02265         -          DEOXYRIBOSE-P-ALD-RXN -             0.0079   12.9   0.1       1.2    5.7   0.0   2.1   2   0   0   2   2   2   2 1-(5-phosphoribosyl)-5-[(5-phosphoribosylamino)methylideneamino] imidazole-4-carboxamide isomerase
PROKKA_01604         -          DETHIOBIOTIN-SYN-RXN -              7e-20   68.4   0.0   1.1e-19   67.8   0.0   1.2   1   1   0   1   1   1   1 dethiobiotin synthetase
PROKKA_01560         -          DETHIOBIOTIN-SYN-RXN -            0.00013   18.5   0.0   0.00041   16.9   0.0   1.7   2   0   0   2   2   2   1 adenosylcobyric acid synthase (glutamine-hydrolysing)
PROKKA_00963         -          DETHIOBIOTIN-SYN-RXN -            0.00017   18.1   0.2    0.0011   15.4   0.0   2.2   3   0   0   3   3   3   1 cobyrinic acid a,c-diamide synthase
PROKKA_02253         -          DGTPTRIPHYDRO-RXN    -             0.0019   13.9   0.0    0.0029   13.3   0.0   1.2   1   0   0   1   1   1   1 HD domain protein
PROKKA_02160         -          DHBDEHYD-RXN         -            1.1e-44  149.9   3.8   2.5e-44  148.7   3.8   1.5   1   1   0   1   1   1   1 3-oxoacyl-[acyl-carrier-protein] reductase
PROKKA_00235         -          DHBDEHYD-RXN         -            2.6e-22   76.6   0.0   3.1e-22   76.3   0.0   1.1   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier protein] reductase
PROKKA_02149         -          DHBDEHYD-RXN         -            6.7e-20   68.6   0.0   8.4e-20   68.3   0.0   1.3   1   1   0   1   1   1   1 NADP-dependent 3-hydroxy acid dehydrogenase YdfG
PROKKA_00773         -          DHBDEHYD-RXN         -            4.7e-16   56.0   0.0   6.3e-16   55.6   0.0   1.0   1   0   0   1   1   1   1 short chain dehydrogenase
PROKKA_00170         -          DHBDEHYD-RXN         -            3.4e-08   30.3   0.0   4.7e-08   29.8   0.0   1.1   1   0   0   1   1   1   1 NAD(P)-dependent dehydrogenase, short-chain alcohol dehydrogenase family
PROKKA_00820         -          DHBDEHYD-RXN         -            1.7e-06   24.7   0.2   5.4e-06   23.1   0.2   1.8   1   1   0   1   1   1   1 Enoyl-[acyl-carrier-protein] reductase [NADH]
PROKKA_01286         -          DHHB-METHYLTRANSFER-RXN -            2.9e-08   30.6   0.0   4.9e-08   29.9   0.0   1.3   1   0   0   1   1   1   1 Methyltransferase domain-containing protein
PROKKA_02331         -          DHHB-METHYLTRANSFER-RXN -              1e-05   22.2   0.0   1.7e-05   21.6   0.0   1.2   1   0   0   1   1   1   1 Glycosyltransferase involved in cell wall bisynthesis
PROKKA_02272         -          DHHB-METHYLTRANSFER-RXN -            0.00054   16.6   0.0   0.00076   16.1   0.0   1.2   1   0   0   1   1   1   1 release factor glutamine methyltransferase
PROKKA_00176         -          DHHB-METHYLTRANSFER-RXN -             0.0013   15.4   0.0    0.0018   14.9   0.0   1.1   1   0   0   1   1   1   1 Methyltransferase domain-containing protein
PROKKA_00952         -          DHHB-METHYLTRANSFER-RXN -              0.012   12.2   0.0     0.015   11.9   0.0   1.1   1   0   0   1   1   1   0 Thiopurine S-methyltransferase (TPMT)
PROKKA_01319         -          DIACYLGLYKIN-RXN     -             0.0043   12.7   0.1    0.0061   12.2   0.1   1.1   1   0   0   1   1   1   1 Diacylglycerol kinase family enzyme
PROKKA_02178         -          DIAMACTRANS-RXN      -            4.6e-09   33.7   0.0   5.8e-09   33.3   0.0   1.2   1   0   0   1   1   1   1 ribosomal-protein-alanine N-acetyltransferase
PROKKA_01270         -          DIAMACTRANS-RXN      -             0.0027   14.9   0.0    0.0036   14.5   0.0   1.1   1   0   0   1   1   1   1 Acetyltransferase (GNAT) domain-containing protein
PROKKA_02354         -          DIAMINOPIMDECARB-RXN -           3.5e-153  507.4   0.0  4.3e-153  507.1   0.0   1.0   1   0   0   1   1   1   1 diaminopimelate decarboxylase
PROKKA_00172         -          DIAMINOPIMEPIM-RXN   -            1.7e-67  225.0   0.0   2.3e-67  224.6   0.0   1.0   1   0   0   1   1   1   1 diaminopimelate epimerase
PROKKA_02160         -          DIENOYLCOAREDUCT-RXN -            8.1e-24   81.4   1.2   9.5e-24   81.1   1.2   1.0   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier-protein] reductase
PROKKA_00235         -          DIENOYLCOAREDUCT-RXN -            1.7e-11   40.9   0.0     2e-11   40.7   0.0   1.0   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier protein] reductase
PROKKA_02149         -          DIENOYLCOAREDUCT-RXN -            7.1e-10   35.5   0.0   8.8e-10   35.2   0.0   1.1   1   0   0   1   1   1   1 NADP-dependent 3-hydroxy acid dehydrogenase YdfG
PROKKA_00820         -          DIENOYLCOAREDUCT-RXN -            9.4e-08   28.6   0.0   1.5e-07   27.9   0.0   1.2   1   0   0   1   1   1   1 Enoyl-[acyl-carrier-protein] reductase [NADH]
PROKKA_00773         -          DIENOYLCOAREDUCT-RXN -            0.00035   16.8   0.0   0.00092   15.4   0.0   1.7   1   1   0   1   1   1   1 short chain dehydrogenase
PROKKA_00170         -          DIENOYLCOAREDUCT-RXN -             0.0007   15.8   0.0    0.0015   14.8   0.0   1.6   1   1   0   1   1   1   1 NAD(P)-dependent dehydrogenase, short-chain alcohol dehydrogenase family
PROKKA_02413         -          DIGLUCOACETYL-DOCOSANOATE-HYDROLYSIS-RXN -             0.0041   13.5   0.1     0.011   12.1   0.0   1.7   2   0   0   2   2   2   1 phospholipase/carboxylesterase
PROKKA_02413         -          DIGLUCODIACETYL-DOCOSANOATE-LYSIS-RXN -             0.0041   13.5   0.1     0.011   12.1   0.0   1.7   2   0   0   2   2   2   1 phospholipase/carboxylesterase
PROKKA_00137         -          DIHYDLIPOXN-RXN      -           3.7e-129  428.7   0.0  4.1e-129  428.5   0.0   1.0   1   0   0   1   1   1   1 dihydrolipoamide dehydrogenase
PROKKA_01157         -          DIHYDLIPOXN-RXN      -           6.4e-100  332.3   0.0  8.2e-100  331.9   0.0   1.0   1   0   0   1   1   1   1 mercuric reductase
PROKKA_00559         -          DIHYDLIPOXN-RXN      -            1.8e-97  324.2   0.0   2.3e-97  323.8   0.0   1.0   1   0   0   1   1   1   1 mercuric reductase
PROKKA_01909         -          DIHYDLIPOXN-RXN      -            1.1e-95  318.4   0.0   1.2e-95  318.2   0.0   1.0   1   0   0   1   1   1   1 dihydrolipoamide dehydrogenase
PROKKA_01391         -          DIHYDLIPOXN-RXN      -            4.4e-84  280.1   0.0   4.9e-84  279.9   0.0   1.0   1   0   0   1   1   1   1 dihydrolipoamide dehydrogenase
PROKKA_01201         -          DIHYDLIPOXN-RXN      -            3.2e-19   66.2   0.0   4.9e-19   65.6   0.0   1.2   1   0   0   1   1   1   1 nitrite reductase (NADH) large subunit
PROKKA_02417         -          DIHYDLIPOXN-RXN      -            1.4e-18   64.1   0.2   5.3e-14   49.0   0.0   2.1   2   0   0   2   2   2   2 thioredoxin reductase (NADPH)
PROKKA_01944         -          DIHYDLIPOXN-RXN      -            1.6e-10   37.5   9.1   6.1e-06   22.4   0.1   3.0   3   0   0   3   3   3   2 NADPH-dependent glutamate synthase beta chain
PROKKA_01451         -          DIHYDLIPOXN-RXN      -            2.2e-09   33.8   5.2    0.0003   16.8   0.2   3.1   2   1   0   2   2   2   2 NADH-quinone oxidoreductase subunit F
PROKKA_02138         -          DIHYDLIPOXN-RXN      -            1.6e-05   21.0   0.1   0.00029   16.9   0.0   2.8   3   1   0   3   3   3   1 NADH dehydrogenase
PROKKA_01678         -          DIHYDLIPOXN-RXN      -            0.00067   15.7   0.0    0.0073   12.3   0.0   2.3   3   0   0   3   3   3   1 sulfide:quinone oxidoreductase
PROKKA_01719         -          DIHYDLIPOXN-RXN      -            0.00067   15.7   0.7    0.0014   14.6   0.7   1.5   2   0   0   2   2   2   1 oxygen-dependent protoporphyrinogen oxidase
PROKKA_00735         -          DIHYDLIPOXN-RXN      -             0.0017   14.4   2.3    0.0031   13.5   2.3   1.4   1   0   0   1   1   1   1 C-3',4' desaturase CrtD
PROKKA_00585         -          DIHYDLIPOXN-RXN      -             0.0021   14.0   0.1    0.0047   12.9   0.1   1.5   1   0   0   1   1   1   1 succinate dehydrogenase / fumarate reductase flavoprotein subunit
PROKKA_00565         -          DIHYDLIPOXN-RXN      -             0.0027   13.7   0.1    0.0038   13.2   0.1   1.2   1   0   0   1   1   1   1 UDP-galactopyranose mutase
PROKKA_00427         -          DIHYDLIPOXN-RXN      -               0.02   10.8   0.1       0.2    7.5   0.1   2.3   3   0   0   3   3   3   0 dissimilatory adenylylsulfate reductase alpha subunit precursor
PROKKA_02352         -          DIHYDRODIPICSYN-RXN  -           1.3e-105  349.6   0.0  1.5e-105  349.4   0.0   1.0   1   0   0   1   1   1   1 4-hydroxy-tetrahydrodipicolinate synthase
PROKKA_01948         -          DIHYDROFOLATESYNTH-RXN -            2.7e-59  198.2   0.0   3.8e-59  197.8   0.0   1.1   1   0   0   1   1   1   1 dihydrofolate synthase / folylpolyglutamate synthase
PROKKA_00595         -          DIHYDROFOLATESYNTH-RXN -            4.8e-07   26.1   0.0   2.5e-05   20.4   0.0   2.1   2   0   0   2   2   2   2 UDP-N-acetylmuramoylalanyl-D-glutamate--2,6-diaminopimelate ligase
PROKKA_00598         -          DIHYDROFOLATESYNTH-RXN -             0.0004   16.5   0.0    0.0019   14.2   0.0   1.8   2   0   0   2   2   2   1 UDP-N-acetylmuramoylalanine--D-glutamate ligase
PROKKA_00620         -          DIHYDROKAEMPFEROL-4-REDUCTASE-RXN -            1.8e-48  162.5   0.0   2.4e-48  162.1   0.0   1.0   1   0   0   1   1   1   1 dihydroflavonol-4-reductase
PROKKA_00572         -          DIHYDROKAEMPFEROL-4-REDUCTASE-RXN -            3.4e-14   49.7   0.0   4.9e-14   49.2   0.0   1.2   1   0   0   1   1   1   1 UDP-glucose 4-epimerase
PROKKA_01629         -          DIHYDROKAEMPFEROL-4-REDUCTASE-RXN -            3.9e-09   33.1   0.0     1e-08   31.7   0.0   1.7   1   1   0   1   1   1   1 ADP-L-glycero-D-manno-heptose 6-epimerase
PROKKA_01783         -          DIHYDROKAEMPFEROL-4-REDUCTASE-RXN -            2.7e-08   30.3   0.1   1.2e-06   24.9   0.0   2.1   2   0   0   2   2   2   1 Nucleoside-diphosphate-sugar epimerase
PROKKA_01526         -          DIHYDROKAEMPFEROL-4-REDUCTASE-RXN -            6.9e-08   29.0   0.0   2.2e-07   27.3   0.0   1.7   1   1   0   1   1   1   1 UDP-glucuronate decarboxylase
PROKKA_00566         -          DIHYDROKAEMPFEROL-4-REDUCTASE-RXN -            2.3e-07   27.3   0.0   4.7e-07   26.2   0.0   1.5   1   1   0   1   1   1   1 UDP-glucuronate 4-epimerase
PROKKA_00656         -          DIHYDROKAEMPFEROL-4-REDUCTASE-RXN -            1.5e-06   24.6   0.0   0.00019   17.7   0.1   3.0   2   1   0   2   2   2   1 UDP-galactose 4-epimerase
PROKKA_00683         -          DIHYDROKAEMPFEROL-4-REDUCTASE-RXN -            6.6e-06   22.5   0.0   0.00023   17.4   0.0   2.2   2   0   0   2   2   2   1 UDP-glucose 4-epimerase
PROKKA_01392         -          DIHYDROKAEMPFEROL-4-REDUCTASE-RXN -              2e-05   20.9   0.0   2.9e-05   20.3   0.0   1.3   1   0   0   1   1   1   1 Nucleoside-diphosphate-sugar epimerase
PROKKA_00469         -          DIHYDROKAEMPFEROL-4-REDUCTASE-RXN -            9.9e-05   18.6   0.0   0.00032   16.9   0.0   1.8   1   1   0   1   1   1   1 NADH dehydrogenase
PROKKA_01778         -          DIHYDROKAEMPFEROL-4-REDUCTASE-RXN -             0.0045   13.1   0.0      0.22    7.6   0.0   2.1   2   0   0   2   2   2   1 GDP-L-fucose synthase
PROKKA_00488         -          DIHYDROKAEMPFEROL-4-REDUCTASE-RXN -             0.0079   12.3   0.0     0.015   11.4   0.0   1.4   2   0   0   2   2   2   1 dTDP-glucose 4,6-dehydratase
PROKKA_01919         -          DIHYDROOROT-RXN      -            1.8e-84  281.5   0.0   2.4e-84  281.1   0.0   1.0   1   0   0   1   1   1   1 dihydroorotase
PROKKA_01916         -          DIHYDROOROT-RXN      -            5.5e-16   55.5   0.0   3.7e-11   39.6   0.0   2.1   1   1   0   2   2   2   2 carbamoyl-phosphate synthase large subunit
PROKKA_01979         -          DIHYDROOROTATE-DEHYDROGENASE-RXN -            4.5e-10   36.6   0.0   6.2e-10   36.1   0.0   1.1   1   0   0   1   1   1   1 Dihydroorotate dehydrogenase
PROKKA_01919         -          DIHYDROPYRIMIDINASE-RXN -              7e-23   78.1   0.0     4e-16   55.8   0.0   2.0   2   0   0   2   2   2   2 dihydroorotase
PROKKA_01826         -          DIHYDROXYISOVALDEHYDRAT-RXN -           2.1e-238  789.3   3.4  2.5e-238  789.0   3.4   1.0   1   0   0   1   1   1   1 dihydroxy-acid dehydratase
PROKKA_01253         -          DIHYDROXYISOVALDEHYDRAT-RXN -             1e-237  787.0   5.0  1.2e-237  786.8   5.0   1.0   1   0   0   1   1   1   1 dihydroxy-acid dehydratase
PROKKA_01217         -          DIHYDROXYISOVALDEHYDRAT-RXN -             8e-217  718.0   0.4    9e-217  717.9   0.4   1.0   1   0   0   1   1   1   1 dihydroxyacid dehydratase 
PROKKA_01562         -          DIHYDROXYISOVALDEHYDRAT-RXN -           3.1e-216  716.1   2.8  3.4e-216  716.0   2.8   1.0   1   0   0   1   1   1   1 dihydroxy-acid dehydratase
PROKKA_01824         -          DIHYDROXYISOVALDEHYDRAT-RXN -             3e-215  712.8   0.1  3.4e-215  712.7   0.1   1.0   1   0   0   1   1   1   1 dihydroxyacid dehydratase 
PROKKA_01826         -          DIHYDROXYMETVALDEHYDRAT-RXN -           2.1e-238  789.3   3.4  2.5e-238  789.0   3.4   1.0   1   0   0   1   1   1   1 dihydroxy-acid dehydratase
PROKKA_01253         -          DIHYDROXYMETVALDEHYDRAT-RXN -             1e-237  787.0   5.0  1.2e-237  786.8   5.0   1.0   1   0   0   1   1   1   1 dihydroxy-acid dehydratase
PROKKA_01217         -          DIHYDROXYMETVALDEHYDRAT-RXN -             8e-217  718.0   0.4    9e-217  717.9   0.4   1.0   1   0   0   1   1   1   1 dihydroxyacid dehydratase 
PROKKA_01562         -          DIHYDROXYMETVALDEHYDRAT-RXN -           3.1e-216  716.1   2.8  3.4e-216  716.0   2.8   1.0   1   0   0   1   1   1   1 dihydroxy-acid dehydratase
PROKKA_01824         -          DIHYDROXYMETVALDEHYDRAT-RXN -             3e-215  712.8   0.1  3.4e-215  712.7   0.1   1.0   1   0   0   1   1   1   1 dihydroxyacid dehydratase 
PROKKA_00187         -          DIMETHYLGLYCINE-DEHYDROGENASE-RXN -              7e-19   64.3   0.0   8.5e-19   64.0   0.0   1.1   1   0   0   1   1   1   1 aminomethyltransferase
PROKKA_01529         -          DIMETHYLGLYCINE-DEHYDROGENASE-RXN -            1.1e-11   40.5   0.0   6.3e-10   34.7   0.0   2.0   2   0   0   2   2   2   2 glycine oxidase
PROKKA_01511         -          DLACTDEHYDROGNAD-RXN -              4e-41  138.3   0.2   9.7e-41  137.0   0.1   1.7   2   1   0   2   2   2   1 D-3-phosphoglycerate dehydrogenase
PROKKA_00954         -          DMBPPRIBOSYLTRANS-RXN -            3.6e-66  220.7   0.5   4.6e-66  220.4   0.5   1.0   1   0   0   1   1   1   1 nicotinate-nucleotide-dimethylbenzimidazole phosphoribosyltransferase
PROKKA_01035         -          DNA-CYTOSINE-5--METHYLTRANSFERASE-RXN -            3.1e-36  121.9   0.0   1.8e-27   93.2   0.0   3.2   2   1   0   2   2   2   2 hypothetical protein
PROKKA_01034         -          DNA-CYTOSINE-5--METHYLTRANSFERASE-RXN -            2.5e-05   20.4   0.0   0.00071   15.7   0.0   2.6   1   1   1   2   2   2   1 DNA methylase
PROKKA_00778         -          DNA-CYTOSINE-5--METHYLTRANSFERASE-RXN -             0.0086   12.1   0.0    0.0089   12.1   0.0   1.1   1   0   0   1   1   1   1 SAM-dependent methyltransferase
PROKKA_01895         -          DNA-LIGASE-ATP-RXN   -           7.8e-103  342.4   0.0  1.1e-102  341.9   0.0   1.0   1   0   0   1   1   1   1 DNA ligase-1
PROKKA_00786         -          DNA-LIGASE-NAD+-RXN  -              0.019   10.4   0.0     0.028    9.8   0.0   1.1   1   0   0   1   1   1   0 Excinuclease ABC subunit C
PROKKA_00490         -          DTDPDEHYDRHAMEPIM-RXN -            1.9e-69  230.4   0.0   2.2e-69  230.2   0.0   1.0   1   0   0   1   1   1   1 dTDP-4-dehydrorhamnose 3,5-epimerase
PROKKA_01635         -          DTDPDEHYRHAMREDUCT-RXN -            3.7e-54  181.3   0.0   4.2e-54  181.1   0.0   1.0   1   0   0   1   1   1   1 dTDP-4-dehydrorhamnose reductase
PROKKA_00656         -          DTDPDEHYRHAMREDUCT-RXN -            8.8e-12   42.2   0.0   1.3e-11   41.7   0.0   1.2   1   0   0   1   1   1   1 UDP-galactose 4-epimerase
PROKKA_01629         -          DTDPDEHYRHAMREDUCT-RXN -            4.5e-08   30.0   0.0   4.3e-07   26.8   0.0   2.3   2   1   0   2   2   2   1 ADP-L-glycero-D-manno-heptose 6-epimerase
PROKKA_00566         -          DTDPDEHYRHAMREDUCT-RXN -              6e-07   26.3   0.0   1.2e-05   22.1   0.0   2.1   1   1   0   1   1   1   1 UDP-glucuronate 4-epimerase
PROKKA_00488         -          DTDPDEHYRHAMREDUCT-RXN -            1.8e-06   24.8   0.0   2.6e-06   24.3   0.0   1.2   1   0   0   1   1   1   1 dTDP-glucose 4,6-dehydratase
PROKKA_00683         -          DTDPDEHYRHAMREDUCT-RXN -            0.00031   17.5   0.0   0.00043   17.0   0.0   1.2   1   0   0   1   1   1   1 UDP-glucose 4-epimerase
PROKKA_00469         -          DTDPDEHYRHAMREDUCT-RXN -             0.0011   15.6   0.0     0.003   14.2   0.0   1.6   1   1   0   2   2   2   1 NADH dehydrogenase
PROKKA_00572         -          DTDPDEHYRHAMREDUCT-RXN -             0.0026   14.4   0.0    0.0055   13.3   0.0   1.5   1   1   0   1   1   1   1 UDP-glucose 4-epimerase
PROKKA_00488         -          DTDPGLUCDEHYDRAT-RXN -             3e-117  388.9   0.0  3.4e-117  388.7   0.0   1.0   1   0   0   1   1   1   1 dTDP-glucose 4,6-dehydratase
PROKKA_01526         -          DTDPGLUCDEHYDRAT-RXN -            3.7e-59  197.9   0.0   4.4e-59  197.7   0.0   1.1   1   0   0   1   1   1   1 UDP-glucuronate decarboxylase
PROKKA_00572         -          DTDPGLUCDEHYDRAT-RXN -            1.2e-58  196.2   0.0   1.4e-58  196.1   0.0   1.0   1   0   0   1   1   1   1 UDP-glucose 4-epimerase
PROKKA_00566         -          DTDPGLUCDEHYDRAT-RXN -            5.4e-52  174.4   0.0   7.6e-46  154.2   0.0   2.0   2   0   0   2   2   2   2 UDP-glucuronate 4-epimerase
PROKKA_00683         -          DTDPGLUCDEHYDRAT-RXN -            5.5e-43  144.8   0.0   6.9e-43  144.4   0.0   1.0   1   0   0   1   1   1   1 UDP-glucose 4-epimerase
PROKKA_00656         -          DTDPGLUCDEHYDRAT-RXN -            1.1e-39  133.9   0.0   1.7e-39  133.3   0.0   1.2   1   1   0   1   1   1   1 UDP-galactose 4-epimerase
PROKKA_01629         -          DTDPGLUCDEHYDRAT-RXN -            6.6e-28   95.2   0.0   7.6e-28   95.0   0.0   1.1   1   0   0   1   1   1   1 ADP-L-glycero-D-manno-heptose 6-epimerase
PROKKA_01783         -          DTDPGLUCDEHYDRAT-RXN -            1.2e-21   74.6   0.0   1.6e-21   74.2   0.0   1.1   1   0   0   1   1   1   1 Nucleoside-diphosphate-sugar epimerase
PROKKA_01778         -          DTDPGLUCDEHYDRAT-RXN -            2.9e-20   70.1   0.0   1.9e-16   57.5   0.0   2.1   2   0   0   2   2   2   2 GDP-L-fucose synthase
PROKKA_00620         -          DTDPGLUCDEHYDRAT-RXN -            2.2e-16   57.3   0.0   3.3e-16   56.7   0.0   1.3   1   0   0   1   1   1   1 dihydroflavonol-4-reductase
PROKKA_00469         -          DTDPGLUCDEHYDRAT-RXN -            1.7e-11   41.2   0.1   1.2e-10   38.4   0.1   2.0   1   1   0   1   1   1   1 NADH dehydrogenase
PROKKA_01635         -          DTDPGLUCDEHYDRAT-RXN -            1.5e-06   25.0   0.0     2e-06   24.6   0.0   1.1   1   0   0   1   1   1   1 dTDP-4-dehydrorhamnose reductase
PROKKA_01392         -          DTDPGLUCDEHYDRAT-RXN -             0.0032   14.0   0.0      0.01   12.4   0.0   2.0   1   1   0   1   1   1   1 Nucleoside-diphosphate-sugar epimerase
PROKKA_00489         -          DTDPGLUCOSEPP-RXN    -           9.8e-116  382.9   0.0  1.2e-115  382.6   0.0   1.0   1   0   0   1   1   1   1 Glucose-1-phosphate thymidylyltransferase
PROKKA_00121         -          DTDPGLUCOSEPP-RXN    -            8.5e-34  114.0   0.0   4.1e-31  105.2   0.0   2.0   1   1   0   1   1   1   1 UDP-glucose pyrophosphorylase 
PROKKA_00537         -          DTDPGLUCOSEPP-RXN    -            4.4e-33  111.7   0.0   3.3e-18   62.8   0.0   2.0   1   1   1   2   2   2   2 UTP--glucose-1-phosphate uridylyltransferase
PROKKA_01782         -          DTDPGLUCOSEPP-RXN    -            5.9e-25   85.0   0.0   6.8e-25   84.8   0.0   1.1   1   0   0   1   1   1   1 D-glycero-alpha-D-manno-heptose 1-phosphate guanylyltransferase
PROKKA_02429         -          DTDPGLUCOSEPP-RXN    -            1.6e-18   63.9   0.0   1.9e-17   60.4   0.0   2.2   1   1   0   1   1   1   1 glucose-1-phosphate adenylyltransferase
PROKKA_02470         -          DTDPGLUCOSEPP-RXN    -            1.2e-14   51.1   0.0   2.2e-14   50.3   0.0   1.3   1   1   0   1   1   1   1 mannose-1-phosphate guanylyltransferase / phosphomannomutase
PROKKA_02107         -          DTDPGLUCOSEPP-RXN    -            1.9e-06   24.2   0.0   2.5e-06   23.8   0.0   1.2   1   0   0   1   1   1   1 bifunctional UDP-N-acetylglucosamine pyrophosphorylase / Glucosamine-1-phosphate N-acetyltransferase
PROKKA_00770         -          DTDPGLUCOSEPP-RXN    -            6.2e-05   19.3   0.1   0.00073   15.8   0.0   2.4   2   1   0   2   2   2   1 mannose-1-phosphate guanylyltransferase / mannose-6-phosphate isomerase
PROKKA_02125         -          DTMPKI-RXN           -            1.9e-28   96.7   0.0   2.3e-28   96.5   0.0   1.0   1   0   0   1   1   1   1 thymidylate kinase
PROKKA_01062         -          DTMPKI-RXN           -            1.5e-21   74.3   0.0   2.1e-21   73.8   0.0   1.3   1   0   0   1   1   1   1 dTMP kinase
PROKKA_02369         -          DTMPKI-RXN           -              4e-17   59.8   0.0   1.4e-16   58.0   0.0   1.7   1   1   0   1   1   1   1 dTMP kinase
PROKKA_02370         -          DTMPKI-RXN           -            9.1e-13   45.6   0.0   1.2e-12   45.2   0.0   1.2   1   0   0   1   1   1   1 dTMP kinase
PROKKA_02588         -          DTMPKI-RXN           -            2.9e-12   43.9   0.0   3.4e-12   43.7   0.0   1.0   1   0   0   1   1   1   1 dTMP kinase
PROKKA_00866         -          DTMPKI-RXN           -            0.00012   19.2   0.0   0.00013   18.9   0.0   1.2   1   0   0   1   1   1   1 hypothetical protein
PROKKA_02587         -          DTMPKI-RXN           -             0.0022   15.0   0.0    0.0022   15.0   0.0   1.0   1   0   0   1   1   1   1 dTMP kinase
PROKKA_00796         -          DTMPKI-RXN           -             0.0057   13.6   0.0     0.017   12.1   0.0   1.8   1   1   0   1   1   1   1 Polyphosphate:AMP phosphotransferase 
PROKKA_00261         -          DUTP-PYROP-RXN       -            3.3e-28   95.8   0.0   3.8e-28   95.6   0.0   1.0   1   0   0   1   1   1   1 dUTP pyrophosphatase
PROKKA_00820         -          ENOYL-ACP-REDUCT-NADH-RXN -            2.3e-51  172.2   0.0   3.2e-48  161.8   0.0   2.0   1   1   0   1   1   1   1 Enoyl-[acyl-carrier-protein] reductase [NADH]
PROKKA_02160         -          ENOYL-ACP-REDUCT-NADH-RXN -            0.00039   16.7   0.4    0.0028   13.9   0.1   1.9   1   1   0   2   2   2   1 3-oxoacyl-[acyl-carrier-protein] reductase
PROKKA_02158         -          ENOYL-ACP-REDUCT-NADPH-RXN -            5.3e-17   57.2   0.2   5.8e-17   57.1   0.2   1.0   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier-protein] synthase II
PROKKA_01717         -          ENOYL-ACP-REDUCT-NADPH-RXN -            3.4e-13   44.6   0.0   3.7e-13   44.4   0.0   1.0   1   0   0   1   1   1   1 NADPH2:quinone reductase
PROKKA_00022         -          ENOYL-ACP-REDUCT-NADPH-RXN -            2.1e-12   41.9   0.0   2.6e-12   41.6   0.0   1.1   1   0   0   1   1   1   1 NADPH:quinone reductase
PROKKA_02161         -          ENOYL-ACP-REDUCT-NADPH-RXN -            8.1e-11   36.7   0.0   9.3e-11   36.5   0.0   1.1   1   0   0   1   1   1   1 [acyl-carrier-protein] S-malonyltransferase
PROKKA_01239         -          ENOYL-ACP-REDUCT-NADPH-RXN -            1.4e-06   22.6   0.2   1.5e-06   22.5   0.2   1.0   1   0   0   1   1   1   1 NADPH2:quinone reductase
PROKKA_02160         -          ENOYL-ACP-REDUCT-NADPH-RXN -            1.7e-05   19.0   0.1   1.9e-05   18.9   0.1   1.0   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier-protein] reductase
PROKKA_00514         -          ENOYL-COA-DELTA-ISOM-RXN -            1.6e-21   73.8   0.0   2.1e-21   73.5   0.0   1.0   1   0   0   1   1   1   1 DSF synthase
PROKKA_01610         -          ENOYL-COA-DELTA-ISOM-RXN -            3.1e-13   46.4   0.0   7.2e-13   45.2   0.0   1.5   2   0   0   2   2   2   1 3-hydroxybutyryl-CoA dehydrogenase
PROKKA_00514         -          ENOYL-COA-HYDRAT-RXN -            5.5e-24   81.8   0.0   6.6e-24   81.5   0.0   1.0   1   0   0   1   1   1   1 DSF synthase
PROKKA_02158         -          ERYTHRONOLIDE-SYNTHASE-RXN -            2.3e-24   81.2   3.5   2.5e-24   81.1   3.5   1.0   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier-protein] synthase II
PROKKA_02161         -          ERYTHRONOLIDE-SYNTHASE-RXN -            8.2e-23   76.1   0.1   9.5e-23   75.9   0.1   1.0   1   0   0   1   1   1   1 [acyl-carrier-protein] S-malonyltransferase
PROKKA_01717         -          ERYTHRONOLIDE-SYNTHASE-RXN -            1.4e-21   71.9   0.3   1.7e-21   71.7   0.3   1.0   1   0   0   1   1   1   1 NADPH2:quinone reductase
PROKKA_00022         -          ERYTHRONOLIDE-SYNTHASE-RXN -            5.3e-13   43.5   0.0   1.3e-12   42.2   0.0   1.5   1   1   0   1   1   1   1 NADPH:quinone reductase
PROKKA_02069         -          ERYTHRONOLIDE-SYNTHASE-RXN -            2.5e-08   28.0   1.7   2.3e-07   24.8   1.0   1.9   1   1   1   2   2   2   2 alcohol dehydrogenase, propanol-preferring
PROKKA_00235         -          ERYTHRONOLIDE-SYNTHASE-RXN -            3.4e-08   27.6   0.3   4.1e-08   27.3   0.3   1.0   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier protein] reductase
PROKKA_01239         -          ERYTHRONOLIDE-SYNTHASE-RXN -            1.3e-07   25.7   1.4   1.5e-07   25.4   1.4   1.0   1   0   0   1   1   1   1 NADPH2:quinone reductase
PROKKA_02160         -          ERYTHRONOLIDE-SYNTHASE-RXN -              4e-07   24.0   1.9   4.6e-07   23.8   1.9   1.0   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier-protein] reductase
PROKKA_02149         -          ERYTHRONOLIDE-SYNTHASE-RXN -            4.2e-07   23.9   0.1   5.3e-07   23.6   0.1   1.0   1   0   0   1   1   1   1 NADP-dependent 3-hydroxy acid dehydrogenase YdfG
PROKKA_00656         -          ERYTHRONOLIDE-SYNTHASE-RXN -            0.00027   14.6   0.0   0.00034   14.3   0.0   1.1   1   0   0   1   1   1   1 UDP-galactose 4-epimerase
PROKKA_00572         -          ERYTHRONOLIDE-SYNTHASE-RXN -            0.00048   13.8   0.0   0.00061   13.4   0.0   1.0   1   0   0   1   1   1   1 UDP-glucose 4-epimerase
PROKKA_02159         -          ERYTHRONOLIDE-SYNTHASE-RXN -               0.01    9.4   0.0     0.011    9.3   0.0   1.0   1   0   0   1   1   1   0 acyl carrier protein
PROKKA_02160         -          ESTRADIOL-17-BETA-DEHYDROGENASE-RXN -            3.2e-41  138.9   0.7   3.5e-41  138.7   0.7   1.0   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier-protein] reductase
PROKKA_00235         -          ESTRADIOL-17-BETA-DEHYDROGENASE-RXN -            9.4e-30  101.1   0.0   1.2e-29  100.9   0.0   1.1   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier protein] reductase
PROKKA_02149         -          ESTRADIOL-17-BETA-DEHYDROGENASE-RXN -            9.7e-19   64.9   0.0   1.1e-18   64.7   0.0   1.0   1   0   0   1   1   1   1 NADP-dependent 3-hydroxy acid dehydrogenase YdfG
PROKKA_00773         -          ESTRADIOL-17-BETA-DEHYDROGENASE-RXN -            1.3e-15   54.7   0.0   1.5e-15   54.4   0.0   1.0   1   0   0   1   1   1   1 short chain dehydrogenase
PROKKA_00170         -          ESTRADIOL-17-BETA-DEHYDROGENASE-RXN -            8.1e-07   25.7   0.0    0.0047   13.4   0.0   2.1   2   0   0   2   2   2   2 NAD(P)-dependent dehydrogenase, short-chain alcohol dehydrogenase family
PROKKA_00828         -          EXOPOLYPHOSPHATASE-RXN -            4.5e-30  101.8   0.0   5.4e-30  101.6   0.0   1.0   1   0   0   1   1   1   1 exopolyphosphatase / guanosine-5'-triphosphate,3'-diphosphate pyrophosphatase
PROKKA_02368         -          EXOPOLYPHOSPHATASE-RXN -            2.8e-19   66.2   0.0   3.6e-19   65.9   0.0   1.2   1   0   0   1   1   1   1 exopolyphosphatase / guanosine-5'-triphosphate,3'-diphosphate pyrophosphatase
PROKKA_02280         -          F16BDEPHOS-RXN       -            1.9e-64  215.2   0.0   2.5e-64  214.9   0.0   1.0   1   0   0   1   1   1   1 fructose-1,6-bisphosphatase I
PROKKA_01995         -          FADSYN-RXN           -            2.1e-40  136.1   0.0   1.7e-39  133.2   0.0   1.8   1   1   0   1   1   1   1 riboflavin kinase / FMN adenylyltransferase
PROKKA_00680         -          FARNESYLTRANSTRANSFERASE-RXN -            1.5e-61  205.4   0.0   1.7e-61  205.1   0.0   1.0   1   0   0   1   1   1   1 farnesyl-diphosphate synthase 
PROKKA_02481         -          FARNESYLTRANSTRANSFERASE-RXN -            6.7e-46  153.9   0.2   8.5e-46  153.6   0.2   1.1   1   0   0   1   1   1   1 octaprenyl-diphosphate synthase
PROKKA_02158         -          FATTY-ACID-SYNTHASE-RXN -            1.2e-23   79.3   1.8   1.4e-23   79.0   1.8   1.0   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier-protein] synthase II
PROKKA_02161         -          FATTY-ACID-SYNTHASE-RXN -            1.4e-13   45.8   0.0   1.8e-13   45.5   0.0   1.0   1   0   0   1   1   1   1 [acyl-carrier-protein] S-malonyltransferase
PROKKA_02161         -          FATTY-ACYL-COA-SYNTHASE-RXN -            1.1e-10   36.3   0.0   6.2e-08   27.2   0.0   2.5   3   0   0   3   3   3   2 [acyl-carrier-protein] S-malonyltransferase
PROKKA_01612         -          FERREDOXIN--NAD+-REDUCTASE-RXN -            4.4e-13   46.0   0.0   4.9e-13   45.8   0.0   1.1   1   0   0   1   1   1   1 Ferredoxin-NADP reductase
PROKKA_00765         -          FERREDOXIN--NAD+-REDUCTASE-RXN -              3e-09   33.4   0.0   3.5e-09   33.2   0.0   1.2   1   0   0   1   1   1   1 Ferredoxin-NADP reductase
PROKKA_00416         -          FERREDOXIN--NAD+-REDUCTASE-RXN -             0.0029   13.7   0.1    0.0032   13.6   0.1   1.1   1   0   0   1   1   1   1 ferredoxin, 2Fe-2S
PROKKA_01626         -          FGAMSYN-RXN          -           2.2e-183  608.6   0.0  2.9e-183  608.3   0.0   1.0   1   0   0   1   1   1   1 phosphoribosylformylglycinamidine synthase subunit II 
PROKKA_01625         -          FGAMSYN-RXN          -            1.8e-62  208.6   0.0   2.6e-62  208.0   0.0   1.2   1   1   0   1   1   1   1 phosphoribosylformylglycinamidine synthase
PROKKA_02242         -          FGAMSYN-RXN          -             0.0036   12.3   0.0    0.0048   11.9   0.0   1.1   1   0   0   1   1   1   1 putative glutamine amidotransferase
PROKKA_01612         -          FLAVONADPREDUCT-RXN  -            2.7e-14   50.4   0.0   3.6e-14   49.9   0.0   1.1   1   0   0   1   1   1   1 Ferredoxin-NADP reductase
PROKKA_00278         -          FLAVONADPREDUCT-RXN  -            3.2e-09   33.7   0.0   5.6e-09   32.9   0.0   1.4   1   0   0   1   1   1   1 NAD(P)H-flavin reductase
PROKKA_00765         -          FLAVONADPREDUCT-RXN  -            9.2e-06   22.3   0.0   1.3e-05   21.8   0.0   1.1   1   0   0   1   1   1   1 Ferredoxin-NADP reductase
PROKKA_01612         -          FMNREDUCT-RXN        -            9.1e-11   38.7   0.0   1.1e-10   38.4   0.0   1.1   1   0   0   1   1   1   1 Ferredoxin-NADP reductase
PROKKA_00278         -          FMNREDUCT-RXN        -            2.3e-07   27.6   0.0   3.4e-07   27.0   0.0   1.2   1   0   0   1   1   1   1 NAD(P)H-flavin reductase
PROKKA_00765         -          FMNREDUCT-RXN        -              4e-06   23.5   0.0   9.9e-06   22.2   0.0   1.6   1   1   0   1   1   1   1 Ferredoxin-NADP reductase
PROKKA_01948         -          FOLYLPOLYGLUTAMATESYNTH-RXN -            7.7e-75  249.4   0.0   9.9e-75  249.0   0.0   1.0   1   0   0   1   1   1   1 dihydrofolate synthase / folylpolyglutamate synthase
PROKKA_00595         -          FOLYLPOLYGLUTAMATESYNTH-RXN -            2.8e-09   33.3   0.0   4.1e-05   19.6   0.0   2.1   2   0   0   2   2   2   2 UDP-N-acetylmuramoylalanyl-D-glutamate--2,6-diaminopimelate ligase
PROKKA_00598         -          FOLYLPOLYGLUTAMATESYNTH-RXN -              9e-05   18.5   0.0   0.00062   15.7   0.0   2.0   2   0   0   2   2   2   1 UDP-N-acetylmuramoylalanine--D-glutamate ligase
PROKKA_00596         -          FOLYLPOLYGLUTAMATESYNTH-RXN -              0.015   11.2   0.6       1.1    4.9   0.1   2.3   2   0   0   2   2   2   0 UDP-N-acetylmuramoyl-tripeptide--D-alanyl-D-alanine ligase
PROKKA_00507         -          FORMALDEHYDE-DEHYDROGENASE-RXN -              2e-09   33.7   5.2   7.2e-09   31.8   5.2   1.8   1   1   0   1   1   1   1 putative zinc-type alcohol dehydrogenase-like protein
PROKKA_02069         -          FORMALDEHYDE-DEHYDROGENASE-RXN -              3e-09   33.1   1.7   5.9e-09   32.1   1.7   1.4   1   1   0   1   1   1   1 alcohol dehydrogenase, propanol-preferring
PROKKA_00329         -          FORMALDEHYDE-DEHYDROGENASE-RXN -            1.1e-08   31.2   0.2   1.8e-08   30.5   0.2   1.3   1   0   0   1   1   1   1 alcohol dehydrogenase, propanol-preferring
PROKKA_00645         -          FORMALDEHYDE-DEHYDROGENASE-RXN -            8.1e-06   21.8   0.1   1.6e-05   20.8   0.1   1.4   1   0   0   1   1   1   1 adenosylhomocysteinase
PROKKA_00022         -          FORMALDEHYDE-DEHYDROGENASE-RXN -             0.0019   14.0   0.0    0.0027   13.4   0.0   1.3   1   0   0   1   1   1   1 NADPH:quinone reductase
PROKKA_01239         -          FORMALDEHYDE-DEHYDROGENASE-RXN -             0.0029   13.4   2.3    0.0039   12.9   2.3   1.2   1   0   0   1   1   1   1 NADPH2:quinone reductase
PROKKA_01706         -          FORMALDEHYDE-TRANSKETOLASE-RXN -            2.5e-98  327.3   0.0   3.5e-98  326.8   0.0   1.1   1   0   0   1   1   1   1 transketolase
PROKKA_00681         -          FORMALDEHYDE-TRANSKETOLASE-RXN -            1.4e-09   34.0   0.1   6.5e-07   25.2   0.0   2.3   2   0   0   2   2   2   2 1-deoxy-D-xylulose-5-phosphate synthase
PROKKA_00693         -          FORMATEDEHYDROG-RXN  -            1.1e-22   78.2   0.0   1.4e-22   77.8   0.0   1.2   1   0   0   1   1   1   1 formate dehydrogenase major subunit
PROKKA_02173         -          FORMATETHFLIG-RXN    -             3e-204  677.9   0.0  7.7e-158  524.3   0.0   2.0   1   1   1   2   2   2   2 Formate-tetrahydrofolate ligase
PROKKA_01542         -          FORMATETHFLIG-RXN    -              5e-81  270.1   0.0     6e-81  269.9   0.0   1.0   1   0   0   1   1   1   1 methylenetetrahydrofolate dehydrogenase (NADP+) / methenyltetrahydrofolate cyclohydrolase
PROKKA_02411         -          FORMYLMETHIONINE-DEFORMYLASE-RXN -            5.5e-38  127.4   0.0   6.2e-38  127.2   0.0   1.0   1   0   0   1   1   1   1 peptide deformylase
PROKKA_02515         -          FORMYLMETHIONINE-DEFORMYLASE-RXN -            5.4e-36  120.9   0.0   6.3e-36  120.7   0.0   1.0   1   0   0   1   1   1   1 peptide deformylase
PROKKA_02410         -          FORMYLTETRAHYDROFOLATE-DEHYDROGENASE-RXN -            1.3e-21   73.8   0.0   2.4e-21   72.9   0.0   1.4   1   1   0   1   1   1   1 methionyl-tRNA formyltransferase
PROKKA_02153         -          FORMYLTETRAHYDROFOLATE-DEHYDROGENASE-RXN -            3.8e-08   29.3   0.0     9e-08   28.1   0.0   1.4   1   1   0   2   2   2   1 phosphoribosylglycinamide formyltransferase-1
PROKKA_02153         -          FORMYLTHFDEFORMYL-RXN -            3.1e-31  106.0   0.0   3.4e-31  105.9   0.0   1.1   1   0   0   1   1   1   1 phosphoribosylglycinamide formyltransferase-1
PROKKA_02410         -          FORMYLTHFDEFORMYL-RXN -            8.3e-11   39.0   0.0   1.7e-10   38.0   0.0   1.4   2   0   0   2   2   2   1 methionyl-tRNA formyltransferase
PROKKA_00680         -          FPPSYN-RXN           -            2.8e-49  165.1   0.0   3.6e-49  164.7   0.0   1.1   1   0   0   1   1   1   1 farnesyl-diphosphate synthase 
PROKKA_02481         -          FPPSYN-RXN           -            2.8e-34  115.8   0.1   3.8e-34  115.4   0.1   1.2   1   0   0   1   1   1   1 octaprenyl-diphosphate synthase
PROKKA_01879         -          FRUCTOKINASE-RXN     -            9.3e-09   32.2   0.4   8.1e-05   19.2   0.0   2.1   2   0   0   2   2   2   2 D-beta-D-heptose 7-phosphate kinase / D-beta-D-heptose 1-phosphate adenosyltransferase
PROKKA_00904         -          FRUCTOKINASE-RXN     -              1e-06   25.5   0.0   1.7e-06   24.7   0.0   1.3   1   0   0   1   1   1   1 Sugar or nucleoside kinase, ribokinase family
PROKKA_02117         -          FUCPALDOL-RXN        -            2.8e-27   92.9   0.0     4e-27   92.4   0.0   1.2   1   0   0   1   1   1   1 methylthioribulose-1-phosphate dehydratase
PROKKA_01556         -          FUMHYDR-RXN          -           2.6e-181  600.8   0.0  3.2e-181  600.5   0.0   1.0   1   0   0   1   1   1   1 fumarase, class II
PROKKA_01372         -          FUMHYDR-RXN          -            7.3e-05   19.3   0.0    0.0001   18.8   0.0   1.2   1   0   0   1   1   1   1 Adenylosuccinate lyase 
PROKKA_02355         -          FUMHYDR-RXN          -              0.011   12.1   0.0     0.032   10.6   0.0   1.6   1   1   0   1   1   1   0 argininosuccinate lyase 
PROKKA_02358         -          GABATRANSAM-RXN      -            1.8e-69  231.5   0.0     2e-66  221.5   0.0   2.8   1   1   0   1   1   1   1 acetylornithine/N-succinyldiaminopimelate aminotransferase
PROKKA_00349         -          GABATRANSAM-RXN      -            7.8e-69  229.4   0.0   6.2e-68  226.5   0.0   1.9   1   1   0   1   1   1   1 diaminobutyrate-2-oxoglutarate transaminase
PROKKA_01321         -          GABATRANSAM-RXN      -            2.8e-47  158.3   0.0   6.2e-47  157.2   0.0   1.4   1   1   0   1   1   1   1 adenosylmethionine-8-amino-7-oxononanoate aminotransferase
PROKKA_02461         -          GABATRANSAM-RXN      -              4e-31  105.1   0.0   2.5e-21   72.8   0.0   2.6   2   1   0   2   2   2   2 glutamate-1-semialdehyde 2,1-aminomutase
PROKKA_02372         -          GALACTOACETYLTRAN-RXN -            9.6e-31  104.2   0.0   1.3e-30  103.8   0.0   1.0   1   0   0   1   1   1   1 ribose 5-phosphate isomerase B
PROKKA_01777         -          GALACTOKIN-RXN       -            9.3e-08   28.3   0.0    0.0015   14.4   0.0   2.1   2   0   0   2   2   2   2 D-glycero-alpha-D-manno-heptose-7-phosphate kinase
PROKKA_01843         -          GALACTONOLACTONE-DEHYDROGENASE-RXN -                2.7    3.7   9.5       3.6    3.3   9.5   1.1   1   0   0   1   1   1   0 hypothetical protein
PROKKA_01495         -          GALACTOSE-OXIDASE-RXN -            1.8e-07   27.2   0.5     0.021   10.5   0.1   3.7   3   1   0   3   3   3   2 N-acetylneuraminic acid mutarotase
PROKKA_00757         -          GALACTOSE-OXIDASE-RXN -            5.4e-07   25.6   0.0     0.019   10.7   0.0   3.0   1   1   2   3   3   3   2 Galactose oxidase, central domain
PROKKA_02426         -          GALACTURIDYLYLTRANS-RXN -            3.6e-41  138.7   0.0   4.9e-41  138.2   0.0   1.2   1   0   0   1   1   1   1 UDPglucose--hexose-1-phosphate uridylyltransferase
PROKKA_00052         -          GAPOXNPHOSPHN-RXN    -           4.4e-136  450.8   0.0  4.9e-136  450.6   0.0   1.0   1   0   0   1   1   1   1 glyceraldehyde 3-phosphate dehydrogenase
PROKKA_01296         -          GAPOXNPHOSPHN-RXN    -               0.03   10.8   0.1     0.042   10.4   0.1   1.2   1   0   0   1   1   1   0 putative dehydrogenase
PROKKA_02154         -          GART-RXN             -           1.4e-100  334.6   0.0  1.8e-100  334.3   0.0   1.0   1   0   0   1   1   1   1 phosphoribosylformylglycinamidine cyclo-ligase
PROKKA_01333         -          GART-RXN             -            1.5e-93  311.3   0.0   1.8e-93  311.1   0.0   1.0   1   0   0   1   1   1   1 phosphoribosylamine--glycine ligase
PROKKA_02153         -          GART-RXN             -              4e-60  200.7   0.0   4.9e-60  200.4   0.0   1.0   1   0   0   1   1   1   1 phosphoribosylglycinamide formyltransferase-1
PROKKA_02410         -          GART-RXN             -              2e-12   42.8   0.0   2.5e-12   42.5   0.0   1.0   1   0   0   1   1   1   1 methionyl-tRNA formyltransferase
PROKKA_00123         -          GART-RXN             -             0.0019   13.1   0.0    0.0026   12.7   0.0   1.1   1   0   0   1   1   1   1 thiamine-monophosphate kinase
PROKKA_01593         -          GART-RXN             -             0.0034   12.2   0.0    0.0047   11.8   0.0   1.1   1   0   0   1   1   1   1 acetyl-CoA carboxylase, biotin carboxylase subunit
PROKKA_00190         -          GCVP-RXN             -           1.3e-136  453.9   0.0  1.7e-136  453.6   0.0   1.0   1   0   0   1   1   1   1 glycine dehydrogenase (decarboxylating) beta subunit 
PROKKA_00189         -          GCVP-RXN             -            1.7e-91  304.6   0.0     2e-91  304.4   0.0   1.0   1   0   0   1   1   1   1 glycine dehydrogenase subunit 1
PROKKA_00187         -          GCVT-RXN             -              9e-54  179.8   0.0   1.1e-53  179.5   0.0   1.1   1   0   0   1   1   1   1 aminomethyltransferase
PROKKA_00190         -          GCVT-RXN             -            2.8e-19   66.2   0.0   3.2e-19   66.0   0.0   1.1   1   0   0   1   1   1   1 glycine dehydrogenase (decarboxylating) beta subunit 
PROKKA_02421         -          GCVT-RXN             -            2.7e-05   20.1   0.0   4.4e-05   19.4   0.0   1.4   2   0   0   2   2   2   1 folate-binding protein YgfZ
PROKKA_01528         -          GDP-MANNOSE-6-DEHYDROGENASE-RXN -            7.9e-66  219.8   0.1     1e-65  219.4   0.1   1.0   1   0   0   1   1   1   1 UDPglucose 6-dehydrogenase
PROKKA_00570         -          GDP-MANNOSE-6-DEHYDROGENASE-RXN -            6.7e-27   91.5   0.0   8.9e-27   91.1   0.0   1.1   1   0   0   1   1   1   1 UDP-N-acetyl-D-glucosamine dehydrogenase
PROKKA_00488         -          GDPMANDEHYDRA-RXN    -            6.1e-13   45.5   0.0   7.7e-13   45.2   0.0   1.0   1   0   0   1   1   1   1 dTDP-glucose 4,6-dehydratase
PROKKA_00656         -          GDPMANDEHYDRA-RXN    -            7.7e-12   41.9   0.0   5.1e-10   35.9   0.0   2.3   1   1   1   2   2   2   2 UDP-galactose 4-epimerase
PROKKA_00572         -          GDPMANDEHYDRA-RXN    -            5.8e-09   32.5   0.0   1.4e-08   31.2   0.0   1.5   2   0   0   2   2   2   1 UDP-glucose 4-epimerase
PROKKA_00620         -          GDPMANDEHYDRA-RXN    -            8.8e-07   25.3   0.0   1.1e-06   24.9   0.0   1.1   1   0   0   1   1   1   1 dihydroflavonol-4-reductase
PROKKA_00566         -          GDPMANDEHYDRA-RXN    -            7.4e-05   19.0   0.0    0.0001   18.5   0.0   1.2   1   0   0   1   1   1   1 UDP-glucuronate 4-epimerase
PROKKA_00683         -          GDPMANDEHYDRA-RXN    -            0.00022   17.4   0.0    0.0018   14.4   0.0   2.2   2   1   0   2   2   2   1 UDP-glucose 4-epimerase
PROKKA_01629         -          GDPMANDEHYDRA-RXN    -            0.00035   16.8   0.0    0.0005   16.2   0.0   1.2   1   0   0   1   1   1   1 ADP-L-glycero-D-manno-heptose 6-epimerase
PROKKA_01526         -          GDPMANDEHYDRA-RXN    -             0.0022   14.1   0.0      0.14    8.2   0.0   2.2   2   0   0   2   2   2   2 UDP-glucuronate decarboxylase
PROKKA_00891         -          GENTAMICIN-2-N-ACETYLTRANSFERASE-RXN -             0.0024   15.1   0.0    0.0031   14.7   0.0   1.2   1   0   0   1   1   1   1 putative N-acetyltransferase YhbS
PROKKA_01704         -          GLU6PDEHYDROG-RXN    -           5.5e-152  504.0   0.0  6.6e-152  503.8   0.0   1.0   1   0   0   1   1   1   1 glucose-6-phosphate 1-dehydrogenase
PROKKA_02150         -          GLU6PDEHYDROG-RXN    -           1.5e-124  413.5   0.0  1.8e-124  413.3   0.0   1.0   1   0   0   1   1   1   1 glucose-6-phosphate 1-dehydrogenase
PROKKA_02429         -          GLUC1PADENYLTRANS-RXN -             8e-115  381.4   0.0  1.6e-114  380.4   0.0   1.4   1   1   0   1   1   1   1 glucose-1-phosphate adenylyltransferase
PROKKA_01782         -          GLUC1PADENYLTRANS-RXN -            2.1e-11   40.3   0.0   9.1e-11   38.2   0.0   1.8   1   1   0   1   1   1   1 D-glycero-alpha-D-manno-heptose 1-phosphate guanylyltransferase
PROKKA_00489         -          GLUC1PADENYLTRANS-RXN -            1.5e-08   30.9   0.1   6.3e-05   19.0   0.0   2.1   2   0   0   2   2   2   2 Glucose-1-phosphate thymidylyltransferase
PROKKA_00537         -          GLUC1PADENYLTRANS-RXN -            1.8e-05   20.8   0.0   3.7e-05   19.7   0.0   1.4   1   1   0   1   1   1   1 UTP--glucose-1-phosphate uridylyltransferase
PROKKA_00121         -          GLUC1PADENYLTRANS-RXN -            0.00021   17.3   0.0     0.014   11.2   0.0   2.1   2   0   0   2   2   2   2 UDP-glucose pyrophosphorylase 
PROKKA_00121         -          GLUC1PURIDYLTRANS-RXN -            9.2e-97  321.0   0.0   1.1e-96  320.8   0.0   1.0   1   0   0   1   1   1   1 UDP-glucose pyrophosphorylase 
PROKKA_00537         -          GLUC1PURIDYLTRANS-RXN -            1.6e-92  307.1   0.0   1.9e-92  306.9   0.0   1.0   1   0   0   1   1   1   1 UTP--glucose-1-phosphate uridylyltransferase
PROKKA_01782         -          GLUC1PURIDYLTRANS-RXN -            4.3e-07   26.7   0.0   1.2e-06   25.3   0.0   1.6   2   0   0   2   2   2   1 D-glycero-alpha-D-manno-heptose 1-phosphate guanylyltransferase
PROKKA_00489         -          GLUC1PURIDYLTRANS-RXN -            1.4e-06   25.0   0.0   1.5e-05   21.7   0.0   2.1   1   1   0   1   1   1   1 Glucose-1-phosphate thymidylyltransferase
PROKKA_01708         -          GLUCOKIN-RXN         -            7.2e-20   69.0   0.0   9.5e-20   68.6   0.0   1.0   1   0   0   1   1   1   1 glucokinase
PROKKA_02160         -          GLUCONATE-5-DEHYDROGENASE-RXN -            2.4e-37  125.8   4.5   2.6e-37  125.7   4.5   1.0   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier-protein] reductase
PROKKA_00235         -          GLUCONATE-5-DEHYDROGENASE-RXN -            6.4e-26   88.4   0.1   8.6e-26   88.0   0.1   1.1   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier protein] reductase
PROKKA_00773         -          GLUCONATE-5-DEHYDROGENASE-RXN -            8.5e-15   52.0   0.0   1.1e-14   51.5   0.0   1.0   1   0   0   1   1   1   1 short chain dehydrogenase
PROKKA_02149         -          GLUCONATE-5-DEHYDROGENASE-RXN -            5.5e-11   39.5   0.0   7.5e-11   39.0   0.0   1.1   1   0   0   1   1   1   1 NADP-dependent 3-hydroxy acid dehydrogenase YdfG
PROKKA_00170         -          GLUCONATE-5-DEHYDROGENASE-RXN -            4.1e-05   20.2   0.0   9.2e-05   19.1   0.0   1.6   1   1   0   1   1   1   1 NAD(P)-dependent dehydrogenase, short-chain alcohol dehydrogenase family
PROKKA_00820         -          GLUCONATE-5-DEHYDROGENASE-RXN -            0.00024   17.7   0.2   0.00029   17.4   0.2   1.1   1   0   0   1   1   1   1 Enoyl-[acyl-carrier-protein] reductase [NADH]
PROKKA_01494         -          GLUCONOLACT-RXN      -            4.6e-11   39.4   1.5   7.6e-09   32.1   0.5   4.0   1   1   0   2   2   2   2 40-residue YVTN family beta-propeller repeat-containing protein
PROKKA_01528         -          GLUCOSE-1-DEHYDROGENASE-NAD+-RXN -            7.9e-66  220.0   0.0   5.2e-65  217.3   0.0   1.8   1   1   0   1   1   1   1 UDPglucose 6-dehydrogenase
PROKKA_00570         -          GLUCOSE-1-DEHYDROGENASE-NAD+-RXN -            3.8e-14   49.5   0.0     5e-14   49.1   0.0   1.0   1   0   0   1   1   1   1 UDP-N-acetyl-D-glucosamine dehydrogenase
PROKKA_02160         -          GLUCOSE-1-DEHYDROGENASE-RXN -            2.6e-12   44.1   0.3   2.4e-10   37.6   0.1   2.0   1   1   1   2   2   2   2 3-oxoacyl-[acyl-carrier-protein] reductase
PROKKA_00235         -          GLUCOSE-1-DEHYDROGENASE-RXN -            0.00042   17.1   0.0     0.013   12.2   0.0   2.0   1   1   0   1   1   1   1 3-oxoacyl-[acyl-carrier protein] reductase
PROKKA_02445         -          GLURS-RXN            -           2.6e-141  468.8   0.0    3e-141  468.6   0.0   1.0   1   0   0   1   1   1   1 glutamyl-tRNA synthetase 
PROKKA_02584         -          GLURS-RXN            -             0.0097   11.8   0.0     0.011   11.6   0.0   1.0   1   0   0   1   1   1   1 YqaJ-like viral recombinase domain protein
PROKKA_02391         -          GLUTAMATE-N-ACETYLTRANSFERASE-RXN -           4.4e-145  480.8   1.4  5.6e-145  480.4   1.4   1.0   1   0   0   1   1   1   1 glutamate N-acetyltransferase
PROKKA_01551         -          GLUTAMATE-SYNTHASE-FERREDOXIN-RXN -             3e-205  681.7   0.0  4.9e-205  680.9   0.0   1.3   1   0   0   1   1   1   1 glutamate synthase (NADPH/NADH) large chain
PROKKA_01551         -          GLUTAMATE-SYNTHASE-NADH-RXN -           1.3e-189  629.6   0.0  4.1e-121  402.5   0.4   2.4   2   1   0   2   2   2   2 glutamate synthase (NADPH/NADH) large chain
PROKKA_01944         -          GLUTAMATE-SYNTHASE-NADH-RXN -            9.1e-51  169.2   0.0   2.2e-49  164.6   0.0   2.6   2   1   0   2   2   2   1 NADPH-dependent glutamate synthase beta chain
PROKKA_01451         -          GLUTAMATE-SYNTHASE-NADH-RXN -              1e-49  165.7   0.0   1.6e-49  165.0   0.0   1.1   1   0   0   1   1   1   1 NADH-quinone oxidoreductase subunit F
PROKKA_00559         -          GLUTAMATE-SYNTHASE-NADH-RXN -            3.4e-05   18.0   0.1    0.0014   12.7   0.0   2.1   2   0   0   2   2   2   2 mercuric reductase
PROKKA_02417         -          GLUTAMATE-SYNTHASE-NADH-RXN -            0.00024   15.2   0.2     0.041    7.8   0.3   2.6   1   1   1   2   2   2   2 thioredoxin reductase (NADPH)
PROKKA_01157         -          GLUTAMATE-SYNTHASE-NADH-RXN -              0.022    8.7   0.7      0.42    4.5   0.3   2.1   2   0   0   2   2   2   0 mercuric reductase
PROKKA_00255         -          GLUTAMATE-SYNTHASE-NADH-RXN -              0.055    7.4   0.0     0.071    7.0   0.0   1.0   1   0   0   1   1   1   0 methylenetetrahydrofolate--tRNA-(uracil-5-)-methyltransferase
PROKKA_01551         -          GLUTAMATESYN-RXN     -           4.4e-131  436.1   0.0  5.8e-131  435.8   0.0   1.0   1   0   0   1   1   1   1 glutamate synthase (NADPH/NADH) large chain
PROKKA_01451         -          GLUTAMATESYN-RXN     -            6.8e-07   25.3   0.0    0.0072   11.9   0.0   2.4   1   1   1   2   2   2   2 NADH-quinone oxidoreductase subunit F
PROKKA_01944         -          GLUTAMATESYN-RXN     -            1.3e-06   24.3   0.1     5e-05   19.1   0.0   2.7   2   1   0   3   3   3   1 NADPH-dependent glutamate synthase beta chain
PROKKA_02445         -          GLUTAMINE--TRNA-LIGASE-RXN -            8.1e-36  120.9   0.0   5.7e-29   98.2   0.0   2.5   2   1   0   2   2   2   2 glutamyl-tRNA synthetase 
PROKKA_01907         -          GLUTAMINESYN-RXN     -           1.4e-104  347.3   0.0  1.6e-104  347.1   0.0   1.0   1   0   0   1   1   1   1 glutamine synthetase
PROKKA_02380         -          GLUTATHIONE-PEROXIDASE-RXN -            3.6e-06   23.5   0.0   2.9e-05   20.5   0.0   2.0   1   1   1   2   2   2   2 Peroxiredoxin
PROKKA_02379         -          GLUTATHIONE-PEROXIDASE-RXN -            0.00053   16.4   0.0   0.00088   15.7   0.0   1.3   1   0   0   1   1   1   1 Peroxiredoxin
PROKKA_02519         -          GLUTATHIONE-PEROXIDASE-RXN -             0.0024   14.3   0.0    0.0042   13.5   0.0   1.3   1   0   0   1   1   1   1 Thiol-disulfide isomerase or thioredoxin
PROKKA_00559         -          GLUTATHIONE-REDUCT-NADPH-RXN -            2.3e-86  287.7   0.0   3.1e-86  287.2   0.0   1.0   1   0   0   1   1   1   1 mercuric reductase
PROKKA_01157         -          GLUTATHIONE-REDUCT-NADPH-RXN -              3e-84  280.7   0.0     4e-84  280.3   0.0   1.0   1   0   0   1   1   1   1 mercuric reductase
PROKKA_01909         -          GLUTATHIONE-REDUCT-NADPH-RXN -              1e-80  269.1   0.0   1.1e-80  268.9   0.0   1.0   1   0   0   1   1   1   1 dihydrolipoamide dehydrogenase
PROKKA_00137         -          GLUTATHIONE-REDUCT-NADPH-RXN -            1.3e-80  268.7   0.0   1.6e-80  268.4   0.0   1.0   1   0   0   1   1   1   1 dihydrolipoamide dehydrogenase
PROKKA_01391         -          GLUTATHIONE-REDUCT-NADPH-RXN -            2.2e-65  218.5   0.0   2.8e-65  218.2   0.0   1.0   1   0   0   1   1   1   1 dihydrolipoamide dehydrogenase
PROKKA_01201         -          GLUTATHIONE-REDUCT-NADPH-RXN -            2.8e-15   53.3   0.0   4.4e-15   52.7   0.0   1.1   1   0   0   1   1   1   1 nitrite reductase (NADH) large subunit
PROKKA_02417         -          GLUTATHIONE-REDUCT-NADPH-RXN -            1.1e-12   44.8   0.3   1.1e-10   38.1   0.0   2.5   2   1   1   3   3   3   2 thioredoxin reductase (NADPH)
PROKKA_01944         -          GLUTATHIONE-REDUCT-NADPH-RXN -            9.9e-10   35.0   0.8   1.2e-06   24.9   0.2   2.4   2   0   0   2   2   2   2 NADPH-dependent glutamate synthase beta chain
PROKKA_01451         -          GLUTATHIONE-REDUCT-NADPH-RXN -            4.5e-07   26.3   3.0     7e-05   19.1   0.6   3.1   2   1   0   2   2   2   1 NADH-quinone oxidoreductase subunit F
PROKKA_00585         -          GLUTATHIONE-REDUCT-NADPH-RXN -             0.0022   14.1   0.3    0.0022   14.1   0.3   1.7   3   0   0   3   3   3   1 succinate dehydrogenase / fumarate reductase flavoprotein subunit
PROKKA_01719         -          GLUTATHIONE-REDUCT-NADPH-RXN -              0.021   10.9   0.4     0.031   10.4   0.4   1.2   1   0   0   1   1   1   0 oxygen-dependent protoporphyrinogen oxidase
PROKKA_00031         -          GLUTATHIONE-REDUCT-NADPH-RXN -              0.045    9.8   0.2       2.2    4.2   0.0   2.0   2   0   0   2   2   2   0 sulfide:quinone oxidoreductase
PROKKA_00735         -          GLUTATHIONE-REDUCT-NADPH-RXN -              0.099    8.7   1.8      0.14    8.2   1.8   1.2   1   0   0   1   1   1   0 C-3',4' desaturase CrtD
PROKKA_00495         -          GLUTDECARBOX-RXN     -           1.1e-108  361.0   0.0  1.3e-108  360.8   0.0   1.0   1   0   0   1   1   1   1 glutamate decarboxylase
PROKKA_00421         -          GLUTDECARBOX-RXN     -             0.0028   13.7   0.0    0.0044   13.1   0.0   1.2   1   0   0   1   1   1   1 cysteine desulfurase
PROKKA_00876         -          GLUTDEHYD-RXN        -               0.01   11.9   0.1     0.018   11.1   0.1   1.3   1   0   0   1   1   1   0 malate dehydrogenase (NAD)
PROKKA_00045         -          GLUTDEHYD-RXN        -              0.025   10.6   0.1     0.035   10.2   0.1   1.1   1   0   0   1   1   1   0 precorrin-2 dehydrogenase / sirohydrochlorin ferrochelatase
PROKKA_01451         -          GLUTDEHYD-RXN        -              0.029   10.4   0.3      0.09    8.8   0.1   1.7   2   0   0   2   2   2   0 NADH-quinone oxidoreductase subunit F
PROKKA_00105         -          GLUTKIN-RXN          -           1.2e-102  340.9   0.0  1.5e-102  340.5   0.0   1.0   1   0   0   1   1   1   1 glutamate 5-kinase
PROKKA_01357         -          GLUTKIN-RXN          -            2.5e-07   27.1   0.1   5.1e-07   26.1   0.1   1.6   1   1   0   1   1   1   1 aspartate kinase
PROKKA_02388         -          GLUTKIN-RXN          -              4e-06   23.2   0.0   5.5e-05   19.4   0.0   2.2   1   1   0   1   1   1   1 uridylate kinase
PROKKA_02243         -          GLUTRACE-RXN         -            8.8e-68  225.6   0.0   1.1e-67  225.3   0.0   1.0   1   0   0   1   1   1   1 glutamate racemase
PROKKA_00106         -          GLUTSEMIALDEHYDROG-RXN -           1.3e-141  469.5   0.0  1.6e-141  469.2   0.0   1.0   1   0   0   1   1   1   1 glutamate-5-semialdehyde dehydrogenase
PROKKA_01320         -          GLY-X-CARBOXYPEPTIDASE-RXN -              0.021   10.2   0.0     0.028    9.8   0.0   1.1   1   0   0   1   1   1   0 hypothetical protein
PROKKA_00137         -          GLYC3PDEHYDROG-RXN   -            1.4e-05   21.5   3.3     0.021   11.1   0.1   2.1   2   0   0   2   2   2   2 dihydrolipoamide dehydrogenase
PROKKA_01854         -          GLYC3PDEHYDROG-RXN   -            5.9e-05   19.4   0.1   0.00016   18.0   0.1   1.6   1   1   0   1   1   1   1 L-2-hydroxyglutarate oxidase LhgO
PROKKA_01529         -          GLYC3PDEHYDROG-RXN   -             0.0002   17.7   0.0    0.0015   14.8   0.1   2.0   2   0   0   2   2   2   1 glycine oxidase
PROKKA_00255         -          GLYC3PDEHYDROG-RXN   -            0.00054   16.3   0.0   0.00076   15.8   0.0   1.1   1   0   0   1   1   1   1 methylenetetrahydrofolate--tRNA-(uracil-5-)-methyltransferase
PROKKA_01719         -          GLYC3PDEHYDROG-RXN   -            0.00071   15.9   1.0    0.0011   15.3   1.0   1.1   1   0   0   1   1   1   1 oxygen-dependent protoporphyrinogen oxidase
PROKKA_01909         -          GLYC3PDEHYDROG-RXN   -             0.0026   14.0   0.1    0.0048   13.2   0.1   1.4   1   0   0   1   1   1   1 dihydrolipoamide dehydrogenase
PROKKA_00427         -          GLYC3PDEHYDROG-RXN   -             0.0096   12.1   0.6      0.72    6.0   0.1   2.2   2   0   0   2   2   2   2 dissimilatory adenylylsulfate reductase alpha subunit precursor
PROKKA_00735         -          GLYC3PDEHYDROG-RXN   -              0.027   10.7   1.3     0.038   10.2   1.3   1.2   1   0   0   1   1   1   0 C-3',4' desaturase CrtD
PROKKA_01157         -          GLYC3PDEHYDROG-RXN   -               0.26    7.4   3.2      0.41    6.8   1.4   2.1   3   0   0   3   3   3   0 mercuric reductase
PROKKA_01511         -          GLYCERATE-DEHYDROGENASE-RXN -            3.4e-36  122.1   0.4   1.1e-34  117.0   0.4   2.1   1   1   0   1   1   1   1 D-3-phosphoglycerate dehydrogenase
PROKKA_01575         -          GLYCINE--TRNA-LIGASE-RXN -              2e-51  172.5   0.0   2.9e-51  171.9   0.0   1.2   1   0   0   1   1   1   1 glycyl-tRNA synthetase beta chain
PROKKA_01576         -          GLYCINE--TRNA-LIGASE-RXN -            3.9e-25   85.7   0.0   4.3e-25   85.6   0.0   1.0   1   0   0   1   1   1   1 glycyl-tRNA synthetase alpha chain
PROKKA_02166         -          GLYCINE--TRNA-LIGASE-RXN -             0.0029   13.5   0.0    0.0041   13.0   0.0   1.1   1   0   0   1   1   1   1 prolyl-tRNA synthetase
PROKKA_00090         -          GLYCINE--TRNA-LIGASE-RXN -              0.008   12.1   0.0      0.11    8.3   0.0   2.6   2   1   2   4   4   4   1 threonyl-tRNA synthetase
PROKKA_00011         -          GLYCINE-N-METHYLTRANSFERASE-RXN -              0.002   14.6   0.0    0.0042   13.6   0.0   1.5   2   0   0   2   2   2   1 HNH endonuclease
PROKKA_00496         -          GLYCOGEN-BRANCH-RXN  -              2e-27   93.1   0.0   3.8e-27   92.2   0.0   1.3   1   0   0   1   1   1   1 maltooligosyl trehalose hydrolase
PROKKA_02234         -          GLYCOGEN-BRANCH-RXN  -            1.9e-23   79.9   0.2   2.8e-23   79.4   0.2   1.1   1   0   0   1   1   1   1 glycogen operon protein
PROKKA_00499         -          GLYCOGEN-BRANCH-RXN  -            2.6e-12   43.1   0.2   1.1e-10   37.8   0.0   2.1   2   0   0   2   2   2   2 maltose alpha-D-glucosyltransferase/ alpha-amylase
PROKKA_02322         -          GLYCOGEN-BRANCH-RXN  -              0.026   10.0   0.0     0.034    9.7   0.0   1.1   1   0   0   1   1   1   0 inorganic pyrophosphatase
PROKKA_01480         -          GLYCOGENSYN-RXN      -             3e-121  402.7   0.0  3.7e-121  402.4   0.0   1.0   1   0   0   1   1   1   1 starch synthase
PROKKA_00545         -          GLYCOGENSYN-RXN      -            1.3e-05   20.8   0.0   1.8e-05   20.4   0.0   1.1   1   0   0   1   1   1   1 Glycosyltransferase involved in cell wall bisynthesis
PROKKA_01816         -          GLYCOGENSYN-RXN      -            0.00016   17.2   0.0   0.00041   15.9   0.0   1.5   2   0   0   2   2   2   1 Glycosyltransferase involved in cell wall bisynthesis
PROKKA_01317         -          GLYCOGENSYN-RXN      -             0.0012   14.3   0.0    0.0017   13.9   0.0   1.1   1   0   0   1   1   1   1 Glycosyltransferase involved in cell wall bisynthesis
PROKKA_02160         -          GLYCOGENSYN-RXN      -             0.0093   11.4   0.0    0.0089   11.5   0.0   1.1   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier-protein] reductase
PROKKA_01632         -          GLYCOGENSYN-RXN      -              0.013   11.0   0.0      0.04    9.3   0.0   1.8   2   0   0   2   2   2   0 Glycosyltransferase involved in cell wall bisynthesis
PROKKA_02541         -          GLYOCARBOLIG-RXN     -            2.3e-97  324.0   0.0   3.1e-97  323.6   0.0   1.1   1   0   0   1   1   1   1 acetolactate synthase, large subunit
PROKKA_02373         -          GLYOHMETRANS-RXN     -           5.2e-186  615.6   0.0  5.8e-186  615.4   0.0   1.0   1   0   0   1   1   1   1 glycine hydroxymethyltransferase
PROKKA_00083         -          GLYOXI-RXN           -            6.2e-14   49.4   0.0   1.7e-06   25.2   0.0   2.1   2   0   0   2   2   2   2 lactoylglutathione lyase
PROKKA_01235         -          GLYOXI-RXN           -            4.4e-07   27.1   0.0   0.00015   18.9   0.0   2.0   1   1   1   2   2   2   2 Glyoxalase-like domain protein
PROKKA_02530         -          GLYOXI-RXN           -            2.5e-06   24.7   0.0   0.00093   16.3   0.0   2.2   2   0   0   2   2   2   2 glyoxylase I family protein
PROKKA_00070         -          GLYOXII-RXN          -            2.6e-16   57.1   0.0     5e-16   56.2   0.0   1.3   1   1   0   1   1   1   1 Glyoxylase, beta-lactamase superfamily II
PROKKA_02231         -          GLYOXII-RXN          -            1.8e-07   28.1   0.0   2.2e-07   27.9   0.0   1.2   1   0   0   1   1   1   1 Glyoxylase, beta-lactamase superfamily II
PROKKA_01227         -          GLYOXII-RXN          -            2.6e-07   27.6   0.0     4e-07   27.0   0.0   1.2   1   0   0   1   1   1   1 Glyoxylase, beta-lactamase superfamily II
PROKKA_00781         -          GLYOXII-RXN          -             0.0042   13.8   0.0    0.0085   12.8   0.0   1.4   1   0   0   1   1   1   1 phosphoribosyl 1,2-cyclic phosphate phosphodiesterase
PROKKA_00669         -          GLYOXII-RXN          -             0.0062   13.3   0.0     0.028   11.1   0.0   1.9   2   0   0   2   2   2   1 ribonuclease J
PROKKA_01333         -          GLYRIBONUCSYN-RXN    -           2.3e-110  367.1   0.0    3e-110  366.7   0.0   1.0   1   0   0   1   1   1   1 phosphoribosylamine--glycine ligase
PROKKA_02154         -          GLYRIBONUCSYN-RXN    -           1.6e-108  361.0   0.0  2.2e-108  360.6   0.0   1.0   1   0   0   1   1   1   1 phosphoribosylformylglycinamidine cyclo-ligase
PROKKA_01593         -          GLYRIBONUCSYN-RXN    -            2.8e-08   29.5   0.0   3.9e-08   29.1   0.0   1.1   1   0   0   1   1   1   1 acetyl-CoA carboxylase, biotin carboxylase subunit
PROKKA_00123         -          GLYRIBONUCSYN-RXN    -            4.4e-05   18.9   0.0   7.1e-05   18.3   0.0   1.3   1   0   0   1   1   1   1 thiamine-monophosphate kinase
PROKKA_00578         -          GLYRIBONUCSYN-RXN    -            5.8e-05   18.6   0.4   7.7e-05   18.1   0.4   1.1   1   0   0   1   1   1   1 citryl-CoA synthetase large subunit
PROKKA_01626         -          GLYRIBONUCSYN-RXN    -             0.0061   11.9   0.2     0.048    8.9   0.1   2.1   2   0   0   2   2   2   1 phosphoribosylformylglycinamidine synthase subunit II 
PROKKA_01901         -          GMP-REDUCT-RXN       -            6.1e-74  246.3   1.1   2.2e-64  214.8   2.2   2.1   2   0   0   2   2   2   2 IMP dehydrogenase
PROKKA_02264         -          GMP-REDUCT-RXN       -            0.00016   17.9   0.1     0.055    9.6   0.0   2.1   2   0   0   2   2   2   2 cyclase
PROKKA_01902         -          GMP-SYN-GLUT-RXN     -           1.3e-203  674.3   0.0  1.5e-203  674.1   0.0   1.0   1   0   0   1   1   1   1 GMP synthase (glutamine-hydrolysing)
PROKKA_01955         -          GMP-SYN-GLUT-RXN     -            5.4e-23   78.4   0.0   6.2e-23   78.2   0.0   1.0   1   0   0   1   1   1   1 anthranilate synthase, component II 
PROKKA_01519         -          GMP-SYN-GLUT-RXN     -            4.4e-11   39.1   0.0   5.6e-11   38.7   0.0   1.1   1   0   0   1   1   1   1 GMP synthase (glutamine-hydrolysing)
PROKKA_02242         -          GMP-SYN-GLUT-RXN     -            2.2e-08   30.2   0.0   2.8e-08   29.8   0.0   1.1   1   0   0   1   1   1   1 putative glutamine amidotransferase
PROKKA_01918         -          GMP-SYN-GLUT-RXN     -            2.5e-08   30.0   0.0   4.9e-08   29.0   0.0   1.4   1   1   0   1   1   1   1 carbamoyl-phosphate synthase small subunit
PROKKA_01367         -          GMP-SYN-GLUT-RXN     -            5.2e-08   28.9   0.0   7.4e-08   28.4   0.0   1.1   1   0   0   1   1   1   1 NAD+ synthase (glutamine-hydrolysing)
PROKKA_02266         -          GMP-SYN-GLUT-RXN     -            2.8e-05   19.9   0.0   0.00013   17.7   0.0   1.7   1   1   0   1   1   1   1 glutamine amidotransferase
PROKKA_02496         -          GMP-SYN-GLUT-RXN     -            5.6e-05   18.9   0.2    0.0011   14.7   0.1   2.4   2   0   0   2   2   2   1 tRNA-specific 2-thiouridylase
PROKKA_00007         -          GMP-SYN-GLUT-RXN     -             0.0054   12.4   0.0     0.013   11.1   0.0   1.5   2   0   0   2   2   2   1 7-cyano-7-deazaguanine synthase
PROKKA_00369         -          GMP-SYN-GLUT-RXN     -               0.01   11.5   0.0     0.018   10.6   0.0   1.4   2   0   0   2   2   2   0 TIGR00269 family protein
PROKKA_00059         -          GPH-RXN              -            1.5e-40  136.2   0.0   1.8e-40  136.0   0.0   1.0   1   0   0   1   1   1   1 phosphoglycolate phosphatase
PROKKA_02197         -          GPH-RXN              -            4.6e-30  101.9   0.0   5.5e-30  101.6   0.0   1.0   1   0   0   1   1   1   1 phosphoglycolate phosphatase
PROKKA_00297         -          GPH-RXN              -            7.4e-11   39.0   0.0   1.2e-10   38.4   0.0   1.3   1   1   0   1   1   1   1 haloacid dehalogenase superfamily, subfamily IA, variant 3 with third motif having DD or ED
PROKKA_01312         -          GPH-RXN              -            9.8e-08   28.8   0.1   0.00015   18.4   0.0   3.0   2   1   1   3   3   3   3 D-glycero-D-manno-heptose 1,7-bisphosphate phosphatase
PROKKA_01874         -          GPH-RXN              -             0.0003   17.4   0.0    0.0028   14.2   0.0   2.1   1   1   1   2   2   2   1 3-deoxy-D-manno-octulosonate 8-phosphate phosphatase (KDO 8-P phosphatase)
PROKKA_00680         -          GPPSYN-RXN           -            3.7e-32  109.0   0.0   4.7e-32  108.7   0.0   1.1   1   0   0   1   1   1   1 farnesyl-diphosphate synthase 
PROKKA_02481         -          GPPSYN-RXN           -              1e-26   91.1   0.1   2.1e-26   90.1   0.1   1.4   1   1   0   1   1   1   1 octaprenyl-diphosphate synthase
PROKKA_02461         -          GSAAMINOTRANS-RXN    -             7e-184  608.5   0.0  8.3e-184  608.3   0.0   1.0   1   0   0   1   1   1   1 glutamate-1-semialdehyde 2,1-aminomutase
PROKKA_02358         -          GSAAMINOTRANS-RXN    -            4.8e-51  170.9   0.0   8.1e-51  170.1   0.0   1.3   1   1   0   1   1   1   1 acetylornithine/N-succinyldiaminopimelate aminotransferase
PROKKA_00349         -          GSAAMINOTRANS-RXN    -            3.2e-34  115.4   0.0   4.9e-34  114.8   0.0   1.2   1   0   0   1   1   1   1 diaminobutyrate-2-oxoglutarate transaminase
PROKKA_01321         -          GSAAMINOTRANS-RXN    -            1.4e-33  113.3   0.0   3.8e-33  111.9   0.0   1.5   1   1   0   1   1   1   1 adenosylmethionine-8-amino-7-oxononanoate aminotransferase
PROKKA_01541         -          GTP-CYCLOHYDRO-I-RXN -              6e-73  242.5   0.0   7.2e-73  242.2   0.0   1.0   1   0   0   1   1   1   1 GTP cyclohydrolase I
PROKKA_01368         -          GTP-CYCLOHYDRO-II-RXN -           4.9e-179  592.6   0.0  5.4e-179  592.4   0.0   1.0   1   0   0   1   1   1   1 GTP cyclohydrolase II /3,4-dihydroxy-2-butanone 4-phosphate synthase (EC 4.1.99.12)
PROKKA_00936         -          GTPPYPHOSKIN-RXN     -           3.3e-221  733.7   0.0  8.7e-221  732.3   0.0   1.5   1   1   0   1   1   1   1 GTP pyrophosphokinase
PROKKA_01533         -          GTPPYPHOSKIN-RXN     -               0.02   10.4   0.0     0.027   10.0   0.0   1.1   1   0   0   1   1   1   0 chorismate mutase / prephenate dehydratase
PROKKA_01586         -          GUANYL-KIN-RXN       -            1.5e-56  188.6   0.0   1.9e-56  188.3   0.0   1.0   1   0   0   1   1   1   1 guanylate kinase
PROKKA_01406         -          GUANYL-KIN-RXN       -                  3    4.2   7.0       1.3    5.3   0.7   2.6   2   0   0   2   2   2   0 putative protein YhaN
PROKKA_01492         -          GUANYLCYC-RXN        -            9.9e-12   41.3   0.0   1.4e-11   40.8   0.0   1.1   1   0   0   1   1   1   1 adenylate cyclase
PROKKA_01842         -          GUANYLCYC-RXN        -               0.64    5.5  17.2       0.5    5.9   8.4   2.0   2   0   0   2   2   2   0 hypothetical protein
PROKKA_01999         -          H2NEOPTERINALDOL-RXN -            5.9e-42  141.2   0.0   7.2e-42  140.9   0.0   1.0   1   0   0   1   1   1   1 Dihydropteroate synthase
PROKKA_02399         -          H2NEOPTERINALDOL-RXN -            3.5e-06   22.9   0.0   8.1e-05   18.4   0.0   1.9   1   1   1   2   2   2   2 dihydroneopterin aldolase
PROKKA_00242         -          H2NEOPTERINALDOL-RXN -            8.5e-06   21.7   0.0     1e-05   21.4   0.0   1.0   1   0   0   1   1   1   1 2-amino-4-hydroxy-6-hydroxymethyldihydropteridine diphosphokinase
PROKKA_00242         -          H2PTERIDINEPYROPHOSPHOKIN-RXN -            1.5e-18   63.9   0.0   1.9e-18   63.5   0.0   1.0   1   0   0   1   1   1   1 2-amino-4-hydroxy-6-hydroxymethyldihydropteridine diphosphokinase
PROKKA_01999         -          H2PTEROATESYNTH-RXN  -            3.9e-73  243.7   0.0   4.6e-73  243.5   0.0   1.0   1   0   0   1   1   1   1 Dihydropteroate synthase
PROKKA_00242         -          H2PTEROATESYNTH-RXN  -              0.013   11.5   0.0     0.014   11.4   0.0   1.0   1   0   0   1   1   1   0 2-amino-4-hydroxy-6-hydroxymethyldihydropteridine diphosphokinase
PROKKA_02269         -          HISTALDEHYD-RXN      -           1.1e-133  443.5   0.0  1.4e-133  443.1   0.0   1.0   1   0   0   1   1   1   1 histidinol dehydrogenase
PROKKA_01224         -          HISTAMINOTRANS-RXN   -            8.4e-88  291.8   0.0     1e-87  291.6   0.0   1.0   1   0   0   1   1   1   1 histidinol-phosphate aminotransferase
PROKKA_02268         -          HISTAMINOTRANS-RXN   -            8.4e-84  278.7   0.0   9.6e-84  278.5   0.0   1.0   1   0   0   1   1   1   1 histidinol-phosphate aminotransferase
PROKKA_01338         -          HISTAMINOTRANS-RXN   -            7.5e-28   94.6   0.0     1e-27   94.1   0.0   1.1   1   0   0   1   1   1   1 aspartate aminotransferase
PROKKA_01352         -          HISTAMINOTRANS-RXN   -            4.3e-26   88.8   0.0   5.6e-26   88.4   0.0   1.0   1   0   0   1   1   1   1 alanine-synthesizing transaminase
PROKKA_01558         -          HISTAMINOTRANS-RXN   -            2.8e-24   82.8   0.0   6.3e-24   81.7   0.0   1.5   1   1   0   1   1   1   1 L-threonine O-3-phosphate decarboxylase 
PROKKA_00241         -          HISTAMINOTRANS-RXN   -            1.1e-21   74.3   0.0   1.3e-21   74.1   0.0   1.0   1   0   0   1   1   1   1 LL-diaminopimelate aminotransferase apoenzyme
PROKKA_01512         -          HISTAMINOTRANS-RXN   -             0.0018   14.4   0.0    0.0025   13.9   0.0   1.1   1   0   0   1   1   1   1 aspartate aminotransferase
PROKKA_00307         -          HISTAMINOTRANS-RXN   -             0.0025   13.9   0.0     0.004   13.2   0.0   1.2   1   0   0   1   1   1   1 methionine-gamma-lyase
PROKKA_00190         -          HISTAMINOTRANS-RXN   -              0.036   10.1   0.0      0.05    9.6   0.0   1.1   1   0   0   1   1   1   0 glycine dehydrogenase (decarboxylating) beta subunit 
PROKKA_02263         -          HISTCYCLOHYD-RXN     -            2.1e-57  191.4   0.0   2.5e-57  191.2   0.0   1.0   1   0   0   1   1   1   1 phosphoribosyl-ATP pyrophosphatase /phosphoribosyl-AMP cyclohydrolase
PROKKA_00145         -          HISTCYCLOHYD-RXN     -            7.4e-05   19.2   0.4    0.0074   12.7   0.0   2.1   1   1   0   2   2   2   2 tetrapyrrole methylase family protein / MazG family protein
PROKKA_01883         -          HISTIDINE--TRNA-LIGASE-RXN -           2.7e-104  346.5   0.0  3.3e-104  346.2   0.0   1.0   1   0   0   1   1   1   1 histidyl-tRNA synthetase
PROKKA_01510         -          HISTIDINE--TRNA-LIGASE-RXN -            2.2e-53  178.7   0.0   2.9e-53  178.3   0.0   1.0   1   0   0   1   1   1   1 ATP phosphoribosyltransferase regulatory subunit
PROKKA_00090         -          HISTIDINE--TRNA-LIGASE-RXN -            4.3e-14   49.3   0.0   1.2e-07   28.0   0.0   2.1   2   0   0   2   2   2   2 threonyl-tRNA synthetase
PROKKA_02166         -          HISTIDINE--TRNA-LIGASE-RXN -            3.4e-06   23.2   0.0    0.0006   15.8   0.0   2.1   2   0   0   2   2   2   2 prolyl-tRNA synthetase
PROKKA_01906         -          HISTIDINE--TRNA-LIGASE-RXN -            4.3e-05   19.6   0.6   0.00084   15.3   0.1   2.7   3   0   0   3   3   3   1 aspartyl-tRNA synthetase 
PROKKA_00095         -          HISTIDINE--TRNA-LIGASE-RXN -             0.0013   14.7   0.1    0.0028   13.6   0.1   1.5   1   0   0   1   1   1   1 phenylalanyl-tRNA synthetase, alpha subunit
PROKKA_00495         -          HISTIDINE-DECARBOXYLASE-RXN -              1e-13   48.0   0.0   1.3e-13   47.7   0.0   1.0   1   0   0   1   1   1   1 glutamate decarboxylase
PROKKA_00421         -          HISTIDINE-DECARBOXYLASE-RXN -              0.017   11.0   0.0     0.026   10.4   0.0   1.2   1   0   0   1   1   1   0 cysteine desulfurase
PROKKA_02267         -          HISTIDPHOS-RXN       -            2.4e-29   99.9   0.0   3.2e-29   99.5   0.0   1.0   1   0   0   1   1   1   1 imidazoleglycerol-phosphate dehydratase
PROKKA_02269         -          HISTOLDEHYD-RXN      -           1.1e-133  443.5   0.0  1.4e-133  443.1   0.0   1.0   1   0   0   1   1   1   1 histidinol dehydrogenase
PROKKA_00563         -          HISTONE-ACETYLTRANSFERASE-RXN -             0.0076   11.8   0.0     0.012   11.2   0.0   1.2   1   1   0   1   1   1   1 hypothetical protein
PROKKA_02263         -          HISTPRATPHYD-RXN     -            3.9e-59  197.2   0.0   4.9e-59  196.9   0.0   1.0   1   0   0   1   1   1   1 phosphoribosyl-ATP pyrophosphatase /phosphoribosyl-AMP cyclohydrolase
PROKKA_00145         -          HISTPRATPHYD-RXN     -            3.6e-06   23.6   2.3    0.0018   14.8   0.1   2.1   1   1   0   2   2   2   2 tetrapyrrole methylase family protein / MazG family protein
PROKKA_00813         -          HOLO-ACP-SYNTH-RXN   -            5.9e-32  107.6   0.0   7.1e-32  107.3   0.0   1.0   1   0   0   1   1   1   1 holo-[acyl-carrier protein] synthase
PROKKA_02378         -          HOLOCYTOCHROME-C-SYNTHASE-RXN -            2.3e-21   73.5   8.1   3.9e-19   66.2   8.1   2.1   1   1   0   1   1   1   1 cytochrome c-type biogenesis protein
PROKKA_00581         -          HOMOACONITATE-HYDRATASE-RXN -            1.3e-98  328.2   0.0   4.7e-98  326.3   0.0   1.7   1   1   0   1   1   1   1 aconitase
PROKKA_00487         -          HOMOACONITATE-HYDRATASE-RXN -            1.9e-76  254.9   0.0   1.9e-42  142.6   0.0   2.0   2   0   0   2   2   2   2 3-isopropylmalate dehydratase, large subunit 
PROKKA_00486         -          HOMOACONITATE-HYDRATASE-RXN -            3.4e-11   39.3   0.0   4.8e-11   38.8   0.0   1.1   1   0   0   1   1   1   1 3-isopropylmalate/(R)-2-methylmalate dehydratase small subunit
PROKKA_01188         -          HOMOCITRATE-SYNTHASE-RXN -            6.4e-76  252.9   0.0   7.8e-76  252.6   0.0   1.0   1   0   0   1   1   1   1 homocitrate synthase NifV
PROKKA_02537         -          HOMOCITRATE-SYNTHASE-RXN -            4.3e-72  240.3   0.0   6.1e-72  239.8   0.0   1.1   1   0   0   1   1   1   1 2-isopropylmalate synthase
PROKKA_01358         -          HOMOCITRATE-SYNTHASE-RXN -            4.2e-55  184.4   0.0     7e-55  183.6   0.0   1.3   1   0   0   1   1   1   1 (R)-citramalate synthase
PROKKA_00019         -          HOMOCITRATE-SYNTHASE-RXN -            7.4e-24   81.6   0.0     2e-23   80.1   0.0   1.7   1   1   0   1   1   1   1 2-isopropylmalate synthase
PROKKA_02354         -          HOMOCYSMET-RXN       -              0.042    9.7   0.0     0.056    9.3   0.0   1.1   1   0   0   1   1   1   0 diaminopimelate decarboxylase
PROKKA_00782         -          HOMOCYSMETB12-RXN    -           1.6e-162  540.1   0.2  3.3e-162  539.1   0.2   1.4   1   1   0   1   1   1   1 5-methyltetrahydrofolate--homocysteine methyltransferase
PROKKA_00783         -          HOMOCYSMETB12-RXN    -            5.6e-07   24.9   0.0   7.1e-07   24.6   0.0   1.1   1   0   0   1   1   1   1 Vitamin B12 dependent methionine synthase, activation domain
PROKKA_00782         -          HOMOCYSTEINE-S-METHYLTRANSFERASE-RXN -            1.3e-17   61.3   0.0   7.9e-13   45.5   0.0   2.3   2   0   0   2   2   2   2 5-methyltetrahydrofolate--homocysteine methyltransferase
PROKKA_01354         -          HOMOSERDEHYDROG-RXN  -            1.6e-88  295.0   0.1   4.3e-88  293.6   0.1   1.5   1   1   0   1   1   1   1 homoserine dehydrogenase
PROKKA_01357         -          HOMOSERDEHYDROG-RXN  -            1.7e-66  222.2   9.2   4.9e-57  190.9   4.6   2.0   1   1   1   2   2   2   2 aspartate kinase
PROKKA_02388         -          HOMOSERDEHYDROG-RXN  -            0.00014   17.4   0.0   0.00015   17.2   0.0   1.1   1   0   0   1   1   1   1 uridylate kinase
PROKKA_00105         -          HOMOSERDEHYDROG-RXN  -             0.0021   13.5   0.0    0.0035   12.8   0.0   1.3   1   0   0   1   1   1   1 glutamate 5-kinase
PROKKA_01978         -          HOMOSERKIN-RXN       -            9.7e-61  202.4   0.0   1.2e-60  202.1   0.0   1.0   1   0   0   1   1   1   1 homoserine kinase
PROKKA_01694         -          HOMOSERKIN-RXN       -            3.7e-14   49.5   0.0     3e-08   30.1   0.0   2.1   2   0   0   2   2   2   2 4-diphosphocytidyl-2-C-methyl-D-erythritol kinase
PROKKA_01777         -          HOMOSERKIN-RXN       -            1.4e-06   24.6   0.0   1.1e-05   21.7   0.0   2.0   2   0   0   2   2   2   1 D-glycero-alpha-D-manno-heptose-7-phosphate kinase
PROKKA_00690         -          HYDROG-RXN           -              0.003   14.4   0.0    0.0033   14.2   0.0   1.2   1   0   0   1   1   1   1 NADH-quinone oxidoreductase subunit D
PROKKA_02078         -          HYDROGEN-DEHYDROGENASE-RXN -            8.9e-21   71.9   0.0   2.2e-20   70.6   0.0   1.5   1   1   0   1   1   1   1 NADH-quinone oxidoreductase subunit F
PROKKA_01487         -          HYDROGEN-DEHYDROGENASE-RXN -            1.5e-10   38.2   0.0   2.1e-10   37.7   0.0   1.2   1   0   0   1   1   1   1 NADH-quinone oxidoreductase subunit F
PROKKA_01451         -          HYDROGEN-DEHYDROGENASE-RXN -            3.7e-08   30.3   0.0   6.2e-08   29.6   0.0   1.3   1   0   0   1   1   1   1 NADH-quinone oxidoreductase subunit F
PROKKA_00692         -          HYDROGEN-DEHYDROGENASE-RXN -            7.8e-06   22.7   0.0   1.1e-05   22.2   0.0   1.3   1   0   0   1   1   1   1 NAD(P)-dependent iron-only hydrogenase diaphorase component flavoprotein
PROKKA_00691         -          HYDROGEN-DEHYDROGENASE-RXN -              8e-06   22.6   0.0   9.3e-06   22.4   0.0   1.0   1   0   0   1   1   1   1 NADH-quinone oxidoreductase subunit E
PROKKA_02541         -          HYDROXYBENZOYLFORMATE-DECARBOXYLASE-RXN -              2e-33  113.0   0.0   6.5e-33  111.2   0.0   1.7   1   1   0   1   1   1   1 acetolactate synthase, large subunit
PROKKA_02537         -          HYDROXYMETHYLGLUTARYL-COA-LYASE-RXN -            1.7e-09   34.7   0.2   4.8e-09   33.2   0.2   1.8   1   1   0   1   1   1   1 2-isopropylmalate synthase
PROKKA_01188         -          HYDROXYMETHYLGLUTARYL-COA-LYASE-RXN -            0.00065   16.4   0.0   0.00096   15.8   0.0   1.4   1   0   0   1   1   1   1 homocitrate synthase NifV
PROKKA_01358         -          HYDROXYMETHYLGLUTARYL-COA-LYASE-RXN -             0.0012   15.5   0.0    0.0068   13.0   0.0   1.9   2   0   0   2   2   2   1 (R)-citramalate synthase
PROKKA_00984         -          HYDROXYMETHYLGLUTARYL-COA-REDUCTASE-RXN -             0.0079   11.4   0.1     0.011   10.9   0.1   1.1   1   0   0   1   1   1   1 Apolipoprotein N-acyltransferase
PROKKA_01511         -          HYDROXYPYRUVATE-REDUCTASE-RXN -            7.2e-48  160.6   0.0   1.1e-47  160.0   0.0   1.1   1   0   0   1   1   1   1 D-3-phosphoglycerate dehydrogenase
PROKKA_01997         -          HYPOXANPRIBOSYLTRAN-RXN -              1e-47  159.4   0.0   1.2e-47  159.2   0.0   1.0   1   0   0   1   1   1   1 hypoxanthine phosphoribosyltransferase
PROKKA_01921         -          HYPOXANPRIBOSYLTRAN-RXN -            1.9e-15   54.0   0.0   2.5e-15   53.7   0.0   1.0   1   0   0   1   1   1   1 pyrimidine operon attenuation protein / uracil phosphoribosyltransferase
PROKKA_00049         -          HYPOXANPRIBOSYLTRAN-RXN -            6.8e-06   22.8   0.0   9.6e-06   22.3   0.0   1.1   1   0   0   1   1   1   1 orotate phosphoribosyltransferase
PROKKA_01627         -          HYPOXANPRIBOSYLTRAN-RXN -            0.00017   18.3   0.1   0.00029   17.5   0.1   1.3   1   0   0   1   1   1   1 amidophosphoribosyltransferase
PROKKA_00110         -          HYPOXANPRIBOSYLTRAN-RXN -             0.0002   18.1   0.0   0.00028   17.6   0.0   1.2   1   0   0   1   1   1   1 putative amidophosphoribosyltransferases
PROKKA_01553         -          HYPOXANPRIBOSYLTRAN-RXN -            0.00071   16.2   0.0    0.0012   15.4   0.0   1.3   1   0   0   1   1   1   1 adenine phosphoribosyltransferase
PROKKA_01692         -          HYPOXANPRIBOSYLTRAN-RXN -             0.0079   12.8   0.4      0.23    8.0   0.2   2.2   2   0   0   2   2   2   1 ribose-phosphate pyrophosphokinase
PROKKA_01953         -          IGPSYN-RXN           -            2.1e-75  250.6   0.0   2.6e-75  250.3   0.0   1.0   1   0   0   1   1   1   1 indole-3-glycerol phosphate synthase
PROKKA_02267         -          IMIDPHOSDEHYD-RXN    -            1.9e-80  267.0   0.0   2.3e-80  266.6   0.0   1.0   1   0   0   1   1   1   1 imidazoleglycerol-phosphate dehydratase
PROKKA_01901         -          IMP-DEHYDROG-RXN     -           6.8e-219  724.8   2.7  8.7e-219  724.4   2.7   1.0   1   0   0   1   1   1   1 IMP dehydrogenase
PROKKA_01581         -          IMP-DEHYDROG-RXN     -            3.4e-12   43.1   2.9   3.9e-12   42.9   2.9   1.0   1   0   0   1   1   1   1 CBS domain-containing protein
PROKKA_01925         -          IMP-DEHYDROG-RXN     -            1.2e-09   34.6   0.0    0.0002   17.4   0.0   2.0   2   0   0   2   2   2   2 CBS domain-containing protein
PROKKA_01722         -          IMP-DEHYDROG-RXN     -            2.8e-09   33.4   0.0   4.1e-09   32.9   0.0   1.1   1   0   0   1   1   1   1 chloride channel protein, CIC family
PROKKA_01875         -          IMP-DEHYDROG-RXN     -            1.1e-08   31.5   0.0   3.2e-08   30.0   0.0   1.7   1   1   0   1   1   1   1 arabinose-5-phosphate isomerase
PROKKA_00425         -          IMP-DEHYDROG-RXN     -            1.3e-07   27.9   0.1     2e-07   27.3   0.1   1.3   1   0   0   1   1   1   1 putative hemolysin
PROKKA_02264         -          IMP-DEHYDROG-RXN     -            4.3e-07   26.2   0.5   0.00012   18.1   0.1   2.2   2   0   0   2   2   2   2 cyclase
PROKKA_01551         -          IMP-DEHYDROG-RXN     -             0.0033   13.4   5.5    0.0021   14.1   2.5   2.0   3   0   0   3   3   3   1 glutamate synthase (NADPH/NADH) large chain
PROKKA_01332         -          IMPCYCLOHYDROLASE-RXN -           1.7e-203  674.4   0.0  1.9e-203  674.3   0.0   1.0   1   0   0   1   1   1   1 phosphoribosylaminoimidazolecarboxamide formyltransferase / IMP cyclohydrolase
PROKKA_01916         -          IMPCYCLOHYDROLASE-RXN -            0.00011   18.4   0.0   0.00018   17.7   0.0   1.2   1   0   0   1   1   1   1 carbamoyl-phosphate synthase large subunit
PROKKA_02322         -          INORGPYROPHOSPHAT-RXN -            1.1e-46  156.2   0.0   1.3e-46  155.9   0.0   1.0   1   0   0   1   1   1   1 inorganic pyrophosphatase
PROKKA_02004         -          ISOCHORMAT-RXN       -            7.9e-07   25.8   0.0   1.1e-06   25.3   0.0   1.2   1   0   0   1   1   1   1 Nicotinamidase-related amidase
PROKKA_01956         -          ISOCHORSYN-RXN       -              5e-49  164.6   0.0   6.4e-49  164.3   0.0   1.1   1   0   0   1   1   1   1 anthranilate synthase component 1
PROKKA_00363         -          ISOCHORSYN-RXN       -            2.3e-36  122.9   0.0     3e-36  122.5   0.0   1.1   1   0   0   1   1   1   1 anthranilate synthase component 1
PROKKA_00965         -          ISOCITDEH-RXN        -            7.5e-52  174.0   0.3   6.7e-32  108.3   0.0   2.5   1   1   1   2   2   2   2 isocitrate dehydrogenase (NAD+)
PROKKA_02536         -          ISOCITDEH-RXN        -            5.1e-22   75.7   0.1   4.6e-21   72.6   0.1   2.0   1   1   0   1   1   1   1 3-isopropylmalate dehydrogenase
PROKKA_00965         -          ISOCITRATE-DEHYDROGENASE-NAD+-RXN -           1.2e-123  409.8   0.0  1.3e-123  409.6   0.0   1.0   1   0   0   1   1   1   1 isocitrate dehydrogenase (NAD+)
PROKKA_02536         -          ISOCITRATE-DEHYDROGENASE-NAD+-RXN -            3.2e-58  194.5   0.0     4e-58  194.2   0.0   1.0   1   0   0   1   1   1   1 3-isopropylmalate dehydrogenase
PROKKA_02476         -          ISOLEUCINE--TRNA-LIGASE-RXN -           2.5e-281  933.0   0.0  3.1e-281  932.7   0.0   1.0   1   0   0   1   1   1   1 Isoleucyl-tRNA synthetase
PROKKA_00867         -          ISOLEUCINE--TRNA-LIGASE-RXN -           2.2e-117  390.4   0.0   9.6e-76  252.6   0.0   3.0   1   1   1   2   2   2   2 valyl-tRNA synthetase
PROKKA_00378         -          ISOLEUCINE--TRNA-LIGASE-RXN -            9.1e-75  249.4   0.0   4.8e-32  108.0   0.0   5.2   1   1   3   4   4   4   4 leucyl-tRNA synthetase
PROKKA_02128         -          ISOLEUCINE--TRNA-LIGASE-RXN -            2.9e-19   65.7   0.0   1.9e-13   46.5   0.0   2.8   3   0   0   3   3   3   2 methionyl-tRNA synthetase
PROKKA_01364         -          ISOLEUCINE--TRNA-LIGASE-RXN -            0.00013   17.3   0.0    0.0015   13.7   0.0   2.3   2   1   0   2   2   2   1 cysteinyl-tRNA synthetase
PROKKA_01876         -          KDO-8PSYNTH-RXN      -            8.8e-99  327.4   0.0   1.2e-98  327.0   0.0   1.0   1   0   0   1   1   1   1 2-dehydro-3-deoxyphosphooctonate aldolase (KDO 8-P synthase)
PROKKA_01534         -          KDO-8PSYNTH-RXN      -            2.2e-21   73.4   0.0   4.3e-21   72.4   0.0   1.4   1   0   0   1   1   1   1 3-deoxy-D-arabinoheptulosonate-7-phosphate synthase
PROKKA_01653         -          KDPGALDOL-RXN        -             0.0041   13.7   0.0    0.0044   13.6   0.0   1.1   1   0   0   1   1   1   1 large subunit ribosomal protein L22
PROKKA_02160         -          KDUD-RXN             -            3.2e-35  119.0   2.5   4.1e-35  118.7   2.5   1.0   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier-protein] reductase
PROKKA_00235         -          KDUD-RXN             -            2.7e-19   66.9   0.0   3.1e-19   66.7   0.0   1.0   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier protein] reductase
PROKKA_02149         -          KDUD-RXN             -            1.8e-07   28.1   0.0     1e-06   25.7   0.0   1.9   1   1   0   1   1   1   1 NADP-dependent 3-hydroxy acid dehydrogenase YdfG
PROKKA_00773         -          KDUD-RXN             -             0.0024   14.6   0.0    0.0032   14.2   0.0   1.2   1   0   0   1   1   1   1 short chain dehydrogenase
PROKKA_02162         -          KETOACYLCOATHIOL-RXN -            1.6e-07   27.7   0.3   4.8e-07   26.2   0.1   1.7   1   1   1   2   2   2   1 3-oxoacyl-[acyl-carrier-protein] synthase-3
PROKKA_02158         -          KETOACYLCOATHIOL-RXN -            0.00038   16.6   2.5    0.0034   13.4   0.2   2.1   2   0   0   2   2   2   2 3-oxoacyl-[acyl-carrier-protein] synthase II
PROKKA_01780         -          KETOISOCAPROATE-RXN  -              9e-92  305.6   0.0     1e-91  305.4   0.0   1.0   1   0   0   1   1   1   1 pyruvate dehydrogenase E1 component beta subunit
PROKKA_01779         -          KETOISOCAPROATE-RXN  -              1e-52  176.7   0.0   1.2e-52  176.5   0.0   1.0   1   0   0   1   1   1   1 pyruvate dehydrogenase E1 component alpha subunit
PROKKA_00681         -          KETOISOCAPROATE-RXN  -            1.3e-30  103.6   0.3   1.3e-24   83.8   0.1   2.1   2   0   0   2   2   2   2 1-deoxy-D-xylulose-5-phosphate synthase
PROKKA_02458         -          KETOISOCAPROATE-RXN  -             0.0021   13.7   0.0    0.0032   13.2   0.0   1.2   1   0   0   1   1   1   1 pyruvate ferredoxin oxidoreductase alpha subunit
PROKKA_01389         -          KETOISOCAPROATE-RXN  -             0.0032   13.1   0.0     0.005   12.5   0.0   1.2   1   0   0   1   1   1   1 xylulose-5-phosphate/fructose-6-phosphate phosphoketolase
PROKKA_01174         -          KYNURENINASE-RXN     -              0.029   10.0   0.0     0.037    9.6   0.0   1.1   1   0   0   1   1   1   0 cysteine desulfurase
PROKKA_02187         -          L-AMINO-ACID-OXIDASE-RXN -            2.4e-07   27.0   0.1   3.8e-07   26.4   0.1   1.2   1   0   0   1   1   1   1 squalene-associated FAD-dependent desaturase
PROKKA_01719         -          L-AMINO-ACID-OXIDASE-RXN -            8.8e-07   25.2   0.2   1.4e-06   24.5   0.2   1.2   1   0   0   1   1   1   1 oxygen-dependent protoporphyrinogen oxidase
PROKKA_01944         -          L-AMINO-ACID-OXIDASE-RXN -            3.2e-05   20.0   0.4   6.1e-05   19.1   0.0   1.5   2   0   0   2   2   2   1 NADPH-dependent glutamate synthase beta chain
PROKKA_00735         -          L-AMINO-ACID-OXIDASE-RXN -            6.8e-05   18.9   3.2   9.7e-05   18.4   3.2   1.2   1   0   0   1   1   1   1 C-3',4' desaturase CrtD
PROKKA_02417         -          L-AMINO-ACID-OXIDASE-RXN -             0.0016   14.4   0.1     0.068    9.0   0.0   2.1   2   0   0   2   2   2   1 thioredoxin reductase (NADPH)
PROKKA_00255         -          L-AMINO-ACID-OXIDASE-RXN -             0.0029   13.6   0.0    0.0038   13.2   0.0   1.1   1   0   0   1   1   1   1 methylenetetrahydrofolate--tRNA-(uracil-5-)-methyltransferase
PROKKA_01451         -          L-AMINO-ACID-OXIDASE-RXN -              0.029   10.3   0.1     0.051    9.5   0.1   1.2   1   0   0   1   1   1   0 NADH-quinone oxidoreductase subunit F
PROKKA_01702         -          L-AMINO-ACID-OXIDASE-RXN -              0.029   10.2   0.1     0.047    9.6   0.1   1.3   1   0   0   1   1   1   0 2-polyprenyl-6-methoxyphenol hydroxylase
PROKKA_00569         -          L-GLN-FRUCT-6-P-AMINOTRANS-RXN -             7e-197  652.6   0.0  7.9e-197  652.4   0.0   1.0   1   0   0   1   1   1   1 glucosamine--fructose-6-phosphate aminotransferase (isomerizing)
PROKKA_02106         -          L-GLN-FRUCT-6-P-AMINOTRANS-RXN -           1.5e-182  605.3   0.0  1.6e-182  605.1   0.0   1.0   1   0   0   1   1   1   1 glucosamine--fructose-6-phosphate aminotransferase (isomerizing)
PROKKA_01627         -          L-GLN-FRUCT-6-P-AMINOTRANS-RXN -            3.2e-29   98.8   0.0   5.4e-29   98.1   0.0   1.3   1   0   0   1   1   1   1 amidophosphoribosyltransferase
PROKKA_01875         -          L-GLN-FRUCT-6-P-AMINOTRANS-RXN -            6.7e-08   28.4   0.0   1.6e-07   27.2   0.0   1.5   2   0   0   2   2   2   1 arabinose-5-phosphate isomerase
PROKKA_02462         -          L-GLN-FRUCT-6-P-AMINOTRANS-RXN -              0.001   14.6   0.0    0.0014   14.2   0.0   1.1   1   0   0   1   1   1   1 D-sedoheptulose 7-phosphate isomerase
PROKKA_00507         -          L-IDITOL-2-DEHYDROGENASE-RXN -            1.6e-35  119.8   0.7   3.8e-35  118.6   0.7   1.4   1   1   0   1   1   1   1 putative zinc-type alcohol dehydrogenase-like protein
PROKKA_02069         -          L-IDITOL-2-DEHYDROGENASE-RXN -            6.6e-35  117.8   0.0   8.6e-35  117.5   0.0   1.1   1   0   0   1   1   1   1 alcohol dehydrogenase, propanol-preferring
PROKKA_00329         -          L-IDITOL-2-DEHYDROGENASE-RXN -            1.1e-29  100.7   0.0   3.8e-29   98.9   0.0   1.7   2   0   0   2   2   2   1 alcohol dehydrogenase, propanol-preferring
PROKKA_00022         -          L-IDITOL-2-DEHYDROGENASE-RXN -            1.6e-28   96.8   0.0   2.5e-28   96.2   0.0   1.3   1   1   0   1   1   1   1 NADPH:quinone reductase
PROKKA_01717         -          L-IDITOL-2-DEHYDROGENASE-RXN -            6.5e-21   71.8   0.0   3.2e-12   43.2   0.0   2.2   1   1   1   2   2   2   2 NADPH2:quinone reductase
PROKKA_01239         -          L-IDITOL-2-DEHYDROGENASE-RXN -            5.3e-13   45.8   3.8   2.2e-12   43.7   2.0   1.9   1   1   1   2   2   2   1 NADPH2:quinone reductase
PROKKA_01451         -          L-IDITOL-2-DEHYDROGENASE-RXN -            0.00018   17.7   0.1    0.0077   12.4   0.1   2.4   2   0   0   2   2   2   1 NADH-quinone oxidoreductase subunit F
PROKKA_00647         -          L-IDITOL-2-DEHYDROGENASE-RXN -             0.0096   12.0   0.0     0.015   11.4   0.0   1.2   1   0   0   1   1   1   1 cysteine synthase 
PROKKA_00876         -          L-IDITOL-2-DEHYDROGENASE-RXN -              0.058    9.5   2.9     0.044    9.9   0.4   1.8   2   0   0   2   2   2   0 malate dehydrogenase (NAD)
PROKKA_01551         -          L-LACTATE-DEHYDROGENASE-CYTOCHROME-RXN -            3.6e-07   26.2   0.1   6.1e-07   25.5   0.1   1.2   1   0   0   1   1   1   1 glutamate synthase (NADPH/NADH) large chain
PROKKA_00876         -          L-LACTATE-DEHYDROGENASE-RXN -            1.1e-55  186.6   0.1   1.4e-55  186.3   0.1   1.0   1   0   0   1   1   1   1 malate dehydrogenase (NAD)
PROKKA_00349         -          L-LYSINE-AMINOTRANSFERASE-RXN -            4.5e-27   92.0   0.0   6.8e-25   84.8   0.0   2.1   2   0   0   2   2   2   2 diaminobutyrate-2-oxoglutarate transaminase
PROKKA_02358         -          L-LYSINE-AMINOTRANSFERASE-RXN -            3.6e-24   82.4   0.0   5.8e-21   71.9   0.0   2.1   2   0   0   2   2   2   2 acetylornithine/N-succinyldiaminopimelate aminotransferase
PROKKA_01321         -          L-LYSINE-AMINOTRANSFERASE-RXN -            6.7e-07   25.5   0.0   1.7e-06   24.2   0.0   1.5   2   0   0   2   2   2   1 adenosylmethionine-8-amino-7-oxononanoate aminotransferase
PROKKA_02461         -          L-LYSINE-AMINOTRANSFERASE-RXN -            1.3e-05   21.3   0.0   2.1e-05   20.6   0.0   1.2   1   0   0   1   1   1   1 glutamate-1-semialdehyde 2,1-aminomutase
PROKKA_02282         -          LACTALDDEHYDROG-RXN  -            4.4e-41  138.3   0.0   5.6e-41  138.0   0.0   1.0   1   0   0   1   1   1   1 Acyl-CoA reductase
PROKKA_01901         -          LACTATE-2-MONOOXYGENASE-RXN -            6.4e-05   19.5   2.2    0.0001   18.8   1.8   1.4   1   1   0   1   1   1   1 IMP dehydrogenase
PROKKA_00616         -          LANOSTEROL-SYNTHASE-RXN -             5e-139  461.7   0.0  6.8e-139  461.3   0.0   1.0   1   0   0   1   1   1   1 squalene-hopene/tetraprenyl-beta-curcumene cyclase
PROKKA_00378         -          LEUCINE--TRNA-LIGASE-RXN -                  0 1043.5   0.0         0 1043.3   0.0   1.0   1   0   0   1   1   1   1 leucyl-tRNA synthetase
PROKKA_00867         -          LEUCINE--TRNA-LIGASE-RXN -            5.9e-74  246.6   0.0   4.7e-47  157.7   0.0   3.7   1   1   2   3   3   3   3 valyl-tRNA synthetase
PROKKA_02476         -          LEUCINE--TRNA-LIGASE-RXN -              6e-58  193.7   0.0   1.4e-34  116.4   0.0   2.3   1   1   1   2   2   2   2 Isoleucyl-tRNA synthetase
PROKKA_02128         -          LEUCINE--TRNA-LIGASE-RXN -            1.2e-31  106.7   0.0   2.7e-15   52.6   0.0   2.1   2   0   0   2   2   2   2 methionyl-tRNA synthetase
PROKKA_01364         -          LEUCINE--TRNA-LIGASE-RXN -            0.00068   14.9   0.0     0.031    9.4   0.0   2.1   2   0   0   2   2   2   2 cysteinyl-tRNA synthetase
PROKKA_00929         -          LEUCINE--TRNA-LIGASE-RXN -             0.0062   11.7   0.0      0.25    6.4   0.0   2.1   2   0   0   2   2   2   2 arginyl-tRNA synthetase
PROKKA_00029         -          LEUCINE--TRNA-LIGASE-RXN -              0.021   10.0   0.4     0.029    9.5   0.4   1.1   1   0   0   1   1   1   0 diguanylate cyclase
PROKKA_00319         -          LEUKOTRIENE-A4-HYDROLASE-RXN -            4.4e-27   92.1   0.0   1.1e-26   90.7   0.0   1.5   1   1   0   1   1   1   1 puromycin-sensitive aminopeptidase
PROKKA_01297         -          LIPIDADISACCHARIDESYNTH-RXN -            5.3e-93  309.1   0.0   6.4e-93  308.8   0.0   1.0   1   0   0   1   1   1   1 lipid-A-disaccharide synthase
PROKKA_00514         -          LONG-CHAIN-ENOYL-COA-HYDRATASE-RXN -            1.6e-11   40.2   0.0   2.1e-11   39.7   0.0   1.1   1   0   0   1   1   1   1 DSF synthase
PROKKA_01610         -          LONG-CHAIN-ENOYL-COA-HYDRATASE-RXN -            2.5e-08   29.6   0.0   3.2e-08   29.2   0.0   1.1   1   0   0   1   1   1   1 3-hydroxybutyryl-CoA dehydrogenase
PROKKA_00876         -          LONG-CHAIN-FATTY-ACYL-COA-REDUCTASE-RXN -            6.5e-88  292.2   2.2   8.3e-88  291.9   2.2   1.0   1   0   0   1   1   1   1 malate dehydrogenase (NAD)
PROKKA_00855         -          LYSDECARBOX-RXN      -            1.8e-82  274.8   0.1   2.3e-67  224.9   0.0   2.9   2   1   0   2   2   2   2 arginine decarboxylase 
PROKKA_01341         -          LYSINE--TRNA-LIGASE-RXN -           3.4e-170  564.3   0.0  3.7e-170  564.2   0.0   1.0   1   0   0   1   1   1   1 lysyl-tRNA synthetase, class II
PROKKA_01906         -          LYSINE--TRNA-LIGASE-RXN -            7.2e-38  127.8   0.0   1.5e-26   90.5   0.0   2.1   2   0   0   2   2   2   2 aspartyl-tRNA synthetase 
PROKKA_00876         -          MALATE-DEH-RXN       -            1.2e-52  176.7   0.0   1.4e-52  176.4   0.0   1.0   1   0   0   1   1   1   1 malate dehydrogenase (NAD)
PROKKA_02535         -          MALATE-DEH-RXN       -             0.0057   13.5   0.0    0.0088   12.9   0.0   1.3   1   0   0   1   1   1   1 aspartate-semialdehyde dehydrogenase
PROKKA_01854         -          MALATE-DEHYDROGENASE-ACCEPTOR-RXN -            1.5e-06   24.2   0.0   2.3e-06   23.6   0.0   1.1   1   0   0   1   1   1   1 L-2-hydroxyglutarate oxidase LhgO
PROKKA_00876         -          MALATE-DEHYDROGENASE-NADP+-RXN -            4.3e-16   55.7   0.0   5.6e-16   55.4   0.0   1.1   1   0   0   1   1   1   1 malate dehydrogenase (NAD)
PROKKA_02161         -          MALONYL-COA-ACP-TRANSACYL-RXN -            9.5e-72  239.6   0.0   1.2e-71  239.2   0.0   1.0   1   0   0   1   1   1   1 [acyl-carrier-protein] S-malonyltransferase
PROKKA_00296         -          MALTACETYLTRAN-RXN   -            1.4e-12   45.1   2.7   2.7e-10   37.6   0.3   2.4   1   1   1   2   2   2   2 Carbonic anhydrase or acetyltransferase, isoleucine patch superfamily
PROKKA_01326         -          MALTACETYLTRAN-RXN   -            5.8e-07   26.7   5.5   2.6e-06   24.6   5.3   1.8   1   1   1   2   2   2   1 serine O-acetyltransferase
PROKKA_02107         -          MALTACETYLTRAN-RXN   -            4.3e-06   23.9   4.7    0.0027   14.7   1.3   2.9   2   1   1   3   3   3   2 bifunctional UDP-N-acetylglucosamine pyrophosphorylase / Glucosamine-1-phosphate N-acetyltransferase
PROKKA_01292         -          MALTACETYLTRAN-RXN   -             0.0026   14.8  13.8     0.057   10.4   2.8   3.5   1   1   2   3   3   3   2 UDP-3-O-[3-hydroxymyristoyl] glucosamine N-acyltransferase
PROKKA_01359         -          MALTACETYLTRAN-RXN   -             0.0059   13.7   1.0      0.95    6.5   0.1   2.4   1   1   1   2   2   2   2 acyl-[acyl-carrier-protein]--UDP-N-acetylglucosamine O-acyltransferase
PROKKA_00770         -          MANNPGUANYLTRANGDP-RXN -           7.3e-136  450.7   0.0  9.1e-136  450.4   0.0   1.0   1   0   0   1   1   1   1 mannose-1-phosphate guanylyltransferase / mannose-6-phosphate isomerase
PROKKA_00489         -          MANNPGUANYLTRANGDP-RXN -              0.017   10.9   0.0     0.017   10.9   0.0   1.2   1   0   0   1   1   1   0 Glucose-1-phosphate thymidylyltransferase
PROKKA_00770         -          MANNPISOM-RXN        -            4.2e-42  141.6   0.0   1.4e-41  139.9   0.0   1.8   1   1   0   1   1   1   1 mannose-1-phosphate guanylyltransferase / mannose-6-phosphate isomerase
PROKKA_00226         -          MCPMETEST-RXN        -            1.3e-75  251.9   0.0   1.6e-75  251.7   0.0   1.1   1   0   0   1   1   1   1 two-component system, chemotaxis family, response regulator CheB
PROKKA_00224         -          MCPMETEST-RXN        -            3.1e-18   63.2   0.4   3.4e-18   63.1   0.4   1.0   1   0   0   1   1   1   1 two-component system, chemotaxis family, response regulator CheY
PROKKA_00517         -          MCPMETEST-RXN        -            3.8e-17   59.6   0.1   5.3e-17   59.2   0.1   1.1   1   0   0   1   1   1   1 two-component system, response regulator RpfG
PROKKA_01214         -          MCPMETEST-RXN        -            9.2e-17   58.4   0.0   1.3e-16   57.9   0.0   1.2   1   0   0   1   1   1   1 two-component system, NtrC family, response regulator AtoC
PROKKA_01831         -          MCPMETEST-RXN        -            1.5e-16   57.6   0.1     3e-16   56.7   0.1   1.4   1   1   0   1   1   1   1 two-component system, NtrC family, response regulator AtoC
PROKKA_02420         -          MCPMETEST-RXN        -            7.1e-14   48.9   0.0   9.3e-14   48.5   0.0   1.1   1   0   0   1   1   1   1 two-component system, NtrC family, response regulator AtoC
PROKKA_00042         -          MCPMETEST-RXN        -            1.6e-13   47.7   0.0   2.8e-13   46.9   0.0   1.4   1   0   0   1   1   1   1 two-component system, NtrC family, nitrogen regulation response regulator NtrX
PROKKA_00654         -          MCPMETEST-RXN        -            4.1e-12   43.1   0.0   4.5e-12   42.9   0.0   1.1   1   0   0   1   1   1   1 two-component system, OmpR family, alkaline phosphatase synthesis response regulator PhoP
PROKKA_00294         -          MCPMETEST-RXN        -            5.1e-12   42.8   0.1   6.2e-12   42.5   0.1   1.0   1   0   0   1   1   1   1 Response regulator receiver domain-containing protein
PROKKA_02292         -          MCPMETEST-RXN        -            1.1e-11   41.7   0.1   1.4e-11   41.3   0.1   1.1   1   0   0   1   1   1   1 two-component system, OmpR family, alkaline phosphatase synthesis response regulator PhoP
PROKKA_00448         -          MCPMETEST-RXN        -            2.3e-11   40.6   0.1   3.2e-11   40.2   0.1   1.2   1   0   0   1   1   1   1 two component transcriptional regulator, LuxR family
PROKKA_00199         -          MCPMETEST-RXN        -            7.9e-11   38.8   0.0     1e-10   38.5   0.0   1.2   1   0   0   1   1   1   1 two-component system, response regulator FlrC
PROKKA_00074         -          MCPMETEST-RXN        -            4.1e-10   36.5   0.0   4.2e-10   36.5   0.0   1.1   1   0   0   1   1   1   1 DNA-binding response regulator, OmpR family, contains REC and winged-helix (wHTH) domain
PROKKA_01596         -          MCPMETEST-RXN        -            5.3e-10   36.1   0.0   6.1e-10   35.9   0.0   1.1   1   0   0   1   1   1   1 Response regulator receiver domain-containing protein
PROKKA_00799         -          MCPMETEST-RXN        -            2.7e-09   33.8   0.6   4.3e-09   33.1   0.6   1.2   1   0   0   1   1   1   1 two-component system, chemotaxis family, response regulator CheV
PROKKA_01867         -          MCPMETEST-RXN        -            4.3e-09   33.1   0.0   5.9e-09   32.7   0.0   1.2   1   0   0   1   1   1   1 two-component system, NtrC family, nitrogen regulation response regulator GlnG
PROKKA_00515         -          MCPMETEST-RXN        -            1.6e-07   28.0   0.0   1.5e-06   24.8   0.0   2.0   2   0   0   2   2   2   1 Hpt domain-containing protein
PROKKA_01595         -          MCPMETEST-RXN        -            4.3e-06   23.3   0.0   6.2e-06   22.7   0.0   1.2   1   0   0   1   1   1   1 PAS domain S-box-containing protein/diguanylate cyclase (GGDEF) domain-containing protein
PROKKA_00534         -          MCPMETEST-RXN        -             0.0027   14.0   0.0     0.004   13.5   0.0   1.2   1   0   0   1   1   1   1 two-component system, NtrC family, response regulator
PROKKA_00128         -          MERCAPYSTRANS-RXN    -              0.013   12.0   0.0     0.022   11.3   0.0   1.2   1   0   0   1   1   1   0 RNAse R
PROKKA_01157         -          MERCURY-II-REDUCTASE-RXN -           8.5e-224  741.6   0.8  1.1e-223  741.3   0.8   1.1   1   0   0   1   1   1   1 mercuric reductase
PROKKA_00559         -          MERCURY-II-REDUCTASE-RXN -             2e-188  624.9   0.0  2.8e-188  624.4   0.0   1.2   1   0   0   1   1   1   1 mercuric reductase
PROKKA_01909         -          MERCURY-II-REDUCTASE-RXN -            6.7e-89  296.3   0.0   7.4e-89  296.2   0.0   1.0   1   0   0   1   1   1   1 dihydrolipoamide dehydrogenase
PROKKA_00137         -          MERCURY-II-REDUCTASE-RXN -            4.9e-68  227.5   0.0   5.6e-68  227.3   0.0   1.0   1   0   0   1   1   1   1 dihydrolipoamide dehydrogenase
PROKKA_01391         -          MERCURY-II-REDUCTASE-RXN -            9.6e-64  213.3   0.1   1.1e-63  213.1   0.1   1.0   1   0   0   1   1   1   1 dihydrolipoamide dehydrogenase
PROKKA_00313         -          MERCURY-II-REDUCTASE-RXN -            9.1e-18   61.5   0.0   5.7e-12   42.3   0.0   2.2   2   0   0   2   2   2   2 Cu+-exporting ATPase
PROKKA_01201         -          MERCURY-II-REDUCTASE-RXN -              1e-14   51.4   0.2   1.9e-14   50.5   0.2   1.3   1   0   0   1   1   1   1 nitrite reductase (NADH) large subunit
PROKKA_02417         -          MERCURY-II-REDUCTASE-RXN -            1.2e-09   34.7   0.4   1.4e-08   31.2   0.1   2.4   2   1   1   3   3   3   1 thioredoxin reductase (NADPH)
PROKKA_01451         -          MERCURY-II-REDUCTASE-RXN -            1.7e-07   27.6   0.2   0.00013   18.1   0.3   2.7   2   1   0   2   2   2   2 NADH-quinone oxidoreductase subunit F
PROKKA_01944         -          MERCURY-II-REDUCTASE-RXN -            1.8e-07   27.5   0.3     9e-05   18.6   0.1   2.8   3   0   0   3   3   3   2 NADPH-dependent glutamate synthase beta chain
PROKKA_00585         -          MERCURY-II-REDUCTASE-RXN -             0.0016   14.4   0.0    0.0042   13.1   0.0   1.6   2   0   0   2   2   2   1 succinate dehydrogenase / fumarate reductase flavoprotein subunit
PROKKA_00427         -          MERCURY-II-REDUCTASE-RXN -             0.0051   12.8   0.5      0.81    5.5   0.0   2.8   3   0   0   3   3   3   2 dissimilatory adenylylsulfate reductase alpha subunit precursor
PROKKA_00077         -          METHANOL-DEHYDROGENASE-RXN -            2.4e-07   26.6   0.0   0.00016   17.3   0.0   2.9   1   1   2   3   3   3   2 Outer membrane protein assembly factor BamB, contains PQQ-like beta-propeller repeat
PROKKA_02302         -          METHANOL-DEHYDROGENASE-RXN -              0.018   10.6   0.0      0.31    6.5   0.0   2.4   2   0   0   2   2   2   0 outer membrane protein assembly factor BamB
PROKKA_01542         -          METHENYLTHFCYCLOHYDRO-RXN -           7.8e-113  373.5   0.1  9.4e-113  373.2   0.1   1.0   1   0   0   1   1   1   1 methylenetetrahydrofolate dehydrogenase (NADP+) / methenyltetrahydrofolate cyclohydrolase
PROKKA_02128         -          METHIONINE--TRNA-LIGASE-RXN -           6.3e-165  547.5   0.0  9.3e-164  543.7   0.0   1.9   1   1   0   1   1   1   1 methionyl-tRNA synthetase
PROKKA_00378         -          METHIONINE--TRNA-LIGASE-RXN -              6e-39  131.3   0.0   9.7e-22   74.5   0.0   4.6   3   2   0   3   3   3   3 leucyl-tRNA synthetase
PROKKA_00867         -          METHIONINE--TRNA-LIGASE-RXN -            2.7e-27   92.8   0.2   1.2e-08   31.2   0.0   4.8   3   2   1   4   4   4   3 valyl-tRNA synthetase
PROKKA_02476         -          METHIONINE--TRNA-LIGASE-RXN -            1.3e-25   87.2   0.0   1.5e-09   34.2   0.0   3.6   3   1   0   3   3   3   3 Isoleucyl-tRNA synthetase
PROKKA_01364         -          METHIONINE--TRNA-LIGASE-RXN -              7e-08   28.7   0.1   0.00039   16.3   0.1   2.1   2   0   0   2   2   2   2 cysteinyl-tRNA synthetase
PROKKA_00307         -          METHIONINE-GAMMA-LYASE-RXN -           1.2e-105  350.7   0.0  1.4e-105  350.4   0.0   1.0   1   0   0   1   1   1   1 methionine-gamma-lyase
PROKKA_01338         -          METHIONINE-GAMMA-LYASE-RXN -            0.00021   17.3   0.0   0.00032   16.7   0.0   1.2   1   0   0   1   1   1   1 aspartate aminotransferase
PROKKA_01781         -          METHIONINE-GAMMA-LYASE-RXN -             0.0094   11.9   0.0     0.013   11.4   0.0   1.2   1   0   0   1   1   1   1 CDP-6-deoxy-D-xylo-4-hexulose-3-dehydrase
PROKKA_02148         -          METHIONINE-S-METHYLTRANSFERASE-RXN -              0.042    8.8   0.0     0.059    8.3   0.0   1.1   1   0   0   1   1   1   0 Methyltransferase domain-containing protein
PROKKA_02410         -          METHIONYL-TRNA-FORMYLTRANSFERASE-RXN -            1.1e-92  307.7   0.0   1.3e-92  307.6   0.0   1.0   1   0   0   1   1   1   1 methionyl-tRNA formyltransferase
PROKKA_02153         -          METHIONYL-TRNA-FORMYLTRANSFERASE-RXN -            4.7e-19   65.9   0.0   5.6e-19   65.7   0.0   1.0   1   0   0   1   1   1   1 phosphoribosylglycinamide formyltransferase-1
PROKKA_00514         -          METHYLACYLYLCOA-HYDROXY-RXN -            5.5e-24   81.8   0.0   6.6e-24   81.5   0.0   1.0   1   0   0   1   1   1   1 DSF synthase
PROKKA_01593         -          METHYLCROTONYL-COA-CARBOXYLASE-RXN -             4e-132  438.9   0.0  4.9e-132  438.6   0.0   1.0   1   0   0   1   1   1   1 acetyl-CoA carboxylase, biotin carboxylase subunit
PROKKA_01916         -          METHYLCROTONYL-COA-CARBOXYLASE-RXN -            4.7e-06   22.1   0.0   0.00021   16.7   0.0   2.1   2   0   0   2   2   2   2 carbamoyl-phosphate synthase large subunit
PROKKA_01592         -          METHYLCROTONYL-COA-CARBOXYLASE-RXN -             0.0029   12.9   0.0    0.0032   12.8   0.0   1.0   1   0   0   1   1   1   1 acetyl-CoA carboxylase biotin carboxyl carrier protein
PROKKA_02038         -          METHYLCROTONYL-COA-CARBOXYLASE-RXN -             0.0071   11.6   0.1      0.01   11.1   0.1   1.1   1   0   0   1   1   1   1 RND family efflux transporter, MFP subunit
PROKKA_01333         -          METHYLCROTONYL-COA-CARBOXYLASE-RXN -              0.014   10.6   0.0      0.02   10.1   0.0   1.1   1   0   0   1   1   1   0 phosphoribosylamine--glycine ligase
PROKKA_01542         -          METHYLENETHFDEHYDROG-NADP-RXN -           1.4e-111  369.4   0.1  1.7e-111  369.1   0.1   1.0   1   0   0   1   1   1   1 methylenetetrahydrofolate dehydrogenase (NADP+) / methenyltetrahydrofolate cyclohydrolase
PROKKA_01441         -          METHYLMALONYL-COA-DECARBOXYLASE-RXN -              4e-12   43.5   0.0   5.1e-12   43.1   0.0   1.0   1   0   0   1   1   1   1 acetyl-CoA carboxylase carboxyl transferase subunit alpha
PROKKA_02255         -          METHYLMALONYL-COA-MUT-RXN -              0.084    8.0  10.4     0.087    8.0   9.2   1.5   1   1   1   2   2   2   0 Putative zinc ribbon domain protein
PROKKA_01780         -          METHYLVALERATE-RXN   -              9e-92  305.6   0.0     1e-91  305.4   0.0   1.0   1   0   0   1   1   1   1 pyruvate dehydrogenase E1 component beta subunit
PROKKA_01779         -          METHYLVALERATE-RXN   -              1e-52  176.7   0.0   1.2e-52  176.5   0.0   1.0   1   0   0   1   1   1   1 pyruvate dehydrogenase E1 component alpha subunit
PROKKA_00681         -          METHYLVALERATE-RXN   -            1.3e-30  103.6   0.3   1.3e-24   83.8   0.1   2.1   2   0   0   2   2   2   2 1-deoxy-D-xylulose-5-phosphate synthase
PROKKA_02458         -          METHYLVALERATE-RXN   -             0.0021   13.7   0.0    0.0032   13.2   0.0   1.2   1   0   0   1   1   1   1 pyruvate ferredoxin oxidoreductase alpha subunit
PROKKA_01389         -          METHYLVALERATE-RXN   -             0.0032   13.1   0.0     0.005   12.5   0.0   1.2   1   0   0   1   1   1   1 xylulose-5-phosphate/fructose-6-phosphate phosphoketolase
PROKKA_01777         -          MEVALONATE-KINASE-RXN -            3.6e-14   49.8   0.0   5.1e-14   49.3   0.0   1.2   1   0   0   1   1   1   1 D-glycero-alpha-D-manno-heptose-7-phosphate kinase
PROKKA_02158         -          MYCOCEROSATE-SYNTHASE-RXN -            1.6e-29   98.7   3.2   1.7e-29   98.5   3.2   1.0   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier-protein] synthase II
PROKKA_01717         -          MYCOCEROSATE-SYNTHASE-RXN -            5.8e-24   80.2   0.3   6.6e-24   80.0   0.3   1.0   1   0   0   1   1   1   1 NADPH2:quinone reductase
PROKKA_00022         -          MYCOCEROSATE-SYNTHASE-RXN -            2.4e-15   51.6   0.1   9.3e-15   49.7   0.1   1.7   1   1   0   1   1   1   1 NADPH:quinone reductase
PROKKA_01239         -          MYCOCEROSATE-SYNTHASE-RXN -            4.5e-14   47.4   1.3   5.4e-14   47.1   1.3   1.0   1   0   0   1   1   1   1 NADPH2:quinone reductase
PROKKA_02161         -          MYCOCEROSATE-SYNTHASE-RXN -            2.1e-11   38.5   0.0   2.9e-11   38.1   0.0   1.2   1   0   0   1   1   1   1 [acyl-carrier-protein] S-malonyltransferase
PROKKA_02069         -          MYCOCEROSATE-SYNTHASE-RXN -            1.5e-05   19.2   4.4    0.0004   14.4   1.1   2.0   1   1   1   2   2   2   2 alcohol dehydrogenase, propanol-preferring
PROKKA_00507         -          MYCOCEROSATE-SYNTHASE-RXN -              0.003   11.5   0.5     0.095    6.5   0.2   2.0   2   0   0   2   2   2   2 putative zinc-type alcohol dehydrogenase-like protein
PROKKA_02159         -          MYCOCEROSATE-SYNTHASE-RXN -             0.0033   11.4   0.0    0.0034   11.3   0.0   1.0   1   0   0   1   1   1   1 acyl carrier protein
PROKKA_02484         -          MYO-INOSITOL-1OR-4-MONOPHOSPHATASE-RXN -            3.8e-76  252.9   0.0   4.4e-76  252.7   0.0   1.0   1   0   0   1   1   1   1 myo-inositol-1(or 4)-monophosphatase
PROKKA_01296         -          MYO-INOSITOL-2-DEHYDROGENASE-RXN -            6.7e-15   52.2   0.0   9.8e-15   51.6   0.0   1.2   1   0   0   1   1   1   1 putative dehydrogenase
PROKKA_02392         -          N-ACETYLGLUTPREDUCT-RXN -            4.3e-82  273.0   0.0   5.3e-82  272.7   0.0   1.0   1   0   0   1   1   1   1 N-acetyl-gamma-glutamyl-phosphate reductase
PROKKA_02535         -          N-ACETYLGLUTPREDUCT-RXN -            3.2e-23   79.6   0.0   4.3e-18   62.7   0.0   2.0   2   0   0   2   2   2   2 aspartate-semialdehyde dehydrogenase
PROKKA_02146         -          N-ACETYLTRANSFER-RXN -            2.3e-52  175.6   0.0   1.8e-29  100.2   0.0   2.0   2   0   0   2   2   2   2 N-acetylglutamate synthase
PROKKA_02391         -          N-ACETYLTRANSFER-RXN -            7.9e-22   74.9   0.4   9.8e-22   74.6   0.4   1.1   1   0   0   1   1   1   1 glutamate N-acetyltransferase
PROKKA_00259         -          N-ACETYLTRANSFER-RXN -            4.4e-10   36.2   0.0   3.3e-06   23.4   0.0   2.0   1   1   1   2   2   2   2 N-acetylglutamate kinase
PROKKA_00788         -          N-ACETYLTRANSFER-RXN -              0.031   10.4   0.0     0.041    9.9   0.0   1.2   1   0   0   1   1   1   0 shikimate dehydrogenase 
PROKKA_02160         -          N-ACYLMANNOSAMINE-1-DEHYDROGENASE-RXN -            9.9e-29   97.9   7.0   1.3e-28   97.6   7.0   1.0   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier-protein] reductase
PROKKA_00773         -          N-ACYLMANNOSAMINE-1-DEHYDROGENASE-RXN -            2.2e-08   31.1   0.0   4.7e-08   30.1   0.0   1.4   1   1   0   1   1   1   1 short chain dehydrogenase
PROKKA_00235         -          N-ACYLMANNOSAMINE-1-DEHYDROGENASE-RXN -            3.3e-07   27.3   0.1   4.3e-07   26.9   0.1   1.2   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier protein] reductase
PROKKA_02149         -          N-ACYLMANNOSAMINE-1-DEHYDROGENASE-RXN -            8.1e-07   26.0   0.4     1e-06   25.7   0.4   1.2   1   0   0   1   1   1   1 NADP-dependent 3-hydroxy acid dehydrogenase YdfG
PROKKA_00820         -          N-ACYLMANNOSAMINE-1-DEHYDROGENASE-RXN -            4.8e-06   23.5   0.8     1e-05   22.4   0.8   1.5   1   1   0   1   1   1   1 Enoyl-[acyl-carrier-protein] reductase [NADH]
PROKKA_02004         -          N-CARBAMOYLSARCOSINE-AMIDASE-RXN -              0.014   11.7   0.0     0.022   11.0   0.0   1.4   1   1   0   1   1   1   0 Nicotinamidase-related amidase
PROKKA_01367         -          NAD-SYNTH-GLN-RXN    -           2.3e-106  353.8   0.0  3.7e-106  353.1   0.0   1.3   1   1   0   1   1   1   1 NAD+ synthase (glutamine-hydrolysing)
PROKKA_01902         -          NAD-SYNTH-GLN-RXN    -             0.0012   15.0   0.0    0.0065   12.5   0.0   1.9   2   0   0   2   2   2   1 GMP synthase (glutamine-hydrolysing)
PROKKA_01367         -          NAD-SYNTH-NH3-RXN    -           5.8e-113  375.2   0.0   9.2e-75  249.1   0.0   2.0   2   0   0   2   2   2   2 NAD+ synthase (glutamine-hydrolysing)
PROKKA_00264         -          NAD-SYNTH-NH3-RXN    -            6.6e-10   35.1   0.0     1e-09   34.4   0.0   1.3   1   1   0   1   1   1   1 putative amidohydrolase
PROKKA_00007         -          NAD-SYNTH-NH3-RXN    -              0.008   11.7   0.1     0.011   11.2   0.1   1.1   1   0   0   1   1   1   1 7-cyano-7-deazaguanine synthase
PROKKA_01902         -          NAD-SYNTH-NH3-RXN    -              0.017   10.6   0.0     0.024   10.1   0.0   1.1   1   0   0   1   1   1   0 GMP synthase (glutamine-hydrolysing)
PROKKA_00698         -          NADH-DEHYDROG-A-RXN_3 -            4.9e-31  105.8  31.2   4.9e-31  105.8  31.2   2.5   1   1   1   2   2   1   1 NADH-quinone oxidoreductase subunit L
PROKKA_02316         -          NADH-DEHYDROG-A-RXN_3 -            5.2e-14   50.4  29.8   1.2e-12   46.0  29.8   2.8   1   1   0   1   1   1   1 NADH-quinone oxidoreductase subunit L
PROKKA_01220         -          NADH-DEHYDROG-A-RXN_3 -              9e-09   33.4  39.2   5.7e-07   27.5  39.2   2.9   1   1   0   1   1   1   1 NAD(P)H-quinone oxidoreductase subunit 5
PROKKA_00700         -          NADH-DEHYDROG-A-RXN_3 -              1e-08   33.2  23.0     1e-08   33.2  23.0   2.7   1   1   1   2   2   2   1 NADH dehydrogenase subunit N 
PROKKA_00699         -          NADH-DEHYDROG-A-RXN_3 -            1.1e-08   33.1  63.5   3.7e-07   28.1  63.4   2.3   1   1   0   1   1   1   1 NADH-quinone oxidoreductase subunit M
PROKKA_01828         -          NADH-DEHYDROG-A-RXN_3 -              2e-08   32.3  40.0     5e-07   27.7  39.8   2.8   1   1   0   1   1   1   1 NAD(P)H-quinone oxidoreductase subunit 5
PROKKA_01821         -          NADH-DEHYDROG-A-RXN_3 -            3.1e-07   28.4  43.1   2.9e-05   22.0  43.1   3.2   1   1   0   1   1   1   1 NAD(P)H-quinone oxidoreductase subunit 5
PROKKA_01750         -          NADH-DEHYDROG-A-RXN_3 -            3.6e-06   24.9  46.2   9.3e-05   20.3  46.2   2.9   1   1   0   1   1   1   1 Formate hydrogenlyase subunit 3/Multisubunit Na+/H+ antiporter, MnhD subunit
PROKKA_01753         -          NADH-DEHYDROG-A-RXN_3 -            2.9e-05   22.0  36.6    0.0048   14.7  36.6   2.7   1   1   0   1   1   1   1 hydrogenase-4 component F
PROKKA_00694         -          NADH-DEHYDROG-A-RXN_3 -            5.2e-05   21.1  23.3   0.00024   19.0  23.3   1.9   1   1   0   1   1   1   1 NADH dehydrogenase subunit H 
PROKKA_00889         -          NADH-DEHYDROG-A-RXN_3 -              0.048   11.5   7.4      0.06   11.2   7.4   1.1   1   0   0   1   1   1   0 hypothetical protein
PROKKA_01345         -          NADH-DEHYDROG-A-RXN_3 -               0.14   10.0   7.0     0.072   10.9   0.4   2.3   2   0   0   2   2   2   0 lipopolysaccharide export system permease protein
PROKKA_00698         -          NADH-DEHYDROG-A-RXN_4 -              2e-40  136.3  45.0   1.2e-39  133.9  44.7   2.2   1   1   0   1   1   1   1 NADH-quinone oxidoreductase subunit L
PROKKA_00699         -          NADH-DEHYDROG-A-RXN_4 -            7.5e-26   88.9  49.2   8.6e-26   88.7  48.1   1.6   1   1   0   1   1   1   1 NADH-quinone oxidoreductase subunit M
PROKKA_00700         -          NADH-DEHYDROG-A-RXN_4 -            1.1e-23   81.7  50.2   3.8e-22   76.8  50.2   2.7   1   1   0   1   1   1   1 NADH dehydrogenase subunit N 
PROKKA_01821         -          NADH-DEHYDROG-A-RXN_4 -            6.2e-19   66.3  25.4   6.2e-19   66.3  25.4   2.4   1   1   0   2   2   2   1 NAD(P)H-quinone oxidoreductase subunit 5
PROKKA_01220         -          NADH-DEHYDROG-A-RXN_4 -            6.8e-19   66.2  24.4   6.8e-19   66.2  24.4   2.7   1   1   1   2   2   2   1 NAD(P)H-quinone oxidoreductase subunit 5
PROKKA_01828         -          NADH-DEHYDROG-A-RXN_4 -            1.9e-18   64.8  36.2   5.4e-18   63.2  26.4   2.9   1   1   1   2   2   2   1 NAD(P)H-quinone oxidoreductase subunit 5
PROKKA_02316         -          NADH-DEHYDROG-A-RXN_4 -            1.2e-15   55.5  32.9   2.2e-15   54.8  31.5   2.0   1   1   0   1   1   1   1 NADH-quinone oxidoreductase subunit L
PROKKA_01753         -          NADH-DEHYDROG-A-RXN_4 -            1.2e-14   52.3  29.4   1.2e-14   52.3  29.4   2.3   1   1   1   2   2   2   1 hydrogenase-4 component F
PROKKA_01750         -          NADH-DEHYDROG-A-RXN_4 -            1.4e-12   45.6  45.8   7.2e-12   43.3  45.9   2.3   1   1   0   1   1   1   1 Formate hydrogenlyase subunit 3/Multisubunit Na+/H+ antiporter, MnhD subunit
PROKKA_00698         -          NADH-DEHYDROG-A-RXN_5 -            1.4e-51  173.1  11.1   2.3e-51  172.4  10.5   1.6   1   1   0   1   1   1   1 NADH-quinone oxidoreductase subunit L
PROKKA_00699         -          NADH-DEHYDROG-A-RXN_5 -            5.2e-38  128.6  25.0   5.2e-38  128.6  25.0   2.1   1   1   1   2   2   2   1 NADH-quinone oxidoreductase subunit M
PROKKA_00700         -          NADH-DEHYDROG-A-RXN_5 -            1.1e-24   84.8  22.0   1.6e-24   84.3  21.3   1.6   1   1   0   1   1   1   1 NADH dehydrogenase subunit N 
PROKKA_01753         -          NADH-DEHYDROG-A-RXN_5 -            1.5e-22   77.8  12.6   1.5e-22   77.8  12.6   1.7   2   1   0   2   2   2   1 hydrogenase-4 component F
PROKKA_02316         -          NADH-DEHYDROG-A-RXN_5 -            5.8e-21   72.6  16.3   1.3e-20   71.4  15.8   1.6   1   1   0   1   1   1   1 NADH-quinone oxidoreductase subunit L
PROKKA_01750         -          NADH-DEHYDROG-A-RXN_5 -            1.8e-20   71.0  19.5   3.5e-20   70.0  19.5   1.5   1   0   0   1   1   1   1 Formate hydrogenlyase subunit 3/Multisubunit Na+/H+ antiporter, MnhD subunit
PROKKA_01220         -          NADH-DEHYDROG-A-RXN_5 -            7.1e-16   55.9  12.2   7.1e-16   55.9  12.2   1.8   2   0   0   2   2   2   1 NAD(P)H-quinone oxidoreductase subunit 5
PROKKA_01821         -          NADH-DEHYDROG-A-RXN_5 -            5.5e-15   53.0  18.0   8.8e-15   52.3  17.5   1.5   1   1   0   1   1   1   1 NAD(P)H-quinone oxidoreductase subunit 5
PROKKA_01828         -          NADH-DEHYDROG-A-RXN_5 -            7.3e-15   52.6  17.5   2.2e-14   51.0  17.5   1.7   1   1   0   1   1   1   1 NAD(P)H-quinone oxidoreductase subunit 5
PROKKA_00694         -          NADH-DEHYDROG-A-RXN_5 -            4.3e-14   50.0  13.4   7.5e-14   49.3  13.4   1.4   1   0   0   1   1   1   1 NADH dehydrogenase subunit H 
PROKKA_02138         -          NADH-DEHYDROGENASE-RXN -            1.1e-10   38.9   0.0   1.8e-10   38.2   0.0   1.3   1   0   0   1   1   1   1 NADH dehydrogenase
PROKKA_01201         -          NADH-PEROXIDASE-RXN  -            5.9e-20   68.5   0.0   8.8e-20   67.9   0.0   1.1   1   0   0   1   1   1   1 nitrite reductase (NADH) large subunit
PROKKA_01157         -          NADH-PEROXIDASE-RXN  -            1.1e-10   37.9   1.1   2.5e-10   36.8   0.0   2.1   2   1   0   2   2   2   1 mercuric reductase
PROKKA_02417         -          NADH-PEROXIDASE-RXN  -            1.7e-10   37.3   0.2   3.2e-10   36.4   0.0   1.5   2   0   0   2   2   2   1 thioredoxin reductase (NADPH)
PROKKA_00559         -          NADH-PEROXIDASE-RXN  -            3.3e-09   33.1   0.4   1.1e-08   31.3   0.0   2.0   2   1   0   2   2   2   1 mercuric reductase
PROKKA_01909         -          NADH-PEROXIDASE-RXN  -            5.1e-07   25.8   0.0   1.8e-06   24.0   0.0   1.8   2   0   0   2   2   2   1 dihydrolipoamide dehydrogenase
PROKKA_01391         -          NADH-PEROXIDASE-RXN  -            8.4e-07   25.1   0.0   1.5e-06   24.3   0.0   1.3   1   0   0   1   1   1   1 dihydrolipoamide dehydrogenase
PROKKA_00137         -          NADH-PEROXIDASE-RXN  -              1e-05   21.6   0.0   1.6e-05   20.9   0.0   1.2   1   0   0   1   1   1   1 dihydrolipoamide dehydrogenase
PROKKA_01451         -          NADH-PEROXIDASE-RXN  -            0.00014   17.8   0.2     0.065    9.0   0.0   3.0   3   0   0   3   3   3   1 NADH-quinone oxidoreductase subunit F
PROKKA_00031         -          NADH-PEROXIDASE-RXN  -            0.00055   15.8   0.0     0.009   11.8   0.0   2.0   2   0   0   2   2   2   1 sulfide:quinone oxidoreductase
PROKKA_01944         -          NADH-PEROXIDASE-RXN  -              0.001   15.0   0.2     0.082    8.7   0.2   2.5   2   1   0   2   2   2   1 NADPH-dependent glutamate synthase beta chain
PROKKA_00565         -          NADH-PEROXIDASE-RXN  -              0.013   11.3   0.0     0.033   10.0   0.0   1.6   1   0   0   1   1   1   0 UDP-galactopyranose mutase
PROKKA_01612         -          NADPH--FERRIHEMOPROTEIN-REDUCTASE-RXN -            4.6e-09   32.3   0.0   7.6e-09   31.6   0.0   1.5   1   1   0   1   1   1   1 Ferredoxin-NADP reductase
PROKKA_00278         -          NADPH--FERRIHEMOPROTEIN-REDUCTASE-RXN -            2.4e-05   20.1   0.0   4.2e-05   19.3   0.0   1.3   1   0   0   1   1   1   1 NAD(P)H-flavin reductase
PROKKA_00469         -          NADPH-DEHYDROGENASE-FLAVIN-RXN -            0.00035   17.2   0.0   0.00052   16.6   0.0   1.2   1   0   0   1   1   1   1 NADH dehydrogenase
PROKKA_00620         -          NADPH-DEHYDROGENASE-FLAVIN-RXN -            0.00048   16.8   0.0   0.00095   15.8   0.0   1.5   2   0   0   2   2   2   1 dihydroflavonol-4-reductase
PROKKA_01783         -          NADPH-DEHYDROGENASE-FLAVIN-RXN -              0.019   11.5   0.0     0.029   10.9   0.0   1.3   1   0   0   1   1   1   0 Nucleoside-diphosphate-sugar epimerase
PROKKA_02084         -          NADPH-DEHYDROGENASE-RXN -            7.3e-76  252.9   0.0   9.2e-76  252.6   0.0   1.1   1   0   0   1   1   1   1 N-ethylmaleimide reductase
PROKKA_01852         -          NADPH-DEHYDROGENASE-RXN -            3.9e-26   89.2   0.0   6.4e-26   88.5   0.0   1.3   1   1   0   1   1   1   1 2,4-dienoyl-CoA reductase
PROKKA_02107         -          NAG1P-URIDYLTRANS-RXN -            2.7e-98  326.7   0.0   3.4e-98  326.4   0.0   1.0   1   0   0   1   1   1   1 bifunctional UDP-N-acetylglucosamine pyrophosphorylase / Glucosamine-1-phosphate N-acetyltransferase
PROKKA_01359         -          NAG1P-URIDYLTRANS-RXN -            2.1e-05   20.5   0.3   6.9e-05   18.8   0.3   1.6   1   1   0   1   1   1   1 acyl-[acyl-carrier-protein]--UDP-N-acetylglucosamine O-acyltransferase
PROKKA_00296         -          NAG1P-URIDYLTRANS-RXN -            3.5e-05   19.7   0.5   6.4e-05   18.9   0.4   1.3   1   1   0   1   1   1   1 Carbonic anhydrase or acetyltransferase, isoleucine patch superfamily
PROKKA_01292         -          NAG1P-URIDYLTRANS-RXN -            0.00026   16.9   7.9   0.00042   16.2   5.6   2.2   1   1   1   2   2   2   1 UDP-3-O-[3-hydroxymyristoyl] glucosamine N-acyltransferase
PROKKA_02470         -          NAG1P-URIDYLTRANS-RXN -             0.0016   14.3   0.0    0.0028   13.4   0.0   1.4   2   0   0   2   2   2   1 mannose-1-phosphate guanylyltransferase / phosphomannomutase
PROKKA_01782         -          NAG1P-URIDYLTRANS-RXN -             0.0029   13.4   0.0    0.0034   13.2   0.0   1.1   1   0   0   1   1   1   1 D-glycero-alpha-D-manno-heptose 1-phosphate guanylyltransferase
PROKKA_01325         -          NAG1P-URIDYLTRANS-RXN -              0.038    9.7   0.0     0.057    9.2   0.0   1.3   1   0   0   1   1   1   0 2-C-methyl-D-erythritol 2,4-cyclodiphosphate synthase
PROKKA_02431         -          NAPHTHALENE-12-DIOXYGENASE-RXN -            1.7e-06   24.6   0.0   1.8e-06   24.5   0.0   1.0   1   0   0   1   1   1   1 nitrite reductase (NADH) small subunit
PROKKA_00278         -          NAPHTHALENE-12-DIOXYGENASE-RXN -            0.00041   16.7   0.0    0.0012   15.2   0.0   1.8   1   1   1   2   2   2   1 NAD(P)H-flavin reductase
PROKKA_00514         -          NAPHTHOATE-SYN-RXN   -            1.7e-07   28.1   0.0     2e-07   27.8   0.0   1.2   1   0   0   1   1   1   1 DSF synthase
PROKKA_00288         -          NAPHTHOATE-SYN-RXN   -             0.0065   13.0   0.0    0.0094   12.5   0.0   1.1   1   0   0   1   1   1   1 serine protease, ClpP class
PROKKA_00043         -          NARINGENIN-CHALCONE-SYNTHASE-RXN -             0.0096   12.2   0.0     0.014   11.7   0.0   1.1   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier-protein] synthase-3
PROKKA_01612         -          NITRATE-REDUCTASE-NADH-RXN -            6.1e-09   31.5   0.0   7.5e-09   31.2   0.0   1.1   1   0   0   1   1   1   1 Ferredoxin-NADP reductase
PROKKA_00278         -          NITRATE-REDUCTASE-NADH-RXN -            9.9e-08   27.5   0.0   1.4e-07   27.0   0.0   1.2   1   0   0   1   1   1   1 NAD(P)H-flavin reductase
PROKKA_01204         -          NITRATE-REDUCTASE-NADH-RXN -            5.3e-07   25.1   0.0   7.6e-07   24.6   0.0   1.2   1   0   0   1   1   1   1 sulfoxide reductase catalytic subunit YedY
PROKKA_00765         -          NITRATE-REDUCTASE-NADH-RXN -             0.0018   13.4   0.0    0.0021   13.1   0.0   1.1   1   0   0   1   1   1   1 Ferredoxin-NADP reductase
PROKKA_01612         -          NITRATE-REDUCTASE-NADPH-RXN -            2.5e-17   59.4   0.0     3e-17   59.1   0.0   1.0   1   0   0   1   1   1   1 Ferredoxin-NADP reductase
PROKKA_01204         -          NITRATE-REDUCTASE-NADPH-RXN -            4.3e-08   28.8   0.0   5.7e-08   28.4   0.0   1.1   1   0   0   1   1   1   1 sulfoxide reductase catalytic subunit YedY
PROKKA_00278         -          NITRATE-REDUCTASE-NADPH-RXN -              2e-07   26.6   0.0   2.8e-07   26.1   0.0   1.2   1   0   0   1   1   1   1 NAD(P)H-flavin reductase
PROKKA_00765         -          NITRATE-REDUCTASE-NADPH-RXN -             0.0015   13.8   0.0    0.0016   13.7   0.0   1.1   1   0   0   1   1   1   1 Ferredoxin-NADP reductase
PROKKA_01612         -          NITRATE-REDUCTASE-NADPORNOPH-RXN -            5.1e-10   35.0   0.0   6.2e-10   34.7   0.0   1.1   1   0   0   1   1   1   1 Ferredoxin-NADP reductase
PROKKA_01204         -          NITRATE-REDUCTASE-NADPORNOPH-RXN -            2.2e-07   26.3   0.0   3.1e-07   25.8   0.0   1.2   1   0   0   1   1   1   1 sulfoxide reductase catalytic subunit YedY
PROKKA_00765         -          NITRATE-REDUCTASE-NADPORNOPH-RXN -            7.5e-05   17.9   0.0   9.7e-05   17.5   0.0   1.0   1   0   0   1   1   1   1 Ferredoxin-NADP reductase
PROKKA_00693         -          NITRATREDUCT-RXN     -            4.2e-39  131.5   0.0   4.3e-38  128.1   0.0   2.2   1   1   0   1   1   1   1 formate dehydrogenase major subunit
PROKKA_00278         -          NITRIC-OXIDE-SYNTHASE-RXN -            4.4e-05   18.4   0.0   5.2e-05   18.2   0.0   1.0   1   0   0   1   1   1   1 NAD(P)H-flavin reductase
PROKKA_01612         -          NITRIC-OXIDE-SYNTHASE-RXN -            0.00088   14.1   0.0     0.001   13.9   0.0   1.0   1   0   0   1   1   1   1 Ferredoxin-NADP reductase
PROKKA_01351         -          NITRITE-REDUCTASE-CYTOCHROME-RXN -              0.003   13.4   0.0    0.0036   13.1   0.0   1.0   1   0   0   1   1   1   1 Cytochrome C oxidase, cbb3-type, subunit III
PROKKA_01163         -          NITROGENASE-RXN      -           4.6e-172  569.7   0.8  5.3e-172  569.5   0.8   1.0   1   0   0   1   1   1   1 Mo-nitrogenase MoFe protein subunit NifD precursor
PROKKA_01164         -          NITROGENASE-RXN      -           2.4e-113  376.3   0.0  2.7e-113  376.1   0.0   1.0   1   0   0   1   1   1   1 Mo-nitrogenase MoFe protein subunit NifK
PROKKA_01165         -          NITROGENASE-RXN      -            8.6e-87  288.9   0.0   1.1e-86  288.5   0.0   1.0   1   0   0   1   1   1   1 nitrogenase molybdenum-cofactor synthesis protein NifE
PROKKA_01166         -          NITROGENASE-RXN      -            3.7e-71  237.3   0.0   4.7e-71  237.0   0.0   1.0   1   0   0   1   1   1   1 nitrogenase molybdenum-iron protein NifN
PROKKA_01162         -          NITROGENASE-RXN      -            2.4e-50  168.8   0.5   3.4e-50  168.3   0.5   1.2   1   0   0   1   1   1   1 nitrogenase iron protein NifH
PROKKA_01524         -          NUCLEOSIDE-DIP-KIN-RXN -            1.4e-51  171.2   0.0   1.6e-51  171.1   0.0   1.0   1   0   0   1   1   1   1 nucleoside diphosphate kinase
PROKKA_00307         -          O-ACETYLHOMOSERINE-THIOL-LYASE-RXN -            1.4e-75  252.0   0.0   6.2e-38  127.9   0.0   2.0   1   1   1   2   2   2   2 methionine-gamma-lyase
PROKKA_00307         -          O-SUCCHOMOSERLYASE-RXN -           1.3e-106  354.2   0.0  1.6e-106  354.0   0.0   1.0   1   0   0   1   1   1   1 methionine-gamma-lyase
PROKKA_00761         -          O-SUCCINYLBENZOATE-COA-LIG-RXN -              2e-28   96.5   0.0   2.1e-21   73.3   0.0   2.1   2   0   0   2   2   2   2 long-chain acyl-CoA synthetase
PROKKA_00084         -          O-SUCCINYLBENZOATE-COA-LIG-RXN -            8.1e-26   87.9   0.0   6.1e-21   71.8   0.0   2.9   2   1   0   2   2   2   2 acetyl-CoA synthetase
PROKKA_02069         -          OCTANOL-DEHYDROGENASE-RXN -            5.6e-18   62.5   0.2   4.3e-13   46.4   0.1   2.2   1   1   1   2   2   2   2 alcohol dehydrogenase, propanol-preferring
PROKKA_00507         -          OCTANOL-DEHYDROGENASE-RXN -            5.3e-13   46.1   1.8   8.3e-12   42.2   1.8   2.1   1   1   0   1   1   1   1 putative zinc-type alcohol dehydrogenase-like protein
PROKKA_00329         -          OCTANOL-DEHYDROGENASE-RXN -            7.7e-13   45.6   0.1   2.1e-12   44.2   0.1   1.5   1   1   1   2   2   2   1 alcohol dehydrogenase, propanol-preferring
PROKKA_00022         -          OCTANOL-DEHYDROGENASE-RXN -            3.6e-07   26.9   0.0    0.0063   13.0   0.0   2.1   1   1   0   2   2   2   2 NADPH:quinone reductase
PROKKA_01717         -          OCTANOL-DEHYDROGENASE-RXN -              6e-05   19.6   0.0   0.00013   18.5   0.0   1.5   1   0   0   1   1   1   1 NADPH2:quinone reductase
PROKKA_02160         -          OHACYL-COA-DEHYDROG-RXN -            3.5e-09   32.8   1.0   0.00027   16.7   0.2   2.0   1   1   1   2   2   2   2 3-oxoacyl-[acyl-carrier-protein] reductase
PROKKA_00514         -          OHACYL-COA-DEHYDROG-RXN -            8.2e-08   28.3   0.0   9.3e-08   28.1   0.0   1.1   1   0   0   1   1   1   1 DSF synthase
PROKKA_01610         -          OHACYL-COA-DEHYDROG-RXN -            8.4e-07   25.0   0.0   1.3e-06   24.3   0.0   1.5   1   1   0   1   1   1   1 3-hydroxybutyryl-CoA dehydrogenase
PROKKA_00652         -          OHACYL-COA-DEHYDROG-RXN -             0.0018   14.0   0.1    0.0022   13.7   0.1   1.1   1   0   0   1   1   1   1 adenylyltransferase and sulfurtransferase
PROKKA_00235         -          OHACYL-COA-DEHYDROG-RXN -              0.013   11.1   0.0     0.054    9.1   0.0   1.7   2   0   0   2   2   2   0 3-oxoacyl-[acyl-carrier protein] reductase
PROKKA_00255         -          OHACYL-COA-DEHYDROG-RXN -              0.014   11.1   0.0      0.02   10.5   0.0   1.1   1   0   0   1   1   1   0 methylenetetrahydrofolate--tRNA-(uracil-5-)-methyltransferase
PROKKA_00570         -          OHACYL-COA-DEHYDROG-RXN -              0.024   10.3   0.0     0.034    9.7   0.0   1.2   1   0   0   1   1   1   0 UDP-N-acetyl-D-glucosamine dehydrogenase
PROKKA_00514         -          OHBUTYRYL-COA-EPIM-RXN -            2.5e-11   39.7   0.0   2.9e-11   39.5   0.0   1.1   1   0   0   1   1   1   1 DSF synthase
PROKKA_01610         -          OHBUTYRYL-COA-EPIM-RXN -            2.2e-07   26.6   0.0   3.1e-07   26.2   0.0   1.1   1   0   0   1   1   1   1 3-hydroxybutyryl-CoA dehydrogenase
PROKKA_01992         -          OHMETHYLBILANESYN-RXN -           2.3e-111  368.9   0.0  2.7e-111  368.7   0.0   1.0   1   0   0   1   1   1   1 hydroxymethylbilane synthase
PROKKA_01594         -          OHMETPYRKIN-RXN      -            9.8e-21   71.1   0.0   1.2e-20   70.8   0.0   1.0   1   0   0   1   1   1   1 thiamine-phosphate pyrophosphorylase
PROKKA_00086         -          OHMETPYRKIN-RXN      -            2.5e-20   69.8   0.0   3.1e-20   69.4   0.0   1.0   1   0   0   1   1   1   1 thiamine-phosphate pyrophosphorylase
PROKKA_00088         -          OHMETPYRKIN-RXN      -            8.1e-20   68.1   0.0     1e-19   67.7   0.0   1.0   1   0   0   1   1   1   1 hydroxymethylpyrimidine/phosphomethylpyrimidine kinase
PROKKA_00899         -          OLIGOGALACTURONIDE-LYASE-RXN -             0.0017   14.3   0.3     0.047    9.6   0.0   2.6   1   1   2   3   3   3   2 TolB protein
PROKKA_02357         -          ORNCARBAMTRANSFER-RXN -           7.8e-112  370.7   0.0  8.9e-112  370.5   0.0   1.0   1   0   0   1   1   1   1 ornithine carbamoyltransferase
PROKKA_01920         -          ORNCARBAMTRANSFER-RXN -            5.5e-33  111.7   0.0   1.1e-32  110.7   0.0   1.5   1   1   0   1   1   1   1 aspartate carbamoyltransferase
PROKKA_02354         -          ORNDECARBOX-RXN      -            1.3e-33  113.6   0.0   1.7e-33  113.3   0.0   1.0   1   0   0   1   1   1   1 diaminopimelate decarboxylase
PROKKA_02358         -          ORNITHINE--OXO-ACID-AMINOTRANSFERASE-RXN -            2.2e-85  284.0   0.0   2.7e-85  283.7   0.0   1.0   1   0   0   1   1   1   1 acetylornithine/N-succinyldiaminopimelate aminotransferase
PROKKA_00349         -          ORNITHINE--OXO-ACID-AMINOTRANSFERASE-RXN -            1.3e-51  172.8   0.0   1.7e-51  172.4   0.0   1.1   1   0   0   1   1   1   1 diaminobutyrate-2-oxoglutarate transaminase
PROKKA_01321         -          ORNITHINE--OXO-ACID-AMINOTRANSFERASE-RXN -            2.1e-35  119.4   0.0   3.4e-35  118.7   0.0   1.3   1   0   0   1   1   1   1 adenosylmethionine-8-amino-7-oxononanoate aminotransferase
PROKKA_02461         -          ORNITHINE--OXO-ACID-AMINOTRANSFERASE-RXN -              8e-34  114.2   0.0   1.4e-33  113.4   0.0   1.3   1   1   0   1   1   1   1 glutamate-1-semialdehyde 2,1-aminomutase
PROKKA_00167         -          ORNITHINE--OXO-ACID-AMINOTRANSFERASE-RXN -              0.031   10.1   0.0     0.049    9.5   0.0   1.2   1   0   0   1   1   1   0 cobalt-zinc-cadmium resistance protein CzcA
PROKKA_01296         -          ORNITHINE-CYCLODEAMINASE-RXN -             0.0021   14.3   0.0     0.003   13.8   0.0   1.2   1   0   0   1   1   1   1 putative dehydrogenase
PROKKA_00049         -          OROPRIBTRANS-RXN     -            6.3e-46  153.8   0.0     8e-46  153.5   0.0   1.0   1   0   0   1   1   1   1 orotate phosphoribosyltransferase
PROKKA_01553         -          OROPRIBTRANS-RXN     -            5.3e-11   39.8   0.0   5.8e-10   36.4   0.0   2.5   2   1   0   2   2   2   1 adenine phosphoribosyltransferase
PROKKA_00110         -          OROPRIBTRANS-RXN     -            4.1e-06   23.8   0.0   5.9e-06   23.3   0.0   1.1   1   0   0   1   1   1   1 putative amidophosphoribosyltransferases
PROKKA_00407         -          OROPRIBTRANS-RXN     -              7e-05   19.8   0.0      0.04   10.8   0.0   2.1   2   0   0   2   2   2   2 putative phosphoribosyltransferase
PROKKA_01921         -          OROPRIBTRANS-RXN     -            0.00052   16.9   0.0    0.0014   15.5   0.0   1.6   2   0   0   2   2   2   1 pyrimidine operon attenuation protein / uracil phosphoribosyltransferase
PROKKA_01997         -          OROPRIBTRANS-RXN     -             0.0009   16.2   0.0    0.0016   15.4   0.0   1.3   2   0   0   2   2   2   1 hypoxanthine phosphoribosyltransferase
PROKKA_01627         -          OROPRIBTRANS-RXN     -             0.0021   14.9   0.0    0.0035   14.2   0.0   1.3   1   0   0   1   1   1   1 amidophosphoribosyltransferase
PROKKA_02516         -          OROTPDECARB-RXN      -            6.7e-31  104.7   0.0   7.7e-31  104.5   0.0   1.0   1   0   0   1   1   1   1 orotidine-5'-phosphate decarboxylase
PROKKA_01236         -          OXALATE-OXIDASE-RXN  -             0.0027   14.7   0.1    0.0032   14.5   0.1   1.1   1   0   0   1   1   1   1 Cupin domain protein
PROKKA_02025         -          OXALATE-OXIDASE-RXN  -              0.013   12.4   0.0     0.015   12.2   0.0   1.1   1   0   0   1   1   1   0 Cupin domain-containing protein
PROKKA_02541         -          OXALYL-COA-DECARBOXYLASE-RXN -              3e-36  122.4   0.0   1.3e-25   87.3   0.0   2.0   2   0   0   2   2   2   2 acetolactate synthase, large subunit
PROKKA_00263         -          PANTOATE-BETA-ALANINE-LIG-RXN -            6.4e-84  278.4   0.0   7.6e-84  278.1   0.0   1.0   1   0   0   1   1   1   1 pantoate--beta-alanine ligase
PROKKA_00220         -          PANTOTHENATE-KIN-RXN -              0.026   11.1   0.0      0.04   10.5   0.0   1.2   1   0   0   1   1   1   0 flagellar biosynthesis protein FlhF
PROKKA_02143         -          PEPSYNTH-RXN         -           3.9e-301  998.1   0.0  4.4e-301  997.9   0.0   1.0   1   0   0   1   1   1   1 phosphoenolpyruvate synthase
PROKKA_02492         -          PEPSYNTH-RXN         -              0.019   10.3   0.0     0.022   10.1   0.0   1.0   1   0   0   1   1   1   0 TusA-related sulfurtransferase
PROKKA_00716         -          PEPTIDE-ASPARTATE-BETA-DIOXYGENASE-RXN -              0.007   11.7   0.1       0.6    5.4   0.0   2.4   1   1   2   3   3   3   2 Tetratricopeptide repeat-containing protein
PROKKA_00713         -          PEPTIDE-ASPARTATE-BETA-DIOXYGENASE-RXN -             0.0077   11.6   0.0    0.0095   11.3   0.0   1.0   1   0   0   1   1   1   1 Tetratricopeptide repeat-containing protein
PROKKA_01001         -          PEPTIDYLAMIDOGLYCOLATE-LYASE-RXN -            8.6e-08   27.5   1.0    0.0046   11.9   0.0   3.1   2   1   1   3   3   3   2 Streptogramin lyase
PROKKA_01494         -          PEPTIDYLAMIDOGLYCOLATE-LYASE-RXN -            5.2e-07   24.9   0.0   6.9e-06   21.2   0.0   2.3   1   1   0   2   2   2   1 40-residue YVTN family beta-propeller repeat-containing protein
PROKKA_01046         -          PEPTIDYLAMIDOGLYCOLATE-LYASE-RXN -              0.015   10.2   4.0      0.22    6.3   0.1   3.4   3   1   1   4   4   4   0 Streptogramin lyase
PROKKA_01001         -          PEPTIDYLGLYCINE-MONOOXYGENASE-RXN -            9.5e-09   31.0   0.5    0.0045   12.2   0.0   3.3   2   1   1   3   3   3   3 Streptogramin lyase
PROKKA_01494         -          PEPTIDYLGLYCINE-MONOOXYGENASE-RXN -              1e-07   27.6   0.0   0.00028   16.2   0.0   2.5   1   1   2   3   3   3   2 40-residue YVTN family beta-propeller repeat-containing protein
PROKKA_01046         -          PEPTIDYLGLYCINE-MONOOXYGENASE-RXN -             0.0093   11.1   5.3      0.26    6.3   0.1   3.7   2   2   2   4   4   4   2 Streptogramin lyase
PROKKA_00780         -          PEPTIDYLPROLYL-ISOMERASE-RXN -            7.9e-36  120.8   0.0   9.7e-36  120.5   0.0   1.0   1   0   0   1   1   1   1 peptidyl-prolyl cis-trans isomerase A (cyclophilin A)
PROKKA_00746         -          PGLUCISOM-RXN        -           4.5e-202  669.5   0.0  5.8e-202  669.2   0.0   1.0   1   0   0   1   1   1   1 glucose-6-phosphate isomerase 
PROKKA_01705         -          PGLUCISOM-RXN        -            2.7e-15   53.1   0.0   7.6e-15   51.6   0.0   1.7   2   0   0   2   2   2   1 transaldolase
PROKKA_01562         -          PGLUCONDEHYDRAT-RXN  -           4.5e-148  491.5   1.3  5.7e-148  491.1   1.3   1.1   1   0   0   1   1   1   1 dihydroxy-acid dehydratase
PROKKA_01217         -          PGLUCONDEHYDRAT-RXN  -             5e-143  474.8   0.1  6.2e-143  474.5   0.1   1.0   1   0   0   1   1   1   1 dihydroxyacid dehydratase 
PROKKA_01824         -          PGLUCONDEHYDRAT-RXN  -             1e-140  467.1   0.1  1.3e-140  466.8   0.1   1.0   1   0   0   1   1   1   1 dihydroxyacid dehydratase 
PROKKA_01826         -          PGLUCONDEHYDRAT-RXN  -           2.6e-126  419.6   1.4  3.8e-126  419.0   1.4   1.1   1   0   0   1   1   1   1 dihydroxy-acid dehydratase
PROKKA_01253         -          PGLUCONDEHYDRAT-RXN  -           1.9e-125  416.7   2.8  2.7e-125  416.2   2.8   1.1   1   0   0   1   1   1   1 dihydroxy-acid dehydratase
PROKKA_01511         -          PGLYCDEHYDROG-RXN    -           2.7e-181  601.0   1.9  3.2e-181  600.7   1.9   1.0   1   0   0   1   1   1   1 D-3-phosphoglycerate dehydrogenase
PROKKA_00645         -          PGLYCDEHYDROG-RXN    -            4.4e-05   19.2   0.1   6.7e-05   18.6   0.1   1.2   1   0   0   1   1   1   1 adenosylhomocysteinase
PROKKA_02541         -          PGLYCDEHYDROG-RXN    -            0.00052   15.7   0.5    0.0009   14.9   0.5   1.3   1   0   0   1   1   1   1 acetolactate synthase, large subunit
PROKKA_01836         -          PGLYCDEHYDROG-RXN    -            0.00072   15.2   0.0     0.001   14.7   0.0   1.1   1   0   0   1   1   1   1 3-hydroxyisobutyrate dehydrogenase
PROKKA_00936         -          PGLYCDEHYDROG-RXN    -            0.00099   14.7   0.7     0.022   10.3   0.2   2.1   2   0   0   2   2   2   1 GTP pyrophosphokinase
PROKKA_01489         -          PGLYCDEHYDROG-RXN    -             0.0018   13.9   0.0    0.0026   13.4   0.0   1.1   1   0   0   1   1   1   1 5-(carboxyamino)imidazole ribonucleotide synthase
PROKKA_00598         -          PGLYCDEHYDROG-RXN    -             0.0034   13.0   0.0    0.0047   12.5   0.0   1.1   1   0   0   1   1   1   1 UDP-N-acetylmuramoylalanine--D-glutamate ligase
PROKKA_00507         -          PGLYCDEHYDROG-RXN    -               0.02   10.5   0.2      0.03    9.9   0.2   1.2   1   0   0   1   1   1   0 putative zinc-type alcohol dehydrogenase-like protein
PROKKA_00861         -          PGPPHOSPHA-RXN       -            0.00054   16.9   2.9     0.023   11.5   1.2   2.1   1   1   1   2   2   2   2 phosphatidylglycerophosphatase A
PROKKA_01862         -          PGPPHOSPHA-RXN       -              0.022   11.6   1.4      0.42    7.4   0.3   2.1   1   1   1   2   2   2   0 undecaprenyl-diphosphatase
PROKKA_01338         -          PHEAMINOTRANS-RXN    -            7.3e-50  167.3   0.0     9e-50  167.0   0.0   1.1   1   0   0   1   1   1   1 aspartate aminotransferase
PROKKA_00241         -          PHEAMINOTRANS-RXN    -            9.7e-32  107.6   0.0   1.2e-31  107.3   0.0   1.0   1   0   0   1   1   1   1 LL-diaminopimelate aminotransferase apoenzyme
PROKKA_01352         -          PHEAMINOTRANS-RXN    -            8.1e-31  104.6   0.0   9.8e-31  104.3   0.0   1.1   1   0   0   1   1   1   1 alanine-synthesizing transaminase
PROKKA_02268         -          PHEAMINOTRANS-RXN    -            2.9e-15   53.4   0.0     5e-15   52.6   0.0   1.4   1   1   0   1   1   1   1 histidinol-phosphate aminotransferase
PROKKA_01224         -          PHEAMINOTRANS-RXN    -            9.1e-09   32.0   0.0   1.2e-08   31.6   0.0   1.1   1   0   0   1   1   1   1 histidinol-phosphate aminotransferase
PROKKA_01558         -          PHEAMINOTRANS-RXN    -            0.00032   17.0   0.0   0.00046   16.5   0.0   1.2   1   0   0   1   1   1   1 L-threonine O-3-phosphate decarboxylase 
PROKKA_00095         -          PHENYLALANINE--TRNA-LIGASE-RXN -            2.7e-69  231.2   0.0   3.9e-69  230.7   0.0   1.4   1   1   0   1   1   1   1 phenylalanyl-tRNA synthetase, alpha subunit
PROKKA_00094         -          PHENYLALANINE--TRNA-LIGASE-RXN -            6.1e-37  124.7   0.0   7.3e-37  124.4   0.0   1.0   1   0   0   1   1   1   1 phenylalanyl-tRNA synthetase beta subunit
PROKKA_01341         -          PHENYLALANINE--TRNA-LIGASE-RXN -              0.012   11.7   0.2     0.093    8.8   0.0   2.2   3   0   0   3   3   3   0 lysyl-tRNA synthetase, class II
PROKKA_01906         -          PHENYLALANINE--TRNA-LIGASE-RXN -              0.016   11.3   0.1     0.033   10.3   0.1   1.5   1   0   0   1   1   1   0 aspartyl-tRNA synthetase 
PROKKA_01533         -          PHENYLALANINE-4-MONOOXYGENASE-RXN -            5.1e-10   35.6   0.0   7.5e-10   35.1   0.0   1.1   1   0   0   1   1   1   1 chorismate mutase / prephenate dehydratase
PROKKA_02082         -          PHOSACETYLGLUCOSAMINEMUT-RXN -              2e-10   37.2   0.0   0.00065   15.7   0.0   4.1   3   1   1   4   4   4   4 phosphoglucosamine mutase
PROKKA_00769         -          PHOSACETYLGLUCOSAMINEMUT-RXN -            4.6e-05   19.5   0.2     0.027   10.4   0.0   3.0   2   1   1   3   3   3   2 phosphomannomutase / phosphoglucomutase
PROKKA_01560         -          PHOSACETYLTRANS-RXN  -             0.0062   12.1   0.0      0.68    5.4   0.0   2.0   2   0   0   2   2   2   2 adenosylcobyric acid synthase (glutamine-hydrolysing)
PROKKA_00053         -          PHOSGLYPHOS-RXN      -           3.1e-162  537.2   0.0  3.4e-162  537.1   0.0   1.0   1   0   0   1   1   1   1 phosphoglycerate kinase
PROKKA_00769         -          PHOSMANMUT-RXN       -           2.2e-121  403.1   0.0  2.8e-121  402.7   0.0   1.0   1   0   0   1   1   1   1 phosphomannomutase / phosphoglucomutase
PROKKA_02082         -          PHOSMANMUT-RXN       -            1.4e-57  192.7   0.0     2e-57  192.2   0.0   1.1   1   0   0   1   1   1   1 phosphoglucosamine mutase
PROKKA_01394         -          PHOSMANMUT-RXN       -            3.2e-34  115.7   0.0   4.3e-34  115.3   0.0   1.0   1   0   0   1   1   1   1 phosphoglucomutase
PROKKA_00597         -          PHOSNACMURPENTATRANS-RXN -           9.4e-119  394.1   9.5    1e-118  394.0   9.5   1.0   1   0   0   1   1   1   1 Phospho-N-acetylmuramoyl-pentapeptide-transferase
PROKKA_01873         -          PHOSPHAGLYPSYN-RXN   -            1.8e-45  152.7  10.1   2.1e-45  152.4  10.1   1.0   1   0   0   1   1   1   1 CDP-diacylglycerol--glycerol-3-phosphate 3-phosphatidyltransferase
PROKKA_01696         -          PHOSPHAGLYPSYN-RXN   -            1.8e-30  103.8   8.3   2.2e-30  103.5   8.3   1.0   1   0   0   1   1   1   1 CDP-diacylglycerol--glycerol-3-phosphate 3-phosphatidyltransferase
PROKKA_02538         -          PHOSPHAGLYPSYN-RXN   -            1.6e-05   22.3   0.7   1.6e-05   22.3   0.7   1.9   2   0   0   2   2   2   1 CDP-diacylglycerol---serine O-phosphatidyltransferase
PROKKA_02539         -          PHOSPHASERDECARB-RXN -             0.0053   12.9   0.1      0.49    6.4   0.0   2.1   2   0   0   2   2   2   2 phosphatidylserine decarboxylase
PROKKA_02538         -          PHOSPHASERSYN-RXN    -            8.2e-40  134.4   4.4   1.1e-39  134.1   4.4   1.0   1   0   0   1   1   1   1 CDP-diacylglycerol---serine O-phosphatidyltransferase
PROKKA_01696         -          PHOSPHASERSYN-RXN    -              2e-05   21.8   3.3   2.5e-05   21.5   3.2   1.2   1   1   0   1   1   1   1 CDP-diacylglycerol--glycerol-3-phosphate 3-phosphatidyltransferase
PROKKA_01873         -          PHOSPHASERSYN-RXN    -             0.0042   14.2   0.1    0.0051   13.9   0.1   1.2   1   0   0   1   1   1   1 CDP-diacylglycerol--glycerol-3-phosphate 3-phosphatidyltransferase
PROKKA_00500         -          PHOSPHATIDYLCHOLINE-DESATURASE-RXN -            4.6e-06   23.1   2.1   0.00025   17.4   0.2   2.1   2   0   0   2   2   2   2 Fatty acid desaturase
PROKKA_01394         -          PHOSPHOGLUCMUT-RXN   -           2.4e-145  482.4   0.0  3.2e-145  482.0   0.0   1.0   1   0   0   1   1   1   1 phosphoglucomutase
PROKKA_00769         -          PHOSPHOGLUCMUT-RXN   -            1.1e-94  315.2   0.0   1.3e-94  315.0   0.0   1.0   1   0   0   1   1   1   1 phosphomannomutase / phosphoglucomutase
PROKKA_02082         -          PHOSPHOGLUCMUT-RXN   -            2.4e-68  228.4   0.0   8.2e-67  223.3   0.0   2.0   1   1   0   1   1   1   1 phosphoglucosamine mutase
PROKKA_01942         -          PLASTOQUINOL--PLASTOCYANIN-REDUCTASE-RXN -            1.2e-11   42.1   0.0   2.6e-11   41.0   0.0   1.6   1   1   0   1   1   1   1 hypothetical protein
PROKKA_01943         -          PLASTOQUINOL--PLASTOCYANIN-REDUCTASE-RXN -              3e-10   37.6   0.0   3.4e-10   37.4   0.0   1.2   1   0   0   1   1   1   1 cytochrome b6-f complex iron-sulfur subunit
PROKKA_00821         -          PLASTOQUINOL--PLASTOCYANIN-REDUCTASE-RXN -            7.1e-10   36.4   0.0   9.8e-10   35.9   0.0   1.3   1   0   0   1   1   1   1 menaquinol-cytochrome c reductase iron-sulfur subunit
PROKKA_00822         -          PLASTOQUINOL--PLASTOCYANIN-REDUCTASE-RXN -              6e-06   23.5   0.1     2e-05   21.9   0.0   1.8   2   0   0   2   2   2   1 ubiquinol-cytochrome c reductase cytochrome b subunit
PROKKA_00903         -          PNP-RXN              -            6.3e-14   49.0   0.0   1.1e-13   48.2   0.0   1.4   1   0   0   1   1   1   1 methylthioadenosine phosphorylase
PROKKA_02187         -          POLYAMINE-OXIDASE-RXN -              0.021   10.4   0.0     0.029    9.9   0.0   1.1   1   0   0   1   1   1   0 squalene-associated FAD-dependent desaturase
PROKKA_01719         -          POLYAMINE-OXIDASE-RXN -              0.022   10.4   0.0     0.036    9.6   0.0   1.2   1   0   0   1   1   1   0 oxygen-dependent protoporphyrinogen oxidase
PROKKA_00565         -          POLYAMINE-OXIDASE-RXN -              0.044    9.4   1.0      0.15    7.6   0.1   1.9   2   0   0   2   2   2   0 UDP-galactopyranose mutase
PROKKA_01994         -          PORPHOBILSYNTH-RXN   -           6.3e-143  473.0   0.0  6.9e-143  472.8   0.0   1.0   1   0   0   1   1   1   1 porphobilinogen synthase
PROKKA_01356         -          PPENTOMUT-RXN        -              0.032   10.1   0.0     0.044    9.7   0.0   1.1   1   0   0   1   1   1   0 2,3-bisphosphoglycerate-independent phosphoglycerate mutase
PROKKA_00936         -          PPGPPSYN-RXN         -           8.6e-202  669.3   0.0  1.1e-201  668.9   0.0   1.0   1   0   0   1   1   1   1 GTP pyrophosphokinase
PROKKA_01952         -          PRAISOM-RXN          -            3.6e-43  145.0   0.0   4.5e-43  144.7   0.0   1.0   1   0   0   1   1   1   1 phosphoribosylanthranilate isomerase
PROKKA_01533         -          PREPHENATEDEHYDRAT-RXN -            8.5e-85  281.7   0.0   1.1e-84  281.3   0.0   1.1   1   0   0   1   1   1   1 chorismate mutase / prephenate dehydratase
PROKKA_01535         -          PREPHENATEDEHYDROG-RXN -            2.4e-35  119.3   0.0   3.1e-35  119.0   0.0   1.0   1   0   0   1   1   1   1 prephenate dehydrogenase
PROKKA_02265         -          PRIBFAICARPISOM-RXN  -            4.7e-74  245.9   0.1   5.8e-74  245.6   0.0   1.0   1   0   0   1   1   1   1 1-(5-phosphoribosyl)-5-[(5-phosphoribosylamino)methylideneamino] imidazole-4-carboxamide isomerase
PROKKA_02264         -          PRIBFAICARPISOM-RXN  -            1.2e-37  126.7   0.0   1.6e-37  126.4   0.0   1.0   1   0   0   1   1   1   1 cyclase
PROKKA_00085         -          PRIBFAICARPISOM-RXN  -              0.022   11.3   0.1     0.039   10.5   0.1   1.4   1   0   0   1   1   1   0 thiazole synthase
PROKKA_02166         -          PROLINE--TRNA-LIGASE-RXN -           5.8e-112  371.9   0.0  6.6e-112  371.8   0.0   1.0   1   0   0   1   1   1   1 prolyl-tRNA synthetase
PROKKA_00090         -          PROLINE--TRNA-LIGASE-RXN -            2.4e-32  109.4   0.0   3.6e-32  108.9   0.0   1.2   1   0   0   1   1   1   1 threonyl-tRNA synthetase
PROKKA_00403         -          PROLINE--TRNA-LIGASE-RXN -            1.6e-10   37.5   0.0   2.3e-10   36.9   0.0   1.1   1   0   0   1   1   1   1 seryl-tRNA synthetase
PROKKA_01593         -          PROPIONYL-COA-CARBOXY-RXN -            3.1e-38  129.0   0.0   4.1e-38  128.5   0.0   1.1   1   0   0   1   1   1   1 acetyl-CoA carboxylase, biotin carboxylase subunit
PROKKA_01949         -          PROPIONYL-COA-CARBOXY-RXN -            4.9e-16   55.7   0.1   6.3e-16   55.3   0.1   1.0   1   0   0   1   1   1   1 acetyl-CoA carboxylase carboxyltransferase subunit alpha
PROKKA_01441         -          PROPIONYL-COA-CARBOXY-RXN -            8.3e-07   25.2   0.0   8.9e-07   25.1   0.0   1.2   1   0   0   1   1   1   1 acetyl-CoA carboxylase carboxyl transferase subunit alpha
PROKKA_00542         -          PROSTAGLANDIN-D-SYNTHASE-RXN -               0.15    8.7   3.2      0.18    8.4   1.1   1.9   2   0   0   2   2   2   0 hypothetical protein
PROKKA_00961         -          PROSTAGLANDIN-E2-9-REDUCTASE-RXN -             0.0003   17.3   0.0   0.00049   16.5   0.0   1.3   1   0   0   1   1   1   1 putative oxidoreductase
PROKKA_01886         -          PROSTAGLANDIN-E2-9-REDUCTASE-RXN -             0.0098   12.3   0.0      0.64    6.3   0.0   2.3   2   0   0   2   2   2   2 putative oxidoreductase
PROKKA_02298         -          PROTEIN-KINASE-RXN   -             0.0026   13.8   0.0    0.0049   12.9   0.0   1.4   1   0   0   1   1   1   1 2-octaprenylphenol hydroxylase 
PROKKA_01344         -          PROTOHEMEFERROCHELAT-RXN -           2.1e-107  356.3   0.0  2.5e-107  356.1   0.0   1.0   1   0   0   1   1   1   1 ferrochelatase
PROKKA_01719         -          PROTOPORGENOXI-RXN   -            3.3e-41  138.9   0.0   4.4e-41  138.4   0.0   1.0   1   0   0   1   1   1   1 oxygen-dependent protoporphyrinogen oxidase
PROKKA_01627         -          PRPPAMIDOTRANS-RXN   -           5.1e-182  602.7   0.0  6.4e-182  602.4   0.0   1.0   1   0   0   1   1   1   1 amidophosphoribosyltransferase
PROKKA_00569         -          PRPPAMIDOTRANS-RXN   -            1.2e-28   97.0   0.0   1.7e-28   96.5   0.0   1.1   1   0   0   1   1   1   1 glucosamine--fructose-6-phosphate aminotransferase (isomerizing)
PROKKA_02106         -          PRPPAMIDOTRANS-RXN   -            4.7e-26   88.5   0.0   6.2e-26   88.1   0.0   1.1   1   0   0   1   1   1   1 glucosamine--fructose-6-phosphate aminotransferase (isomerizing)
PROKKA_01551         -          PRPPAMIDOTRANS-RXN   -            3.6e-08   29.5   0.0   1.3e-07   27.6   0.0   1.8   2   1   0   2   2   2   1 glutamate synthase (NADPH/NADH) large chain
PROKKA_00407         -          PRPPAMIDOTRANS-RXN   -            2.6e-05   20.1   0.0    0.0027   13.4   0.0   2.0   2   0   0   2   2   2   2 putative phosphoribosyltransferase
PROKKA_01573         -          PRPPAMIDOTRANS-RXN   -              0.013   11.2   0.0     0.018   10.7   0.0   1.1   1   0   0   1   1   1   0 riboflavin synthase alpha chain
PROKKA_01692         -          PRPPSYN-RXN          -             7e-130  430.0   0.1  7.8e-130  429.8   0.1   1.0   1   0   0   1   1   1   1 ribose-phosphate pyrophosphokinase
PROKKA_00110         -          PRPPSYN-RXN          -             0.0059   12.9   0.0    0.0085   12.4   0.0   1.1   1   0   0   1   1   1   1 putative amidophosphoribosyltransferases
PROKKA_01627         -          PRPPSYN-RXN          -              0.013   11.8   0.0     0.016   11.5   0.0   1.2   1   0   0   1   1   1   0 amidophosphoribosyltransferase
PROKKA_01954         -          PRTRANS-RXN          -            8.1e-86  285.5   0.0     1e-85  285.1   0.0   1.0   1   0   0   1   1   1   1 anthranilate phosphoribosyltransferase
PROKKA_00144         -          PRTRANS-RXN          -              5e-16   56.1   0.0   6.5e-16   55.7   0.0   1.1   1   0   0   1   1   1   1 anthranilate phosphoribosyltransferase
PROKKA_02050         -          PSERPHOSPHA-RXN      -            2.3e-08   30.9   0.8   3.2e-05   20.6   0.8   2.4   1   1   0   1   1   1   1 Cu+-exporting ATPase
PROKKA_00454         -          PSERPHOSPHA-RXN      -            5.9e-06   23.0   0.3    0.0015   15.1   0.4   2.4   1   1   1   2   2   2   2 K+-transporting ATPase ATPase B chain
PROKKA_01874         -          PSERPHOSPHA-RXN      -             0.0043   13.6   0.0    0.0067   13.0   0.0   1.3   1   0   0   1   1   1   1 3-deoxy-D-manno-octulosonate 8-phosphate phosphatase (KDO 8-P phosphatase)
PROKKA_01312         -          PSERPHOSPHA-RXN      -              0.016   11.8   0.0     0.084    9.4   0.0   1.9   2   0   0   2   2   2   0 D-glycero-D-manno-heptose 1,7-bisphosphate phosphatase
PROKKA_01512         -          PSERTRANSAM-RXN      -            7.1e-08   28.8   0.0   1.1e-07   28.1   0.0   1.4   1   1   0   1   1   1   1 aspartate aminotransferase
PROKKA_00244         -          PURINE-NUCLEOSIDASE-RXN -            6.1e-51  170.7   0.0   7.9e-51  170.3   0.0   1.0   1   0   0   1   1   1   1 A/G-specific DNA-adenine glycosylase
PROKKA_01452         -          PURINE-NUCLEOSIDASE-RXN -              4e-19   65.9   0.0     5e-19   65.6   0.0   1.1   1   0   0   1   1   1   1 endonuclease-3
PROKKA_01888         -          PURINE-NUCLEOSIDASE-RXN -            3.5e-14   49.6   0.0   4.4e-14   49.3   0.0   1.0   1   0   0   1   1   1   1 DNA-(apurinic or apyrimidinic site) lyase /endonuclease III
PROKKA_00856         -          PUTRESCINE-N-METHYLTRANSFERASE-RXN -            1.5e-24   83.8   0.0   1.8e-24   83.5   0.0   1.2   1   1   0   1   1   1   1 spermidine synthase
PROKKA_01719         -          PUTRESCINE-OXIDASE-RXN -            3.2e-05   20.2   0.4   0.00024   17.3   0.1   2.0   2   0   0   2   2   2   1 oxygen-dependent protoporphyrinogen oxidase
PROKKA_00565         -          PUTRESCINE-OXIDASE-RXN -            0.00017   17.8   0.0   0.00024   17.3   0.0   1.2   1   0   0   1   1   1   1 UDP-galactopyranose mutase
PROKKA_02187         -          PUTRESCINE-OXIDASE-RXN -             0.0022   14.2   0.0    0.0033   13.5   0.0   1.2   1   0   0   1   1   1   1 squalene-associated FAD-dependent desaturase
PROKKA_01944         -          PUTRESCINE-OXIDASE-RXN -             0.0037   13.4   0.1    0.0037   13.4   0.1   1.8   2   0   0   2   2   2   1 NADPH-dependent glutamate synthase beta chain
PROKKA_01201         -          PUTRESCINE-OXIDASE-RXN -             0.0041   13.2   0.3    0.0088   12.1   0.3   1.4   1   0   0   1   1   1   1 nitrite reductase (NADH) large subunit
PROKKA_01702         -          PUTRESCINE-OXIDASE-RXN -              0.007   12.5   0.2     0.011   11.9   0.2   1.2   1   0   0   1   1   1   1 2-polyprenyl-6-methoxyphenol hydroxylase
PROKKA_01886         -          PYRIDOXINE-4-DEHYDROGENASE-RXN -            1.4e-44  149.5   0.0   1.9e-44  149.1   0.0   1.1   1   0   0   1   1   1   1 putative oxidoreductase
PROKKA_00961         -          PYRIDOXINE-4-DEHYDROGENASE-RXN -            7.7e-39  130.6   0.0     1e-38  130.2   0.0   1.1   1   0   0   1   1   1   1 putative oxidoreductase
PROKKA_01736         -          PYRIDOXINE-4-DEHYDROGENASE-RXN -            1.3e-11   41.2   0.0   3.9e-11   39.6   0.0   1.8   1   1   0   1   1   1   1 putative oxidoreductase
PROKKA_00088         -          PYRIMSYN3-RXN        -              7e-28   94.6   0.0     9e-28   94.2   0.0   1.0   1   0   0   1   1   1   1 hydroxymethylpyrimidine/phosphomethylpyrimidine kinase
PROKKA_01879         -          PYRIMSYN3-RXN        -             0.0085   12.4   0.0     0.015   11.5   0.0   1.4   1   0   0   1   1   1   1 D-beta-D-heptose 7-phosphate kinase / D-beta-D-heptose 1-phosphate adenosyltransferase
PROKKA_02282         -          PYRROLINECARBDEHYDROG-RXN -            1.7e-56  188.9   0.0   2.2e-56  188.5   0.0   1.0   1   0   0   1   1   1   1 Acyl-CoA reductase
PROKKA_00706         -          PYRROLINECARBDEHYDROG-RXN -            6.5e-08   28.0   0.0   1.2e-07   27.1   0.0   1.4   1   1   0   1   1   1   1 L-proline dehydrogenase
PROKKA_00610         -          PYRROLINECARBREDUCT-RXN -            7.4e-61  203.1   0.0   8.7e-61  202.8   0.0   1.0   1   0   0   1   1   1   1 pyrroline-5-carboxylate reductase
PROKKA_01535         -          PYRROLINECARBREDUCT-RXN -            3.6e-08   30.3   0.0   4.8e-08   29.9   0.0   1.1   1   0   0   1   1   1   1 prephenate dehydrogenase
PROKKA_02151         -          PYRROLINECARBREDUCT-RXN -              0.025   11.1   0.0     0.035   10.6   0.0   1.2   1   0   0   1   1   1   0 6-phosphogluconate dehydrogenase (decarboxylating) 
PROKKA_02457         -          PYRUFLAVREDUCT-RXN   -            1.1e-20   71.7   0.1   1.4e-20   71.4   0.1   1.0   1   0   0   1   1   1   1 pyruvate ferredoxin oxidoreductase beta subunit
PROKKA_02452         -          PYRUFLAVREDUCT-RXN   -              7e-16   56.0   0.0   8.3e-16   55.8   0.0   1.1   1   0   0   1   1   1   1 pyruvate ferredoxin oxidoreductase beta subunit
PROKKA_02458         -          PYRUFLAVREDUCT-RXN   -            1.1e-12   45.5   0.0   1.5e-12   45.1   0.0   1.1   1   0   0   1   1   1   1 pyruvate ferredoxin oxidoreductase alpha subunit
PROKKA_02453         -          PYRUFLAVREDUCT-RXN   -            2.8e-07   27.8   0.3   1.5e-06   25.4   0.3   1.9   1   1   0   1   1   1   1 pyruvate ferredoxin oxidoreductase alpha subunit
PROKKA_01593         -          PYRUVATE-CARBOXYLASE-RXN -           1.3e-165  550.1   0.0  1.6e-165  549.8   0.0   1.0   1   0   0   1   1   1   1 acetyl-CoA carboxylase, biotin carboxylase subunit
PROKKA_01916         -          PYRUVATE-CARBOXYLASE-RXN -              6e-25   84.2   0.0   2.9e-13   45.5   0.0   2.1   2   0   0   2   2   2   2 carbamoyl-phosphate synthase large subunit
PROKKA_01592         -          PYRUVATE-CARBOXYLASE-RXN -            1.6e-09   33.1   0.0     2e-09   32.8   0.0   1.0   1   0   0   1   1   1   1 acetyl-CoA carboxylase biotin carboxyl carrier protein
PROKKA_02537         -          PYRUVATE-CARBOXYLASE-RXN -              9e-07   24.0   0.0   3.5e-06   22.1   0.0   1.7   2   0   0   2   2   2   1 2-isopropylmalate synthase
PROKKA_01333         -          PYRUVATE-CARBOXYLASE-RXN -            8.7e-06   20.8   0.0   1.3e-05   20.2   0.0   1.2   1   0   0   1   1   1   1 phosphoribosylamine--glycine ligase
PROKKA_01489         -          PYRUVATE-CARBOXYLASE-RXN -            5.3e-05   18.2   0.0   7.1e-05   17.7   0.0   1.0   1   0   0   1   1   1   1 5-(carboxyamino)imidazole ribonucleotide synthase
PROKKA_02038         -          PYRUVATE-CARBOXYLASE-RXN -             0.0015   13.3   0.0     0.002   12.9   0.0   1.1   1   0   0   1   1   1   1 RND family efflux transporter, MFP subunit
PROKKA_02541         -          PYRUVATE-DECARBOXYLASE-RXN -            9.3e-52  173.1   0.0   1.3e-51  172.7   0.0   1.2   1   0   0   1   1   1   1 acetolactate synthase, large subunit
PROKKA_00681         -          PYRUVATE-DECARBOXYLASE-RXN -            0.00085   14.8   0.0    0.0013   14.2   0.0   1.2   1   0   0   1   1   1   1 1-deoxy-D-xylulose-5-phosphate synthase
PROKKA_02143         -          PYRUVATEORTHOPHOSPHATE-DIKINASE-RXN -            9.1e-29   97.3   1.7   1.2e-11   40.6   0.1   4.5   3   1   2   5   5   5   4 phosphoenolpyruvate synthase
PROKKA_01842         -          PYRUVFORMLY-RXN      -             0.0099   11.2   9.9     0.027    9.8   0.6   2.3   2   0   0   2   2   2   2 hypothetical protein
PROKKA_01717         -          QOR-RXN              -            1.7e-50  169.2   0.0   1.8e-50  169.1   0.0   1.0   1   0   0   1   1   1   1 NADPH2:quinone reductase
PROKKA_00022         -          QOR-RXN              -            1.8e-30  103.4   0.0   1.8e-22   77.1   0.0   2.2   2   1   0   2   2   2   2 NADPH:quinone reductase
PROKKA_02069         -          QOR-RXN              -            1.1e-22   77.8   0.0     1e-13   48.3   0.0   2.1   2   0   0   2   2   2   2 alcohol dehydrogenase, propanol-preferring
PROKKA_01239         -          QOR-RXN              -            5.3e-18   62.4   0.0   6.5e-18   62.1   0.0   1.0   1   0   0   1   1   1   1 NADPH2:quinone reductase
PROKKA_00507         -          QOR-RXN              -              3e-12   43.5   0.0   3.4e-06   23.5   0.0   2.2   2   0   0   2   2   2   2 putative zinc-type alcohol dehydrogenase-like protein
PROKKA_00329         -          QOR-RXN              -             0.0025   14.2   0.0      0.34    7.1   0.0   2.6   3   0   0   3   3   3   2 alcohol dehydrogenase, propanol-preferring
PROKKA_00930         -          QUEUOSINE-TRNA-RIBOSYLTRANSFERASE-RXN -            2.7e-99  330.1   0.0   3.1e-99  329.9   0.0   1.0   1   0   0   1   1   1   1 tRNA-guanine transglycosylase
PROKKA_00788         -          QUINATE-5-DEHYDROGENASE-RXN -            3.6e-08   29.8   0.0   5.2e-08   29.2   0.0   1.1   1   0   0   1   1   1   1 shikimate dehydrogenase 
PROKKA_00868         -          QUINOPRIBOTRANS-RXN  -            4.9e-80  266.1   0.0   5.9e-80  265.9   0.0   1.0   1   0   0   1   1   1   1 nicotinate-nucleotide pyrophosphorylase [carboxylating]
PROKKA_01549         -          R-6-HYDROXYNICOTINE-OXIDASE-RXN -            2.6e-06   23.5   0.0   0.00017   17.5   0.1   2.4   2   0   0   2   2   2   2 FAD/FMN-containing dehydrogenase
PROKKA_00329         -          RETINOL-DEHYDROGENASE-RXN -            1.9e-13   47.3   0.1   2.9e-13   46.7   0.1   1.2   1   0   0   1   1   1   1 alcohol dehydrogenase, propanol-preferring
PROKKA_02069         -          RETINOL-DEHYDROGENASE-RXN -            1.5e-12   44.4   1.0   9.6e-11   38.4   0.2   2.5   1   1   1   2   2   2   2 alcohol dehydrogenase, propanol-preferring
PROKKA_00507         -          RETINOL-DEHYDROGENASE-RXN -            4.7e-09   32.8   3.0   1.4e-08   31.3   3.0   1.7   1   1   0   1   1   1   1 putative zinc-type alcohol dehydrogenase-like protein
PROKKA_00022         -          RETINOL-DEHYDROGENASE-RXN -            5.7e-07   26.0   0.0    0.0035   13.5   0.0   2.1   2   0   0   2   2   2   2 NADPH:quinone reductase
PROKKA_02160         -          RETINOL-DEHYDROGENASE-RXN -            3.1e-06   23.5   0.8   5.4e-06   22.8   0.7   1.4   1   1   0   1   1   1   1 3-oxoacyl-[acyl-carrier-protein] reductase
PROKKA_01239         -          RETINOL-DEHYDROGENASE-RXN -            0.00023   17.4   0.3    0.0004   16.6   0.1   1.5   1   1   0   1   1   1   1 NADPH2:quinone reductase
PROKKA_00235         -          RETINOL-DEHYDROGENASE-RXN -            0.00049   16.3   0.0   0.00086   15.5   0.0   1.3   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier protein] reductase
PROKKA_01717         -          RETINOL-DEHYDROGENASE-RXN -             0.0032   13.6   0.0    0.0055   12.9   0.0   1.3   1   0   0   1   1   1   1 NADPH2:quinone reductase
PROKKA_00773         -          RETINOL-DEHYDROGENASE-RXN -             0.0035   13.5   0.0    0.0048   13.1   0.0   1.2   1   0   0   1   1   1   1 short chain dehydrogenase
PROKKA_02372         -          RIB5PISOM-RXN        -             0.0017   15.3   0.0    0.0029   14.6   0.0   1.5   1   1   0   1   1   1   1 ribose 5-phosphate isomerase B
PROKKA_01340         -          RIB5PISOM-RXN        -             0.0038   14.2   0.1    0.0065   13.4   0.1   1.3   1   0   0   1   1   1   1 lipoprotein-releasing system permease protein
PROKKA_02160         -          RIBITOL-2-DEHYDROGENASE-RXN -            5.1e-23   79.0   1.3     6e-23   78.8   1.3   1.0   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier-protein] reductase
PROKKA_00235         -          RIBITOL-2-DEHYDROGENASE-RXN -            2.7e-20   70.1   0.0   3.7e-20   69.6   0.0   1.1   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier protein] reductase
PROKKA_02149         -          RIBITOL-2-DEHYDROGENASE-RXN -            9.6e-17   58.5   0.1   1.2e-16   58.2   0.1   1.1   1   0   0   1   1   1   1 NADP-dependent 3-hydroxy acid dehydrogenase YdfG
PROKKA_00773         -          RIBITOL-2-DEHYDROGENASE-RXN -            1.3e-07   28.6   0.0   1.7e-07   28.2   0.0   1.1   1   0   0   1   1   1   1 short chain dehydrogenase
PROKKA_00170         -          RIBITOL-2-DEHYDROGENASE-RXN -            8.5e-05   19.3   0.0   0.00011   19.0   0.0   1.1   1   0   0   1   1   1   1 NAD(P)-dependent dehydrogenase, short-chain alcohol dehydrogenase family
PROKKA_01573         -          RIBOFLAVIN-SYN-RXN   -            5.9e-43  143.9   0.0   7.3e-43  143.6   0.0   1.0   1   0   0   1   1   1   1 riboflavin synthase alpha chain
PROKKA_01369         -          RIBOFLAVIN-SYN-RXN   -            1.5e-10   38.0   0.0   1.9e-10   37.7   0.0   1.1   1   0   0   1   1   1   1 6,7-dimethyl-8-ribityllumazine synthase
PROKKA_01995         -          RIBOFLAVINKIN-RXN    -            2.1e-40  136.1   0.0   1.7e-39  133.2   0.0   1.8   1   1   0   1   1   1   1 riboflavin kinase / FMN adenylyltransferase
PROKKA_01879         -          RIBOKIN-RXN          -            4.7e-24   82.6   0.1   1.1e-23   81.4   0.1   1.4   1   1   0   1   1   1   1 D-beta-D-heptose 7-phosphate kinase / D-beta-D-heptose 1-phosphate adenosyltransferase
PROKKA_00904         -          RIBOKIN-RXN          -            1.2e-13   48.4   0.0   2.9e-13   47.2   0.0   1.5   2   0   0   2   2   2   1 Sugar or nucleoside kinase, ribokinase family
PROKKA_01041         -          RIBONUCLEOSIDE-DIP-REDUCTI-RXN -             5e-136  452.0   0.0  6.8e-136  451.5   0.0   1.1   1   0   0   1   1   1   1 ribonucleoside-diphosphate reductase class II
PROKKA_00360         -          RIBONUCLEOSIDE-DIP-REDUCTI-RXN -             4e-133  442.4   0.0  5.2e-133  442.0   0.0   1.1   1   0   0   1   1   1   1 ribonucleoside-diphosphate reductase class II
PROKKA_02524         -          RIBULOSE-BISPHOSPHATE-CARBOXYLASE-RXN -            2.5e-32  109.6   0.0   3.2e-32  109.3   0.0   1.0   1   0   0   1   1   1   1 ribulose-bisphosphate carboxylase large chain
PROKKA_02119         -          RIBULOSE-BISPHOSPHATE-CARBOXYLASE-RXN -              1e-28   97.7   0.0   1.3e-28   97.4   0.0   1.0   1   0   0   1   1   1   1 2,3-diketo-5-methylthiopentyl-1-phosphate enolase
PROKKA_02408         -          RIBULP3EPIM-RXN      -            8.7e-83  274.2   0.0   1.1e-82  273.9   0.0   1.0   1   0   0   1   1   1   1 ribulose-5-phosphate 3-epimerase 
PROKKA_02117         -          RIBULPEPIM-RXN       -            6.6e-10   35.9   0.0   1.2e-08   31.8   0.0   1.9   2   0   0   2   2   2   2 methylthioribulose-1-phosphate dehydratase
PROKKA_01701         -          RNA-DIRECTED-DNA-POLYMERASE-RXN_11 -             0.0032   12.8   0.0    0.0045   12.3   0.0   1.1   1   0   0   1   1   1   1 trigger factor
PROKKA_00015         -          RNA-DIRECTED-DNA-POLYMERASE-RXN_9 -             0.0045   11.9   0.0    0.0045   11.9   0.0   1.0   1   0   0   1   1   1   1 putative transposase
PROKKA_01798         -          RNA-DIRECTED-DNA-POLYMERASE-RXN_9 -             0.0045   11.9   0.0    0.0045   11.9   0.0   1.0   1   0   0   1   1   1   1 putative transposase
PROKKA_01786         -          RNA-DIRECTED-DNA-POLYMERASE-RXN_9 -             0.0059   11.5   0.0    0.0068   11.3   0.0   1.0   1   0   0   1   1   1   1 putative transposase
PROKKA_01021         -          RNA-DIRECTED-RNA-POLYMERASE-RXN -             0.0025   11.0   0.0    0.0035   10.5   0.0   1.1   1   0   0   1   1   1   1 exodeoxyribonuclease-5
PROKKA_00507         -          RR-BUTANEDIOL-DEHYDROGENASE-RXN -            1.6e-09   34.5   1.3   3.7e-09   33.3   1.3   1.5   1   1   0   1   1   1   1 putative zinc-type alcohol dehydrogenase-like protein
PROKKA_00329         -          RR-BUTANEDIOL-DEHYDROGENASE-RXN -            1.7e-08   31.1   0.0   2.7e-08   30.5   0.0   1.2   1   0   0   1   1   1   1 alcohol dehydrogenase, propanol-preferring
PROKKA_02069         -          RR-BUTANEDIOL-DEHYDROGENASE-RXN -            3.3e-08   30.2   0.1   4.9e-08   29.6   0.1   1.3   1   1   0   1   1   1   1 alcohol dehydrogenase, propanol-preferring
PROKKA_01201         -          RUBREDOXIN--NAD+-REDUCTASE-RXN -            5.3e-25   85.3   0.0     8e-25   84.7   0.0   1.2   1   0   0   1   1   1   1 nitrite reductase (NADH) large subunit
PROKKA_01391         -          RUBREDOXIN--NAD+-REDUCTASE-RXN -            3.2e-14   49.8   0.0   1.4e-13   47.8   0.0   1.8   2   0   0   2   2   2   1 dihydrolipoamide dehydrogenase
PROKKA_01157         -          RUBREDOXIN--NAD+-REDUCTASE-RXN -            3.4e-13   46.4   0.0   6.4e-13   45.6   0.0   1.3   1   0   0   1   1   1   1 mercuric reductase
PROKKA_01909         -          RUBREDOXIN--NAD+-REDUCTASE-RXN -            7.8e-13   45.3   0.0   6.7e-12   42.2   0.0   2.0   2   0   0   2   2   2   1 dihydrolipoamide dehydrogenase
PROKKA_00559         -          RUBREDOXIN--NAD+-REDUCTASE-RXN -            1.9e-09   34.1   0.2     7e-09   32.3   0.0   2.0   2   0   0   2   2   2   1 mercuric reductase
PROKKA_00137         -          RUBREDOXIN--NAD+-REDUCTASE-RXN -            0.00017   17.8   0.0   0.00023   17.4   0.0   1.1   1   0   0   1   1   1   1 dihydrolipoamide dehydrogenase
PROKKA_02417         -          RUBREDOXIN--NAD+-REDUCTASE-RXN -            0.00041   16.6   0.0   0.00057   16.1   0.0   1.2   1   0   0   1   1   1   1 thioredoxin reductase (NADPH)
PROKKA_01201         -          RUBREDOXIN--NADP+-REDUCTASE-RXN -            3.9e-40  134.9   0.0   5.3e-40  134.5   0.0   1.1   1   0   0   1   1   1   1 nitrite reductase (NADH) large subunit
PROKKA_01391         -          RUBREDOXIN--NADP+-REDUCTASE-RXN -            2.1e-08   30.5   0.1   3.2e-08   29.9   0.1   1.3   1   0   0   1   1   1   1 dihydrolipoamide dehydrogenase
PROKKA_01157         -          RUBREDOXIN--NADP+-REDUCTASE-RXN -            7.2e-08   28.7   0.1   7.2e-08   28.7   0.1   2.1   2   0   0   2   2   2   1 mercuric reductase
PROKKA_00559         -          RUBREDOXIN--NADP+-REDUCTASE-RXN -            4.6e-07   26.1   2.9   1.4e-06   24.5   0.0   2.2   2   0   0   2   2   2   1 mercuric reductase
PROKKA_01909         -          RUBREDOXIN--NADP+-REDUCTASE-RXN -            0.00095   15.1   0.0     0.021   10.7   0.0   2.2   2   0   0   2   2   2   1 dihydrolipoamide dehydrogenase
PROKKA_01451         -          RUBREDOXIN--NADP+-REDUCTASE-RXN -             0.0032   13.4   0.7      0.35    6.7   0.1   3.2   3   0   0   3   3   3   1 NADH-quinone oxidoreductase subunit F
PROKKA_00137         -          RUBREDOXIN--NADP+-REDUCTASE-RXN -             0.0087   12.0   3.4     0.045    9.6   0.1   2.1   2   0   0   2   2   2   1 dihydrolipoamide dehydrogenase
PROKKA_00507         -          RXN-11453            -            3.6e-09   32.9   6.1   5.2e-08   29.1   4.3   2.7   2   1   0   2   2   2   1 putative zinc-type alcohol dehydrogenase-like protein
PROKKA_00329         -          RXN-11453            -            3.8e-07   26.3   0.0   5.3e-07   25.8   0.0   1.1   1   0   0   1   1   1   1 alcohol dehydrogenase, propanol-preferring
PROKKA_02069         -          RXN-11453            -            3.5e-05   19.8   2.2   0.00022   17.1   2.2   1.9   1   1   0   1   1   1   1 alcohol dehydrogenase, propanol-preferring
PROKKA_00645         -          RXN-11453            -            0.00049   16.0   0.7    0.0013   14.6   0.2   1.8   2   0   0   2   2   2   1 adenosylhomocysteinase
PROKKA_00022         -          RXN-11453            -              0.018   10.9   0.1     0.024   10.4   0.1   1.2   1   0   0   1   1   1   0 NADPH:quinone reductase
PROKKA_01537         -          RXN-11832            -            6.8e-53  176.8   0.0     8e-53  176.6   0.0   1.0   1   0   0   1   1   1   1 cytidylate kinase
PROKKA_01643         -          RXN-11832            -            7.6e-13   45.9   0.0     4e-10   37.0   0.0   2.7   2   1   0   2   2   2   2 Adenylate kinase
PROKKA_01586         -          RXN-11832            -            8.4e-05   19.6   0.1   0.00018   18.5   0.0   1.6   2   0   0   2   2   2   1 guanylate kinase
PROKKA_00290         -          RXN-11832            -             0.0024   14.8   0.0    0.0054   13.7   0.0   1.5   1   0   0   1   1   1   1 Holliday junction DNA helicase subunit RuvB
PROKKA_00885         -          RXN-11832            -               0.01   12.8   0.0     0.041   10.8   0.0   1.8   2   0   0   2   2   2   0 shikimate kinase
PROKKA_02125         -          RXN-11832            -              0.026   11.5   0.0     0.052   10.5   0.0   1.5   1   0   0   1   1   1   0 thymidylate kinase
PROKKA_01400         -          RXN-11832            -               0.13    9.2   3.9       2.1    5.2   0.1   2.8   3   0   0   3   3   3   0 sulfate-transporting ATPase
PROKKA_02248         -          RXN-11832            -               0.46    7.4   8.7      0.58    7.1   0.0   3.2   4   0   0   4   4   4   0 ATP-binding cassette, subfamily F, member 3
PROKKA_02188         -          RXN-12263            -            9.4e-11   38.3   0.0   5.1e-08   29.3   0.0   2.1   2   0   0   2   2   2   2 farnesyl-diphosphate farnesyltransferase
PROKKA_02234         -          RXN-1824             -            7.7e-41  137.3   0.0   9.8e-41  136.9   0.0   1.1   1   0   0   1   1   1   1 glycogen operon protein
PROKKA_00499         -          RXN-1824             -            6.3e-23   78.0   0.0   3.2e-22   75.7   0.0   1.9   1   1   0   1   1   1   1 maltose alpha-D-glucosyltransferase/ alpha-amylase
PROKKA_00496         -          RXN-1824             -            5.8e-18   61.6   0.0   7.7e-17   57.9   0.0   2.3   2   1   0   2   2   2   1 maltooligosyl trehalose hydrolase
PROKKA_00498         -          RXN-1824             -            5.7e-13   45.1   0.0   3.6e-08   29.2   0.0   2.1   2   0   0   2   2   2   2 maltose alpha-D-glucosyltransferase/ alpha-amylase
PROKKA_00497         -          RXN-1824             -            4.7e-09   32.2   0.0   4.4e-06   22.3   0.0   2.2   2   0   0   2   2   2   2 maltooligosyl trehalose synthase 
PROKKA_00505         -          RXN-1824             -            0.00035   16.0   0.0   0.00046   15.6   0.0   1.1   1   0   0   1   1   1   1 alpha-1,4-glucan:maltose-1-phosphate maltosyltransferase
PROKKA_01392         -          RXN-3562             -            1.5e-07   28.2   0.0   2.4e-07   27.5   0.0   1.3   1   0   0   1   1   1   1 Nucleoside-diphosphate-sugar epimerase
PROKKA_00656         -          RXN-3562             -              6e-05   19.6   0.1   9.2e-05   19.0   0.1   1.3   1   0   0   1   1   1   1 UDP-galactose 4-epimerase
PROKKA_00572         -          RXN-3562             -             0.0027   14.2   0.0    0.0039   13.7   0.0   1.2   1   0   0   1   1   1   1 UDP-glucose 4-epimerase
PROKKA_01778         -          RXN-3562             -              0.019   11.4   0.0       0.6    6.5   0.0   2.1   2   0   0   2   2   2   0 GDP-L-fucose synthase
PROKKA_02160         -          RXN-7698             -            8.9e-70  232.2   2.9   1.1e-69  231.9   2.9   1.0   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier-protein] reductase
PROKKA_00235         -          RXN-7698             -            4.5e-30  102.1   0.0   5.5e-30  101.8   0.0   1.1   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier protein] reductase
PROKKA_00773         -          RXN-7698             -            9.8e-16   55.2   0.0   1.3e-15   54.7   0.0   1.0   1   0   0   1   1   1   1 short chain dehydrogenase
PROKKA_02149         -          RXN-7698             -            2.6e-15   53.8   0.0   3.5e-15   53.4   0.0   1.1   1   0   0   1   1   1   1 NADP-dependent 3-hydroxy acid dehydrogenase YdfG
PROKKA_00170         -          RXN-7698             -            5.7e-08   29.7   0.0   8.5e-08   29.2   0.0   1.2   1   0   0   1   1   1   1 NAD(P)-dependent dehydrogenase, short-chain alcohol dehydrogenase family
PROKKA_00820         -          RXN-7698             -            3.3e-05   20.7   0.1   3.6e-05   20.6   0.1   1.4   1   1   0   1   1   1   1 Enoyl-[acyl-carrier-protein] reductase [NADH]
PROKKA_00572         -          RXN-7698             -              0.014   12.1   0.0     0.023   11.4   0.0   1.3   1   0   0   1   1   1   0 UDP-glucose 4-epimerase
PROKKA_00761         -          RXN-7904             -            1.4e-73  245.4   0.0   2.1e-73  244.8   0.0   1.2   1   0   0   1   1   1   1 long-chain acyl-CoA synthetase
PROKKA_00084         -          RXN-7904             -            1.1e-46  156.5   0.0   3.3e-24   82.3   0.0   2.1   2   0   0   2   2   2   2 acetyl-CoA synthetase
PROKKA_02282         -          RXN-821              -            6.1e-49  164.0   0.0   8.1e-49  163.5   0.0   1.0   1   0   0   1   1   1   1 Acyl-CoA reductase
PROKKA_00706         -          RXN-821              -            1.2e-05   20.6   0.1   1.5e-05   20.2   0.1   1.1   1   0   0   1   1   1   1 L-proline dehydrogenase
PROKKA_02171         -          RXN-8999             -            9.2e-61  202.9   0.2   1.1e-30  104.2   0.1   2.0   1   1   1   2   2   2   2 undecaprenyl diphosphate synthase
PROKKA_00680         -          RXN-8999             -              0.013   12.1   0.0       1.8    5.1   0.0   2.3   2   0   0   2   2   2   0 farnesyl-diphosphate synthase 
PROKKA_01979         -          RXN-9929             -            5.8e-56  186.8   0.0   6.9e-56  186.5   0.0   1.0   1   0   0   1   1   1   1 Dihydroorotate dehydrogenase
PROKKA_02264         -          RXN-9929             -            3.2e-07   26.4   0.0   0.00055   15.8   0.0   2.1   2   0   0   2   2   2   2 cyclase
PROKKA_01901         -          RXN-9929             -            0.00087   15.2   2.0     0.078    8.7   0.5   2.3   1   1   0   2   2   2   2 IMP dehydrogenase
PROKKA_01592         -          RXN0-1133            -            4.9e-08   29.4   1.0     6e-08   29.1   0.5   1.2   1   1   0   1   1   1   1 acetyl-CoA carboxylase biotin carboxyl carrier protein
PROKKA_00326         -          RXN0-1133            -            0.00013   18.1   0.0   0.00024   17.2   0.0   1.4   1   1   0   1   1   1   1 RND family efflux transporter, MFP subunit
PROKKA_01620         -          RXN0-1133            -             0.0017   14.5   0.0    0.0021   14.1   0.0   1.2   1   0   0   1   1   1   1 membrane fusion protein, multidrug efflux system
PROKKA_02012         -          RXN0-1133            -             0.0069   12.4   0.0     0.016   11.2   0.0   1.5   1   1   0   1   1   1   1 RND family efflux transporter, MFP subunit
PROKKA_00874         -          RXN0-1133            -             0.0099   11.9   0.0     0.024   10.6   0.0   1.7   1   1   0   1   1   1   1 membrane fusion protein, multidrug efflux system
PROKKA_01780         -          RXN0-1134            -            1.4e-36  123.9   0.0   1.7e-36  123.6   0.0   1.0   1   0   0   1   1   1   1 pyruvate dehydrogenase E1 component beta subunit
PROKKA_01779         -          RXN0-1134            -            8.7e-17   58.7   0.0   1.2e-16   58.3   0.0   1.1   1   0   0   1   1   1   1 pyruvate dehydrogenase E1 component alpha subunit
PROKKA_00681         -          RXN0-1134            -            0.00029   17.5   0.0   0.00047   16.8   0.0   1.4   1   1   0   1   1   1   1 1-deoxy-D-xylulose-5-phosphate synthase
PROKKA_00628         -          RXN0-1461            -            9.8e-44  146.7   0.0   1.2e-43  146.3   0.0   1.0   1   0   0   1   1   1   1 oxygen-independent coproporphyrinogen-3 oxidase
PROKKA_01473         -          RXN0-1461            -              1e-08   31.8   0.0   3.9e-08   29.9   0.0   1.8   1   1   0   1   1   1   1 oxygen-independent coproporphyrinogen-3 oxidase
PROKKA_01776         -          RXN0-1461            -              0.019   11.3   0.0      0.04   10.2   0.0   1.7   1   1   0   1   1   1   0 hypothetical protein
PROKKA_02067         -          RXN0-2605            -            4.4e-28   95.0   0.0   5.6e-08   28.4   0.0   4.0   4   0   0   4   4   4   4 DNA helicase/exodeoxyribonuclease V, beta subunit
PROKKA_02066         -          RXN0-2605            -              0.004   12.3   0.2      0.84    4.6   0.2   2.7   2   1   1   3   3   3   2 DNA helicase/exodeoxyribonuclease V, alpha subunit
PROKKA_02068         -          RXN0-2605            -              0.011   10.9   0.3      0.33    6.0   0.0   2.5   3   0   0   3   3   3   0 DNA helicase/exodeoxyribonuclease V, gamma subunit
PROKKA_02496         -          RXN0-5144            -            7.4e-40  134.4   0.3   3.6e-39  132.1   0.3   1.8   1   1   0   1   1   1   1 tRNA-specific 2-thiouridylase
PROKKA_00369         -          RXN0-5144            -            0.00062   16.3   0.0    0.0023   14.4   0.0   1.7   2   0   0   2   2   2   1 TIGR00269 family protein
PROKKA_01393         -          RXN0-5184            -            2.5e-21   72.8   0.0   2.1e-10   36.7   0.0   3.8   3   1   0   3   3   3   3 starch phosphorylase
PROKKA_00807         -          RXN0-5398            -            6.1e-48  160.9   0.0   7.8e-48  160.6   0.0   1.0   1   0   0   1   1   1   1 tRNA pseudouridine55 synthase
PROKKA_00411         -          RXN0-5398            -            7.4e-05   19.6   0.0   0.00059   16.7   0.0   2.1   1   1   0   1   1   1   1 tRNA pseudouridine38-40 synthase
PROKKA_00902         -          RXN0-6274            -              3e-69  231.1   0.0   3.6e-69  230.8   0.0   1.0   1   0   0   1   1   1   1 tRNA dimethylallyltransferase
PROKKA_01537         -          RXN0-6274            -             0.0004   16.6   0.0   0.00078   15.7   0.0   1.5   2   0   0   2   2   2   1 cytidylate kinase
PROKKA_00927         -          RXN0-6274            -             0.0041   13.3   0.1      0.04   10.0   0.0   2.5   2   0   0   2   2   2   1 ATP-dependent Clp protease ATP-binding subunit ClpC
PROKKA_00220         -          RXN0-6274            -             0.0051   13.0   0.0     0.007   12.5   0.0   1.1   1   0   0   1   1   1   1 flagellar biosynthesis protein FlhF
PROKKA_01699         -          RXN0-6274            -             0.0071   12.5   0.0     0.013   11.7   0.0   1.3   1   0   0   1   1   1   1 ATP-dependent Clp protease ATP-binding subunit ClpX
PROKKA_01792         -          RXN0-6274            -             0.0078   12.4   0.0      0.01   12.0   0.0   1.2   1   0   0   1   1   1   1 hypothetical protein
PROKKA_01165         -          RXN1F-10             -            2.5e-13   47.2   0.0   3.1e-13   46.9   0.0   1.2   1   0   0   1   1   1   1 nitrogenase molybdenum-cofactor synthesis protein NifE
PROKKA_01164         -          RXN1F-10             -            6.6e-09   32.6   0.0     6e-08   29.5   0.0   2.2   1   1   0   1   1   1   1 Mo-nitrogenase MoFe protein subunit NifK
PROKKA_01163         -          RXN1F-10             -            8.1e-09   32.3   0.0   1.3e-08   31.7   0.0   1.3   1   0   0   1   1   1   1 Mo-nitrogenase MoFe protein subunit NifD precursor
PROKKA_01286         -          RXN3O-178            -            4.1e-09   33.2   0.0   6.2e-09   32.6   0.0   1.2   1   0   0   1   1   1   1 Methyltransferase domain-containing protein
PROKKA_00176         -          RXN3O-178            -            4.1e-06   23.3   0.0   5.6e-06   22.9   0.0   1.1   1   0   0   1   1   1   1 Methyltransferase domain-containing protein
PROKKA_02366         -          RXN3O-178            -            8.3e-05   19.0   0.0   0.00012   18.5   0.0   1.2   1   0   0   1   1   1   1 demethylmenaquinone methyltransferase / 2-methoxy-6-polyprenyl-1,4-benzoquinol methylase
PROKKA_00036         -          RXN3O-178            -            0.00013   18.4   0.0   0.00018   17.9   0.0   1.1   1   0   0   1   1   1   1 Methyltransferase domain-containing protein
PROKKA_02148         -          RXN3O-178            -             0.0019   14.6   0.0    0.0027   14.1   0.0   1.2   1   0   0   1   1   1   1 Methyltransferase domain-containing protein
PROKKA_02272         -          RXN3O-178            -             0.0089   12.3   0.0     0.012   11.9   0.0   1.1   1   0   0   1   1   1   1 release factor glutamine methyltransferase
PROKKA_01901         -          S-2-HYDROXY-ACID-OXIDASE-RXN -             0.0042   13.7   0.4    0.0095   12.5   0.2   1.6   2   0   0   2   2   2   1 IMP dehydrogenase
PROKKA_00644         -          S-ADENMETSYN-RXN     -           7.2e-174  575.4   0.0  8.6e-174  575.1   0.0   1.0   1   0   0   1   1   1   1 methionine adenosyltransferase
PROKKA_00647         -          S-CARBOXYMETHYLCYSTEINE-SYNTHASE-RXN -            2.6e-74  247.8   0.0   3.1e-74  247.6   0.0   1.0   1   0   0   1   1   1   1 cysteine synthase 
PROKKA_01355         -          S-CARBOXYMETHYLCYSTEINE-SYNTHASE-RXN -            1.5e-07   28.5   0.8   4.5e-07   26.9   0.8   1.6   1   1   0   1   1   1   1 threonine synthase
PROKKA_01951         -          S-CARBOXYMETHYLCYSTEINE-SYNTHASE-RXN -              0.013   12.2   0.7      0.76    6.4   0.1   2.1   2   0   0   2   2   2   0 tryptophan synthase beta chain
PROKKA_01373         -          SAICARSYN-RXN        -            7.9e-72  239.1   0.0   9.3e-72  238.8   0.0   1.0   1   0   0   1   1   1   1 phosphoribosylaminoimidazole-succinocarboxamide synthase
PROKKA_01702         -          SALICYLATE-1-MONOOXYGENASE-RXN -            3.9e-11   39.3   0.2   1.1e-10   37.8   0.0   1.8   3   0   0   3   3   3   1 2-polyprenyl-6-methoxyphenol hydroxylase
PROKKA_01529         -          SARCOX-RXN           -            6.1e-12   42.4   0.0   5.1e-06   22.9   0.0   2.4   2   1   0   2   2   2   2 glycine oxidase
PROKKA_01391         -          SARCOX-RXN           -              0.011   11.9   1.2      0.28    7.3   0.1   2.7   2   1   0   3   3   3   0 dihydrolipoamide dehydrogenase
PROKKA_01702         -          SARCOX-RXN           -              0.011   11.8   0.1      0.13    8.4   0.1   2.0   2   0   0   2   2   2   0 2-polyprenyl-6-methoxyphenol hydroxylase
PROKKA_02417         -          SARCOX-RXN           -              0.011   11.8   0.0      0.17    8.0   0.0   2.0   2   0   0   2   2   2   0 thioredoxin reductase (NADPH)
PROKKA_00137         -          SARCOX-RXN           -              0.025   10.7   0.0      0.72    5.9   0.1   2.1   2   0   0   2   2   2   0 dihydrolipoamide dehydrogenase
PROKKA_02280         -          SEDOHEPTULOSE-BISPHOSPHATASE-RXN -            1.4e-33  113.4   0.0   1.8e-33  113.0   0.0   1.1   1   0   0   1   1   1   1 fructose-1,6-bisphosphatase I
PROKKA_02149         -          SEPIAPTERIN-REDUCTASE-RXN -            1.3e-08   31.9   0.1     4e-08   30.3   0.1   1.6   1   1   0   1   1   1   1 NADP-dependent 3-hydroxy acid dehydrogenase YdfG
PROKKA_02160         -          SEPIAPTERIN-REDUCTASE-RXN -            3.2e-07   27.4   1.1   5.6e-07   26.5   1.1   1.3   1   1   0   1   1   1   1 3-oxoacyl-[acyl-carrier-protein] reductase
PROKKA_00235         -          SEPIAPTERIN-REDUCTASE-RXN -            5.2e-06   23.4   0.1   8.1e-06   22.7   0.1   1.3   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier protein] reductase
PROKKA_00773         -          SEPIAPTERIN-REDUCTASE-RXN -            0.00018   18.3   0.5    0.0014   15.4   0.5   1.9   1   1   0   1   1   1   1 short chain dehydrogenase
PROKKA_00170         -          SEPIAPTERIN-REDUCTASE-RXN -            0.00039   17.2   0.0   0.00093   16.0   0.0   1.6   1   1   0   1   1   1   1 NAD(P)-dependent dehydrogenase, short-chain alcohol dehydrogenase family
PROKKA_01512         -          SERINE--PYRUVATE-AMINOTRANSFERASE-RXN -            1.6e-42  143.2   0.0   1.9e-42  142.9   0.0   1.0   1   0   0   1   1   1   1 aspartate aminotransferase
PROKKA_01174         -          SERINE--PYRUVATE-AMINOTRANSFERASE-RXN -            9.2e-09   32.0   0.0   1.2e-08   31.6   0.0   1.1   1   0   0   1   1   1   1 cysteine desulfurase
PROKKA_00421         -          SERINE--PYRUVATE-AMINOTRANSFERASE-RXN -             0.0037   13.5   0.0    0.0056   13.0   0.0   1.2   1   0   0   1   1   1   1 cysteine desulfurase
PROKKA_00403         -          SERINE--TRNA-LIGASE-RXN -           4.7e-158  523.5   0.0  5.7e-158  523.2   0.0   1.0   1   0   0   1   1   1   1 seryl-tRNA synthetase
PROKKA_02166         -          SERINE--TRNA-LIGASE-RXN -            2.7e-13   46.6   0.0   1.5e-12   44.1   0.0   1.8   1   1   0   1   1   1   1 prolyl-tRNA synthetase
PROKKA_00090         -          SERINE--TRNA-LIGASE-RXN -            3.6e-13   46.1   0.0   6.2e-13   45.4   0.0   1.3   1   0   0   1   1   1   1 threonyl-tRNA synthetase
PROKKA_01291         -          SERINE--TRNA-LIGASE-RXN -             0.0083   12.0   2.8      0.01   11.7   2.8   1.1   1   0   0   1   1   1   1 periplasmic chaperone for outer membrane proteins Skp
PROKKA_02440         -          SERINE--TRNA-LIGASE-RXN -              0.018   10.9   3.8     0.032   10.1   3.8   1.4   1   0   0   1   1   1   0 ATP-dependent Clp protease ATP-binding subunit ClpB
PROKKA_01580         -          SERINE-C-PALMITOYLTRANSFERASE-RXN -            4.3e-51  171.0   0.0   6.1e-51  170.5   0.0   1.2   1   0   0   1   1   1   1 8-amino-7-oxononanoate synthase
PROKKA_01326         -          SERINE-O-ACETTRAN-RXN -            2.3e-80  266.9   0.1   2.7e-80  266.7   0.1   1.0   1   0   0   1   1   1   1 serine O-acetyltransferase
PROKKA_00296         -          SERINE-O-ACETTRAN-RXN -            1.6e-10   37.9   0.8   2.2e-07   27.6   0.1   2.3   2   0   0   2   2   2   2 Carbonic anhydrase or acetyltransferase, isoleucine patch superfamily
PROKKA_01292         -          SERINE-O-ACETTRAN-RXN -              5e-09   33.0  24.2   1.7e-08   31.2  10.3   3.4   1   1   2   3   3   3   3 UDP-3-O-[3-hydroxymyristoyl] glucosamine N-acyltransferase
PROKKA_02107         -          SERINE-O-ACETTRAN-RXN -              4e-07   26.7   1.2     4e-07   26.7   1.2   2.6   2   1   1   3   3   3   2 bifunctional UDP-N-acetylglucosamine pyrophosphorylase / Glucosamine-1-phosphate N-acetyltransferase
PROKKA_01294         -          SERINE-O-ACETTRAN-RXN -            0.00015   18.3   8.1   0.00029   17.3   0.3   3.1   2   1   1   3   3   3   1 acyl-[acyl-carrier-protein]--UDP-N-acetylglucosamine O-acyltransferase
PROKKA_01359         -          SERINE-O-ACETTRAN-RXN -              0.087    9.2   7.7      0.47    6.8   4.8   2.7   1   1   2   3   3   3   0 acyl-[acyl-carrier-protein]--UDP-N-acetylglucosamine O-acyltransferase
PROKKA_00788         -          SHIKIMATE-5-DEHYDROGENASE-RXN -            4.5e-45  151.5   0.0   5.3e-45  151.3   0.0   1.0   1   0   0   1   1   1   1 shikimate dehydrogenase 
PROKKA_00652         -          SHIKIMATE-5-DEHYDROGENASE-RXN -             0.0071   12.3   0.0    0.0085   12.0   0.0   1.2   1   0   0   1   1   1   1 adenylyltransferase and sulfurtransferase
PROKKA_02417         -          SHIKIMATE-5-DEHYDROGENASE-RXN -             0.0081   12.1   0.1     0.011   11.6   0.1   1.2   1   0   0   1   1   1   1 thioredoxin reductase (NADPH)
PROKKA_01991         -          SHIKIMATE-5-DEHYDROGENASE-RXN -              0.013   11.4   0.0     0.019   10.8   0.0   1.1   1   0   0   1   1   1   0 glutamyl-tRNA reductase
PROKKA_01157         -          SHIKIMATE-5-DEHYDROGENASE-RXN -               0.02   10.8   0.0     0.028   10.3   0.0   1.2   1   0   0   1   1   1   0 mercuric reductase
PROKKA_00885         -          SHIKIMATE-KINASE-RXN -            1.7e-39  133.0   0.0   2.2e-39  132.7   0.0   1.0   1   0   0   1   1   1   1 shikimate kinase
PROKKA_01699         -          SHIKIMATE-KINASE-RXN -            9.1e-05   19.0   0.0   0.00014   18.4   0.0   1.3   1   0   0   1   1   1   1 ATP-dependent Clp protease ATP-binding subunit ClpX
PROKKA_00258         -          SHIKIMATE-KINASE-RXN -            0.00011   18.7   0.0   0.00019   17.9   0.0   1.3   1   0   0   1   1   1   1 ATP-dependent HslUV protease ATP-binding subunit HslU
PROKKA_02248         -          SHIKIMATE-KINASE-RXN -            0.00023   17.7   0.0     0.035   10.5   0.0   2.5   2   1   0   2   2   2   1 ATP-binding cassette, subfamily F, member 3
PROKKA_01889         -          SHIKIMATE-KINASE-RXN -            0.00024   17.6   0.0   0.00047   16.7   0.0   1.5   1   0   0   1   1   1   1 ATP-dependent Lon protease
PROKKA_00927         -          SHIKIMATE-KINASE-RXN -            0.00088   15.8   3.2      0.23    7.8   0.6   2.4   1   1   0   2   2   2   2 ATP-dependent Clp protease ATP-binding subunit ClpC
PROKKA_00122         -          SHIKIMATE-KINASE-RXN -             0.0041   13.6   0.0    0.0058   13.1   0.0   1.3   1   0   0   1   1   1   1 ATP-dependent proteinase. Serine peptidase. MEROPS family S16
PROKKA_01400         -          SHIKIMATE-KINASE-RXN -             0.0056   13.1   0.0      0.37    7.1   0.0   2.3   2   0   0   2   2   2   1 sulfate-transporting ATPase
PROKKA_01385         -          SHIKIMATE-KINASE-RXN -              0.012   12.1   0.2      0.65    6.4   0.2   2.1   2   0   0   2   2   2   0 ABC-2 type transport system ATP-binding protein
PROKKA_00980         -          SHIKIMATE-KINASE-RXN -              0.016   11.6   0.0     0.023   11.1   0.0   1.4   1   1   0   1   1   1   0 type IV secretion system protein VirB11
PROKKA_00156         -          SHIKIMATE-KINASE-RXN -              0.025   11.0   0.0     0.038   10.4   0.0   1.2   1   0   0   1   1   1   0 DNA polymerase-3 subunit gamma/tau
PROKKA_00290         -          SHIKIMATE-KINASE-RXN -              0.033   10.6   0.0      0.05   10.0   0.0   1.2   1   0   0   1   1   1   0 Holliday junction DNA helicase subunit RuvB
PROKKA_02160         -          SORB6PDEHYDROG-RXN   -            3.5e-42  141.7   4.1   1.1e-41  140.1   4.1   1.6   1   1   0   1   1   1   1 3-oxoacyl-[acyl-carrier-protein] reductase
PROKKA_00235         -          SORB6PDEHYDROG-RXN   -              2e-21   73.7   0.0   3.8e-21   72.8   0.0   1.4   1   1   0   1   1   1   1 3-oxoacyl-[acyl-carrier protein] reductase
PROKKA_02149         -          SORB6PDEHYDROG-RXN   -            8.4e-12   42.1   0.0   1.1e-11   41.7   0.0   1.1   1   0   0   1   1   1   1 NADP-dependent 3-hydroxy acid dehydrogenase YdfG
PROKKA_00773         -          SORB6PDEHYDROG-RXN   -            9.6e-09   32.1   0.0   7.1e-08   29.3   0.0   1.9   1   1   0   1   1   1   1 short chain dehydrogenase
PROKKA_00170         -          SORB6PDEHYDROG-RXN   -            2.5e-07   27.4   0.0   6.3e-07   26.1   0.0   1.6   1   1   0   1   1   1   1 NAD(P)-dependent dehydrogenase, short-chain alcohol dehydrogenase family
PROKKA_00820         -          SORB6PDEHYDROG-RXN   -             0.0017   14.9   0.1    0.0063   13.0   0.1   2.0   1   1   0   1   1   1   1 Enoyl-[acyl-carrier-protein] reductase [NADH]
PROKKA_01783         -          SORB6PDEHYDROG-RXN   -             0.0031   14.0   0.0    0.0055   13.2   0.0   1.4   1   0   0   1   1   1   1 Nucleoside-diphosphate-sugar epimerase
PROKKA_00572         -          SORB6PDEHYDROG-RXN   -              0.026   11.0   0.0     0.041   10.3   0.0   1.3   1   0   0   1   1   1   0 UDP-glucose 4-epimerase
PROKKA_00856         -          SPERMIDINESYN-RXN    -            6.2e-65  216.4   0.0   8.3e-65  215.9   0.0   1.0   1   0   0   1   1   1   1 spermidine synthase
PROKKA_00856         -          SPERMINE-SYNTHASE-RXN -            5.4e-10   36.2   0.0   7.3e-10   35.8   0.0   1.2   1   0   0   1   1   1   1 spermidine synthase
PROKKA_01702         -          SQUALENE-MONOOXYGENASE-RXN -            4.5e-16   55.8   0.0   9.3e-11   38.3   0.0   2.1   2   0   0   2   2   2   2 2-polyprenyl-6-methoxyphenol hydroxylase
PROKKA_00565         -          SQUALENE-MONOOXYGENASE-RXN -            1.3e-06   24.6   0.5   2.2e-06   23.8   0.5   1.3   1   0   0   1   1   1   1 UDP-galactopyranose mutase
PROKKA_01529         -          SQUALENE-MONOOXYGENASE-RXN -            7.4e-05   18.8   0.0   0.00013   18.0   0.0   1.3   1   0   0   1   1   1   1 glycine oxidase
PROKKA_01854         -          SQUALENE-MONOOXYGENASE-RXN -             0.0025   13.8   0.1    0.0035   13.3   0.1   1.1   1   0   0   1   1   1   1 L-2-hydroxyglutarate oxidase LhgO
PROKKA_01391         -          SQUALENE-MONOOXYGENASE-RXN -              0.018   11.0   0.1      0.13    8.1   0.1   2.0   2   0   0   2   2   2   0 dihydrolipoamide dehydrogenase
PROKKA_00559         -          SQUALENE-MONOOXYGENASE-RXN -              0.041    9.8   1.8       1.3    4.8   0.6   2.2   2   0   0   2   2   2   0 mercuric reductase
PROKKA_02377         -          SQUALENE-MONOOXYGENASE-RXN -               0.05    9.5   0.6       1.2    4.9   0.5   2.4   2   1   0   2   2   2   0 geranylgeranyl reductase family protein
PROKKA_01201         -          SQUALENE-MONOOXYGENASE-RXN -              0.089    8.7   1.5      0.14    8.0   1.5   1.2   1   0   0   1   1   1   0 nitrite reductase (NADH) large subunit
PROKKA_00620         -          STEROID-DELTA-ISOMERASE-RXN -            2.3e-17   60.2   0.0   5.8e-14   49.0   0.0   2.2   2   0   0   2   2   2   2 dihydroflavonol-4-reductase
PROKKA_00572         -          STEROID-DELTA-ISOMERASE-RXN -            1.2e-16   57.8   0.0   9.9e-15   51.5   0.0   2.1   2   0   0   2   2   2   2 UDP-glucose 4-epimerase
PROKKA_00656         -          STEROID-DELTA-ISOMERASE-RXN -              2e-15   53.8   0.0   4.2e-14   49.4   0.0   2.0   2   0   0   2   2   2   1 UDP-galactose 4-epimerase
PROKKA_01629         -          STEROID-DELTA-ISOMERASE-RXN -            4.4e-11   39.5   0.0   6.4e-11   39.0   0.0   1.3   1   0   0   1   1   1   1 ADP-L-glycero-D-manno-heptose 6-epimerase
PROKKA_00488         -          STEROID-DELTA-ISOMERASE-RXN -              5e-09   32.8   0.0   1.7e-08   31.0   0.0   1.7   2   0   0   2   2   2   1 dTDP-glucose 4,6-dehydratase
PROKKA_00469         -          STEROID-DELTA-ISOMERASE-RXN -            6.2e-07   25.9   0.0   2.5e-06   23.9   0.0   1.8   1   1   0   1   1   1   1 NADH dehydrogenase
PROKKA_00566         -          STEROID-DELTA-ISOMERASE-RXN -              1e-06   25.2   0.0   1.3e-06   24.8   0.0   1.2   1   0   0   1   1   1   1 UDP-glucuronate 4-epimerase
PROKKA_01526         -          STEROID-DELTA-ISOMERASE-RXN -              0.011   11.9   0.0     0.031   10.4   0.0   1.7   2   0   0   2   2   2   0 UDP-glucuronate decarboxylase
PROKKA_01783         -          STEROID-DELTA-ISOMERASE-RXN -              0.018   11.2   0.0     0.032   10.4   0.0   1.4   1   0   0   1   1   1   0 Nucleoside-diphosphate-sugar epimerase
PROKKA_00585         -          SUCC-FUM-OXRED-RXN   -            5.9e-18   62.2   0.0     9e-18   61.6   0.0   1.2   1   0   0   1   1   1   1 succinate dehydrogenase / fumarate reductase flavoprotein subunit
PROKKA_02443         -          SUCC-FUM-OXRED-RXN   -            1.7e-16   57.5   0.0   5.7e-16   55.8   0.0   1.7   1   1   0   1   1   1   1 L-aspartate oxidase
PROKKA_00586         -          SUCCCOASYN-RXN       -           1.5e-138  459.2   0.7  1.8e-138  459.0   0.7   1.0   1   0   0   1   1   1   1 succinyl-CoA synthetase beta subunit
PROKKA_00587         -          SUCCCOASYN-RXN       -              7e-58  193.7   3.7   2.4e-40  136.0   1.5   3.1   1   1   2   3   3   3   3 succinyl-CoA synthetase alpha subunit
PROKKA_00579         -          SUCCCOASYN-RXN       -            2.8e-47  158.8   7.8   1.1e-35  120.6   1.2   2.1   2   0   0   2   2   2   2 succinyl-CoA synthetase alpha subunit
PROKKA_00578         -          SUCCCOASYN-RXN       -            9.7e-34  114.3   1.0   1.7e-33  113.5   1.0   1.4   1   1   0   1   1   1   1 citryl-CoA synthetase large subunit
PROKKA_00839         -          SUCCCOASYN-RXN       -             0.0007   16.0   0.3     0.056    9.7   0.1   2.0   1   1   1   2   2   2   2 protease-4
PROKKA_00587         -          SUCCINATE--COA-LIGASE-GDP-FORMING-RXN -            2.2e-80  268.1   0.3   3.6e-78  260.8   0.3   2.0   1   1   0   1   1   1   1 succinyl-CoA synthetase alpha subunit
PROKKA_00586         -          SUCCINATE--COA-LIGASE-GDP-FORMING-RXN -            4.6e-78  260.5   0.7   6.3e-78  260.0   0.7   1.1   1   0   0   1   1   1   1 succinyl-CoA synthetase beta subunit
PROKKA_00578         -          SUCCINATE--COA-LIGASE-GDP-FORMING-RXN -            6.9e-12   42.7   0.0   1.7e-11   41.4   0.0   1.5   1   1   0   1   1   1   1 citryl-CoA synthetase large subunit
PROKKA_02443         -          SUCCINATE-DEHYDROGENASE-UBIQUINONE-RXN -            3.7e-07   27.3   0.0   4.8e-07   26.9   0.0   1.1   1   0   0   1   1   1   1 L-aspartate oxidase
PROKKA_00585         -          SUCCINATE-DEHYDROGENASE-UBIQUINONE-RXN -            1.8e-06   25.1   0.0   2.4e-06   24.6   0.0   1.1   1   0   0   1   1   1   1 succinate dehydrogenase / fumarate reductase flavoprotein subunit
PROKKA_02282         -          SUCCINATE-SEMIALDEHYDE-DEHYDROGENASE-RXN -            1.1e-49  167.0   0.0   1.4e-49  166.5   0.0   1.0   1   0   0   1   1   1   1 Acyl-CoA reductase
PROKKA_02282         -          SUCCSEMIALDDEHYDROG-RXN -            3.5e-68  227.4   0.0   4.5e-68  227.1   0.0   1.0   1   0   0   1   1   1   1 Acyl-CoA reductase
PROKKA_01317         -          SUCROSE-PHOSPHATE-SYNTHASE-RXN -              3e-08   29.1   0.0   4.3e-08   28.6   0.0   1.1   1   0   0   1   1   1   1 Glycosyltransferase involved in cell wall bisynthesis
PROKKA_00544         -          SUCROSE-PHOSPHATE-SYNTHASE-RXN -            2.8e-05   19.3   0.0   4.1e-05   18.7   0.0   1.2   1   0   0   1   1   1   1 Glycosyltransferase involved in cell wall bisynthesis
PROKKA_00546         -          SUCROSE-PHOSPHATE-SYNTHASE-RXN -            4.4e-05   18.6   0.0   5.9e-05   18.2   0.0   1.1   1   0   0   1   1   1   1 Glycosyltransferase involved in cell wall bisynthesis
PROKKA_01809         -          SUCROSE-PHOSPHATE-SYNTHASE-RXN -            0.00017   16.6   0.0   0.00026   16.0   0.0   1.1   1   0   0   1   1   1   1 Glycosyltransferase involved in cell wall bisynthesis
PROKKA_01632         -          SUCROSE-PHOSPHATE-SYNTHASE-RXN -             0.0029   12.6   0.0    0.0042   12.1   0.0   1.2   1   0   0   1   1   1   1 Glycosyltransferase involved in cell wall bisynthesis
PROKKA_00544         -          SUCROSE-SYNTHASE-RXN -            5.2e-05   18.6   0.0   8.1e-05   17.9   0.0   1.2   1   0   0   1   1   1   1 Glycosyltransferase involved in cell wall bisynthesis
PROKKA_01632         -          SUCROSE-SYNTHASE-RXN -            0.00019   16.8   0.0   0.00028   16.2   0.0   1.2   1   0   0   1   1   1   1 Glycosyltransferase involved in cell wall bisynthesis
PROKKA_00546         -          SUCROSE-SYNTHASE-RXN -            0.00046   15.4   0.0   0.00065   14.9   0.0   1.1   1   0   0   1   1   1   1 Glycosyltransferase involved in cell wall bisynthesis
PROKKA_01809         -          SUCROSE-SYNTHASE-RXN -             0.0012   14.1   0.0     0.002   13.3   0.0   1.3   1   0   0   1   1   1   1 Glycosyltransferase involved in cell wall bisynthesis
PROKKA_01317         -          SUCROSE-SYNTHASE-RXN -              0.002   13.3   0.0     0.003   12.8   0.0   1.1   1   0   0   1   1   1   1 Glycosyltransferase involved in cell wall bisynthesis
PROKKA_01816         -          SUCROSE-SYNTHASE-RXN -             0.0048   12.1   0.0     0.006   11.7   0.0   1.1   1   0   0   1   1   1   1 Glycosyltransferase involved in cell wall bisynthesis
PROKKA_00429         -          SULFATE-ADENYLYLTRANS-RXN -            1.5e-95  317.5   0.0   1.6e-95  317.4   0.0   1.0   1   0   0   1   1   1   1 sulfate adenylyltransferase
PROKKA_02426         -          SULFATE-ADENYLYLTRANSFERASE-ADP-RXN -            0.00018   17.7   0.0   0.00026   17.2   0.0   1.2   1   0   0   1   1   1   1 UDPglucose--hexose-1-phosphate uridylyltransferase
PROKKA_01204         -          SULFITE-DEHYDROGENASE-RXN -            0.00017   17.7   0.0   0.00049   16.2   0.0   1.6   2   0   0   2   2   2   1 sulfoxide reductase catalytic subunit YedY
PROKKA_01204         -          SULFITE-OXIDASE-RXN  -            1.4e-10   37.4   0.0   2.1e-10   36.8   0.0   1.1   1   0   0   1   1   1   1 sulfoxide reductase catalytic subunit YedY
PROKKA_01612         -          SULFITE-REDUCT-RXN   -              0.042    9.5   0.0     0.067    8.9   0.0   1.5   2   1   0   2   2   2   0 Ferredoxin-NADP reductase
PROKKA_01477         -          SULFITE-REDUCTASE-FERREDOXIN-RXN -            1.1e-39  133.2   0.0   2.5e-27   92.4   0.0   2.0   1   1   0   2   2   2   2 sulfite reductase (ferredoxin)
PROKKA_01201         -          SULFITE-REDUCTASE-FERREDOXIN-RXN -              2e-37  125.7   0.0   4.9e-26   88.1   0.0   2.7   2   1   0   2   2   2   2 nitrite reductase (NADH) large subunit
PROKKA_01477         -          SULFITE-REDUCTASE-RXN -            1.3e-12   44.4   0.0   2.5e-07   27.0   0.0   2.2   2   0   0   2   2   2   2 sulfite reductase (ferredoxin)
PROKKA_01201         -          SULFITE-REDUCTASE-RXN -             0.0034   13.4   0.7      0.65    5.9   0.0   2.9   4   0   0   4   4   4   2 nitrite reductase (NADH) large subunit
PROKKA_01182         -          SULFITE-REDUCTASE-RXN -              0.063    9.2   8.0     0.064    9.2   0.3   2.0   1   1   1   2   2   2   0 ferredoxin III, nif-specific
PROKKA_00695         -          SULFITE-REDUCTASE-RXN -              0.099    8.6   3.3     0.038    9.9   0.5   1.6   1   1   1   2   2   2   0 NADH-quinone oxidoreductase subunit I
PROKKA_00692         -          SULFITE-REDUCTASE-RXN -                0.1    8.5   6.1      0.18    7.7   6.1   1.4   1   0   0   1   1   1   0 NAD(P)-dependent iron-only hydrogenase diaphorase component flavoprotein
PROKKA_02536         -          TARTRATE-DECARBOXYLASE-RXN -            1.3e-57  192.5   0.0   1.6e-57  192.1   0.0   1.0   1   0   0   1   1   1   1 3-isopropylmalate dehydrogenase
PROKKA_00965         -          TARTRATE-DECARBOXYLASE-RXN -            1.3e-48  162.9   0.0   1.4e-48  162.8   0.0   1.0   1   0   0   1   1   1   1 isocitrate dehydrogenase (NAD+)
PROKKA_02536         -          TARTRATE-DEHYDROGENASE-RXN -              7e-60  199.9   0.0   9.1e-60  199.6   0.0   1.0   1   0   0   1   1   1   1 3-isopropylmalate dehydrogenase
PROKKA_00965         -          TARTRATE-DEHYDROGENASE-RXN -            9.8e-51  169.8   0.0   1.1e-50  169.7   0.0   1.0   1   0   0   1   1   1   1 isocitrate dehydrogenase (NAD+)
PROKKA_00693         -          TDCEACT-RXN          -              2e-05   21.1   0.5   4.8e-05   19.8   0.5   1.6   1   0   0   1   1   1   1 formate dehydrogenase major subunit
PROKKA_01917         -          TDCEACT-RXN          -             0.0005   16.5   0.0     0.025   10.9   0.1   2.1   2   0   0   2   2   2   1 putative pyruvate formate lyase activating enzyme
PROKKA_01182         -          TDCEACT-RXN          -             0.0071   12.7   8.8     0.093    9.0   2.4   2.0   1   1   1   2   2   2   2 ferredoxin III, nif-specific
PROKKA_00428         -          TDCEACT-RXN          -              0.015   11.6   1.9     0.018   11.4   1.9   1.1   1   0   0   1   1   1   0 adenylylsulfate reductase subunit B
PROKKA_01944         -          TDCEACT-RXN          -               0.06    9.6   4.0      0.73    6.1   0.2   2.3   2   0   0   2   2   2   0 NADPH-dependent glutamate synthase beta chain
PROKKA_00695         -          TDCEACT-RXN          -              0.079    9.2   3.1       0.2    7.9   2.8   1.7   1   1   1   2   2   2   0 NADH-quinone oxidoreductase subunit I
PROKKA_02450         -          TDCEACT-RXN          -               0.13    8.5   8.8      0.15    8.3   8.8   1.0   1   0   0   1   1   1   0 pyruvate ferredoxin oxidoreductase
PROKKA_01962         -          TDCEACT-RXN          -               0.95    5.7   8.2       1.9    4.7   8.3   1.4   1   1   0   1   1   1   0 Polyferredoxin
PROKKA_01326         -          TETHYDPICSUCC-RXN    -              0.027   11.2   1.2     0.038   10.7   1.2   1.2   1   0   0   1   1   1   0 serine O-acetyltransferase
PROKKA_02107         -          TETHYDPICSUCC-RXN    -              0.029   11.1   2.6     0.079    9.7   0.6   2.1   1   1   0   2   2   2   0 bifunctional UDP-N-acetylglucosamine pyrophosphorylase / Glucosamine-1-phosphate N-acetyltransferase
PROKKA_00296         -          TETHYDPICSUCC-RXN    -              0.056   10.2   3.2     0.071    9.8   2.5   1.5   1   1   0   1   1   1   0 Carbonic anhydrase or acetyltransferase, isoleucine patch superfamily
PROKKA_01292         -          TETHYDPICSUCC-RXN    -                  2    5.0  10.7      0.28    7.9   0.8   2.4   1   1   0   2   2   2   0 UDP-3-O-[3-hydroxymyristoyl] glucosamine N-acyltransferase
PROKKA_01301         -          TETRAACYLDISACC4KIN-RXN -            6.2e-52  174.1   0.0   1.5e-51  172.8   0.0   1.5   1   1   0   1   1   1   1 lipid-A-disaccharide kinase
PROKKA_00123         -          THI-P-KIN-RXN        -            6.4e-30  101.5   0.0   7.2e-30  101.3   0.0   1.0   1   0   0   1   1   1   1 thiamine-monophosphate kinase
PROKKA_01594         -          THI-P-SYN-RXN        -            3.3e-48  160.8   0.0   3.9e-48  160.6   0.0   1.0   1   0   0   1   1   1   1 thiamine-phosphate pyrophosphorylase
PROKKA_00086         -          THI-P-SYN-RXN        -            3.6e-35  118.2   0.0   4.6e-35  117.8   0.0   1.0   1   0   0   1   1   1   1 thiamine-phosphate pyrophosphorylase
PROKKA_02265         -          THI-P-SYN-RXN        -              0.022   11.0   0.0     0.037   10.3   0.0   1.3   1   0   0   1   1   1   0 1-(5-phosphoribosyl)-5-[(5-phosphoribosylamino)methylideneamino] imidazole-4-carboxamide isomerase
PROKKA_00814         -          THIAZOLSYN3-RXN      -              0.003   14.2   0.0    0.0051   13.4   0.0   1.2   1   0   0   1   1   1   1 NAD(P)H-hydrate epimerase
PROKKA_02417         -          THIOREDOXIN-REDUCT-NADPH-RXN -           1.2e-116  386.3   0.0  1.3e-116  386.2   0.0   1.0   1   0   0   1   1   1   1 thioredoxin reductase (NADPH)
PROKKA_00559         -          THIOREDOXIN-REDUCT-NADPH-RXN -            4.8e-32  108.2   0.5   1.2e-24   84.0   0.0   2.1   2   0   0   2   2   2   2 mercuric reductase
PROKKA_01909         -          THIOREDOXIN-REDUCT-NADPH-RXN -            6.4e-27   91.4   0.0   2.3e-26   89.6   0.0   1.6   1   1   0   1   1   1   1 dihydrolipoamide dehydrogenase
PROKKA_01157         -          THIOREDOXIN-REDUCT-NADPH-RXN -            6.7e-27   91.3   3.0   2.1e-19   66.7   0.0   2.1   2   0   0   2   2   2   2 mercuric reductase
PROKKA_00137         -          THIOREDOXIN-REDUCT-NADPH-RXN -            2.2e-25   86.3   0.0   7.2e-25   84.7   0.0   1.7   1   1   0   1   1   1   1 dihydrolipoamide dehydrogenase
PROKKA_01391         -          THIOREDOXIN-REDUCT-NADPH-RXN -            5.2e-25   85.1   0.0   2.1e-24   83.1   0.0   1.7   1   1   0   1   1   1   1 dihydrolipoamide dehydrogenase
PROKKA_01944         -          THIOREDOXIN-REDUCT-NADPH-RXN -            2.1e-23   79.8   1.5   2.3e-15   53.5   0.1   2.7   1   1   0   2   2   2   2 NADPH-dependent glutamate synthase beta chain
PROKKA_01451         -          THIOREDOXIN-REDUCT-NADPH-RXN -            2.4e-22   76.4   0.2   4.6e-13   45.9   0.1   3.2   2   1   0   2   2   2   2 NADH-quinone oxidoreductase subunit F
PROKKA_01201         -          THIOREDOXIN-REDUCT-NADPH-RXN -            3.5e-15   52.8   0.0   5.3e-15   52.2   0.0   1.2   1   0   0   1   1   1   1 nitrite reductase (NADH) large subunit
PROKKA_00427         -          THIOREDOXIN-REDUCT-NADPH-RXN -              3e-08   30.0   0.3   0.00031   16.8   0.2   2.4   2   0   0   2   2   2   2 dissimilatory adenylylsulfate reductase alpha subunit precursor
PROKKA_00585         -          THIOREDOXIN-REDUCT-NADPH-RXN -            1.5e-06   24.5   0.2   9.3e-05   18.6   0.1   2.8   3   0   0   3   3   3   1 succinate dehydrogenase / fumarate reductase flavoprotein subunit
PROKKA_02501         -          THIOREDOXIN-REDUCT-NADPH-RXN -            4.6e-05   19.6   0.0     7e-05   19.0   0.0   1.2   1   0   0   1   1   1   1 tRNA uridine 5-carboxymethylaminomethyl modification enzyme
PROKKA_01529         -          THIOREDOXIN-REDUCT-NADPH-RXN -            0.00013   18.1   0.0   0.00016   17.8   0.0   1.4   1   1   0   1   1   1   1 glycine oxidase
PROKKA_02138         -          THIOREDOXIN-REDUCT-NADPH-RXN -            0.00013   18.0   0.0      0.22    7.5   0.0   3.2   3   1   0   3   3   3   1 NADH dehydrogenase
PROKKA_00255         -          THIOREDOXIN-REDUCT-NADPH-RXN -            0.00095   15.3   0.0    0.0029   13.7   0.0   1.6   2   0   0   2   2   2   1 methylenetetrahydrofolate--tRNA-(uracil-5-)-methyltransferase
PROKKA_01719         -          THIOREDOXIN-REDUCT-NADPH-RXN -             0.0011   15.1   0.1    0.0038   13.3   0.0   1.8   2   0   0   2   2   2   1 oxygen-dependent protoporphyrinogen oxidase
PROKKA_00442         -          THIOREDOXIN-REDUCT-NADPH-RXN -             0.0016   14.5   0.3      0.15    8.1   0.3   2.7   2   1   0   2   2   2   1 sulfide:quinone oxidoreductase
PROKKA_02443         -          THIOREDOXIN-REDUCT-NADPH-RXN -             0.0017   14.4   0.0      0.44    6.5   0.0   2.5   2   0   0   2   2   2   2 L-aspartate oxidase
PROKKA_00031         -          THIOREDOXIN-REDUCT-NADPH-RXN -             0.0032   13.5   0.0     0.016   11.2   0.1   2.0   2   1   0   2   2   2   1 sulfide:quinone oxidoreductase
PROKKA_02377         -          THIOREDOXIN-REDUCT-NADPH-RXN -              0.018   11.0   0.1     0.034   10.1   0.1   1.5   1   1   0   1   1   1   0 geranylgeranyl reductase family protein
PROKKA_00735         -          THIOREDOXIN-REDUCT-NADPH-RXN -              0.023   10.7   0.8     0.034   10.1   0.8   1.2   1   0   0   1   1   1   0 C-3',4' desaturase CrtD
PROKKA_01854         -          THIOREDOXIN-REDUCT-NADPH-RXN -               0.03   10.3   0.0     0.037   10.0   0.0   1.2   1   0   0   1   1   1   0 L-2-hydroxyglutarate oxidase LhgO
PROKKA_01702         -          THIOREDOXIN-REDUCT-NADPH-RXN -              0.039   10.0   0.3     0.087    8.8   0.1   1.6   2   0   0   2   2   2   0 2-polyprenyl-6-methoxyphenol hydroxylase
PROKKA_01355         -          THREDEHYD-RXN        -            6.8e-28   94.7   0.2   8.6e-28   94.4   0.2   1.0   1   0   0   1   1   1   1 threonine synthase
PROKKA_00649         -          THREDEHYD-RXN        -            6.7e-27   91.4   0.0   8.8e-27   91.1   0.0   1.0   1   0   0   1   1   1   1 threonine synthase
PROKKA_00647         -          THREDEHYD-RXN        -            1.3e-23   80.6   0.0   1.2e-21   74.2   0.0   2.0   1   1   0   1   1   1   1 cysteine synthase 
PROKKA_01951         -          THREDEHYD-RXN        -            2.7e-11   40.0   0.0   1.9e-10   37.3   0.0   2.0   2   0   0   2   2   2   1 tryptophan synthase beta chain
PROKKA_02069         -          THREODEHYD-RXN       -            2.6e-37  125.7   0.0   3.3e-37  125.4   0.0   1.2   1   0   0   1   1   1   1 alcohol dehydrogenase, propanol-preferring
PROKKA_00022         -          THREODEHYD-RXN       -            1.2e-27   93.9   0.0   1.5e-27   93.6   0.0   1.1   1   0   0   1   1   1   1 NADPH:quinone reductase
PROKKA_00507         -          THREODEHYD-RXN       -            7.2e-27   91.4   0.1   1.1e-26   90.7   0.1   1.2   1   0   0   1   1   1   1 putative zinc-type alcohol dehydrogenase-like protein
PROKKA_00329         -          THREODEHYD-RXN       -            5.9e-23   78.5   0.0   8.4e-23   78.0   0.0   1.3   1   0   0   1   1   1   1 alcohol dehydrogenase, propanol-preferring
PROKKA_01717         -          THREODEHYD-RXN       -            7.4e-13   45.3   0.0     1e-09   35.0   0.0   2.5   1   1   1   2   2   2   2 NADPH2:quinone reductase
PROKKA_01239         -          THREODEHYD-RXN       -            4.6e-12   42.7   0.8   5.7e-09   32.5   1.7   2.1   1   1   1   2   2   2   2 NADPH2:quinone reductase
PROKKA_00090         -          THREONINE--TRNA-LIGASE-RXN -             2e-230  763.8   0.0  2.5e-230  763.5   0.0   1.0   1   0   0   1   1   1   1 threonyl-tRNA synthetase
PROKKA_02166         -          THREONINE--TRNA-LIGASE-RXN -            5.4e-21   71.9   0.0   5.3e-15   52.1   0.0   2.0   2   0   0   2   2   2   2 prolyl-tRNA synthetase
PROKKA_01883         -          THREONINE--TRNA-LIGASE-RXN -            6.9e-06   22.0   0.0   1.2e-05   21.3   0.0   1.3   1   0   0   1   1   1   1 histidyl-tRNA synthetase
PROKKA_00403         -          THREONINE--TRNA-LIGASE-RXN -            3.1e-05   19.8   0.0   4.2e-05   19.4   0.0   1.2   1   0   0   1   1   1   1 seryl-tRNA synthetase
PROKKA_00863         -          THREONINE--TRNA-LIGASE-RXN -            0.00085   15.1   0.0    0.0013   14.5   0.0   1.2   1   0   0   1   1   1   1 alanyl-tRNA synthetase
PROKKA_00649         -          THRESYN-RXN          -           1.5e-120  400.0   0.0  1.9e-120  399.6   0.0   1.0   1   0   0   1   1   1   1 threonine synthase
PROKKA_01355         -          THRESYN-RXN          -           2.1e-114  379.7   0.0  2.5e-114  379.4   0.0   1.0   1   0   0   1   1   1   1 threonine synthase
PROKKA_00647         -          THRESYN-RXN          -            2.8e-20   69.7   0.0     1e-16   58.0   0.0   2.0   1   1   1   2   2   2   2 cysteine synthase 
PROKKA_01951         -          THRESYN-RXN          -            2.1e-10   37.2   0.0   1.1e-09   34.8   0.0   1.8   1   1   0   1   1   1   1 tryptophan synthase beta chain
PROKKA_02248         -          THYKI-RXN            -             0.0011   15.8   0.0     0.083    9.7   0.0   2.3   1   1   0   2   2   2   1 ATP-binding cassette, subfamily F, member 3
PROKKA_00220         -          THYKI-RXN            -             0.0013   15.6   0.0    0.0016   15.4   0.0   1.2   1   0   0   1   1   1   1 flagellar biosynthesis protein FlhF
PROKKA_00723         -          THYKI-RXN            -             0.0026   14.6   0.0    0.0036   14.1   0.0   1.2   1   0   0   1   1   1   1 type II secretion system protein E (GspE)
PROKKA_01385         -          THYKI-RXN            -             0.0033   14.3   0.0      0.27    8.0   0.0   2.2   2   0   0   2   2   2   1 ABC-2 type transport system ATP-binding protein
PROKKA_00841         -          THYKI-RXN            -             0.0082   13.0   0.0     0.015   12.2   0.0   1.4   1   0   0   1   1   1   1 signal recognition particle subunit FFH/SRP54 (srp54)
PROKKA_00927         -          THYKI-RXN            -              0.012   12.5   0.0     0.067   10.0   0.0   1.9   2   0   0   2   2   2   0 ATP-dependent Clp protease ATP-binding subunit ClpC
PROKKA_02180         -          THYKI-RXN            -              0.012   12.4   0.0     0.017   12.0   0.0   1.2   1   0   0   1   1   1   0 DNA repair protein RadA/Sms
PROKKA_01400         -          THYKI-RXN            -              0.018   11.8   0.0      0.83    6.4   0.0   2.2   2   0   0   2   2   2   0 sulfate-transporting ATPase
PROKKA_02545         -          THYKI-RXN            -              0.022   11.6   0.0      0.07    9.9   0.0   1.5   1   1   0   1   1   1   0 Type II secretory pathway ATPase GspE/PulE or T4P pilus assembly pathway ATPase PilB
PROKKA_02142         -          THYKI-RXN            -              0.023   11.5   0.0       2.1    5.1   0.0   2.2   2   0   0   2   2   2   0 GTP-binding protein
PROKKA_01889         -          THYKI-RXN            -              0.024   11.5   0.0     0.032   11.0   0.0   1.2   1   0   0   1   1   1   0 ATP-dependent Lon protease
PROKKA_01124         -          THYKI-RXN            -              0.028   11.2   0.0     0.089    9.6   0.0   1.4   1   1   0   1   1   1   0 Type II secretory pathway ATPase GspE/PulE or T4P pilus assembly pathway ATPase PilB
PROKKA_01954         -          THYM-PHOSPH-RXN      -            2.5e-13   46.8   0.0   2.4e-11   40.3   0.0   2.1   2   0   0   2   2   2   2 anthranilate phosphoribosyltransferase
PROKKA_00144         -          THYM-PHOSPH-RXN      -              0.027   10.4   0.0     0.039    9.9   0.0   1.1   1   0   0   1   1   1   0 anthranilate phosphoribosyltransferase
PROKKA_00852         -          THYROXINE-DEIODINASE-RXN -            1.3e-11   40.9   0.0   1.6e-11   40.6   0.0   1.0   1   0   0   1   1   1   1 thioredoxin
PROKKA_00318         -          THYROXINE-DEIODINASE-RXN -            6.8e-07   25.3   0.0   8.3e-07   25.0   0.0   1.1   1   0   0   1   1   1   1 thioredoxin
PROKKA_02160         -          TOLUENE-DIOL-DEHYDROGENASE-RXN -            4.6e-19   65.8   6.7   1.5e-18   64.1   6.7   1.7   1   1   0   1   1   1   1 3-oxoacyl-[acyl-carrier-protein] reductase
PROKKA_02149         -          TOLUENE-DIOL-DEHYDROGENASE-RXN -            1.5e-08   31.4   0.0     2e-08   30.9   0.0   1.2   1   0   0   1   1   1   1 NADP-dependent 3-hydroxy acid dehydrogenase YdfG
PROKKA_00235         -          TOLUENE-DIOL-DEHYDROGENASE-RXN -            5.1e-08   29.6   0.0   6.4e-08   29.3   0.0   1.1   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier protein] reductase
PROKKA_00773         -          TOLUENE-DIOL-DEHYDROGENASE-RXN -            2.1e-05   21.0   0.0   2.8e-05   20.6   0.0   1.1   1   0   0   1   1   1   1 short chain dehydrogenase
PROKKA_00572         -          TOLUENE-DIOL-DEHYDROGENASE-RXN -             0.0029   14.0   0.2    0.0046   13.3   0.2   1.2   1   0   0   1   1   1   1 UDP-glucose 4-epimerase
PROKKA_00170         -          TOLUENE-DIOL-DEHYDROGENASE-RXN -               0.02   11.2   0.1      0.17    8.2   0.1   2.0   1   1   0   1   1   1   0 NAD(P)-dependent dehydrogenase, short-chain alcohol dehydrogenase family
PROKKA_01201         -          TOLUENE-DIOXYGENASE-RXN -            4.6e-06   23.4   0.0   9.1e-06   22.4   0.0   1.4   1   0   0   1   1   1   1 nitrite reductase (NADH) large subunit
PROKKA_02481         -          TRANS-PENTAPRENYLTRANSFERASE-RXN -            1.1e-36  123.7   0.6   1.6e-17   60.5   0.1   3.2   1   1   2   3   3   3   3 octaprenyl-diphosphate synthase
PROKKA_00680         -          TRANS-PENTAPRENYLTRANSFERASE-RXN -            1.6e-16   57.2   0.3   2.4e-07   26.9   0.0   2.8   1   1   1   3   3   3   2 farnesyl-diphosphate synthase 
PROKKA_01705         -          TRANSALDOL-RXN       -            5.7e-61  203.9   0.0   8.3e-61  203.4   0.0   1.1   1   0   0   1   1   1   1 transaldolase
PROKKA_00022         -          TRANSENOYLCOARED-RXN -            1.1e-13   47.8   0.0   3.9e-12   42.7   0.0   2.4   1   1   0   1   1   1   1 NADPH:quinone reductase
PROKKA_02069         -          TRANSENOYLCOARED-RXN -              3e-11   39.8   0.2   8.1e-11   38.3   0.1   1.6   1   1   1   2   2   2   1 alcohol dehydrogenase, propanol-preferring
PROKKA_01239         -          TRANSENOYLCOARED-RXN -            6.4e-10   35.4   0.4   8.2e-08   28.4   0.1   2.1   2   0   0   2   2   2   2 NADPH2:quinone reductase
PROKKA_01717         -          TRANSENOYLCOARED-RXN -            8.8e-08   28.3   0.1   4.9e-06   22.6   0.0   2.5   1   1   2   3   3   3   2 NADPH2:quinone reductase
PROKKA_00499         -          TRE6PHYDRO-RXN       -            5.1e-79  263.4   4.3   8.6e-65  216.5   2.6   3.0   2   1   1   3   3   3   3 maltose alpha-D-glucosyltransferase/ alpha-amylase
PROKKA_00498         -          TRE6PHYDRO-RXN       -            5.7e-51  170.8   0.4   1.4e-36  123.3   0.1   3.0   2   1   1   3   3   3   2 maltose alpha-D-glucosyltransferase/ alpha-amylase
PROKKA_00496         -          TRE6PHYDRO-RXN       -            2.9e-11   39.7   0.0   9.3e-11   38.1   0.0   1.8   3   0   0   3   3   3   1 maltooligosyl trehalose hydrolase
PROKKA_00497         -          TRE6PHYDRO-RXN       -            1.4e-08   30.8   0.0   4.4e-08   29.2   0.0   1.8   2   0   0   2   2   2   1 maltooligosyl trehalose synthase 
PROKKA_02234         -          TRE6PHYDRO-RXN       -            1.5e-08   30.7   0.1   2.1e-06   23.7   0.1   2.6   2   1   1   3   3   3   2 glycogen operon protein
PROKKA_00505         -          TRE6PHYDRO-RXN       -            6.4e-06   22.1   0.0    0.0012   14.6   0.0   2.3   1   1   1   2   2   2   2 alpha-1,4-glucan:maltose-1-phosphate maltosyltransferase
PROKKA_01633         -          TREHALOSE6PSYN-RXN   -            1.4e-77  258.5   0.0   1.7e-77  258.2   0.0   1.0   1   0   0   1   1   1   1 trehalose 6-phosphate synthase
PROKKA_01633         -          TREHALOSEPHOSPHA-RXN -            2.1e-43  145.5   0.0   2.5e-43  145.3   0.0   1.0   1   0   0   1   1   1   1 trehalose 6-phosphate synthase
PROKKA_01634         -          TREHALOSEPHOSPHA-RXN -            1.6e-35  119.5   0.0     2e-35  119.1   0.0   1.0   1   0   0   1   1   1   1 trehalose 6-phosphate phosphatase
PROKKA_02413         -          TRIACYLGLYCEROL-LIPASE-RXN -            0.00059   16.9   0.0   0.00059   16.9   0.0   1.1   1   0   0   1   1   1   1 phospholipase/carboxylesterase
PROKKA_00962         -          TRIACYLGLYCEROL-LIPASE-RXN -             0.0013   15.8   0.0    0.0013   15.8   0.0   1.1   1   0   0   1   1   1   1 alpha-ribazole phosphatase
PROKKA_00054         -          TRIOSEPISOMERIZATION-RXN -              1e-59  199.2   0.0   1.2e-59  199.0   0.0   1.1   1   0   0   1   1   1   1 triosephosphate isomerase
PROKKA_02246         -          TRNA-NUCLEOTIDYLTRANSFERASE-RXN -           6.2e-105  346.8   0.0  7.9e-105  346.5   0.0   1.0   1   0   0   1   1   1   1 ribonuclease PH
PROKKA_00809         -          TRNA-NUCLEOTIDYLTRANSFERASE-RXN -            9.4e-31  103.9   0.9   4.8e-19   65.6   0.0   3.0   2   1   0   2   2   2   2 polyribonucleotide nucleotidyltransferase
PROKKA_00411         -          TRNA-PSEUDOURIDINE-SYNTHASE-I-RXN -            5.1e-39  131.4   0.0   6.4e-39  131.1   0.0   1.0   1   0   0   1   1   1   1 tRNA pseudouridine38-40 synthase
PROKKA_01835         -          TROPINESTERASE-RXN   -            3.5e-05   20.9   0.0     5e-05   20.4   0.0   1.2   1   0   0   1   1   1   1 Pimeloyl-ACP methyl ester carboxylesterase
PROKKA_02160         -          TROPINONE-REDUCTASE-RXN -            5.1e-35  118.1   0.4   6.9e-35  117.7   0.4   1.0   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier-protein] reductase
PROKKA_00235         -          TROPINONE-REDUCTASE-RXN -            3.5e-33  112.1   0.0   4.1e-33  111.8   0.0   1.0   1   0   0   1   1   1   1 3-oxoacyl-[acyl-carrier protein] reductase
PROKKA_00773         -          TROPINONE-REDUCTASE-RXN -            9.8e-14   48.3   0.0   1.2e-13   48.0   0.0   1.0   1   0   0   1   1   1   1 short chain dehydrogenase
PROKKA_02149         -          TROPINONE-REDUCTASE-RXN -            1.8e-13   47.4   0.0   3.7e-13   46.4   0.0   1.4   1   1   0   1   1   1   1 NADP-dependent 3-hydroxy acid dehydrogenase YdfG
PROKKA_00170         -          TROPINONE-REDUCTASE-RXN -            4.2e-08   29.8   0.0   9.8e-08   28.6   0.0   1.5   1   1   0   1   1   1   1 NAD(P)-dependent dehydrogenase, short-chain alcohol dehydrogenase family
PROKKA_00820         -          TROPINONE-REDUCTASE-RXN -            3.4e-05   20.3   0.0   5.5e-05   19.6   0.0   1.2   1   0   0   1   1   1   1 Enoyl-[acyl-carrier-protein] reductase [NADH]
PROKKA_01951         -          TRYPSYN-RXN          -           1.7e-172  571.0   0.0  2.2e-172  570.6   0.0   1.0   1   0   0   1   1   1   1 tryptophan synthase beta chain
PROKKA_01950         -          TRYPSYN-RXN          -            4.8e-12   42.7   0.0   1.5e-08   31.2   0.0   2.8   1   1   0   1   1   1   1 tryptophan synthase, alpha chain
PROKKA_00647         -          TRYPSYN-RXN          -            4.7e-11   39.4   0.0   6.5e-05   19.2   0.0   2.7   3   0   0   3   3   3   2 cysteine synthase 
PROKKA_01355         -          TRYPSYN-RXN          -            2.2e-07   27.4   0.0   0.00016   17.9   0.0   2.2   2   0   0   2   2   2   2 threonine synthase
PROKKA_00649         -          TRYPSYN-RXN          -            4.5e-07   26.3   0.0    0.0093   12.1   0.0   2.1   2   0   0   2   2   2   2 threonine synthase
PROKKA_01440         -          TRYPTOPHAN--TRNA-LIGASE-RXN -           1.3e-109  363.8   0.0  1.6e-109  363.5   0.0   1.0   1   0   0   1   1   1   1 tryptophanyl-tRNA synthetase
PROKKA_01522         -          TRYPTOPHAN--TRNA-LIGASE-RXN -              2e-11   41.0   0.0   4.7e-11   39.8   0.0   1.4   1   1   0   1   1   1   1 tyrosyl-tRNA synthetase
PROKKA_01944         -          TRYPTOPHAN-2-MONOOXYGENASE-RXN -            0.00029   16.0   0.0   0.00029   16.0   0.0   1.4   2   0   0   2   2   2   1 NADPH-dependent glutamate synthase beta chain
PROKKA_01451         -          TRYPTOPHAN-2-MONOOXYGENASE-RXN -            0.00047   15.3   0.0      0.03    9.3   0.0   2.1   2   0   0   2   2   2   2 NADH-quinone oxidoreductase subunit F
PROKKA_02187         -          TRYPTOPHAN-2-MONOOXYGENASE-RXN -             0.0017   13.4   0.0    0.0023   13.0   0.0   1.1   1   0   0   1   1   1   1 squalene-associated FAD-dependent desaturase
PROKKA_00876         -          TRYPTOPHAN-2-MONOOXYGENASE-RXN -             0.0019   13.3   0.1    0.0028   12.7   0.1   1.2   1   0   0   1   1   1   1 malate dehydrogenase (NAD)
PROKKA_00735         -          TRYPTOPHAN-2-MONOOXYGENASE-RXN -             0.0066   11.5   0.9    0.0087   11.1   0.9   1.1   1   0   0   1   1   1   1 C-3',4' desaturase CrtD
PROKKA_01719         -          TRYPTOPHAN-2-MONOOXYGENASE-RXN -              0.014   10.4   0.3     0.022    9.8   0.3   1.1   1   0   0   1   1   1   0 oxygen-dependent protoporphyrinogen oxidase
PROKKA_01533         -          TRYPTOPHAN-5-MONOOXYGENASE-RXN -            4.3e-08   29.5   0.0   6.5e-08   28.9   0.0   1.2   1   0   0   1   1   1   1 chorismate mutase / prephenate dehydratase
PROKKA_01522         -          TYROSINE--TRNA-LIGASE-RXN -           1.9e-102  340.3   0.0  2.5e-102  339.9   0.0   1.0   1   0   0   1   1   1   1 tyrosyl-tRNA synthetase
PROKKA_01440         -          TYROSINE--TRNA-LIGASE-RXN -            8.5e-18   61.6   0.0   1.2e-17   61.2   0.0   1.1   1   0   0   1   1   1   1 tryptophanyl-tRNA synthetase
PROKKA_02445         -          TYROSINE--TRNA-LIGASE-RXN -              0.015   11.4   0.0     0.026   10.6   0.0   1.3   1   0   0   1   1   1   0 glutamyl-tRNA synthetase 
PROKKA_01338         -          TYROSINE-AMINOTRANSFERASE-RXN -            1.7e-61  205.5   0.0   2.2e-61  205.2   0.0   1.0   1   0   0   1   1   1   1 aspartate aminotransferase
PROKKA_01352         -          TYROSINE-AMINOTRANSFERASE-RXN -            9.4e-44  147.1   0.0   1.1e-43  146.9   0.0   1.0   1   0   0   1   1   1   1 alanine-synthesizing transaminase
PROKKA_00241         -          TYROSINE-AMINOTRANSFERASE-RXN -            6.1e-43  144.5   0.0   7.7e-43  144.1   0.0   1.0   1   0   0   1   1   1   1 LL-diaminopimelate aminotransferase apoenzyme
PROKKA_02268         -          TYROSINE-AMINOTRANSFERASE-RXN -            1.2e-19   67.8   0.0   1.6e-19   67.4   0.0   1.1   1   0   0   1   1   1   1 histidinol-phosphate aminotransferase
PROKKA_01224         -          TYROSINE-AMINOTRANSFERASE-RXN -            4.9e-16   55.9   0.0   6.1e-16   55.6   0.0   1.1   1   0   0   1   1   1   1 histidinol-phosphate aminotransferase
PROKKA_01558         -          TYROSINE-AMINOTRANSFERASE-RXN -            1.7e-06   24.6   0.0   2.3e-06   24.1   0.0   1.2   1   0   0   1   1   1   1 L-threonine O-3-phosphate decarboxylase 
PROKKA_01781         -          TYROSINE-AMINOTRANSFERASE-RXN -              1e-05   22.0   0.0   1.4e-05   21.5   0.0   1.1   1   0   0   1   1   1   1 CDP-6-deoxy-D-xylo-4-hexulose-3-dehydrase
PROKKA_00602         -          UDP-NACMUR-ALA-LIG-RXN -           7.4e-154  510.1   0.0    9e-154  509.9   0.0   1.0   1   0   0   1   1   1   1 UDP-N-acetylmuramate--L-alanine ligase
PROKKA_00595         -          UDP-NACMUR-ALA-LIG-RXN -            5.4e-13   45.8   0.0   7.5e-10   35.5   0.0   2.1   2   0   0   2   2   2   2 UDP-N-acetylmuramoylalanyl-D-glutamate--2,6-diaminopimelate ligase
PROKKA_00598         -          UDP-NACMUR-ALA-LIG-RXN -            3.1e-10   36.7   0.0   6.7e-10   35.6   0.0   1.5   1   1   0   1   1   1   1 UDP-N-acetylmuramoylalanine--D-glutamate ligase
PROKKA_00596         -          UDP-NACMUR-ALA-LIG-RXN -            0.00055   16.1   0.0      0.19    7.8   0.0   2.5   2   1   0   2   2   2   2 UDP-N-acetylmuramoyl-tripeptide--D-alanyl-D-alanine ligase
PROKKA_00598         -          UDP-NACMURALA-GLU-LIG-RXN -            2.7e-83  277.5   0.0   5.1e-83  276.6   0.0   1.3   1   1   0   1   1   1   1 UDP-N-acetylmuramoylalanine--D-glutamate ligase
PROKKA_00602         -          UDP-NACMURALA-GLU-LIG-RXN -            4.9e-69  230.5   0.0   6.5e-69  230.2   0.0   1.0   1   0   0   1   1   1   1 UDP-N-acetylmuramate--L-alanine ligase
PROKKA_00595         -          UDP-NACMURALA-GLU-LIG-RXN -            3.6e-28   95.9   0.0   4.5e-27   92.2   0.0   2.0   1   1   0   1   1   1   1 UDP-N-acetylmuramoylalanyl-D-glutamate--2,6-diaminopimelate ligase
PROKKA_00596         -          UDP-NACMURALA-GLU-LIG-RXN -            7.4e-09   32.2   0.0    0.0007   15.8   0.0   2.1   2   0   0   2   2   2   2 UDP-N-acetylmuramoyl-tripeptide--D-alanyl-D-alanine ligase
PROKKA_01948         -          UDP-NACMURALA-GLU-LIG-RXN -              0.001   15.3   0.0     0.057    9.5   0.0   2.3   2   1   0   2   2   2   2 dihydrofolate synthase / folylpolyglutamate synthase
PROKKA_00596         -          UDP-NACMURALGLDAPAALIG-RXN -            1.3e-63  212.6   0.0   1.6e-47  159.6   0.0   2.4   2   1   0   2   2   2   2 UDP-N-acetylmuramoyl-tripeptide--D-alanyl-D-alanine ligase
PROKKA_00602         -          UDP-NACMURALGLDAPAALIG-RXN -              1e-15   54.7   0.0   1.4e-10   37.7   0.0   2.5   2   1   0   2   2   2   2 UDP-N-acetylmuramate--L-alanine ligase
PROKKA_00595         -          UDP-NACMURALGLDAPAALIG-RXN -            1.2e-12   44.6   0.0   4.8e-07   26.1   0.0   2.5   2   1   0   2   2   2   2 UDP-N-acetylmuramoylalanyl-D-glutamate--2,6-diaminopimelate ligase
PROKKA_00598         -          UDP-NACMURALGLDAPAALIG-RXN -            2.3e-07   27.1   0.0   1.4e-05   21.3   0.0   2.1   2   0   0   2   2   2   2 UDP-N-acetylmuramoylalanine--D-glutamate ligase
PROKKA_00595         -          UDP-NACMURALGLDAPLIG-RXN -           9.1e-142  470.4   0.0  1.1e-141  470.1   0.0   1.0   1   0   0   1   1   1   1 UDP-N-acetylmuramoylalanyl-D-glutamate--2,6-diaminopimelate ligase
PROKKA_00602         -          UDP-NACMURALGLDAPLIG-RXN -            2.9e-27   92.8   0.0   1.2e-23   80.9   0.0   2.1   2   0   0   2   2   2   2 UDP-N-acetylmuramate--L-alanine ligase
PROKKA_00596         -          UDP-NACMURALGLDAPLIG-RXN -            1.1e-18   64.5   0.0   8.7e-15   51.6   0.0   2.1   2   0   0   2   2   2   2 UDP-N-acetylmuramoyl-tripeptide--D-alanyl-D-alanine ligase
PROKKA_00598         -          UDP-NACMURALGLDAPLIG-RXN -            2.5e-14   50.1   0.0   8.1e-12   41.8   0.0   2.8   1   1   0   1   1   1   1 UDP-N-acetylmuramoylalanine--D-glutamate ligase
PROKKA_01948         -          UDP-NACMURALGLDAPLIG-RXN -              2e-08   30.6   0.0    0.0012   14.8   0.0   2.2   2   0   0   2   2   2   2 dihydrofolate synthase / folylpolyglutamate synthase
PROKKA_00656         -          UDPGLUCEPIM-RXN      -           3.4e-108  358.8   0.0  4.2e-108  358.5   0.0   1.0   1   0   0   1   1   1   1 UDP-galactose 4-epimerase
PROKKA_00572         -          UDPGLUCEPIM-RXN      -            1.4e-54  182.5   0.0   1.7e-54  182.2   0.0   1.0   1   0   0   1   1   1   1 UDP-glucose 4-epimerase
PROKKA_00566         -          UDPGLUCEPIM-RXN      -              1e-43  146.8   0.0   3.1e-42  141.9   0.0   2.4   1   1   0   1   1   1   1 UDP-glucuronate 4-epimerase
PROKKA_01629         -          UDPGLUCEPIM-RXN      -            8.4e-41  137.2   0.0   1.1e-40  136.8   0.0   1.1   1   0   0   1   1   1   1 ADP-L-glycero-D-manno-heptose 6-epimerase
PROKKA_00683         -          UDPGLUCEPIM-RXN      -            3.1e-37  125.5   0.0   4.6e-37  124.9   0.0   1.2   1   0   0   1   1   1   1 UDP-glucose 4-epimerase
PROKKA_00488         -          UDPGLUCEPIM-RXN      -            3.5e-31  105.6   0.0   2.5e-30  102.8   0.0   1.9   1   1   0   1   1   1   1 dTDP-glucose 4,6-dehydratase
PROKKA_01526         -          UDPGLUCEPIM-RXN      -            2.3e-29   99.6   0.0     4e-29   98.8   0.0   1.3   1   1   0   1   1   1   1 UDP-glucuronate decarboxylase
PROKKA_01783         -          UDPGLUCEPIM-RXN      -            3.2e-24   82.7   0.0     1e-19   67.9   0.0   2.0   2   0   0   2   2   2   2 Nucleoside-diphosphate-sugar epimerase
PROKKA_00620         -          UDPGLUCEPIM-RXN      -            4.7e-18   62.4   0.0     1e-16   58.0   0.0   2.0   2   0   0   2   2   2   1 dihydroflavonol-4-reductase
PROKKA_01778         -          UDPGLUCEPIM-RXN      -              4e-17   59.4   0.0   5.6e-14   49.0   0.0   2.4   1   1   0   1   1   1   1 GDP-L-fucose synthase
PROKKA_01635         -          UDPGLUCEPIM-RXN      -            8.5e-08   28.7   0.0   1.1e-07   28.3   0.0   1.1   1   0   0   1   1   1   1 dTDP-4-dehydrorhamnose reductase
PROKKA_01392         -          UDPGLUCEPIM-RXN      -              4e-07   26.5   0.0   6.5e-06   22.5   0.0   2.4   3   0   0   3   3   3   1 Nucleoside-diphosphate-sugar epimerase
PROKKA_00469         -          UDPGLUCEPIM-RXN      -              2e-05   20.9   0.1    0.0046   13.1   0.2   3.1   2   1   0   2   2   2   1 NADH dehydrogenase
PROKKA_00565         -          UDPGLUCEPIM-RXN      -             0.0064   12.7   0.0    0.0089   12.2   0.0   1.2   1   0   0   1   1   1   1 UDP-galactopyranose mutase
PROKKA_02149         -          UDPGLUCEPIM-RXN      -              0.011   11.9   0.6     0.043    9.9   0.0   2.1   2   1   0   2   2   2   0 NADP-dependent 3-hydroxy acid dehydrogenase YdfG
PROKKA_01294         -          UDPNACETYLGLUCOSAMACYLTRANS-RXN -            4.6e-93  308.4   0.0     6e-93  308.1   0.0   1.0   1   0   0   1   1   1   1 acyl-[acyl-carrier-protein]--UDP-N-acetylglucosamine O-acyltransferase
PROKKA_01359         -          UDPNACETYLGLUCOSAMACYLTRANS-RXN -            3.5e-70  233.4   0.2   4.3e-70  233.1   0.2   1.0   1   0   0   1   1   1   1 acyl-[acyl-carrier-protein]--UDP-N-acetylglucosamine O-acyltransferase
PROKKA_01292         -          UDPNACETYLGLUCOSAMACYLTRANS-RXN -            1.8e-21   73.8   4.6   2.6e-21   73.2   4.6   1.2   1   0   0   1   1   1   1 UDP-3-O-[3-hydroxymyristoyl] glucosamine N-acyltransferase
PROKKA_02107         -          UDPNACETYLGLUCOSAMACYLTRANS-RXN -            4.9e-07   26.5   0.5     2e-05   21.1   0.5   3.0   1   1   0   1   1   1   1 bifunctional UDP-N-acetylglucosamine pyrophosphorylase / Glucosamine-1-phosphate N-acetyltransferase
PROKKA_01326         -          UDPNACETYLGLUCOSAMACYLTRANS-RXN -            5.2e-05   19.8   1.0     0.021   11.3   0.3   2.2   1   1   1   2   2   2   2 serine O-acetyltransferase
PROKKA_00296         -          UDPNACETYLGLUCOSAMACYLTRANS-RXN -            0.00025   17.6   3.8    0.0061   13.0   4.1   2.4   1   1   1   2   2   2   2 Carbonic anhydrase or acetyltransferase, isoleucine patch superfamily
PROKKA_02271         -          UDPNACETYLGLUCOSAMENOLPYRTRANS-RXN -           5.2e-155  513.5   0.3  1.2e-138  459.6   0.4   2.0   1   1   1   2   2   2   2 UDP-N-acetylglucosamine 1-carboxyvinyltransferase
PROKKA_01536         -          UDPNACETYLGLUCOSAMENOLPYRTRANS-RXN -            0.00033   16.7   0.0    0.0011   14.9   0.0   1.8   2   0   0   2   2   2   1 3-phosphoshikimate 1-carboxyvinyltransferase
PROKKA_00603         -          UDPNACETYLMURAMATEDEHYDROG-RXN -            8.8e-30  101.0   0.0   3.6e-16   56.2   0.0   2.9   1   1   1   2   2   2   2 UDP-N-acetylmuramate dehydrogenase
PROKKA_01528         -          UGD-RXN              -           6.8e-132  437.4   0.0  7.9e-132  437.2   0.0   1.0   1   0   0   1   1   1   1 UDPglucose 6-dehydrogenase
PROKKA_00570         -          UGD-RXN              -            8.2e-45  150.4   0.0   1.4e-44  149.7   0.0   1.2   1   0   0   1   1   1   1 UDP-N-acetyl-D-glucosamine dehydrogenase
PROKKA_02174         -          UNDECAPRENOL-KINASE-RXN -            5.1e-24   82.4   2.9   6.6e-24   82.1   2.9   1.1   1   0   0   1   1   1   1 undecaprenyl-diphosphatase
PROKKA_01921         -          URACIL-PRIBOSYLTRANS-RXN -            2.6e-25   86.4   0.0   3.5e-25   86.0   0.0   1.2   1   0   0   1   1   1   1 pyrimidine operon attenuation protein / uracil phosphoribosyltransferase
PROKKA_01997         -          URACIL-PRIBOSYLTRANS-RXN -            4.2e-07   26.9   0.0   5.1e-07   26.6   0.0   1.1   1   0   0   1   1   1   1 hypoxanthine phosphoribosyltransferase
PROKKA_01553         -          URACIL-PRIBOSYLTRANS-RXN -            1.3e-06   25.3   0.0   2.9e-06   24.2   0.0   1.5   1   0   0   1   1   1   1 adenine phosphoribosyltransferase
PROKKA_01692         -          URACIL-PRIBOSYLTRANS-RXN -            9.6e-05   19.2   0.2   0.00014   18.7   0.2   1.2   1   0   0   1   1   1   1 ribose-phosphate pyrophosphokinase
PROKKA_01627         -          URACIL-PRIBOSYLTRANS-RXN -            0.00074   16.3   0.0    0.0013   15.5   0.0   1.3   1   0   0   1   1   1   1 amidophosphoribosyltransferase
PROKKA_00049         -          URACIL-PRIBOSYLTRANS-RXN -            0.00089   16.0   0.0    0.0013   15.5   0.0   1.2   1   0   0   1   1   1   1 orotate phosphoribosyltransferase
PROKKA_00110         -          URACIL-PRIBOSYLTRANS-RXN -             0.0046   13.7   0.0    0.0068   13.2   0.0   1.1   1   0   0   1   1   1   1 putative amidophosphoribosyltransferases
PROKKA_01593         -          UREA-CARBOXYLASE-RXN -           1.8e-104  347.3   0.0  2.2e-104  347.0   0.0   1.0   1   0   0   1   1   1   1 acetyl-CoA carboxylase, biotin carboxylase subunit
PROKKA_02101         -          UREA-CARBOXYLASE-RXN -            1.6e-41  138.6   0.0   6.2e-41  136.7   0.0   1.7   1   1   0   1   1   1   1 aspartyl/glutamyl-tRNA(Asn/Gln) amidotransferase subunit A
PROKKA_00604         -          UREA-CARBOXYLASE-RXN -             0.0055   10.9   0.0    0.0071   10.5   0.0   1.0   1   0   0   1   1   1   1 D-alanine--D-alanine ligase
PROKKA_01592         -          UREA-CARBOXYLASE-RXN -              0.018    9.1   0.0     0.021    8.9   0.0   1.0   1   0   0   1   1   1   0 acetyl-CoA carboxylase biotin carboxyl carrier protein
PROKKA_00120         -          URIDINEKIN-RXN       -             0.0016   15.1   0.6      0.37    7.4   0.1   2.3   2   0   0   2   2   2   2 dephospho-CoA kinase
PROKKA_01537         -          URIDINEKIN-RXN       -             0.0085   12.8   1.7     0.093    9.4   0.0   2.6   2   2   0   2   2   2   1 cytidylate kinase
PROKKA_00980         -          URIDINEKIN-RXN       -              0.018   11.7   0.0     0.035   10.8   0.0   1.4   1   0   0   1   1   1   0 type IV secretion system protein VirB11
PROKKA_00120         -          URKI-RXN             -             0.0016   15.1   0.6      0.37    7.4   0.1   2.3   2   0   0   2   2   2   2 dephospho-CoA kinase
PROKKA_01537         -          URKI-RXN             -             0.0085   12.8   1.7     0.093    9.4   0.0   2.6   2   2   0   2   2   2   1 cytidylate kinase
PROKKA_00980         -          URKI-RXN             -              0.018   11.7   0.0     0.035   10.8   0.0   1.4   1   0   0   1   1   1   0 type IV secretion system protein VirB11
PROKKA_00627         -          UROGENDECARBOX-RXN   -           1.9e-127  422.0   0.0  2.3e-127  421.7   0.0   1.0   1   0   0   1   1   1   1 uroporphyrinogen decarboxylase
PROKKA_01993         -          UROGENIIISYN-RXN     -              5e-42  140.9   0.0   6.4e-42  140.6   0.0   1.2   1   0   0   1   1   1   1 uroporphyrinogen III methyltransferase / synthase
PROKKA_00867         -          VALINE--TRNA-LIGASE-RXN -           4.4e-298  988.2   0.0  5.8e-298  987.8   0.0   1.0   1   0   0   1   1   1   1 valyl-tRNA synthetase
PROKKA_02476         -          VALINE--TRNA-LIGASE-RXN -           4.5e-120  399.2   0.0  2.6e-119  396.6   0.0   1.8   1   1   0   1   1   1   1 Isoleucyl-tRNA synthetase
PROKKA_00378         -          VALINE--TRNA-LIGASE-RXN -            2.9e-82  274.1   0.0   6.9e-36  120.6   2.0   6.1   2   2   1   3   3   3   3 leucyl-tRNA synthetase
PROKKA_02128         -          VALINE--TRNA-LIGASE-RXN -            1.2e-24   83.4   0.0   1.1e-13   47.2   0.0   3.4   1   1   2   3   3   3   3 methionyl-tRNA synthetase
PROKKA_00929         -          VALINE--TRNA-LIGASE-RXN -               0.01   10.9   0.0      0.35    5.8   0.0   2.1   2   0   0   2   2   2   2 arginyl-tRNA synthetase
PROKKA_01338         -          VALINE-PYRUVATE-AMINOTRANSFER-RXN -            7.3e-41  137.8   0.0   9.5e-41  137.4   0.0   1.0   1   0   0   1   1   1   1 aspartate aminotransferase
PROKKA_01352         -          VALINE-PYRUVATE-AMINOTRANSFER-RXN -            5.1e-39  131.7   0.0   6.5e-39  131.4   0.0   1.0   1   0   0   1   1   1   1 alanine-synthesizing transaminase
PROKKA_00241         -          VALINE-PYRUVATE-AMINOTRANSFER-RXN -              4e-30  102.4   0.0   5.3e-30  102.0   0.0   1.0   1   0   0   1   1   1   1 LL-diaminopimelate aminotransferase apoenzyme
PROKKA_02268         -          VALINE-PYRUVATE-AMINOTRANSFER-RXN -            3.7e-15   53.2   0.0   4.9e-15   52.7   0.0   1.1   1   0   0   1   1   1   1 histidinol-phosphate aminotransferase
PROKKA_01224         -          VALINE-PYRUVATE-AMINOTRANSFER-RXN -              5e-07   26.4   0.0   1.1e-06   25.2   0.0   1.6   1   1   0   1   1   1   1 histidinol-phosphate aminotransferase
PROKKA_01558         -          VALINE-PYRUVATE-AMINOTRANSFER-RXN -              2e-06   24.3   0.0   2.7e-06   23.9   0.0   1.1   1   0   0   1   1   1   1 L-threonine O-3-phosphate decarboxylase 
PROKKA_01781         -          VALINE-PYRUVATE-AMINOTRANSFER-RXN -            0.00053   16.4   0.0   0.00079   15.8   0.0   1.2   1   0   0   1   1   1   1 CDP-6-deoxy-D-xylo-4-hexulose-3-dehydrase
PROKKA_00347         -          VIOMYCIN-KINASE-RXN  -              0.023   11.1   0.0     0.034   10.6   0.0   1.2   1   0   0   1   1   1   0 ectoine hydroxylase
PROKKA_01997         -          XANPRIBOSYLTRAN-RXN  -             0.0012   16.0   0.0    0.0015   15.8   0.0   1.1   1   0   0   1   1   1   1 hypoxanthine phosphoribosyltransferase
PROKKA_00949         -          XANTHINE-OXIDASE-RXN -              0.097    7.1   3.1      0.13    6.7   3.1   1.0   1   0   0   1   1   1   0 molecular chaperone DnaJ
PROKKA_00948         -          XYLULOKIN-RXN        -            5.4e-05   19.2   0.2   0.00079   15.3   0.0   2.2   2   0   0   2   2   2   1 molecular chaperone DnaK
PROKKA_00417         -          XYLULOKIN-RXN        -            0.00025   17.0   0.2     0.001   15.0   0.0   1.9   2   0   0   2   2   2   1 molecular chaperone HscA
#
# Program:         hmmsearch
# Version:         3.1b2 (February 2015)
# Pipeline mode:   SEARCH
# Query file:      /work/projects/lcsbdata/HMM//metacyc/metacyc.hmm
# Target file:     lf_merged.faa
# Option settings: hmmsearch --tblout hmm_anno/lf_merged.faa.metacyc.res --noali --notextw --cpu 12 /work/projects/lcsbdata/HMM//metacyc/metacyc.hmm lf_merged.faa 
# Current dir:     /mnt/gaiagpfs/users/workdirs/mherold/REPOS/LF_annotation_new
# Date:            Wed Dec  7 16:00:37 2016
# [ok]
